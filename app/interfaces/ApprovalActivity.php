<?php
/** 
 * User: Sandeep Maurya
 * Date: 15/12/2019
 * Time: 8:26 PM
 */
 
namespace App\interfaces;
interface ApprovalActivity
{ 
    public function ApproveRequest(array $options);
    public function CancelRequest(array $options);
    public function RejectRequest(array $options);
    public function ReconsiderRequest(array $options);
    public function EnqueueActivity(array $payloads);
}