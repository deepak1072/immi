<?php
/**
* Created by Sandeep Maurya 
* 21st April 2019
*/
namespace App\interfaces;
use Illuminate\Http\Request;
 

interface ActivityInterface{ 
	// for add new one and update 
	public function AddUpdate(Request $request);

	//get list 
	public function FetchList(Request $request);

	// update status 
	public function UpdateStatus(Request $request);

	// get details 
	public function GetDetails(Request $request);
	
}