<?php

namespace App\Jobs;

use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use GenericHelpers;
use GlobalHelpers;
use App\libraries\leave\OpeningBalance;
use App\libraries\reports\ImportExports;
error_reporting(1);
class ImportOpeningBalance implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;
    private $_id;
    private $path;
    private $user_id;
    private $activity_time;
    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct($_id,$path,$user_id)
    {
        $this->_id = $_id;
        $this->path = $path;
        $this->user_id = $user_id;
        $this->activity_time = date('Y-m-d H:i:s');
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $data = GenericHelpers::csvToArray($this->path);
        $import_export = new ImportExports();
        $import_export->updateStatus($this->_id,'In Progress','File processing started ');
        try{
            if(count($data) > 1){
                $balance  = new OpeningBalance();
                $opening = array();
                $error = array();
                $error[] = $data[0];
                $error[0][] = "Error Details";
                foreach($data as $key => $value){
                    if($key > 0){
                        $leave = GlobalHelpers::getLeaveByShortName($value[1]);
                        if(is_null($leave)){
                            $value[] = "Invalid Leave Type";
                            $error[] = $value;
                            continue;
                        }
                        $user = GlobalHelpers::getUserByCode($value[0]);
                        if(is_null($user)){
                            $value[] = "Invalid User Code";
                            $error[] = $value;
                            continue;
                        }
                        if(empty($value[3]) || !is_numeric($value[3])){
                            $value[] = "Invalid Leave Balance";
                            $error[] = $value;
                            continue; 
                        }
                        if(empty($value[2])){
                            $value[] = "Invalid Effective Date";
                            $error[] = $value;
                            continue; 
                        }

                        $bal = array(
                            'lb_key' => rand(1,9999999999),
                            'user_id' => $user->user_id,
                            'leave_type' =>$leave->leave_id,
                            'balance' => $value[3],
                            'effective_date' => date('Y-m-d',strtotime($value[2])),
                            'lb_type' => 'Opening',
                            'status' => 1,
                            'created_by' => $this->user_id,
                            'updated_by' => $this->user_id,
                            'remarks' => $value[4],
                            'created_at' => $this->activity_time ,
                            'updated_at' => $this->activity_time ,
                        );
                        array_push($opening,$bal);
                    }
                }

                if(count($opening) > 0){
                    //credit opening balance
                    $res = $balance->CreditOpeningBalance($opening);
                }

                if(count($error) > 1){
                    $filename = GenericHelpers::arrayToCsv($error,"uploads/errors/Opening_Balance");
                    if($filename){
                        $import_export->updateStatus($this->_id,'Error','File processing end ',$filename);
                        return true;
                    }
                }
            }
            $import_export->updateStatus($this->_id,'Success','File processing end ');
        }catch(\Exception $e){
            $import_export->updateStatus($this->_id,'Failed',$e->getMessage());
        }
        return true ;
    }
}
