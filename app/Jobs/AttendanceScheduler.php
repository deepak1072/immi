<?php

namespace App\Jobs;

use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;

use App\libraries\attendance\Attendance;
use Illuminate\Support\Facades\DB;
class AttendanceScheduler implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;
    private $user_id;
    private $start_date ;
    private $end_date;
    /**
     * Create a new job instance.
     *
     * @return void
     */ 
    public function __construct($user_id, $start_date, $end_date)
    {
        $this->start_date = $start_date ;
        $this->end_date = $end_date ;
        $this->user_id = $user_id;
    }

    /**
     * Execute the job for attendace 
     *
     * @return void
     */
    public function handle()
    { 
        $attendance = new Attendance();
        $attendance->runScheduler($this->user_id, $this->start_date, $this->end_date); 
    } 
}
