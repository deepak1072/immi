<?php

namespace App\Jobs;

use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use App\libraries\attendance\RosterAssignment ;
use TeamHelpers;
class RosterScheduler implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;
    private $user_id;
    private $roster_id;
    private $start_date;
    private $end_date;
    private $ou ;
    private $activist;
    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct($user_id = '',$roster_id,$start_date,$end_date,$ou ='',$activist)
    {
        $this->user_id = $user_id;
        $this->roster_id = $roster_id;
        $this->start_date = $start_date;
        $this->end_date = $end_date;
        $this->ou = $ou;
        $this->activist = $activist ;

    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $team = TeamHelpers::OuTeam($this->ou);
        $roster = new RosterAssignment();
        if(count($team) > 0){
            foreach($team as $user){
                $roster->assign($user->user_id,$this->roster_id, $this->start_date, $this->end_date,$this->activist);
            } 
        } 
    }
}
