<?php

namespace App\Jobs;

use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;

class Import implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;
    private $_id;
    private $path; 
    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct($_id,$path)
    {
        $this->_id = $_id;
        $this->path = $path; 
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {

        echo "ok $this->_id  : $this->path";die;
    }
}
