<?php

namespace App\Jobs;

use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Support\Facades\DB;
use App\libraries\reports\ImportExports;
use GenericHelpers;
error_reporting(1);
class ImmigrationReports implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;
    private $_id;
    private $path;
    private $user_id;
    private $activity_time;
    private $period;
    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct($_id,$user_id,$period)
    {
        $this->_id = $_id; 
        $this->user_id = $user_id;
        $this->period = $period;
        $this->activity_time = date('Y-m-d H:i:s');
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $import_export = new ImportExports();
        $import_export->updateStatus($this->_id,'In Progress','File processing started ');
        try{

            $start_date = '';
            $end_date = '';
            $today = date('Y-m-d');
            $data = array();
            // today
            if($this->period == 1 ){
                $start_date = $today.' 00:00:00' ;
                $end_date = $today.' 23:59:59' ;
            }
            // last seven days
            if($this->period == 7 ){
                $start_date = $today.' 00:00:00' ;
                $start_date = date('Y-m-d H:i:s',strtotime(' -7 days'));
                $end_date = $today.' 23:59:59' ;
            }
            // last 15 days 
            if($this->period == 15 ){
                $start_date = $today.' 00:00:00' ;
                $start_date = date('Y-m-d H:i:s',strtotime(' -15 days'));
                $end_date = $today.' 23:59:59' ;
            }
            // last 30 days 
            if($this->period == 30 ){
                $start_date = $today.' 00:00:00' ;
                $start_date = date('Y-m-d H:i:s',strtotime(' -30 days'));
                $end_date = $today.' 23:59:59' ;
            }
            if($this->period == 180 ){
                $start_date = $today.' 00:00:00' ;
                $start_date = date('Y-m-d H:i:s',strtotime(' -180 days'));
                $end_date = $today.' 23:59:59' ;
            }
            if( empty($start_date) || empty($end_date) ){
                return $data;
            }
          
            $query=DB::table("customer as c");
            $query->selectRaw("c.id,c.name,c.email,c.mobile,result,doc_updated, crm_update, visit, online_councelling, oncall_councelling, cic, verification_mail, approval, hr_finalization,em.emp_name, `doc_comments`, `crm_comments`, `visit_comments`, `online_comments`, `oncall_comments`, `cic_comments`, `mail_comments`, `approval_comments`, `hr_comments`, `result_comments`");
            $query->join('emp_master as em','em.user_id','=','c.created_by');
            $query->where('c.is_deleted',0);
            $query->whereBetween("c.created_at",[$start_date,$end_date]);
            $res = $query->get();
            if(  $res  ){
                $records = array();   
                $header = [
                    'Client Name',
                    'Mobile No.',
                    'Email Id',
                    'Agent Name',
                    'Document Uploaded',
                    'DOC Comments',
                    'CRM Update',
                    'CRM Comments',
                    'Visit',
                    'Visit Comments',
                    'OnLine Counselling',
                    'Online Councelling Comments',
                    'On Call Counselling',
                    'On Call Councelling Comments',
                    'CIC',
                    'CIC Comments',
                    'Mail Verification',
                    'Mail Comments',
                    'Approval',
                    'Approval Comments',
                    'HR Final Call',
                    'HR Comments ',
                    'Result',
                    ' Result Comments'
                ];
                array_push( $records , $header );
                foreach( $res  as $key => $value ){
                    $data  = array(
                        'name' => $value->name,
                        'mobile' => $value->mobile,
                        'email' => $value->email,
                        'agents' => $value->emp_name,
                        'doc' => (  $value->doc_updated == 1) ? 'Yes' : 'No',
                        'doc_comments' =>  $value->doc_comments ,
                        'crm_update' => ( $value->crm_update == 1) ? 'Yes' : 'No',
                        'crm_comments' =>  $value->crm_comments ,
                        'visit' => ( $value->visit == 1 ) ? 'Yes' : 'No',
                        'visit_comments' =>  $value->visit_comments ,
                        'online_councelling' => ( $value->online_councelling == 1 ) ? 'Yes' : 'No',
                        'online_comments' =>  $value->online_comments ,
                        'oncall_councelling' => ( $value->oncall_councelling == 1 ) ? 'Yes' : 'No',
                        'oncall_comments' =>  $value->oncall_comments ,
                        'cic' => ( $value->cic == 1 ) ? 'Yes' : 'No',
                        'cic_comments' =>  $value->cic_comments ,
                        'verification_mail' => ( $value->verification_mail == 1 ) ? 'Yes' : 'No',
                        'mail_comments' =>  $value->mail_comments ,
                        'approval' => ( $value->approval == 1 ) ? 'Yes' : 'No',
                        'approval_comments' =>  $value->approval_comments ,
                        'hr_finalization' => ( $value->hr_finalization ) ? 'Yes' : 'No',
                        'hr_comments' =>  $value->hr_comments ,
                        'result' => $value->result,
                        'result_comments' =>  $value->result_comments ,
                    );
                    array_push($records , $data);
                } 
                 
                if(count($records) > 1){
                    $filename = GenericHelpers::arrayToCsv($records,"generated/Immigration_Reports"); 
                    if($filename){
                        $import_export->updateStatusSuccess($this->_id,'Success','File Generated Successfully ',$filename);
                        return true;
                    }else{
                        $import_export->updateStatus($this->_id,'Error','File processing end ');
                    }
                }else{
                    $import_export->updateStatus($this->_id,'Error','File processing end ');
                }
            }else{
                $import_export->updateStatus($this->_id,'Error','File processing end ');
            }
            
        }catch(\Exception $e){
            $import_export->updateStatus($this->_id,'Failed',$e->getMessage());
        }
        return true ;
    }
}
