<?php

namespace App\models\auth;

use Illuminate\Database\Eloquent\Model;
 
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
/**
 * @Written by Sandeep Kumar Maurya 
 * @ 7:16 AM 31st Jan 2018
 */

class PasswordResetModel extends Model
{
 
    /**
     * update password 
     */
    public function UpdatePassword(Request $request){
        DB::beginTransaction();
        try {
            $password = Hash::make($request->post('new_password'));
            DB::table('users')
                ->where('id',$request->input('req_user_id'))
                ->update([
                    'password'=>$password,
                    'updated_at'        => date('Y-m-d H:i:s'),
                    'updated_by'        => ($request->post('user_id') ) ? $request->post('user_id')  : $request->post('req_user_id')
                ]);
            DB::commit();

            $this->CreateChangeHistory($request,$password);        
            return true ;
        } catch (\Exception $e) { 
            DB::rollback();
            return false;
        }
    }

    /**
     * create history of password changes
     */
    private function CreateChangeHistory(Request $request,String $password){
        DB::beginTransaction();
        try {
            $data = array(
                'user_id'=>$request->post('req_user_id'),
                'password'=>$password,
                'created_by'=>$request->post('user_id'),
                'created_at'=>now(),
                'ip_address'=>$request->ip()
            );
            DB::table('password_history')
                ->insert($data);
            DB::commit();
            return true ;
        } catch (\Exception $e) { 
            DB::rollback();
            return false;
        }
    }

  
}
