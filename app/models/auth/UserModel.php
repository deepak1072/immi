<?php

namespace App\models\auth;

use Illuminate\Database\Eloquent\Model;
 
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
/**
 * @Written by Sandeep Kumar Maurya 
 * @ 7:16 AM 31st Jan 2018
 */

class UserModel extends Model
{
 
    //
    public static function getUser(Request $request){

        $code = $request->post('user_code');
        $data=DB::table("users")
            ->selectRaw("id,company_code,user_code,oemail_id,password,is_access_allowed,is_locked")
            ->where("is_deleted","=",'0')
            ->where(function ($query) use ($code)  {
                $query->where('user_code', '=', $code)
                    ->orWhere('oemail_id', '=', $code);
            })
            ->orderBy('id','asc')
            ->first();
        return $data;
    }

    public static function EmpShortInfo($id){
        $data=DB::table("emp_master")
            ->selectRaw("user_id,emp_name,profile_image")
            ->where("user_id",$id)
            ->first();
        return $data; 
    }

    public static function getCompanyLoginInfo($ip){
        $data=DB::table("company_masters")
            ->selectRaw("company_id as id,theme,url,code,name, api_version as version, logo,logo_top,login_banner")
            ->where("is_deleted","=",'0')
            ->where(function ($query) use ($ip){
                $query->where('ip_address', '=', $ip)
                    ->orWhere('url', '=', $ip);
            }) 
            ->first();
        return $data;
    }

    public static function getUserRoles($code){
         
        $current_time = date('Y-m-d');
        $data=DB::table("user_roles")
            ->join('role_masters','role_id','=','role_masters_id')
            ->selectRaw("user_role_id,super_role_id,role_masters_id,role_name,is_default")
            ->where("user_roles.is_deleted","=",'0')
            ->where("is_active","=","1")
            ->where("user_roles.user_id","=",$code) 
            // ->whereDate("active_from", "<=",$current_time)
            // ->whereDate("active_to", ">=",$current_time)
            ->orderBy('user_role_id','asc')
            ->orderBy('is_default','desc')
            ->get();
        return $data;
    }

    public static function getAssignedMenus(Request $request){
        $code = $request->post('role_masters_id');
        $final = [];
        $data=DB::table("role_masters") 
            ->selectRaw("role_menu_access") 
            ->where("role_id","=",$code) 
            ->get();
        if(count($data) > 0){
            $menus = json_decode($data[0]->role_menu_access,true) ;
            $ids = $menus['all'];
            $data=DB::table("menu_masters") 
            ->selectRaw("menu_id,parent_menu_id,menu_name,menu_url,menu_icons,menu_status") 
            ->whereIn("menu_id",$ids) 
            ->where('menu_status','1')
            ->where('is_deleted','0')
            ->orderBy('parent_menu_id','asc')
            ->get();
            if(count($data) > 0){

                foreach ($data as $key => $value) {
                    if($value->parent_menu_id == 0){
                        $final[$value->menu_id] = array(
                            'menu_id' => $value->menu_id,
                            'parent_menu_id' => $value->parent_menu_id,
                            'menu_name' => $value->menu_name,
                            'menu_url' => $value->menu_url,
                            'menu_icons' => $value->menu_icons
                        );                         
                    }else{

                        $final[$value->parent_menu_id]['children'][] = array(
                            'menu_id' => $value->menu_id,
                            'parent_menu_id' => $value->parent_menu_id,
                            'menu_name' => $value->menu_name,
                            'menu_url' => $value->menu_url,
                            'menu_icons' => $value->menu_icons
                        ); 
                    }
                }
            }
        }
        return $final;
    }

    public static function logActivity($data){
        DB::beginTransaction();
        try {
            DB::table('login_activity')
                ->insert($data);
            DB::commit();
            return true ;
        } catch (\Exception $e) {
            DB::rollback();
            return false;
        }
    }

    public static function updateFailedAttempt($id,$time){
        DB::beginTransaction();
        try {
            DB::table('users')
                ->where('id',$id)
                ->increment('failed_attempt');
            DB::commit();
            $dd = DB::table('users')
                    ->selectRaw('failed_attempt,is_locked')
                    ->where('id',$id)
                    ->first();
            if($dd->failed_attempt >= 5){
                DB::beginTransaction();
                DB::table('users')
                    ->where('id',$id)
                    ->update(['is_locked'=>1,'locked_at'=>$time]);
                DB::commit();
            }
            return true ;
        } catch (\Exception $e) {
            DB::rollback();
            return false;
        }
    }
    
    public static function clearLock($id){
        DB::beginTransaction();
        try {
            DB::table('users')
                ->where('id',$id)
                ->update(['is_locked'=>0,'failed_attempt'=>0]);
            DB::commit();
            return true ;
        } catch (\Exception $e) {
            DB::rollback();
            return false;
        }
    }

    public static function updatePassword($data, $id){
        DB::beginTransaction();
        try {
            DB::table('users')
                ->where('id',$id)
                ->update($data);
            DB::commit();
            return true ;
        } catch (\Exception $e) {
            DB::rollback();
            return false;
        }
    }

    public static function getOldPassword($id){
        $data=DB::table("users") 
            ->selectRaw("password") 
            ->where("id",$id) 
            ->first();
        return $data;
    }

    public static function logPasswordActivity($data){
        DB::beginTransaction();
        try {
            DB::table('password_history')
                ->insert($data);
            DB::commit();
            return true ;
        } catch (\Exception $e) {
            DB::rollback();
            return false;
        }
    }
}
