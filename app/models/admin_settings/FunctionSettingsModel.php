<?php
/**
 * Created by Sandeep Maurya on 15th dec 2018.
 */

namespace App\models\admin_settings;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;


class FunctionSettingsModel extends Model
{

	/**
	 * check for availability
	 */
	public static function CheckFunctionAvailability(Request $request){
	  
        $data=DB::table("function_masters")
            ->selectRaw("function_id,function_name,function_code")              
            ->where("function_code",$request->post('function_code'))
            ->where("is_deleted","=",0)
            ->get();

        if(count($data) > 0){
        	return false;
        }   else{
        	return true ;
        } 
	}

	/**
	 * create new role 
	 */
	public static function AddFunction(Request $request){

		 
		$process = array(
            'function_name'         => $request->post('function_name'), 
            'function_code'          =>$request->post('function_code') ,           
            'function_head'          =>$request->post('function_head') ,           
            'created_by'    => 'Sandeep Maurya',
            'created_at'    => date('Y-m-d H:i:s'),
            'updated_by'    => ''
        );

        
        DB::beginTransaction();
        try {
            DB::table('function_masters')
                ->insert($process);
            DB::commit();
            return true ;
        } catch (\Exception $e) {
            // echo $e->getMessage();die;
            DB::rollback();
            return false;
        }
	}

	public static function UpdateFunction(Request $request){
		 
		 

		$process = array(
            'function_name'         => $request->post('function_name'), 
            'function_code'          =>$request->post('function_code') ,
            'function_head'          =>$request->post('function_head') ,
            'updated_at'    => date('Y-m-d H:i:s'),

        );

        // echo "<pre>";
        // print_r($role);
        // die;

        DB::beginTransaction();
        try {
            DB::table('function_masters')
            	->where('function_id',$request->post('function_id'))
                ->update($process);
            DB::commit();
            return true ;
        } catch (\Exception $e) {
            // echo $e->getMessage();die;
            DB::rollback();
            return false;
        }
	}


	/**
     * get roles list
     */
    public static function fetchFunctionList(Request $request){
        
        $limit = ($request->get('pageSize')) ? $request->get('pageSize') : 5;
        $offset = $limit * (($request->get('page')) ? $request->get('page') : 0);
        $filter = ($request->get('filter')) ? $request->get('filter') : [];
        $sort = ($request->get('sort')) ? $request->get('sort') : [];
        
        $query=DB::table("function_masters");
        $query->selectRaw("function_name,function_id,created_by,created_at,function_status,function_code,function_head");
        $query->where("is_deleted","=",0);
        
        // applying filter 
        if(count($filter) > 0){

            foreach ($filter as $key => $value) {
               $filtered = json_decode($value);
                $searchValue =  strtolower(htmlspecialchars(stripcslashes(trim($filtered->value))));
                $id = " lower(".$filtered->id.") ";
                $where  = "".$id ." like ('%$searchValue%') " ;
                if($key == 0){
                    $query->whereRaw($where);
                }else{
                    $query->orWhere($where); 
                }
            }
        }

        // applying sorting
        if(count($sort) > 0){
            foreach ($sort as $key => $value) {
                $sorted = json_decode($value);
                $query->orderBy($sorted->id,($sorted->desc)?'desc':'asc');
            }
        }
       
        
        $count = $query->count();
        $query->offset($offset);
        $query->limit($limit);
    
        // echo $query->toSql();

        $data= $query->get();
        return array('count'=>$count,'data'=>$data);
    }

    /**
     * update role status 
     */
    public static function UpdateFunctionStatus(Request $request){
    	DB::beginTransaction();
        try {
            DB::table('function_masters')
                 ->where('function_id',$request->input('id'))
                ->update(['function_status'=>(int)($request->input('status') == 0)  ? 1 : 0]);
            DB::commit();
            return true ;
        } catch (\Exception $e) {
            // echo $e->getMessage();die;
            DB::rollback();
            return false;
        }
    }

    /**
     * get process details 
     */
    public static function GetFunctionDetails(Request $request){
     
        $data=DB::table("function_masters")
            ->selectRaw("function_name,function_id,function_code,function_head")
            ->where("function_id",$request->get('id'))
            ->get();
        return $data;
    
    }


}