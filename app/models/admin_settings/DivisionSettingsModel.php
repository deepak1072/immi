<?php
/**
 * Created by Sandeep Maurya on 12/12/2018.
 */

namespace App\models\admin_settings;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;


class DivisionSettingsModel extends Model
{

	/**
	 * check for availability
	 */
	public static function CheckDivisionsAvailability(Request $request){
	  
        $data=DB::table("division_masters")
            ->selectRaw("division_id,division_name,company_code,division_code")              
            ->where("division_code",$request->post('division_code'))
            ->where("is_deleted","=",0)
            ->get();

        if(count($data) > 0){
        	return false;
        }   else{
        	return true ;
        } 
	}

	/**
	 * create new role 
	 */
	public static function AddDivisions(Request $request){

		 
		$division = array(
            'division_name'         => $request->post('division_name'), 
            'division_code'          =>$request->post('division_code') ,
            'division_descriptions'          =>($request->post('division_descriptions')) ? $request->post('division_descriptions') : '' ,
            'created_by'    => 'Sandeep Maurya',
            'created_at'    => date('Y-m-d H:i:s'),
            'updated_by'    => ''
        );

        
        DB::beginTransaction();
        try {
            DB::table('division_masters')
                ->insert($division);
            DB::commit();
            return true ;
        } catch (\Exception $e) {
            // echo $e->getMessage();die;
            DB::rollback();
            return false;
        }
	}

	public static function UpdateDivisions(Request $request){
		 
		 

		$division = array(
            'division_name'         => $request->post('division_name'), 
            'division_code'          =>$request->post('division_code') ,
            'division_descriptions'          =>($request->post('division_descriptions')) ? $request->post('division_descriptions') : '' ,
            'updated_at'    => date('Y-m-d H:i:s'),

        );

        // echo "<pre>";
        // print_r($role);
        // die;

        DB::beginTransaction();
        try {
            DB::table('division_masters')
            	->where('division_id',$request->post('division_id'))
                ->update($division);
            DB::commit();
            return true ;
        } catch (\Exception $e) {
            // echo $e->getMessage();die;
            DB::rollback();
            return false;
        }
	}


	/**
     * get roles list
     */
    public static function fetchDivisionList(Request $request){
        
        $limit = ($request->get('pageSize')) ? $request->get('pageSize') : 5;
        $offset = $limit * (($request->get('page')) ? $request->get('page') : 0);
        $filter = ($request->get('filter')) ? $request->get('filter') : [];
        $sort = ($request->get('sort')) ? $request->get('sort') : [];
        
        $query=DB::table("division_masters");
        $query->selectRaw("division_name,division_id,created_by,created_at,division_status,division_code");
        $query->where("is_deleted","=",0);
        
        // applying filter 
        if(count($filter) > 0){

            foreach ($filter as $key => $value) {
               $filtered = json_decode($value);
                $searchValue =  strtolower(htmlspecialchars(stripcslashes(trim($filtered->value))));
                $id = " lower(".$filtered->id.") ";
                $where  = "".$id ." like ('%$searchValue%') " ;
                if($key == 0){
                    $query->whereRaw($where);
                }else{
                    $query->orWhere($where); 
                }
            }
        }

        // applying sorting
        if(count($sort) > 0){
            foreach ($sort as $key => $value) {
                $sorted = json_decode($value);
                $query->orderBy($sorted->id,($sorted->desc)?'desc':'asc');
            }
        }
       
        
        $count = $query->count();
        $query->offset($offset);
        $query->limit($limit);
    
        // echo $query->toSql();

        $data= $query->get();
        return array('count'=>$count,'data'=>$data);
    }

    /**
     * update role status 
     */
    public static function UpdateDivisionStatus(Request $request){
    	DB::beginTransaction();
        try {
            DB::table('division_masters')
                 ->where('division_id',$request->input('id'))
                ->update(['division_status'=>(int)($request->input('status') == 0)  ? 1 : 0]);
            DB::commit();
            return true ;
        } catch (\Exception $e) {
            // echo $e->getMessage();die;
            DB::rollback();
            return false;
        }
    }

    /**
     * get division details 
     */
    public static function GetDivisionDetails(Request $request){
     
        $data=DB::table("division_masters")
            ->selectRaw("division_name,division_id,division_code,division_descriptions")
            ->where("division_id",$request->get('id'))
            ->get();
        return $data;
    
    }


}