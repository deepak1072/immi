<?php
/**
 * Created by Sandeep Maurya on 15th dec 2018.
 */

namespace App\models\admin_settings;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;


class QualificationSettingsModel extends Model
{

	/**
	 * check for availability
	 */
	public static function CheckQualificationAvailability(Request $request){
	  
        $data=DB::table("qualification_masters")
            ->selectRaw("qualification_id,qualification_name,qualification_code")              
            ->where("qualification_code",$request->post('qualification_code'))
            ->where("is_deleted","=",0)
            ->get();

        if(count($data) > 0){
        	return false;
        }   else{
        	return true ;
        } 
	}

	/**
	 * create new qualification 
	 */
	public static function AddQualification(Request $request){

		 
		$process = array(
            'qualification_name'         => $request->post('qualification_name'), 
            'qualification_code'          =>$request->post('qualification_code') ,           
            'created_by'    => 'Sandeep Maurya',
            'created_at'    => date('Y-m-d H:i:s'),
            'updated_by'    => ''
        );

        
        DB::beginTransaction();
        try {
            DB::table('qualification_masters')
                ->insert($process);
            DB::commit();
            return true ;
        } catch (\Exception $e) {
            // echo $e->getMessage();die;
            DB::rollback();
            return false;
        }
	}

	public static function UpdateQualification(Request $request){
		 
		 

		$process = array(
            'qualification_name'         => $request->post('qualification_name'), 
            'qualification_code'          =>$request->post('qualification_code') ,
            'updated_at'    => date('Y-m-d H:i:s'),

        );

        // echo "<pre>";
        // print_r($role);
        // die;

        DB::beginTransaction();
        try {
            DB::table('qualification_masters')
            	->where('qualification_id',$request->post('qualification_id'))
                ->update($process);
            DB::commit();
            return true ;
        } catch (\Exception $e) {
            // echo $e->getMessage();die;
            DB::rollback();
            return false;
        }
	}


	/**
     * get qualification list
     */
    public static function fetchQualificationList(Request $request){
        
        $limit = ($request->get('pageSize')) ? $request->get('pageSize') : 5;
        $offset = $limit * (($request->get('page')) ? $request->get('page') : 0);
        $filter = ($request->get('filter')) ? $request->get('filter') : [];
        $sort = ($request->get('sort')) ? $request->get('sort') : [];
        
        $query=DB::table("qualification_masters");
        $query->selectRaw("qualification_name,qualification_id,created_by,created_at,qualification_status,qualification_code");
        $query->where("is_deleted","=",0);
        
        // applying filter 
        if(count($filter) > 0){

            foreach ($filter as $key => $value) {
               $filtered = json_decode($value);
                $searchValue =  strtolower(htmlspecialchars(stripcslashes(trim($filtered->value))));
                $id = " lower(".$filtered->id.") ";
                $where  = "".$id ." like ('%$searchValue%') " ;
                if($key == 0){
                    $query->whereRaw($where);
                }else{
                    $query->orWhere($where); 
                }
            }
        }

        // applying sorting
        if(count($sort) > 0){
            foreach ($sort as $key => $value) {
                $sorted = json_decode($value);
                $query->orderBy($sorted->id,($sorted->desc)?'desc':'asc');
            }
        }
       
        
        $count = $query->count();
        $query->offset($offset);
        $query->limit($limit);
    
        // echo $query->toSql();

        $data= $query->get();
        return array('count'=>$count,'data'=>$data);
    }

    /**
     * update qualification status 
     */
    public static function UpdateQualificationStatus(Request $request){
    	DB::beginTransaction();
        try {
            DB::table('qualification_masters')
                 ->where('qualification_id',$request->input('id'))
                ->update(['qualification_status'=>(int)($request->input('status') == 0)  ? 1 : 0]);
            DB::commit();
            return true ;
        } catch (\Exception $e) {
            // echo $e->getMessage();die;
            DB::rollback();
            return false;
        }
    }

    /**
     * get qualification details 
     */
    public static function GetQualificationDetails(Request $request){
     
        $data=DB::table("qualification_masters")
            ->selectRaw("qualification_name,qualification_id,qualification_code")
            ->where("qualification_id",$request->get('id'))
            ->get();
        return $data;
    
    }


}