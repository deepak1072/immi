<?php
/**
 * Created by Sandeep Maurya on 12/12/2018.
 */

namespace App\models\admin_settings;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB; 


class RosterSettingsModel extends Model 
{
    public static function updateRosterStatus(Request $request){
        DB::beginTransaction();
        try {
            DB::table('roster')
                 ->where('id',$request->id)
                ->update([
                    'status'=>(int)($request->status == 0)  ? 1 : 0 ,
                    'updated_by' => ($request->user_id) ? $request->user_id: 1 ,
                    'updated_at' => date('Y-m-d H:i:s')
                ]);
            DB::commit();
            return true ;
        } catch (\Exception $e) {
    
            DB::rollback();
            return false;
        } 
    }

    

    public static function deleteInvalidRoster($rp_id)
    {
        $data=DB::table("roster_patterns")
            ->where("rp_id",$rp_id)
            ->delete();
        return $data;
    }

    public static function checkRoster($name){
        $data=DB::table("roster")
            ->selectRaw("id,rp_name")
            ->where("rp_name",$name) 
            ->where("is_deleted","=",0)
            ->get();

        if(count($data) > 0){
        	return false;
        }   else{
        	return true ;
        }  
    }

    public static function createRoster($roster){
        DB::beginTransaction();
        try {
            $id = DB::table('roster')
                ->insertGetId($roster);
            DB::commit();
            return $id ;
        } catch (\Exception $e) {
    
            DB::rollback();
            return false;
        }
    }

    public static function createPattern($pattern){
        DB::beginTransaction();
        try {
            DB::table('roster_pattern')
                ->insert($pattern);
            DB::commit();
            return true ;
        } catch (\Exception $e) {
    
            DB::rollback();
            return false;
        }
    }

    public static function getRoster(Request $request){
        
        $limit = ($request->get('pageSize')) ? $request->get('pageSize') : 5;
        $offset = $limit * (($request->get('page')) ? $request->get('page') : 0);
        $filter = ($request->get('filter')) ? $request->get('filter') : [];
        $sort = ($request->get('sort')) ? $request->get('sort') : [];
        
        $query=DB::table("roster as r");
        $query->selectRaw("id, rp_name,status, case when is_fixed = 1 then 'Fixed' else 'Variable' end as is_fixed, r.created_by, r.created_at,em.emp_name");
        $query->join('emp_master as em','em.user_id','=','r.created_by');
        $query->where("r.is_deleted","=",0);
        
        // applying filter 
        if(count($filter) > 0){
            foreach ($filter as $key => $value) {
               $filtered = json_decode($value);
                $searchValue =  strtolower(htmlspecialchars(stripcslashes(trim($filtered->value))));
                $id = " lower(".$filtered->id.") ";
                $where  = "".$id ." like ('%$searchValue%') " ;
                if($key == 0){
                    $query->whereRaw($where);
                }else{
                    $query->orWhere($where); 
                }
            }
        }
        // applying sorting
        if(count($sort) > 0){
            foreach ($sort as $key => $value) {
                $sorted = json_decode($value);
                $query->orderBy($sorted->id,($sorted->desc)?'desc':'asc');
            }
        }
        $count = $query->count();
        $query->offset($offset);
        $query->limit($limit);
        $data= $query->get();
        return array('count'=>$count,'data'=>$data);
    }

    public static function getRosterDetails(Request $request){
        $data=DB::table("roster")
            ->selectRaw("id,rp_id, rp_name, is_fixed , day, first_week, second_week, third_week, fourth_week, fifth_week")
            ->join('roster_pattern as rp','rp_id','=','id')
            ->where("rp_id",$request->get('id'))
            ->get();
        return $data;
    }

}