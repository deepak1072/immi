<?php
/**
 * Created by Sandeep Maurya on 15th dec 2018.
 */

namespace App\models\admin_settings;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;


class WorkFlowSettingsModel extends Model
{
  
 
	/**
     * get roles list
     */
    public static function fetchList(Request $request){
        
        $limit = ($request->get('pageSize')) ? $request->get('pageSize') : 5;
        $offset = $limit * (($request->get('page')) ? $request->get('page') : 0);
        $filter = ($request->get('filter')) ? $request->get('filter') : [];
        $sort = ($request->get('sort')) ? $request->get('sort') : [];
        
        $query=DB::table("workflows");
        $query->selectRaw("wf_id,`workflow_name`, `workflow_code`, `module_name`,created_by,created_at,flow_status");  
        // applying filter 
        if(count($filter) > 0){

            foreach ($filter as $key => $value) {
               $filtered = json_decode($value);
                $searchValue =  strtolower(htmlspecialchars(stripcslashes(trim($filtered->value))));
                $id = " lower(".$filtered->id.") ";
                $where  = "".$id ." like ('%$searchValue%') " ;
                if($key == 0){
                    $query->whereRaw($where);
                }else{
                    $query->orWhere($where); 
                }
            }
        }

        // applying sorting
        if(count($sort) > 0){
            foreach ($sort as $key => $value) {
                $sorted = json_decode($value);
                $query->orderBy($sorted->id,($sorted->desc)?'desc':'asc');
            }
        }
       
        
        $count = $query->count();
        $query->offset($offset);
        $query->limit($limit);
    
        // echo $query->toSql();

        $data= $query->get();
        return array('count'=>$count,'data'=>$data);
    }

    /**
     * update role status 
     */
    public static function UpdateStatus(Request $request){
    	DB::beginTransaction();
        try {
            DB::table('workflows')
                 ->where('wf_id',$request->input('id'))
                ->update(['flow_status'=>(int)($request->input('status') == 0)  ? 1 : 0]);
            DB::commit();
            return true ;
        } catch (\Exception $e) { 
            DB::rollback();
            return false;
        }
    }

    /**
     * check duplicate workflow
     */
    public static function CheckAvailability(Request $request){
        $data=DB::table("workflows")
            ->selectRaw("workflow_name,wf_id") 
            ->where("workflow_name",$request->post('wf_name'))
            ->orWhere('workflow_code',$request->post('wf_code'))
            ->get();

        if(count($data) > 0){
        	return false;
        }   else{
        	return true ;
        } 
    }

    /**
     * add new workflow 
     */
    public static function AddWorkflow(Request $request){
        
		$data = array( 
            'workflow_name'         => $request->post('wf_name'), 
            'workflow_code'        =>$request->post('wf_code'), 
            'module_name'        =>$request->post('wf_module'), 
            'wf_events'        =>$request->post('wf_events'), 
            'applicable_ou'        =>json_encode($request->post('applicable_ou')), 
            'flow_status'        =>'0' , 
            'flow_type'        =>$request->post('wf_type'), 
            // 'created_by'    => ($request->post('user_code')) ? $request->post('user_code') : '' ,
            'created_by'    =>  1 ,
            'created_at' => date('Y-m-d H:i:s')
        ); 
        DB::beginTransaction();
        try {
            $id = DB::table('workflows')
                ->insertGetId($data);
            DB::commit(); 
            if($request->post('wf_type') == 1){ 
                $request->request->add(['wf_id'=>$id]); 
                 
                self::CreateLevels($request);
            }
            return true ;
        } catch (\Exception $e) { 
            echo $e->getMessage();die;
            DB::rollback();
            return false;
        }
    }

    /**
     * update workflow 
     */
    public static function UpdateWorkflow(Request $request){
        $data = array( 
            'module_name'        =>$request->post('wf_module'), 
            'wf_events'        =>$request->post('wf_events'), 
            'applicable_ou'        =>json_encode($request->post('applicable_ou')), 
            'flow_type'        =>$request->post('wf_type'), 
            // 'created_by'    => ($request->post('user_code')) ? $request->post('user_code') : '' ,
            'updated_by'    =>  1 ,
            'updated_at' => date('Y-m-d H:i:s')
        ); 
        DB::beginTransaction();
        try {
            DB::table('workflows')
                ->where('wf_id',$request->post('wf_id'))
                ->update($data);
            DB::commit();
            if($request->post('wf_type') == 1){
                self::CreateLevels($request);
            }
            return true ;
        } catch (\Exception $e) { 
            echo $e->getMessage();die;
            DB::rollback();
            return false;
        }
    }

    /**
     * get process details 
     */
    public static function GetDetails(Request $request){
     
        $data=DB::table("workflows")
            ->selectRaw("workflow_name as wf_name,wf_id,workflow_code as wf_code,module_name as wf_module,wf_events,applicable_ou,flow_type as wf_type")
            ->where("wf_id",$request->get('id'))
            ->first(); 
        $data = json_decode(json_encode($data),true);  
        $data['wf_levels'] = self::GetLevels($data['wf_id']);
        return $data; 
    }

    /**
     * get events 
     */
    public static function GetEvents($id){
        $data=DB::table("appr_events")
            ->selectRaw("ae_name as name,ae_code as value")
            ->where("appr_modules_code",$id)
            ->where('status','1')
            ->get();
        return $data;
    }

    public static function GetAuthorities(){
        $data=DB::table("approvers")
            ->selectRaw("`appr_code`, `appr_name`")
            ->where("status",'1')
            ->get();
        return $data;
    }

    public static function GetModules(){
        $data=DB::table("appr_modules")
            ->selectRaw("`module_code`, `module_name`")
            ->where("status",'1')
            ->get();
        return $data; 
    }

    public static function CreateLevels(Request $request){
        $data = array();
        if($request->post('wf_events') != 'Create'){
            self::DeleteLevels($request->post('wf_id'));
        } 
        foreach($request->post('wf_levels') as $level){
            $data[] = array( 
                'wf_id'         => $request->post('wf_id'), 
                'wf_level'        =>$level['level'], 
                'approver'        =>$level['approver'], 
                'requ_noti'        =>($level['requ_noti'])?$level['requ_noti']:0, 
                'approver_noti'        =>($level['approver_noti'])?$level['approver_noti']:0, 
                'next_level_noti'        =>($level['next_level_noti'])?$level['next_level_noti']:0, 
                'days_limit'        =>$level['wf_days'], 
                'fixed_cc'        =>($level['f_cc'])? json_encode($level['f_cc']) : json_encode([]) , 
                'fixed_bcc'        =>($level['f_bcc']) ? json_encode($level['f_bcc'])  : json_encode([]) ,
                'dynamic_cc' => ($level['dynamic_cc'])? $level['dynamic_cc'] : '',
                'dynamic_bcc'=>($level['dynamic_bcc'])? $level['dynamic_bcc'] : '', 
                'updated_by'=>1, 
                'created_by'    =>  1 ,
                'created_at' => date('Y-m-d H:i:s')
            );   
        } 
        
        self::CreateLevelsNew($data);
        return true;
    }

    

    public static  function CreateLevelsNew($data){
        
        DB::beginTransaction();
        try {
            DB::table('workflow_levels')
                ->insert($data);
            DB::commit();
            return true ;
        } catch (\Exception $e) {  
            DB::rollback();
            echo $e->getMessage();die;
            return false;
        }
    }

    public static  function DeleteLevels($id){
        return DB::table('workflow_levels')
        ->where('wf_id',$id)
        ->delete();
        // ->update(array('is_deleted'=>'1'));
    }

    public static function GetLevels($id){
        $data=DB::table("workflow_levels")
            ->selectRaw("wf_level_id as _id, wf_id, wf_level as 'level', approver, requ_noti, approver_noti, next_level_noti, days_limit as wf_days, fixed_cc as f_cc, fixed_bcc as f_bcc, dynamic_cc, dynamic_bcc")
            ->where("wf_id",$id)
            ->where("is_deleted",'0')
            ->orderBy('_id','ASC')
            ->get();
        return $data;
    }

}