<?php
/**
 * Created by Sandeep Maurya on 15th dec 2018.
 */

namespace App\models\admin_settings;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;


class EmployeeTypeSettingsModel extends Model
{

	/**
	 * check for availability
	 */
	public static function CheckEmployeeTypeAvailability(Request $request){
	  
        $data=DB::table("employee_type_masters")
            ->selectRaw("employee_type_id,employee_type_name,company_code,employee_type_code")             
            ->where("employee_type_code",$request->post('employee_type_code'))
            ->where("is_deleted","=",0)
            ->get();

        if(count($data) > 0){
        	return false;
        }   else{
        	return true ;
        } 
	}

	/**
	 * create new role 
	 */
	public static function AddEmployeeType(Request $request){

		 
		$employee_type = array(
            'employee_type_name'         => $request->post('employee_type_name'), 
            'employee_type_code'          =>$request->post('employee_type_code') ,           
            'created_by'    => 'Sandeep Maurya',
            'created_at'    => date('Y-m-d H:i:s'),
            'updated_by'    => ''
        );

        
        DB::beginTransaction();
        try {
            DB::table('employee_type_masters')
                ->insert($employee_type);
            DB::commit();
            return true ;
        } catch (\Exception $e) {
            // echo $e->getMessage();die;
            DB::rollback();
            return false;
        }
	}

	public static function UpdateEmployeeType(Request $request){
		 
		 

		$employee_type = array(
            'employee_type_name'         => $request->post('employee_type_name'), 
            'employee_type_code'          =>$request->post('employee_type_code') ,
            'updated_at'    => date('Y-m-d H:i:s'),

        );

        // echo "<pre>";
        // print_r($role);
        // die;

        DB::beginTransaction();
        try {
            DB::table('employee_type_masters')
            	->where('employee_type_id',$request->post('employee_type_id'))
                ->update($employee_type);
            DB::commit();
            return true ;
        } catch (\Exception $e) {
            // echo $e->getMessage();die;
            DB::rollback();
            return false;
        }
	}


	/**
     * get roles list
     */
    public static function fetchEmployeeTypeList(Request $request){
        
        $limit = ($request->get('pageSize')) ? $request->get('pageSize') : 5;
        $offset = $limit * (($request->get('page')) ? $request->get('page') : 0);
        $filter = ($request->get('filter')) ? $request->get('filter') : [];
        $sort = ($request->get('sort')) ? $request->get('sort') : [];
        
        $query=DB::table("employee_type_masters");
        $query->selectRaw("employee_type_name,employee_type_id,created_by,created_at,employee_type_status,employee_type_code");
        $query->where("is_deleted","=",0);
        
        // applying filter 
        if(count($filter) > 0){

            foreach ($filter as $key => $value) {
               $filtered = json_decode($value);
                $searchValue =  strtolower(htmlspecialchars(stripcslashes(trim($filtered->value))));
                $id = " lower(".$filtered->id.") ";
                $where  = "".$id ." like ('%$searchValue%') " ;
                if($key == 0){
                    $query->whereRaw($where);
                }else{
                    $query->orWhere($where); 
                }
            }
        }

        // applying sorting
        if(count($sort) > 0){
            foreach ($sort as $key => $value) {
                $sorted = json_decode($value);
                $query->orderBy($sorted->id,($sorted->desc)?'desc':'asc');
            }
        }
       
        
        $count = $query->count();
        $query->offset($offset);
        $query->limit($limit);
    
        // echo $query->toSql();

        $data= $query->get();
        return array('count'=>$count,'data'=>$data);
    }

    /**
     * update role status 
     */
    public static function UpdateEmployeeTypeStatus(Request $request){
    	DB::beginTransaction();
        try {
            DB::table('employee_type_masters')
                 ->where('employee_type_id',$request->input('id'))
                ->update(['employee_type_status'=>(int)($request->input('status') == 0)  ? 1 : 0]);
            DB::commit();
            return true ;
        } catch (\Exception $e) {
            // echo $e->getMessage();die;
            DB::rollback();
            return false;
        }
    }

    /**
     * get employee_type details 
     */
    public static function GetEmployeeTypeDetails(Request $request){
     
        $data=DB::table("employee_type_masters")
            ->selectRaw("employee_type_name,employee_type_id,employee_type_code")
            ->where("employee_type_id",$request->get('id'))
            ->get();
        return $data;
    
    }


}