<?php
/**
 * Created by Sandeep Maurya on 12/09/2018.
 */

namespace App\models\admin_settings;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;


class CompanySettingsModel extends Model
{
	public static function createNewCompany(Request $request){
		$company = array(
            'company_name'    	=> $request->post('company_name') ,
            'company_url'       => $request->post('company_url'),
            'company_code'      => strtolower($request->post('company_code'))  ,
            'company_ip_address' =>  $request->post('company_ip_address') ,
            'created_by'        => 'Sandeep Maurya',
            'created_at'        => date('Y-m-d H:i:s')
        );

        DB::beginTransaction();
        try {
            DB::table('company_masters')
                ->insert($company);
            DB::commit();
            return true ;
        } catch (\Exception $e) {
            echo $e->getMessage();die;
            DB::rollback();
            return false;
        }
	}

	public static function UpdateCompany(Request $request){
		 
		$company = array(
            'company_name'    	=> $request->post('company_name') ,
            'company_url'       => $request->post('company_url'),
            'company_code'      => strtolower($request->post('company_code'))  ,
            'company_ip_address' =>  $request->post('company_ip_address') ,
            'updated_by'        => 'Sandeep Maurya',
            'updated_at'        => date('Y-m-d H:i:s')
        );

        DB::beginTransaction();
        try {
            DB::table('company_masters')
            	->where('company_id',$request->post('company_id'))
                ->update($company);
            DB::commit();
            return true ;
        } catch (\Exception $e) {
            echo $e->getMessage();die;
            DB::rollback();
            return false;
        }
	}


	/**
     * get company list
     */
    public static function fetchCompanyList(Request $request){
        
        $limit = ($request->get('pageSize')) ? $request->get('pageSize') : 5;
        $offset = $limit * (($request->get('page')) ? $request->get('page') : 0);
        $filter = ($request->get('filter')) ? $request->get('filter') : [];
        $sort = ($request->get('sort')) ? $request->get('sort') : [];
        
        $query=DB::table("company_masters");
        $query->selectRaw("company_name,company_id,company_url,created_by,created_at,company_code,company_ip_address");
        $query->where("is_deleted","=",0);
        
        // applying filter 
        if(count($filter) > 0){

            foreach ($filter as $key => $value) {
               $filtered = json_decode($value);
                $searchValue =  strtolower(htmlspecialchars(stripcslashes(trim($filtered->value))));
                $id = " lower(".$filtered->id.") ";
                $where  = "".$id ." like ('%$searchValue%') " ;
                if($key == 0){
                    $query->whereRaw($where);
                }else{
                    $query->orWhere($where); 
                }
            }
        }

        // applying sorting
        if(count($sort) > 0){
            foreach ($sort as $key => $value) {
                $sorted = json_decode($value);
                $query->orderBy($sorted->id,($sorted->desc)?'desc':'asc');
            }
        }
       
        
        $count = $query->count();
        $query->offset($offset);
        $query->limit($limit);
    
        // echo $query->toSql();

        $data= $query->get();
        return array('count'=>$count,'data'=>$data);
    }


    public static function fetchCompanyDetails(Request $request){
    	$data=DB::table("company_masters")
            ->selectRaw("company_name,company_id,company_url,company_ip_address,company_code")
            ->where("company_id",$request->get('id'))
            ->get();
        return $data;
    }

    public static function getActiveCompany(){
    	$data=DB::table("company_masters")
            ->selectRaw("company_name,company_code")
            ->where("is_deleted",0)
            ->get();
        return $data;
    }

}