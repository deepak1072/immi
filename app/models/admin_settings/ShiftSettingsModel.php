<?php
/**
 * Created by Sandeep Maurya on 12/12/2018.
 */

namespace App\models\admin_settings;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB; 


class ShiftSettingsModel extends Model 
{

	/**
	 * check for availability
	 */
	public static function CheckAvailability(Request $request){
	  
        $data=DB::table("shift_masters")
            ->selectRaw("shift_id,shift_name") 
            ->where("shift_name",$request->post('shift_name'))
            ->where("is_deleted","=",0)
            ->get();

        if(count($data) > 0){
        	return false;
        }   else{
        	return true ;
        } 
	}

	/**
	 * create new holiday 
	 */
	public static function Create(Request $request){

	 
		$data = array( 
            'shift_name'         => $request->post('shift_name'), 
            'shift_code'        =>$request->post('code'),
            'shift_status'        => 0,
            'shift_start'        => date('h:i:s',strtotime($request->post('shift_start'))),
            'shift_end'        =>  date('h:i:s',strtotime($request->post('shift_end')))  ,
            'shift_start_mandatory'    => date('h:i:s',strtotime($request->post('mandate_start')))   ,
            'shift_end_mandatory'    => date('h:i:s',strtotime($request->post('mandate_end')))  ,
            'mandate_time_full_day'    => date('h:i:s',strtotime($request->post('full_day_mandate_hours')))  ,
            'mandate_time_half_day'    => date('h:i:s',strtotime($request->post('half_day_mandate_hours'))) ,
            'round_clock_shift'    => $request->post('round_clock') ,
            'mark_out_mandate'    => $request->post('mark_out') ,
            'swipe_allowed_before_shift'    => date('h:i:s',strtotime($request->post('swipe_before_shift_allowed'))) ,
            'swipe_allowed_after_shift'    => date('h:i:s',strtotime($request->post('swipe_after_shift_allowed')))  ,
            'is_break_shift'    =>  $request->post('break'),
            'break_start'    =>  date('h:i:s',strtotime($request->post('break_start')))  ,
            'break_end'    => date('h:i:s',strtotime($request->post('break_end')))  ,
            'late_coming_allowed'    => date('h:i:s',strtotime($request->post('late_allowed_minute')))  ,
            'late_coming_frequency'    =>  $request->post('late_allowed_frequency')  ,
            'late_coming_grace_period'    =>   date('h:i:s',strtotime($request->post('late_allowed_grace_period')))   ,
            'early_going_allowed'    => date('h:i:s',strtotime($request->post('early_going_minute')))   ,
            'early_going_frequency'    => $request->post('early_going_frequency') ,
            'early_going_grace_period'    => date('h:i:s',strtotime($request->post('early_going_grace_period')))   ,
            'created_by'    => ($request->post('user_code')) ? $request->post('user_code') : '' ,
            'created_at' => date('Y-m-d H:i:s')
        );


        DB::beginTransaction();
        try {
            DB::table('shift_masters')
                ->insert($data);
            DB::commit();
            return true ;
        } catch (\Exception $e) {
    
            DB::rollback();
            return false;
        }
	}

	public static function UpdateShift(Request $request){
        $data = array(
            'shift_name'         => $request->post('shift_name'),
            'shift_code'        =>$request->post('code'),
            'shift_start'        => date(config('constants.TIME'),strtotime($request->post('shift_start'))),
            'shift_end'        =>  date(config('constants.TIME'),strtotime($request->post('shift_end')))  ,
            'shift_start_mandatory'    => date(config('constants.TIME'),strtotime($request->post('mandate_start')))   ,
            'shift_end_mandatory'    => date(config('constants.TIME'),strtotime($request->post('mandate_end')))  ,
            'mandate_time_full_day'    => date('h:i:s',strtotime($request->post('full_day_mandate_hours')))  ,
            'mandate_time_half_day'    => date('h:i:s',strtotime($request->post('half_day_mandate_hours'))) ,
            'round_clock_shift'    => $request->post('round_clock') ,
            'mark_out_mandate'    => $request->post('mark_out') ,
            'swipe_allowed_before_shift'    => date('h:i:s',strtotime($request->post('swipe_before_shift_allowed'))) ,
            'swipe_allowed_after_shift'    => date('h:i:s',strtotime($request->post('swipe_after_shift_allowed')))  ,
            'is_break_shift'    =>  $request->post('break'),
            'break_start'    =>  date(config('constants.TIME'),strtotime($request->post('break_start')))  ,
            'break_end'    => date(config('constants.TIME'),strtotime($request->post('break_end')))  ,
            'late_coming_allowed'    => date('h:i:s',strtotime($request->post('late_allowed_minute')))  ,
            'late_coming_frequency'    =>  $request->post('late_allowed_frequency')  ,
            'late_coming_grace_period'    =>   date('h:i:s',strtotime($request->post('late_allowed_grace_period')))   ,
            'early_going_allowed'    => date('h:i:s',strtotime($request->post('early_going_minute')))   ,
            'early_going_frequency'    => $request->post('early_going_frequency') ,
            'early_going_grace_period'    => date('h:i:s',strtotime($request->post('early_going_grace_period')))   ,
            'updated_by'    => ($request->post('user_code')) ? $request->post('user_code') : '' ,
            'updated_at' => date('Y-m-d H:i:s')
        );




        DB::beginTransaction();
        try {
            DB::table('shift_masters')
            	->where('shift_id',$request->post('shift_id'))
                ->update($data);
            DB::commit();
            return true ;
        } catch (\Exception $e) {
            
            DB::rollback();
            return false;
        }
	}


	/**
     * get roles list
     */
    public static function fetchList(Request $request){

        
        $limit = ($request->get('pageSize')) ? $request->get('pageSize') : 5;
        $offset = $limit * (($request->get('page')) ? $request->get('page') : 0);
        $filter = ($request->get('filter')) ? $request->get('filter') : [];
        $sort = ($request->get('sort')) ? $request->get('sort') : [];
        
        $query=DB::table("shift_masters");
        $query->selectRaw("shift_name,shift_id,created_by,created_at,shift_status,shift_code");
        $query->where("is_deleted","=",0);
        
        // applying filter 
        if(count($filter) > 0){

            foreach ($filter as $key => $value) {
               $filtered = json_decode($value);
                $searchValue =  strtolower(htmlspecialchars(stripcslashes(trim($filtered->value))));
                $id = " lower(".$filtered->id.") ";
                $where  = "".$id ." like ('%$searchValue%') " ;
                if($key == 0){
                    $query->whereRaw($where);
                }else{
                    $query->orWhere($where); 
                }
            }
        }

        // applying sorting
        if(count($sort) > 0){
            foreach ($sort as $key => $value) {
                $sorted = json_decode($value);
                $query->orderBy($sorted->id,($sorted->desc)?'desc':'asc');
            }
        }
       
        
        $count = $query->count();
        $query->offset($offset);
        $query->limit($limit);
    
        

        $data= $query->get();
        return array('count'=>$count,'data'=>$data);
    }

    /**
     * update role status 
     */
    public static function UpdateStatus(Request $request){
    	DB::beginTransaction();
        try {
            DB::table('shift_masters')
                 ->where('shift_id',$request->input('id'))
                ->update([
                    'shift_status'=>(int)($request->input('status') == 0)  ? 1 : 0 ,
                    'updated_by' => ($request->post('user_code')) ? $request->post('user_code') : '' ,
                    'updated_at' => date('Y-m-d H:i:s')
                ]);
            DB::commit();
            return true ;
        } catch (\Exception $e) {
    
            DB::rollback();
            return false;
        }
    }

    /**
     * get role details 
     */
    public static function GetDetails(Request $request){

        $data=DB::table("shift_masters")
            ->selectRaw("shift_id,shift_name,shift_code,shift_status,shift_start,shift_end,shift_start_mandatory,shift_end_mandatory,mandate_time_full_day,mandate_time_half_day,round_clock_shift,mark_out_mandate,swipe_allowed_before_shift,swipe_allowed_after_shift,is_break_shift,break_start,break_end,late_coming_allowed,late_coming_grace_period,late_coming_frequency,early_going_allowed,early_going_frequency,early_going_grace_period")
            ->where("shift_id",$request->get('id'))
            ->get();
        return $data;
    
    }
 
    public static function getActiveShift()
    {
        $data=DB::table("shift_masters")
            ->selectRaw("shift_id,shift_name,shift_code,shift_start_mandatory,shift_end_mandatory")
            ->where("shift_status",1)
            ->get();
        return $data;
    }
}