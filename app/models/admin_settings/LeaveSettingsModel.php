<?php
/**
 * Created by Sandeep Maurya on 25th dec 2018.
 */

namespace App\models\admin_settings;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;


class LeaveSettingsModel extends Model
{

	/**
	 * check for availability
	 */
	public static function CheckLeaveAvailability(Request $request){
	  
        $data=DB::table("leave_types")
            ->selectRaw("leave_id,leave_name")              
            ->where("leave_name",$request->post('leave_name'))
            ->where("is_deleted","=",0)
            ->get();

        if(count($data) > 0){
        	return false;
        }   else{
        	return true ;
        } 
	}

	/**
	 * create new leave 
	 */
	public static function AddLeave(Request $request){

		 
		$leavetypes = array(
            'leave_name'         => $request->post('leave_name'), 
            'leave_master_id'          =>$request->post('leave_master_id') , 
            'leave_short_name'          =>$request->post('leave_short_name') , 
            'leave_desc'          =>$request->post('leave_desc') , 
            'leave_settings'    =>json_encode($request->all()),
            'created_by'    => $request->post('user_code'),
            'created_by_name'    => ' ',
            'created_at'    => date('Y-m-d H:i:s'),
            'updated_by'    => '',
        );

        
        DB::beginTransaction();
        try {
            DB::table('leave_types')
                ->insert($leavetypes);
            DB::commit();
            return true ;
        } catch (\Exception $e) {
            echo $e->getMessage();die;
            DB::rollback();
            return false;
        }
	}

	public static function UpdateLeave(Request $request){
		 
	
        $leavetypes = array(
            'leave_name'         => $request->post('leave_name'),  
            'leave_desc'          =>$request->post('leave_desc') , 
            'leave_settings'    =>json_encode($request->all()),

            'updated_at'    => date('Y-m-d H:i:s'),
            'updated_by'    => $request->post('user_code'),
        );
 

        DB::beginTransaction();
        try {
            DB::table('leave_types')
            	->where('leave_id',$request->post('leave_id'))
                ->update($leavetypes);
            DB::commit();
            return true ;
        } catch (\Exception $e) {
            // echo $e->getMessage();die;
            DB::rollback();
            return false;
        }
	}


	/**
     * get leaves list
     */
    public static function fetchLeaveList(Request $request){
        
        $limit = ($request->get('pageSize')) ? $request->get('pageSize') : 5;
        $offset = $limit * (($request->get('page')) ? $request->get('page') : 0);
        $filter = ($request->get('filter')) ? $request->get('filter') : [];
        $sort = ($request->get('sort')) ? $request->get('sort') : [];
        
        $query=DB::table("leave_types as lv");
        $query->selectRaw("leave_name,leave_id,lv.created_by,lv.created_at,leave_status,lv.leave_short_name as leave_short_name,leave_master_name,lv.leave_master_id");
        $query->join('leave_masters as lm','lm.leave_master_id','=','lv.leave_master_id');
        $query->where("is_deleted","=",0);
        
        // applying filter 
        if(count($filter) > 0){

            foreach ($filter as $key => $value) {
               $filtered = json_decode($value);
                $searchValue =  strtolower(htmlspecialchars(stripcslashes(trim($filtered->value))));

                $id  = ($filtered->id == 'leave_master_name') ? " lower(".$filtered->id.") " : " lower(lv.".$filtered->id.") ";

                $where  = "".$id ." like ('%$searchValue%') " ;
                if($key == 0){
                    $query->whereRaw($where);
                }else{
                    $query->orWhere($where); 
                }
            }
        }

        // applying sorting
        if(count($sort) > 0){
            foreach ($sort as $key => $value) {
                $sorted = json_decode($value);
                $query->orderBy($sorted->id,($sorted->desc)?'desc':'asc');
            }
        }
       
        
        $count = $query->count();
        $query->offset($offset);
        $query->limit($limit);
    
        // echo $query->toSql();

        $data= $query->get();
        return array('count'=>$count,'data'=>$data);
    }

    /**
     * update leave status 
     */
    public static function UpdateLeaveStatus(Request $request){
    	DB::beginTransaction();
        try {
            DB::table('leave_types')
                 ->where('leave_id',$request->input('id'))
                ->update([
                    'leave_status'=>(int)($request->input('status') == 0)  ? 1 : 0 ,
                    'updated_by' => $request->input('user_code'),
                    'updated_at' => date('Y-m-d H:i:s')
                ]);
            DB::commit();
            return true ;
        } catch (\Exception $e) {
            // echo $e->getMessage();die;
            DB::rollback();
            return false;
        }
    }

    /**
     * get leave details 
     */
    public static function GetLeaveDetails(Request $request){
     
        $data=DB::table("leave_types")
            ->selectRaw("leave_name,leave_id,leave_short_name,leave_desc,leave_settings,leave_master_id")
            ->where("leave_id",$request->get('id'))
            ->get();
        return $data;
    
    }

    /**
     * get leave masters 
     */
    public static function fetchLeaveMasters(){
        $data=DB::table("leave_masters")
            ->selectRaw("leave_master_name,leave_master_id")
            ->get();
        return $data;
    }



    


}