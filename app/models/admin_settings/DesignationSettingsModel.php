<?php
/**
 * Created by Sandeep Maurya on 15th dec 2018.
 */

namespace App\models\admin_settings;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;


class DesignationSettingsModel extends Model
{

	/**
	 * check for availability
	 */
	public static function CheckDesignationAvailability(Request $request){
	  
        $data=DB::table("designation_masters")
            ->selectRaw("designation_id,designation_name,designation_code") 
             
            ->where("designation_code",$request->post('designation_code'))
            ->where("is_deleted","=",0)
            ->get();

        if(count($data) > 0){
        	return false;
        }   else{
        	return true ;
        } 
	}

	/**
	 * create new role 
	 */
	public static function AddDesignation(Request $request){

		 
		$designation = array(
            'designation_name'         => $request->post('designation_name'), 
            'designation_code'          =>$request->post('designation_code') ,           
            'created_by'    => 'Sandeep Maurya',
            'created_at'    => date('Y-m-d H:i:s'),
            'updated_by'    => ''
        );

        
        DB::beginTransaction();
        try {
            DB::table('designation_masters')
                ->insert($designation);
            DB::commit();
            return true ;
        } catch (\Exception $e) {
            // echo $e->getMessage();die;
            DB::rollback();
            return false;
        }
	}

	public static function UpdateDesignation(Request $request){
		 
		 

		$designation = array(
            'designation_name'         => $request->post('designation_name'), 
            'designation_code'          =>$request->post('designation_code') ,
            'updated_at'    => date('Y-m-d H:i:s'),

        );

        // echo "<pre>";
        // print_r($role);
        // die;

        DB::beginTransaction();
        try {
            DB::table('designation_masters')
            	->where('designation_id',$request->post('designation_id'))
                ->update($designation);
            DB::commit();
            return true ;
        } catch (\Exception $e) {
            // echo $e->getMessage();die;
            DB::rollback();
            return false;
        }
	}


	/**
     * get roles list
     */
    public static function fetchDesignationList(Request $request){
        
        $limit = ($request->get('pageSize')) ? $request->get('pageSize') : 5;
        $offset = $limit * (($request->get('page')) ? $request->get('page') : 0);
        $filter = ($request->get('filter')) ? $request->get('filter') : [];
        $sort = ($request->get('sort')) ? $request->get('sort') : [];
        
        $query=DB::table("designation_masters");
        $query->selectRaw("designation_name,designation_id,created_by,created_at,designation_status,designation_code");
        $query->where("is_deleted","=",0);
        
        // applying filter 
        if(count($filter) > 0){

            foreach ($filter as $key => $value) {
               $filtered = json_decode($value);
                $searchValue =  strtolower(htmlspecialchars(stripcslashes(trim($filtered->value))));
                $id = " lower(".$filtered->id.") ";
                $where  = "".$id ." like ('%$searchValue%') " ;
                if($key == 0){
                    $query->whereRaw($where);
                }else{
                    $query->orWhere($where); 
                }
            }
        }

        // applying sorting
        if(count($sort) > 0){
            foreach ($sort as $key => $value) {
                $sorted = json_decode($value);
                $query->orderBy($sorted->id,($sorted->desc)?'desc':'asc');
            }
        }
       
        
        $count = $query->count();
        $query->offset($offset);
        $query->limit($limit);
    
        // echo $query->toSql();

        $data= $query->get();
        return array('count'=>$count,'data'=>$data);
    }

    /**
     * update role status 
     */
    public static function UpdateDesignationStatus(Request $request){
    	DB::beginTransaction();
        try {
            DB::table('designation_masters')
                 ->where('designation_id',$request->input('id'))
                ->update(['designation_status'=>(int)($request->input('status') == 0)  ? 1 : 0]);
            DB::commit();
            return true ;
        } catch (\Exception $e) {
            // echo $e->getMessage();die;
            DB::rollback();
            return false;
        }
    }

    /**
     * get designation details 
     */
    public static function GetDesignationDetails(Request $request){
     
        $data=DB::table("designation_masters")
            ->selectRaw("designation_name,designation_id,designation_code")
            ->where("designation_id",$request->get('id'))
            ->get();
        return $data;
    
    }


}