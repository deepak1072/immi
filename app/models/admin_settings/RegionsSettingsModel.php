<?php
/**
 * Created by Sandeep Maurya on 15th dec 2018.
 */

namespace App\models\admin_settings;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;


class RegionsSettingsModel extends Model
{

	/**
	 * check for availability
	 */
	public static function CheckRegionsAvailability(Request $request){
	  
        $data=DB::table("regions_masters")
            ->selectRaw("regions_id,regions_name,regions_code")              
            ->where("regions_code",$request->post('regions_code'))
            ->where("is_deleted","=",0)
            ->get();

        if(count($data) > 0){
        	return false;
        }   else{
        	return true ;
        } 
	}

	/**
	 * create new role 
	 */
	public static function AddRegions(Request $request){

		 
		$regions = array(
            'regions_name'         => $request->post('regions_name'), 
            'regions_code'          =>$request->post('regions_code') ,           
            'created_by'    => 'Sandeep Maurya',
            'created_at'    => date('Y-m-d H:i:s'),
            'updated_by'    => ''
        );

        
        DB::beginTransaction();
        try {
            DB::table('regions_masters')
                ->insert($regions);
            DB::commit();
            return true ;
        } catch (\Exception $e) {
            // echo $e->getMessage();die;
            DB::rollback();
            return false;
        }
	}

	public static function UpdateRegions(Request $request){
		 
		 

		$regions = array(
            'regions_name'         => $request->post('regions_name'), 
            'regions_code'          =>$request->post('regions_code') ,
            'updated_at'    => date('Y-m-d H:i:s'),

        );

        // echo "<pre>";
        // print_r($role);
        // die;

        DB::beginTransaction();
        try {
            DB::table('regions_masters')
            	->where('regions_id',$request->post('regions_id'))
                ->update($regions);
            DB::commit();
            return true ;
        } catch (\Exception $e) {
            // echo $e->getMessage();die;
            DB::rollback();
            return false;
        }
	}


	/**
     * get roles list
     */
    public static function fetchRegionsList(Request $request){
        
        $limit = ($request->get('pageSize')) ? $request->get('pageSize') : 5;
        $offset = $limit * (($request->get('page')) ? $request->get('page') : 0);
        $filter = ($request->get('filter')) ? $request->get('filter') : [];
        $sort = ($request->get('sort')) ? $request->get('sort') : [];
        
        $query=DB::table("regions_masters");
        $query->selectRaw("regions_name,regions_id,created_by,created_at,regions_status,regions_code");
        $query->where("is_deleted","=",0);
        
        // applying filter 
        if(count($filter) > 0){

            foreach ($filter as $key => $value) {
               $filtered = json_decode($value);
                $searchValue =  strtolower(htmlspecialchars(stripcslashes(trim($filtered->value))));
                $id = " lower(".$filtered->id.") ";
                $where  = "".$id ." like ('%$searchValue%') " ;
                if($key == 0){
                    $query->whereRaw($where);
                }else{
                    $query->orWhere($where); 
                }
            }
        }

        // applying sorting
        if(count($sort) > 0){
            foreach ($sort as $key => $value) {
                $sorted = json_decode($value);
                $query->orderBy($sorted->id,($sorted->desc)?'desc':'asc');
            }
        }
       
        
        $count = $query->count();
        $query->offset($offset);
        $query->limit($limit);
    
        // echo $query->toSql();

        $data= $query->get();
        return array('count'=>$count,'data'=>$data);
    }

    /**
     * update role status 
     */
    public static function UpdateRegionsStatus(Request $request){
    	DB::beginTransaction();
        try {
            DB::table('regions_masters')
                 ->where('regions_id',$request->input('id'))
                ->update(['regions_status'=>(int)($request->input('status') == 0)  ? 1 : 0]);
            DB::commit();
            return true ;
        } catch (\Exception $e) {
            // echo $e->getMessage();die;
            DB::rollback();
            return false;
        }
    }

    /**
     * get regions details 
     */
    public static function GetRegionsDetails(Request $request){
     
        $data=DB::table("regions_masters")
            ->selectRaw("regions_name,regions_id,regions_code")
            ->where("regions_id",$request->get('id'))
            ->get();
        return $data;
    
    }


}