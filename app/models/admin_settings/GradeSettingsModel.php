<?php
/**
 * Created by Sandeep Maurya on 15th dec 2018.
 */

namespace App\models\admin_settings;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;


class GradeSettingsModel extends Model
{

	/**
	 * check for availability
	 */
	public static function CheckGradeAvailability(Request $request){
	  
        $data=DB::table("grade_masters")
            ->selectRaw("grade_id,grade_name,grade_code") 
            ->where("grade_code",$request->post('grade_code'))
            ->where("is_deleted","=",0)
            ->get();

        if(count($data) > 0){
        	return false;
        }   else{
        	return true ;
        } 
	}

	/**
	 * create new role 
	 */
	public static function AddGrade(Request $request){

		 
		$grade = array(
            'grade_name'         => $request->post('grade_name'), 
            'grade_code'          =>$request->post('grade_code') , 
            'min_salary_scale'          =>$request->post('min_salary_scale') , 
            'max_salary_scale'          =>$request->post('max_salary_scale') , 
            'min_salary_tolerance'          =>$request->post('min_salary_tolerance') , 
            'max_salary_tolerance'          =>$request->post('max_salary_tolerance') , 

            'created_by'    => 'Sandeep Maurya',
            'created_at'    => date('Y-m-d H:i:s'),
            'updated_by'    => ''
        );

        
        DB::beginTransaction();
        try {
            DB::table('grade_masters')
                ->insert($grade);
            DB::commit();
            return true ;
        } catch (\Exception $e) {
            // echo $e->getMessage();die;
            DB::rollback();
            return false;
        }
	}

	public static function UpdateGrade(Request $request){
		 
		 

		$grade = array(
            'grade_name'         => $request->post('grade_name'), 
            'grade_code'          =>$request->post('grade_code') ,
            'min_salary_scale'          =>$request->post('min_salary_scale') , 
            'max_salary_scale'          =>$request->post('max_salary_scale') , 
            'min_salary_tolerance'          =>$request->post('min_salary_tolerance') , 
            'max_salary_tolerance'          =>$request->post('max_salary_tolerance') , 
            'updated_at'    => date('Y-m-d H:i:s'),

        );
 

        DB::beginTransaction();
        try {
            DB::table('grade_masters')
            	->where('grade_id',$request->post('grade_id'))
                ->update($grade);
            DB::commit();
            return true ;
        } catch (\Exception $e) {
            // echo $e->getMessage();die;
            DB::rollback();
            return false;
        }
	}


	/**
     * get roles list
     */
    public static function fetchGradeList(Request $request){
        
        $limit = ($request->get('pageSize')) ? $request->get('pageSize') : 5;
        $offset = $limit * (($request->get('page')) ? $request->get('page') : 0);
        $filter = ($request->get('filter')) ? $request->get('filter') : [];
        $sort = ($request->get('sort')) ? $request->get('sort') : [];
        
        $query=DB::table("grade_masters");
        $query->selectRaw("grade_name,grade_id,created_by,created_at,grade_status,grade_code");
        $query->where("is_deleted","=",0);
        
        // applying filter 
        if(count($filter) > 0){

            foreach ($filter as $key => $value) {
               $filtered = json_decode($value);
                $searchValue =  strtolower(htmlspecialchars(stripcslashes(trim($filtered->value))));
                $id = " lower(".$filtered->id.") ";
                $where  = "".$id ." like ('%$searchValue%') " ;
                if($key == 0){
                    $query->whereRaw($where);
                }else{
                    $query->orWhere($where); 
                }
            }
        }

        // applying sorting
        if(count($sort) > 0){
            foreach ($sort as $key => $value) {
                $sorted = json_decode($value);
                $query->orderBy($sorted->id,($sorted->desc)?'desc':'asc');
            }
        }
       
        
        $count = $query->count();
        $query->offset($offset);
        $query->limit($limit);
    
        // echo $query->toSql();

        $data= $query->get();
        return array('count'=>$count,'data'=>$data);
    }

    /**
     * update role status 
     */
    public static function UpdateGradeStatus(Request $request){
    	DB::beginTransaction();
        try {
            DB::table('grade_masters')
                 ->where('grade_id',$request->input('id'))
                ->update(['grade_status'=>(int)($request->input('status') == 0)  ? 1 : 0]);
            DB::commit();
            return true ;
        } catch (\Exception $e) {
            // echo $e->getMessage();die;
            DB::rollback();
            return false;
        }
    }

    /**
     * get grade details 
     */
    public static function GetGradeDetails(Request $request){
     
        $data=DB::table("grade_masters")
            ->selectRaw("grade_name,grade_id,grade_code,min_salary_scale,max_salary_scale,min_salary_tolerance,max_salary_tolerance")
            ->where("grade_id",$request->get('id'))
            ->get();
        return $data;
    
    }


}