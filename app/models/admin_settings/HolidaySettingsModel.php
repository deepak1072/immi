<?php
/**
 * Created by Sandeep Maurya on 12/12/2018.
 */

namespace App\models\admin_settings;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;


class HolidaySettingsModel extends Model
{

	/**
	 * check for availability
	 */
	public static function CheckAvailability(Request $request){
	  
        $data=DB::table("holidays")
            ->selectRaw("h_id,h_name") 
            ->where("h_name",$request->post('h_name'))
            ->where("is_deleted","=",0)
            ->get();

        if(count($data) > 0){
        	return false;
        }   else{
        	return true ;
        } 
	}

	/**
	 * create new holiday 
	 */
	public static function Create(Request $request){

	 
		$holidays = array( 
            'h_name'         => $request->post('h_name'), 
            'h_date'        => date('Y-m-d',strtotime($request->post('h_date'))),
            'h_status'        => $request->post('h_status'),
            'h_type'        => $request->post('h_type'),
            'is_additional'        => $request->post('is_additional'),
            'h_applicable'    => json_encode($request->post('h_applicable')) ,
            'created_by'    => ($request->post('user_code')) ? $request->post('user_code') : '' ,
            'created_at' => date('Y-m-d H:i:s')
        );


        DB::beginTransaction();
        try {
            DB::table('holidays')
                ->insert($holidays);
            DB::commit();
            return true ;
        } catch (\Exception $e) {
            // echo $e->getMessage();die;
            DB::rollback();
            return false;
        }
	}

	public static function UpdateHoliday(Request $request){
		 
		$holidays = array( 
            'h_name'         => $request->post('h_name'), 
            'h_date'        => date('Y-m-d',strtotime($request->post('h_date'))),
            'h_status'        => $request->post('h_status'),
            'h_type'        => $request->post('h_type'),
            'is_additional'        => $request->post('is_additional'),
            'h_applicable'    => json_encode($request->post('h_applicable')) ,
            'updated_by'    => ($request->post('user_code')) ? $request->post('user_code') : '' ,
            'updated_at' => date('Y-m-d H:i:s')
        );


        DB::beginTransaction();
        try {
            DB::table('holidays')
            	->where('h_id',$request->post('h_id'))
                ->update($holidays);
            DB::commit();
            return true ;
        } catch (\Exception $e) {
            // echo $e->getMessage();die;
            DB::rollback();
            return false;
        }
	}


	/**
     * get roles list
     */
    public static function fetchList(Request $request){
        
        $limit = ($request->get('pageSize')) ? $request->get('pageSize') : 5;
        $offset = $limit * (($request->get('page')) ? $request->get('page') : 0);
        $filter = ($request->get('filter')) ? $request->get('filter') : [];
        $sort = ($request->get('sort')) ? $request->get('sort') : [];
        
        $query=DB::table("holidays");
        $query->selectRaw("h_name,h_id,created_by,created_at,h_status,h_date");
        $query->where("is_deleted","=",0);
        
        // applying filter 
        if(count($filter) > 0){

            foreach ($filter as $key => $value) {
               $filtered = json_decode($value);
                $searchValue =  strtolower(htmlspecialchars(stripcslashes(trim($filtered->value))));
                $id = " lower(".$filtered->id.") ";
                $where  = "".$id ." like ('%$searchValue%') " ;
                if($key == 0){
                    $query->whereRaw($where);
                }else{
                    $query->orWhere($where); 
                }
            }
        }

        // applying sorting
        if(count($sort) > 0){
            foreach ($sort as $key => $value) {
                $sorted = json_decode($value);
                $query->orderBy($sorted->id,($sorted->desc)?'desc':'asc');
            }
        }
       
        
        $count = $query->count();
        $query->offset($offset);
        $query->limit($limit);
    
        // echo $query->toSql();

        $data= $query->get();
        return array('count'=>$count,'data'=>$data);
    }

    /**
     * update role status 
     */
    public static function UpdateStatus(Request $request){
    	DB::beginTransaction();
        try {
            DB::table('holidays')
                 ->where('h_id',$request->input('id'))
                ->update([
                    'h_status'=>(int)($request->input('status') == 0)  ? 1 : 0 ,
                    'updated_by' => ($request->post('user_code')) ? $request->post('user_code') : '' ,
                    'updated_at' => date('Y-m-d H:i:s')
                ]);
            DB::commit();
            return true ;
        } catch (\Exception $e) {
            echo $e->getMessage();die;
            DB::rollback();
            return false;
        }
    }

    /**
     * get role details 
     */
    public static function GetDetails(Request $request){
     
        $data=DB::table("holidays")
            ->selectRaw("h_name,h_id,h_date,h_status,created_by,created_at,h_applicable,is_additional,h_type")
            ->where("h_id",$request->get('id'))
            ->get();
        return $data;
    
    }
 

}