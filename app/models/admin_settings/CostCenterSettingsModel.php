<?php
/**
 * Created by Sandeep Maurya on 15th dec 2018.
 */

namespace App\models\admin_settings;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;


class CostCenterSettingsModel extends Model
{

	/**
	 * check for availability
	 */
	public static function CheckCostCenterAvailability(Request $request){
	  
        $data=DB::table("cost_center_masters")
            ->selectRaw("cost_center_id,cost_center_name,cost_center_code")            
            ->where("cost_center_code",$request->post('cost_center_code'))
            ->where("is_deleted","=",0)
            ->get();

        if(count($data) > 0){
        	return false;
        }   else{
        	return true ;
        } 
	}

	/**
	 * create new role 
	 */
	public static function AddCostCenter(Request $request){

		 
		$designation = array(
            'cost_center_name'         => $request->post('cost_center_name'), 
            'cost_center_code'          =>$request->post('cost_center_code') ,           
            'created_by'    => 'Sandeep Maurya',
            'created_at'    => date('Y-m-d H:i:s'),
            'updated_by'    => ''
        );

        
        DB::beginTransaction();
        try {
            DB::table('cost_center_masters')
                ->insert($designation);
            DB::commit();
            return true ;
        } catch (\Exception $e) {
            // echo $e->getMessage();die;
            DB::rollback();
            return false;
        }
	}

	public static function UpdateCostCenter(Request $request){
		 
		 

		$designation = array(
            'cost_center_name'         => $request->post('cost_center_name'), 
            'cost_center_code'          =>$request->post('cost_center_code') ,
            'updated_at'    => date('Y-m-d H:i:s'),

        );

        // echo "<pre>";
        // print_r($role);
        // die;

        DB::beginTransaction();
        try {
            DB::table('cost_center_masters')
            	->where('cost_center_id',$request->post('cost_center_id'))
                ->update($designation);
            DB::commit();
            return true ;
        } catch (\Exception $e) {
            // echo $e->getMessage();die;
            DB::rollback();
            return false;
        }
	}


	/**
     * get roles list
     */
    public static function fetchCostCenterList(Request $request){
        
        $limit = ($request->get('pageSize')) ? $request->get('pageSize') : 5;
        $offset = $limit * (($request->get('page')) ? $request->get('page') : 0);
        $filter = ($request->get('filter')) ? $request->get('filter') : [];
        $sort = ($request->get('sort')) ? $request->get('sort') : [];
        
        $query=DB::table("cost_center_masters");
        $query->selectRaw("cost_center_name,cost_center_id,created_by,created_at,cost_center_status,cost_center_code");
        $query->where("is_deleted","=",0);
        
        // applying filter 
        if(count($filter) > 0){

            foreach ($filter as $key => $value) {
               $filtered = json_decode($value);
                $searchValue =  strtolower(htmlspecialchars(stripcslashes(trim($filtered->value))));
                $id = " lower(".$filtered->id.") ";
                $where  = "".$id ." like ('%$searchValue%') " ;
                if($key == 0){
                    $query->whereRaw($where);
                }else{
                    $query->orWhere($where); 
                }
            }
        }

        // applying sorting
        if(count($sort) > 0){
            foreach ($sort as $key => $value) {
                $sorted = json_decode($value);
                $query->orderBy($sorted->id,($sorted->desc)?'desc':'asc');
            }
        }
       
        
        $count = $query->count();
        $query->offset($offset);
        $query->limit($limit);
    
        // echo $query->toSql();

        $data= $query->get();
        return array('count'=>$count,'data'=>$data);
    }

    /**
     * update role status 
     */
    public static function UpdateCostCenterStatus(Request $request){
    	DB::beginTransaction();
        try {
            DB::table('cost_center_masters')
                 ->where('cost_center_id',$request->input('id'))
                ->update(['cost_center_status'=>(int)($request->input('status') == 0)  ? 1 : 0]);
            DB::commit();
            return true ;
        } catch (\Exception $e) {
            // echo $e->getMessage();die;
            DB::rollback();
            return false;
        }
    }

    /**
     * get designation details 
     */
    public static function GetCostCenterDetails(Request $request){
     
        $data=DB::table("cost_center_masters")
            ->selectRaw("cost_center_name,cost_center_id,cost_center_code")
            ->where("cost_center_id",$request->get('id'))
            ->get();
        return $data;
    
    }


}