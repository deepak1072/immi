<?php
/**
 * Created by Sandeep Maurya on 11/24/2018.
 */

namespace App\models\admin_settings;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;


class AdminSettingsModel extends Model
{
    /**
     * get parent menu
     */
    public static function getParentMenu(){
        $data=DB::table("menu_masters")
            ->selectRaw("menu_name,menu_id,parent_menu_id")
            ->where("menu_status","=",1)
            ->where("parent_menu_id","=",0)
            ->where("is_deleted","=",0)
            ->get();

        return $data;
    }

    /**
     * get menu list
     */
    public static function fetchMenuList(Request $request){
        
        $limit = ($request->get('pageSize')) ? $request->get('pageSize') : 5;
        $offset = $limit * (($request->get('page')) ? $request->get('page') : 0);
        $filter = ($request->get('filter')) ? $request->get('filter') : [];
        $sort = ($request->get('sort')) ? $request->get('sort') : [];
        
        $query=DB::table("menu_masters");
        $query->selectRaw("menu_name,menu_id,parent_menu_id,menu_status,menu_url,created_by,created_at");
        $query->where("is_deleted","=",0);
        
        // applying filter 
        if(count($filter) > 0){

            foreach ($filter as $key => $value) {
               $filtered = json_decode($value);
                $searchValue =  strtolower(htmlspecialchars(stripcslashes(trim($filtered->value))));
                $id = " lower(".$filtered->id.") ";
                $where  = "".$id ." like ('%$searchValue%') " ;
                if($key == 0){
                    $query->whereRaw($where);
                }else{
                    $query->orWhere($where); 
                }
            }
        }

        // applying sorting
        if(count($sort) > 0){
            foreach ($sort as $key => $value) {
                $sorted = json_decode($value);
                $query->orderBy($sorted->id,($sorted->desc)?'desc':'asc');
            }
        }
       
        
        $count = $query->count();
        $query->offset($offset);
        $query->limit($limit);
    
        // echo $query->toSql();

        $data= $query->get();
        return array('count'=>$count,'data'=>$data);
    }

    /**
     *   create new menu
     */
    public static function createNewMenu(Request $request){
        $menu = array(
            'parent_menu_id'    => ($request->post('parent_menu')) ? $request->post('parent_menu') : 0,
            'menu_name'         => $request->post('menu_name'),
            'menu_url'          => ($request->post('menu_url')) ? $request->post('menu_url') : '' ,
            'menu_icons'        => ($request->post('menu_icons')) ? $request->post('menu_icons') : '',
            'menu_attribute'    => ($request->post('menu_attribute')) ? $request->post('menu_attribute') : '',
            'menu_attribute'    => ($request->post('menu_attribute')) ? $request->post('menu_attribute') : '',
            'is_menu_hidden'    => ($request->post('is_menu_hidden')) ? $request->post('is_menu_hidden') : 0,
            'menu_status'       => 0,
            'created_by'        => ($request->post('user_code') ) ? $request->post('user_code')  : '',
            'created_at'        => date('Y-m-d H:i:s'),
            'updated_by'        => ''
        );

        DB::beginTransaction();
        try {
            DB::table('menu_masters')
                ->insert($menu);
            DB::commit();
            return true ;
        } catch (\Exception $e) {
            // echo $e->getMessage();die;
            DB::rollback();
            return false;
        }
    }

    /**
     * change menu status
     */
    public static function updateMenuStatus(Request $request)
    {
        
        DB::beginTransaction();
        try {
            DB::table('menu_masters')
                 ->where('menu_id',$request->input('id'))
                ->update([
                    'menu_status'=>(int)($request->input('status') == 0)  ? 1 : 0,
                    'updated_at'        => date('Y-m-d H:i:s'),
                    'updated_by'        => ($request->post('user_code') ) ? $request->post('user_code')  : ''
                ]);
            DB::commit();
            return true ;
        } catch (\Exception $e) {
            // echo $e->getMessage();die;
            DB::rollback();
            return false;
        }
    }

    public static function getMenuDetails(Request $request)
    {
        $data=DB::table("menu_masters")
            ->selectRaw("menu_name,menu_id,parent_menu_id,menu_status,menu_url,menu_icons,created_by,created_at,menu_attribute")
            ->where("menu_id",$request->get('id'))
            ->get();
        return $data;
    }

    public static  function UpdateMenu(Request $request){
        $menu = array(
            'parent_menu_id'    => ($request->post('parent_menu')) ? $request->post('parent_menu') : 0,
            'menu_name'         => $request->post('menu_name'),
            'menu_url'          => ($request->post('menu_url')) ? $request->post('menu_url') : '' ,
            'menu_icons'        => ($request->post('menu_icons')) ? $request->post('menu_icons') : '',
            'menu_attribute'    => ($request->post('menu_attribute')) ? $request->post('menu_attribute') : '',
            'menu_attribute'    => ($request->post('menu_attribute')) ? $request->post('menu_attribute') : '',
            'is_menu_hidden'    => ($request->post('is_menu_hidden')) ? $request->post('is_menu_hidden') : 0,
            'updated_at'        => date('Y-m-d H:i:s'),
            'updated_by'        => ($request->post('user_code') ) ? $request->post('user_code')  : ''
        );

        DB::beginTransaction();
        try {
            DB::table('menu_masters')
                ->where('menu_id',$request->post('menu_id'))
                ->update($menu);
            DB::commit();
            return true ;
        } catch (\Exception $e) {
            // echo $e->getMessage();die;
            DB::rollback();
            return false;
        }
    }

     
    // get menu masters
    public static  function getMenuMasters(){
        $data=DB::table("menu_masters")
            ->selectRaw("menu_name,menu_id,parent_menu_id")
            ->where("menu_status",1)
            ->where("is_deleted",0)
            ->get();

        $master_menu = array();    
        if(count($data) > 0){
            $prepare_data = array();
            foreach ($data as $key => $value) {
                $prepare_data[$value->parent_menu_id][] = $value;
            }

            foreach ($prepare_data[0] as $key => $value) {
                $master_menu[$key]['value'] = $value->menu_id;
                $master_menu[$key]['label'] = $value->menu_name;
                if(isset($prepare_data[$value->menu_id])){
                    foreach ($prepare_data[$value->menu_id] as $keyy => $valuee) {
                        $master_menu[$key]['children'][] = array(
                            'value' => $valuee->menu_id,
                            'label' => $valuee->menu_name    
                        );  
                    }
                }
            }
        }  

        return $master_menu;
    }

    // get ou details 
    public static function getOuDetails(Request $request){
        $ou_data = array();
        // sub company in ou
        $company=DB::table("sub_company_masters")
            ->selectRaw("sub_company_id,sub_company_name,sub_company_code")
            ->where("company_status",1)
            ->where("is_deleted",0) 
            ->get();

        if(count($company) > 0){
            $company_array = array();
            $company_array['name']= 'Company';
            $company_array['flag'] = '0';
            foreach ($company as $key => $value) {
                $company_array['children'][] = array(
                    'id' => $value->sub_company_id,
                    'name' => $value->sub_company_name,
                    'flag' => '0'
                );
            }
            array_push($ou_data,$company_array);
        } 
        
        // process in ou
        $process=DB::table("process_masters")
            ->selectRaw("process_id,process_name,process_code")
            ->where("process_status",1)
            ->where("is_deleted",0) 
            ->get();

        if(count($process) > 0){
            $process_array = array();
            $process_array['name']= 'Process';
            $process_array['flag'] = '0';
            foreach ($process as $key => $value) {
                $process_array['children'][] = array(
                    'id' => $value->process_id,
                    'name' => $value->process_name,
                    'flag' => '0'
                );
            }
            array_push($ou_data,$process_array);
        } 
         
        // cost in ou
        $cost=DB::table("cost_center_masters")
            ->selectRaw("cost_center_id,cost_center_name,cost_center_code")
            ->where("cost_center_status",1)
            ->where("is_deleted",0) 
            ->get();

        if(count($cost) > 0){
            $cost_array = array();
            $cost_array['name']= 'Cost Centers';
            $cost_array['flag'] = '0';
            foreach ($cost as $key => $value) {
                $cost_array['children'][] = array(
                    'id' => $value->cost_center_id,
                    'name' => $value->cost_center_name,
                    'flag' => '0'
                );
            }
            array_push($ou_data,$cost_array);
        } 
        return $ou_data;   
    }
 

}
