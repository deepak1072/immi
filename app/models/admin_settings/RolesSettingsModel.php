<?php
/**
 * Created by Sandeep Maurya on 12/12/2018.
 */

namespace App\models\admin_settings;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;


class RolesSettingsModel extends Model
{

	/**
	 * check for availability
	 */
	public static function CheckRolesAvailability(Request $request){
	  
        $data=DB::table("role_masters")
            ->selectRaw("role_id,role_name") 
            ->where("role_name",$request->post('role_name'))
            ->where("is_deleted","=",0)
            ->get();

        if(count($data) > 0){
        	return false;
        }   else{
        	return true ;
        } 
	}

	/**
	 * create new role 
	 */
	public static function AddRoles(Request $request){

		$role_menu_access = array(
			'checked'=>$request->post('checked'),
			'expanded' =>$request->post('expanded'),
			'all' => array_merge($request->post('checked'),$request->post('expanded'))
		);
		$role_data_access = array(
			'sandeep000',
			'sandy'
		);

		$role = array(
            'super_role_id'    => ($request->post('role_master_id')) ? $request->post('super_role_id') : 1, 
            'role_name'         => $request->post('role_name'), 
            'role_menu_access'        => json_encode($role_menu_access),
            'role_data_access'    => json_encode($role_data_access) ,
            'created_by'    => ($request->post('user_code')) ? $request->post('user_code') : '' ,
            'created_at' => date('Y-m-d H:i:s')
        ); 
        DB::beginTransaction();
        try {
            DB::table('role_masters')
                ->insert($role);
            DB::commit();
            return true ;
        } catch (\Exception $e) {
            
            DB::rollback();
            return false;
        }
	}

	public static function UpdateRoles(Request $request){
		$role_menu_access = array(
			'checked'=>$request->post('checked'),
			'expanded' =>$request->post('expanded'),
			'all' => array_merge($request->post('checked'),$request->post('expanded'))
		);
		$role_data_access = array(
			'sandeep000',
			'sandy'
		);
 

		$role = array(
           
            'role_name'         => $request->post('role_name'), 
            'role_menu_access'        => json_encode($role_menu_access),
            'role_data_access'    => json_encode($role_data_access) ,
            'updated_by'	=> ($request->post('user_code')) ? $request->post('user_code') : '',
            'updated_at'	=>	date('Y-m-d H:i:s')
        );



        DB::beginTransaction();
        try {
            DB::table('role_masters')
            	->where('role_id',$request->post('role_id'))
                ->update($role);
            DB::commit();
            return true ;
        } catch (\Exception $e) {
            
            DB::rollback();
            return false;
        }
	}


	/**
     * get roles list
     */
    public static function fetchRolesList(Request $request){
        
        $limit = ($request->get('pageSize')) ? $request->get('pageSize') : 5;
        $offset = $limit * (($request->get('page')) ? $request->get('page') : 0);
        $filter = ($request->get('filter')) ? $request->get('filter') : [];
        $sort = ($request->get('sort')) ? $request->get('sort') : [];
        
        $query=DB::table("role_masters");
        $query->selectRaw("role_name,role_id,created_by,created_at,role_status,super_role_id");
        $query->where("is_deleted","=",0);
        
        // applying filter 
        if(count($filter) > 0){

            foreach ($filter as $key => $value) {
               $filtered = json_decode($value);
                $searchValue =  strtolower(htmlspecialchars(stripcslashes(trim($filtered->value))));
                $id = " lower(".$filtered->id.") ";
                $where  = "".$id ." like ('%$searchValue%') " ;
                if($key == 0){
                    $query->whereRaw($where);
                }else{
                    $query->orWhere($where); 
                }
            }
        }

        // applying sorting
        if(count($sort) > 0){
            foreach ($sort as $key => $value) {
                $sorted = json_decode($value);
                $query->orderBy($sorted->id,($sorted->desc)?'desc':'asc');
            }
        }
       
        
        $count = $query->count();
        $query->offset($offset);
        $query->limit($limit);
    
        // echo $query->toSql();

        $data= $query->get();
        return array('count'=>$count,'data'=>$data);
    }

    /**
     * update role status 
     */
    public static function UpdateRoleStatus(Request $request){
    	DB::beginTransaction();
        try {
            DB::table('role_masters')
                 ->where('role_id',$request->input('id'))
                ->update([
                    'role_status'=>(int)($request->input('status') == 0)  ? 1 : 0 ,
                    'updated_by' => ($request->post('user_code')) ? $request->post('user_code') : '' ,
                    'updated_at' => date('Y-m-d H:i:s')
                ]);
            DB::commit();
            return true ;
        } catch (\Exception $e) {
            
            DB::rollback();
            return false;
        }
    }

    /**
     * get role details 
     */
    public static function GetRoleDetails(Request $request){
     
        $data=DB::table("role_masters")
            ->selectRaw("role_name,role_id,super_role_id,role_status,created_by,created_at,role_menu_access,role_data_access")
            ->where("role_id",$request->get('id'))
            ->get();
        return $data;
    
    }

    /**
     * get assigned roles list 
     */
    public static function getAssignedRolesList(Request $request){
        $data=DB::table("user_roles as ur")
            ->join("role_masters as rm","rm.role_id","=","ur.role_masters_id")
            ->selectRaw("role_name,user_role_id,is_active,user_code,ur.created_at")
            ->where("ur.is_deleted","0")
            ->get();
        return $data;
    }

    /**
     * available user's and role's list
     */
    public static function getUsersRolesList(Request $request){
        $data=DB::table("user_roles as ur")
            ->join("role_masters as rm","rm.role_id","=","ur.role_masters_id")
            ->selectRaw("role_name,user_role_id,role_masters_id,is_active,user_code,ur.created_at")
            ->where("ur.is_deleted","0")
            ->get();
        return $data;
    }

     /**
     * update assigned role status 
     */
    public static function AssignedRolesStatusUpdate(Request $request){
        DB::beginTransaction();
        try {
            DB::table('user_roles')
                 ->where('user_role_id',$request->input('id'))
                ->update([
                    'is_active'=>(int)($request->input('status') == 0)  ? 1 : 0 ,
                    'updated_by' => ($request->post('user_code')) ? $request->post('user_code') : '' ,
                    'updated_at' => date('Y-m-d H:i:s')
                ]);
            DB::commit();
            return true ;
        } catch (\Exception $e) {
            
            DB::rollback();
            return false;
        }
    }


}