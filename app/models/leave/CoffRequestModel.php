<?php
namespace App\models\leave;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class CoffRequestModel extends Model
{
    public function create($leave){ 
        DB::beginTransaction();
        try {
            $id = DB::table('comp_off') 
                ->insertGetId($leave);
            DB::commit();
            
            return $id ;
        } catch (\Exception $e) { 
            DB::rollback();
            // echo $e->getMessage(); 
            return false;
        }
    }

    public function create_levels($levels){ 
        DB::beginTransaction();
        try {
            DB::table('comp_off_transactions') 
                ->insert($levels);
            DB::commit();
            return true  ;
        } catch (\Exception $e) {
            DB::rollback(); 
            return false;
        }
    }

    // public function update(array $mark,int $id){ 
    //     DB::beginTransaction();
    //     try {
    //         $id = DB::where('leave_id',$id)
    //             ->update('leave',$leave);
    //         DB::commit();
    //         return true ;
    //     } catch (\Exception $e) { 
    //         DB::rollback(); 
    //         return false;
    //     }
    // }
    
    public function isDuplicate($mark){
        $data=DB::table("comp_off")
            ->selectRaw("user_id,status,work_date")
            ->where("user_id",$mark['user_id'])
            ->whereDate('work_date',$mark['work_date']) 
            ->whereIn("status",[1,2,5,7,8])
            ->get();
        if(count($data) > 0){
            return true;
        }else{
            return false;
        }
    }
}