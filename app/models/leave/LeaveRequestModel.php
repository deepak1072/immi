<?php
namespace App\models\leave;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class LeaveRequestModel extends Model
{
    public function create($leave){ 
        DB::beginTransaction();
        try {
            $id = DB::table('leave') 
                ->insertGetId($leave);
            DB::commit();
            
            return $id ;
        } catch (\Exception $e) { 
            DB::rollback();
            // echo $e->getMessage(); 
            return false;
        }
    }

    public function create_levels($levels){ 
        DB::beginTransaction();
        try {
            DB::table('leave_transactions') 
                ->insert($levels);
            DB::commit();
            return true  ;
        } catch (\Exception $e) {  
            DB::rollback(); 
            return false;
        }
    }

    // public function update(array $leave,int $id){ 
    //     DB::beginTransaction();
    //     try {
    //         $id = DB::where('leave_id',$id)
    //             ->update('leave',$leave);
    //         DB::commit();
    //         return true ;
    //     } catch (\Exception $e) { 
    //         DB::rollback(); 
    //         return false;
    //     }
    // }

}