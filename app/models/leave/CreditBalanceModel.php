<?php
/**
 * Created by Sandeep Maurya on 15th dec 2018.
 */

namespace App\models\leave;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;


class CreditBalanceModel extends Model
{
    // get opening balance 
    public function OpeningBalance($user_id, $leave_type , $start_date,$end_date,$effective_date){
		$data=DB::table("leave_balance")
            ->selectRaw("sum(balance) as balance")
            ->whereBetween("effective_date",[$start_date,$effective_date])
            ->where("user_id",$user_id)
            ->where("leave_type",$leave_type)
            ->where("lb_type",'Opening')
            ->first(); 
        return (empty($data->balance)) ? 0 : $data->balance;  
    }
    // get adjustment
    public function Adjustment($user_id, $leave_type , $start_date,$end_date,$effective_date){
		$data=DB::table("leave_balance")
            ->selectRaw("sum(balance) as balance")
            ->whereBetween("effective_date",[$start_date,$effective_date])
            ->where("user_id",$user_id)
            ->where("leave_type",$leave_type)
            ->where("lb_type",'Adjustment')
            ->first(); 
        return (empty($data->balance)) ? 0 : $data->balance;  
    }

    // get credit
    public function CreditBalance($user_id, $leave_type , $start_date,$end_date,$effective_date){
		$data=DB::table("leave_balance")
            ->selectRaw("sum(balance) as balance")
            ->whereBetween("effective_date",[$start_date,$effective_date])
            ->where("user_id",$user_id)
            ->where("leave_type",$leave_type)
            ->where("lb_type",'Credit')
            ->first();
        return (empty($data->balance)) ? 0 : $data->balance;  
    }

    // get compoff credit 
    public function CreditCompoffBalance($user_id, $rule , $start_date,$end_date,$effective_date){
         
        $query=DB::table("comp_off")
            ->selectRaw("sum(credit_days) as balance")
            ->where("user_id",$user_id)
            ->whereIn("status",[2,5,7]) ;
        if(0 == $rule['compoff_credit_lapse_rules']){
            $query->whereBetween('work_date',[$start_date,$end_date]);
        }else{
            $col = ($rule['compoff_credit_lapse_basis'] == 0) ? 'work_date' : 'approved_at' ;
            $start_date = date('Y-m-d',strtotime($effective_date.' -'.$rule['compoff_credit_lapse_days'].' days'));
            $query->whereBetween($col,[$start_date,$effective_date]);
        } 
        $data = $query->first(); 
        return (empty($data->balance)) ? 0 : $data->balance;  
    }

    // get debit balance
    public function DebitBalance($user_id, $leave_type , $start_date,$end_date,$effective_date){
		$data=DB::table("leave")
            ->selectRaw("sum(leave_days) as debit")
            ->whereBetween("effective_date",[$start_date,$effective_date])
            ->where("user_id",$user_id)
            ->where("leave_type",$leave_type)
            ->whereIn("status",[1,2,5,7])
            ->first(); 
        return (empty($data->debit)) ? 0 : $data->debit;  
    }

    // get compoff debit balance
    public function CoffDebitBalance($user_id, $rule , $start_date,$end_date,$effective_date){
         
		$query=DB::table("comp_off")
            ->selectRaw("sum(credit_days) as balance,sum(debit_days) as debit")
            ->where("user_id",$user_id)
            ->whereIn("status",[2,5,7]) ;
        if(0 == $rule['compoff_credit_lapse_rules']){
            $query->whereBetween('work_date',[$start_date,$end_date]);
        }else{
            $col = ($rule['compoff_credit_lapse_basis'] == 0) ? 'work_date' : 'approved_at' ;
            $start_date = date('Y-m-d',strtotime($effective_date.' -'.$rule['compoff_credit_lapse_days'].' days'));
            $query->whereBetween($col,[$start_date,$effective_date]);
        } 
        $data = $query->first(); 
        return (empty($data->balance)) ? 0 : ($data->balance - $data->debit); 
    }


	// check for credit balance  
	public function  checkCreditBalance($balance){
		$data=DB::table("leave_balance")
            ->selectRaw("`lb_id`, `lb_key`, `user_id`, `leave_type`, `balance`, `effective_date`")
            ->where("user_id",$balance['user_id'])
            ->where("effective_date",$balance['effective_date'])
            ->where("leave_type",$balance['leave_type'])
            ->first(); 
        return $data; 		  
    }
    
    // update credited balance 
    public function updateCreditBalance($leave_balance,$check_credit){
        DB::beginTransaction();
        try {
            DB::table('leave_balance')
                ->where('lb_id',$check_credit->lb_id)
                ->update([
                    'balance'=> $leave_balance['balance'],
                    'lb_type'=> $leave_balance['lb_type'],
                    'updated_at'=>$leave_balance['updated_at']
                ]);
            DB::commit();
            return true ;
        } catch (\Exception $e) { 
            DB::rollback();
            return false;
        }
    }
     
    // credit balance 
    public function addCreditBalance($leave_balance){
        DB::beginTransaction();
        try {
            DB::table('leave_balance') 
                ->insert($leave_balance);
            DB::commit();
            return true ;
        } catch (\Exception $e) { 
            DB::rollback(); 
            return false;
        }
    } 

    public function UpdateBalanceReports($leave_balance){
        
        $data = array_map(function($balance){
            $yetto = (isset($balance['yettotaccrue'])) ? $balance['yettotaccrue'] : 0 ;
            return  array(
                'lv_id' => $balance['lv_id'],
                'user_id' => $balance['user_id'],
                'opening'=> $balance['opening'],
                'adjustment'=>$balance['adjustment'],
                'credit'=>$balance['credit'],
                'debit'=> $balance['debit'],
                'balance'=>$balance['balance'],
                'yettotaccrue'=>$yetto ,
                'yearendbalance'=>$balance['yearendbalance'],
                'yearendlapsed'=>$balance['yearendlapsed']
            );
        },$leave_balance);
         
        DB::beginTransaction();
        try {
            DB::table('leave_reports') 
                ->insert($data);
            DB::commit();
            return true ;
        } catch (\Exception $e) { 
            DB::rollback(); 
            return false;
        }
    }

}