<?php
namespace App\models\leave;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class LeaveApprovalModel extends Model
{
    public static function LeaveWorkflow($events = 100)
    { 
		$data=DB::table("workflows")
            ->selectRaw(" `wf_id`, `workflow_name`, `workflow_code`, `module_name`, `wf_events`, `applicable_ou`, `flow_status`, `flow_type` ")
            ->Where("module_name",'LV')
            ->where(function($query)use($events ){
                $query->where("wf_events",$events)
                ->orWhere("wf_events",100);
            }) 
            ->where("flow_status",1)
            ->orderBy('wf_id','desc')
            ->get(); 
        return $data ;
    }

    public static function LeaveTransactionData($options)
    { 
        $data=DB::table("leave as l")
            ->selectRaw(" l.leave_id,lt.ltrx_id, approver_role, wf_level, level, approval_by, approved_by, l.status as final_status, flag , exp_approval_date, approver_remarks, trx_date, trx_code, leave_type, created_by, user_id, from_date, to_date, from_period, to_period, leave_days, lt.status, reason, workflow_id, plan_unplan, pup_reason, user_remarks, attachments, declarations, pre_yr_leave, user_last_update, effective_date ")
            ->join('leave_transactions as lt','l.leave_id','=','lt.leave_id')
            ->where("l.leave_id",$options['leave_id'])
            ->where("lt.ltrx_id",$options['ltrx_id']) 
            ->first(); 
        return $data ;
    }

    public static function UpdateLevel($options , $trx_id){
        DB::beginTransaction();
        try {
            DB::table('leave_transactions') 
                ->where('ltrx_id',$trx_id)
                ->update($options);
            DB::commit();
            return true  ;
        } catch (\Exception $e) {  
            DB::rollback(); 
            return false;
        }
    }
    public static function NextLevel($leave_id , $level){
        $level = $level + 1  ;
        $data=DB::table("leave_transactions")
            ->selectRaw("leave_id,ltrx_id, approver_role, wf_level, level, approval_by, approved_by, status, flag  ") 
            ->where("leave_id",$leave_id)
            ->where("level",$level) 
            ->first(); 
        return $data ;
    }

    public static function NotifyNextLevel($id){
        DB::beginTransaction();
        try {
            DB::table('leave_transactions') 
                ->where('ltrx_id',$id)
                ->update(['flag'=> 1 ,'active'=>1]);
            DB::commit();
            return true  ;
        } catch (\Exception $e) {  
            DB::rollback(); 
            return false;
        }
    }

    public static function UpdateLevelActiveness($id){
        DB::beginTransaction();
        try {
            DB::table('leave_transactions') 
                ->where('ltrx_id',$id)
                ->update(['active'=> 0]);
            DB::commit();
            return true  ;
        } catch (\Exception $e) {  
            DB::rollback(); 
            return false;
        }
    }

    public static function FinalStatus($leave_id , $options ){
        DB::beginTransaction();
        try {
            DB::table('leave') 
                ->where('leave_id',$leave_id)
                ->update($options);
            DB::commit();
            return true  ;
        } catch (\Exception $e) {  
            DB::rollback(); 
            return false;
        }
    }
}

?>