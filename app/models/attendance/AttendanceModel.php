<?php
/**
 * Created by Sandeep Maurya on 15th dec 2018.
 */

namespace App\models\attendance;  
use Illuminate\Support\Facades\DB;
use Illuminate\Database\Eloquent\Model;


class AttendanceModel extends Model
{
    public function getShiftOfDay($user_id, $date){
		$data=DB::table("attendance as att")
            ->selectRaw("sm.shift_id, sm.shift_name, shift_code, shift_start, shift_end, shift_start_mandatory, shift_end_mandatory, general_shift, mandate_time_full_day, mandate_time_half_day, round_clock_shift, mark_out_mandate, swipe_allowed_before_shift, swipe_allowed_after_shift, is_break_shift, break_start, break_end, late_coming_allowed, late_coming_frequency, late_coming_grace_period, early_going_allowed, early_going_frequency, early_going_grace_period,shift_status, _id, att.user_id, actual_shift_start, actual_shift_end, final_status, pay_status, final_lengend, is_weekoff, is_holiday, holiday_id, is_leave, is_od, is_mark")
            ->join('shift_masters as sm','sm.shift_id','=','att.shift_id')
            ->whereDate("att_date",$date)
            ->where("att.user_id",$user_id)
            ->first(); 
        return $data;
    }

    public function getRosterOfDate($user_id, $date){
        $data = DB::table('roster_schema')
                ->selectRaw('id, roster_id, user_id, shift_id, roster_date')
                ->where('user_id',$user_id)
                ->whereDate('roster_date',$date)
                ->first();
        return $data;
    }

    public function getShiftDetails($shift_id){
        $data=DB::table("shift_masters")
            ->selectRaw("shift_id, shift_name, shift_code, shift_start, shift_end, shift_start_mandatory, shift_end_mandatory, general_shift, mandate_time_full_day, mandate_time_half_day, round_clock_shift, mark_out_mandate, swipe_allowed_before_shift, swipe_allowed_after_shift, is_break_shift, break_start, break_end, late_coming_allowed, late_coming_frequency, late_coming_grace_period, early_going_allowed, early_going_frequency, early_going_grace_period,shift_status")
            ->where("shift_id",$shift_id)
            ->first(); 
        return $data;
    }

    public function updateAttendance($attendance,$id){
        DB::beginTransaction();
        try {
            DB::table('attendance')
                ->where('_id',$id)
                ->update($attendance);
            DB::commit();
            return true ;
        } catch (\Exception $e) {
            DB::rollback();
            return false;
        }
    }

    public function insertAttendance($attendance){
        DB::beginTransaction();
        try {
            DB::table('attendance')
                ->insert($attendance);
            DB::commit();
            return true ;
        } catch (\Exception $e) {
            DB::rollback();
            return false;
        }
    }

    public function getPunchesOfDay($user,$date){
        $data=DB::table("punches")
            ->selectRaw("id, user_id, punch_date, punch_time, punch_type")
            ->where("user_id",$user)
            ->whereDate('punch_date',$date)
            ->orderBy('punch_time','asc')
            ->get(); 
        return $data;
    }

    public function getLeaveOfDay($user,$date){
        $data=DB::table("leave as l")
            ->selectRaw("l.leave_id,leave_type, user_id, from_date, to_date, from_period, to_period, leave_days, l.status,lt.leave_name, lt.leave_desc, lt.leave_short_name, lt.leave_master_id")
            ->join('leave_types as lt','l.leave_type','=','lt.leave_id')
            ->where("user_id",$user)
            ->whereNotIn('l.status',[3,4,6])
            ->where(function($query) use($date){
                $query->whereDate('from_date','<=',$date);
                $query->whereDate('to_date','>=',$date);
            })
            ->get();
        return $data;
    }
    public function getMarkOfDay($user,$date){
        $data=DB::table("mark_attendance")
            ->selectRaw("id as mark_id,mark_date,mark_in,mark_out,status")
            ->where("user_id",$user)
            ->whereNotIn('status',[3,4,6])
            ->whereDate('mark_date','=',$date)
            ->orderBy('id','asc')
            ->get(); 
        return $data;
    }
    public function getOdOfDay($user,$date){
        $data=DB::table("out_on_duty")
            ->selectRaw("id as od_id,od_date,od_type,from_type,to_type,status,od_intime,od_outtime")
            ->where("user_id",$user)
            ->whereNotIn('status',[3,4,6])
            ->whereDate('od_date','=',$date)
            ->get(); 
        return $data;
    }

    public function getCompanyEffectiveDate($code){
        $data=DB::table("company_masters")
            ->selectRaw("is_live,live_date")
            ->where("code",$code)
            ->where("is_live",1)
            ->first(); 
        return $data;
    }
}