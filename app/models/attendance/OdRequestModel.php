<?php
namespace App\models\attendance;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class OdRequestModel extends Model
{
    public function create($leave){ 
        DB::beginTransaction();
        try {
            $id = DB::table('out_on_duty') 
                ->insertGetId($leave);
            DB::commit();
            return $id ;
        } catch (\Exception $e) { 
            DB::rollback();
            return false;
        }
    }

    public function create_levels($levels){ 
        DB::beginTransaction();
        try {
            DB::table('od_transactions') 
                ->insert($levels);
            DB::commit();
            return true  ;
        } catch (\Exception $e) {
            DB::rollback(); 
            echo $e->getMessage();
            return false;
        }
    }

    // public function update(array $mark,int $id){ 
    //     DB::beginTransaction();
    //     try {
    //         $id = DB::where('leave_id',$id)
    //             ->update('leave',$leave);
    //         DB::commit();
    //         return true ;
    //     } catch (\Exception $e) { 
    //         DB::rollback(); 
    //         return false;
    //     }
    // }
    
    public function isDuplicate($od){
        $data=DB::table("out_on_duty")
            ->selectRaw("user_id,status,od_date")
            ->where("user_id",$od['user_id'])
            ->whereDate('od_date',$od['od_date']) 
            ->whereIn("status",[1,2,5,7,8])
            ->get();
        if(count($data) > 0){
            return true;
        }else{
            return false;
        }
    }
}