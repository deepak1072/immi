<?php
namespace App\models\attendance;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class OdApprovalModel extends Model
{
    public static function Workflow($events = 100)
    { 
		$data=DB::table("workflows")
            ->selectRaw(" `wf_id`, `workflow_name`, `workflow_code`, `module_name`, `wf_events`, `applicable_ou`, `flow_status`, `flow_type` ")
            ->where("module_name",'ATT')
            ->where(function($query)use($events ){
                $query->where("wf_events",$events)
                ->orWhere("wf_events",100);
            }) 
            ->where("flow_status",1)
            ->orderBy('wf_id','desc')
            ->get(); 
        return $data ;
    }

    public static function TransactionData($options)
    { 
        $data=DB::table("out_on_duty as od")
            ->selectRaw(" od.id as od_id,odt.id as trx_id, approver_role, wf_level, level, approval_by, approved_by, od.status as final_status, flag , expected_approval, approver_remarks, trx_date, trx_code, created_by, user_id,   od_date ,odt.status, reason, workflow_id, user_remarks ")
            ->join('od_transactions as odt','od.id','=','odt.od_id')
            ->where("od.id",$options['od_id'])
            ->where("odt.id",$options['trx_id']) 
            ->first(); 
        return $data ;
    }
    public static function UpdateLevel($options , $trx_id){
        DB::beginTransaction();
        try {
            DB::table('od_transactions') 
                ->where('id',$trx_id)
                ->update($options);
            DB::commit();
            return true  ;
        } catch (\Exception $e) {  
            DB::rollback(); 
            return false;
        }
    }
    public static function NextLevel($mark_id , $level){
        $level = $level + 1  ;
        $data=DB::table("od_transactions")
            ->selectRaw("od_id,id as trx_id, approver_role, wf_level, level, approval_by, approved_by, status, flag  ") 
            ->where("od_id",$mark_id)
            ->where("level",$level) 
            ->first(); 
        return $data ;
    }

    public static function NotifyNextLevel($id){
        DB::beginTransaction();
        try {
            DB::table('od_transactions') 
                ->where('id',$id)
                ->update(['flag'=> 1 ,'active'=>1]);
            DB::commit();
            return true  ;
        } catch (\Exception $e) {  
            DB::rollback(); 
            return false;
        }
    }

    public static function UpdateLevelActiveness($id){
        DB::beginTransaction();
        try {
            DB::table('od_transactions') 
                ->where('id',$id)
                ->update(['active'=> 0]);
            DB::commit();
            return true  ;
        } catch (\Exception $e) {  
            DB::rollback(); 
            return false;
        }
    }

    public static function FinalStatus($mark_id , $options ){
        DB::beginTransaction();
        try {
            DB::table('out_on_duty') 
                ->where('id',$mark_id)
                ->update($options);
            DB::commit();
            return true  ;
        } catch (\Exception $e) {  
            DB::rollback(); 
            return false;
        }
    }

    public static function InitCancellation($od_id){
        $options = array(
            'flag' => 0,
            'active' => 0
        );
        DB::beginTransaction();
        try {
            DB::table('od_transactions') 
                ->where('od_id',$od_id)
                ->update($options);
            DB::commit();

            $data=DB::table("od_transactions")
            ->selectRaw("id as trx_id  ") 
            ->where("od_id",$od_id)
            ->where("level",1) 
            ->first();

            $options = array(
                'flag' => 1,
                'active' => 1
            );
            DB::beginTransaction();
            DB::table('od_transactions') 
                ->where('id',$data->trx_id)
                ->update($options);
            DB::commit();

            return true  ;
        } catch (\Exception $e) {  
            DB::rollback(); 
            return false;
        }
    }
}

?>