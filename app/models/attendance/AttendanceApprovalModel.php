<?php
namespace App\models\attendance;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class AttendanceApprovalModel extends Model
{
    public static function Workflow($events = 100)
    { 
		$data=DB::table("workflows")
            ->selectRaw(" `wf_id`, `workflow_name`, `workflow_code`, `module_name`, `wf_events`, `applicable_ou`, `flow_status`, `flow_type` ")
            ->where("module_name",'ATT')
            ->where(function($query)use($events ){
                $query->where("wf_events",$events)
                ->orWhere("wf_events",100);
            }) 
            ->where("flow_status",1)
            ->orderBy('wf_id','desc')
            ->get(); 
        return $data ;
    }

    public static function TransactionData($options)
    { 
        $data=DB::table("mark_attendance as ma")
            ->selectRaw(" ma.id as mark_id,mt.id as trx_id, approver_role, wf_level, level, approval_by, approved_by, ma.status as final_status, flag , expected_approval, approver_remarks, trx_date, trx_code, created_by, user_id,   mark_date ,mt.status, reason, workflow_id, user_remarks ")
            ->join('mark_transactions as mt','ma.id','=','mt.mark_id')
            ->where("ma.id",$options['mark_id'])
            ->where("mt.id",$options['trx_id']) 
            ->first(); 
        return $data ;
    }
    public static function UpdateLevel($options , $trx_id){
        DB::beginTransaction();
        try {
            DB::table('mark_transactions') 
                ->where('id',$trx_id)
                ->update($options);
            DB::commit();
            return true  ;
        } catch (\Exception $e) {  
            DB::rollback(); 
            return false;
        }
    }
    public static function NextLevel($mark_id , $level){
        $level = $level + 1  ;
        $data=DB::table("mark_transactions")
            ->selectRaw("mark_id,id as trx_id, approver_role, wf_level, level, approval_by, approved_by, status, flag  ") 
            ->where("mark_id",$mark_id)
            ->where("level",$level) 
            ->first(); 
        return $data ;
    }

    public static function NotifyNextLevel($id){
        DB::beginTransaction();
        try {
            DB::table('mark_transactions') 
                ->where('id',$id)
                ->update(['flag'=> 1 ,'active'=>1]);
            DB::commit();
            return true  ;
        } catch (\Exception $e) {  
            DB::rollback(); 
            return false;
        }
    }

    public static function UpdateLevelActiveness($id){
        DB::beginTransaction();
        try {
            DB::table('mark_transactions') 
                ->where('id',$id)
                ->update(['active'=> 0]);
            DB::commit();
            return true  ;
        } catch (\Exception $e) {  
            DB::rollback(); 
            return false;
        }
    }

    public static function FinalStatus($mark_id , $options ){
        DB::beginTransaction();
        try {
            DB::table('mark_attendance') 
                ->where('id',$mark_id)
                ->update($options);
            DB::commit();
            return true  ;
        } catch (\Exception $e) {  
            DB::rollback(); 
            return false;
        }
    }
}

?>