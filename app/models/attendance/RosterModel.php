<?php
/**
 * Created by Sandeep Maurya on 15th dec 2018.
 */
namespace App\models\attendance;  
use Illuminate\Support\Facades\DB;
use Illuminate\Database\Eloquent\Model;
class RosterModel extends Model
{
    // get roster  
    public function getRosterDetails($roster){
		$data=DB::table("roster")
            ->selectRaw("id,rp_id, rp_name, is_fixed , day, first_week, second_week, third_week, fourth_week, fifth_week")
            ->join('roster_pattern as rp','rp_id','=','id')
            ->where("rp_id",$roster)
            ->where('status',1)
            ->get();
        return $data;
    }

    // check mapped roster 
    public function isRosterAlreadyMapped($user,$date){
        $data=DB::table("roster_schema")
            ->selectRaw("id, roster_id, user_id, shift_id, roster_date")
            ->whereDate("roster_date",$date)
            ->where("user_id",$user)
            ->first();
        return $data;
    }

    public function updateUserRoster($data,$id){
        DB::beginTransaction();
        try {
            DB::table('roster_schema')
                ->where('id',$id)
                ->update($data);
            DB::commit();
            return true ;
        } catch (\Exception $e) {
            DB::rollback();
            return false;
        }
    }

    public function insertUserRoster($data){
        DB::beginTransaction();
        try {
            DB::table('roster_schema')
                ->insert($data);
            DB::commit();
            return true ;
        } catch (\Exception $e) {
            DB::rollback();
            return false;
        }
    }

    public function assignedRoster($data){
        $data=DB::table("roster_schema as rs")
            ->selectRaw("rs.id, rs.roster_id, rs.user_id, rs.shift_id, rs.roster_date,em.emp_name,em.emp_code,sm.shift_start_mandatory,sm.shift_end_mandatory,sm.shift_name,sm.shift_code,sm.general_shift")
            ->join('emp_master as em','em.user_id','=','rs.user_id')
            ->join('shift_masters as sm','sm.shift_id','=','rs.shift_id')
            ->where("rs.user_id",$data['users'])
            ->whereMonth('rs.roster_date', $data['month'])
            ->whereYear('rs.roster_date', $data['year'])
            ->orderBy('rs.roster_date','ASC')
            ->orderBy('rs.user_id','ASC')
            ->get(); 
        return $data;
    }
}