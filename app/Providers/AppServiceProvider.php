<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use App\libraries\leave\LeaveBalance;
class AppServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
        // $this->app->bind(LeaveBalance::class, function(){
        //     return new LeaveBalance();
        // });
    }
}
