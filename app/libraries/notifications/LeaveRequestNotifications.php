<?php
/**
 * Created by PhpStorm.
 * User: Sandeep Maurya
 * Date: 5/1/2020
 * Time: 11:26 AM
 */
namespace App\libraries\notifications;
 
use GlobalHelpers;  
// use App\models\leave\LeaveNotificationModel;  
use App\libraries\mails\DispatchMails;
use App\libraries\workflows\Workflow;
class LeaveRequestNotifications
{
    private $user ;
    private $leave ;
    private $levels;
    private $rule ;
    private $wf_id ;
    private $wf_level ; 
    private $approver ;
    public function leaveCreate($user, $leave, $levels,$rules){
        $this->user = $user;
        $this->leave = $leave;
        $this->levels = $levels ;
        $this->rule = $rules;
        $this->wf_id = $this->leave['workflow_id'];
        $workflow = new Workflow($this->wf_id);
        $this->wf_level  = $workflow->getLevelInfo(1);

        if($this->wf_level->requ_noti){ 
            $url = secure_url('/');
            $data = array(
                'instance' => 'CreateLeaveRequest',
                'to' => $this->user->oemail_id,
                'cc' => '',
                'bcc' => '',
                'req_code' => $this->user->emp_code,
                'req_name' => $this->user->emp_name, 
                'from_date' =>  date(config('constants.DATE_F'), strtotime($this->leave['from_date'])),
                'to_date' =>  date(config('constants.DATE_F'), strtotime($this->leave['to_date'])),
                'days' => $this->leave['leave_days'],
                'leave' => $this->rule['leave_name'],
                'reason' => $this->leave['reason'],
                'action' => $url 
            );
            $mail = new DispatchMails($data);
        }
        if($this->wf_level->next_level_noti){  
            $approver = GlobalHelpers::EmpConfirmationBasic($this->levels[0]['approval_by']);
            $url = secure_url('/');
            $data = array(
                'instance' => 'SubmittedLeaveRequest',
                'to' => $approver->oemail_id,
                'cc' => '',
                'bcc' => '',
                'req_code' => $this->user->emp_code,
                'req_name' => $this->user->emp_name, 
                'approver_code' => $approver->emp_code,
                'approver_name' => $approver->emp_name,
                'from_date' =>  date(config('constants.DATE_F'), strtotime($leave['from_date'])),
                'to_date' =>  date(config('constants.DATE_F'), strtotime($leave['to_date'])),
                'days' => $leave['leave_days'],
                'leave' => $this->rule['leave_name'],
                'reason' => $leave['reason'],
                'action' => $url 
            );  
            $mail = new DispatchMails($data); 
        }   
    }

    public function leaveUpdate($user, $leave, $levels,$rules){
        $this->user = $user;
        $this->leave = $leave;
        $this->levels = $levels ;
        $this->rule = $rules;
        $this->wf_id = $this->leave['workflow_id'];
        $workflow = new Workflow($this->wf_id);
        $this->wf_level  = $workflow->getLevelInfo(1);
         
        if($this->wf_level->requ_noti){ 
            $url = secure_url('/');
            $data = array(
                'instance' => 'UpdateLeaveRequest',
                'to' => $this->user->oemail_id,
                'cc' => '',
                'bcc' => '',
                'req_code' => $this->user->emp_code,
                'req_name' => $this->user->emp_name, 
                'from_date' =>  date(config('constants.DATE_F'), strtotime($this->leave['from_date'])),
                'to_date' =>  date(config('constants.DATE_F'), strtotime($this->leave['to_date'])),
                'days' => $this->leave['leave_days'],
                'leave' => $this->rule['leave_name'],
                'reason' => $this->leave['reason'],
                'action' => $url 
            );
            $mail = new DispatchMails($data);
        }
        if($this->wf_level->next_level_noti){  

            $approver = GlobalHelpers::EmpConfirmationBasic($this->levels[0]['approval_by']);
            $url = secure_url('/');
            $data = array(
                'instance' => 'UpdatedLeaveRequest',
                'to' => $approver->oemail_id,
                'cc' => '',
                'bcc' => '',
                'req_code' => $this->user->emp_code,
                'req_name' => $this->user->emp_name, 
                'approver_code' => $approver->emp_code,
                'approver_name' => $approver->emp_name,
                'from_date' =>  date(config('constants.DATE_F'), strtotime($leave['from_date'])),
                'to_date' =>  date(config('constants.DATE_F'), strtotime($leave['to_date'])),
                'days' => $leave['leave_days'],
                'leave' => $this->rule['leave_name'],
                'reason' => $leave['reason'],
                'action' => $url 
            );  
            $mail = new DispatchMails($data); 
        } 
    }

    public function FinalNotificationToRequest($user,$trx,$rule_name){
        $this->user = $user ;
        $this->leave = $trx ;
        $url = secure_url('/');
        $data = array(
            'instance' => 'FinalLeaveApproval',
            'to' => $this->user->oemail_id,
            'cc' => '',
            'bcc' => '',
            'req_code' => $this->user->emp_code,
            'req_name' => $this->user->emp_name, 
            'from_date' =>  date(config('constants.DATE_F'), strtotime($this->leave->from_date)),
            'to_date' =>  date(config('constants.DATE_F'), strtotime($this->leave->to_date)),
            'days' => $this->leave->leave_days,
            'leave' => $rule_name,
            'reason' => $this->leave->reason,
            'action' => $url 
        );
        $mail = new DispatchMails($data);
    }

    public function ApproversNotifications($user, $trx, $rule_name, $nextLevel){
        $this->user = $user ;
        $this->leave = $trx ;
        $this->wf_id = $this->leave->workflow_id;
        $workflow = new Workflow($this->wf_id);
        $this->wf_level  = $workflow->getLevelInfo($this->leave->wf_level);
        
        if( $this->wf_level->requ_noti){
            $url = secure_url('/');
            $approver =  GlobalHelpers::EmpConfirmationBasic($this->leave->approved_by);
            $data = array(
                'instance' => 'LeaveApprovedLevel',
                'to' => $this->user->oemail_id,
                'cc' => '',
                'bcc' => '',
                'req_code' => $this->user->emp_code,
                'req_name' => $this->user->emp_name, 
                'approver_code' => $approver->emp_code,
                'approver_name' => $approver->emp_name,
                'from_date' =>  date(config('constants.DATE_F'), strtotime($this->leave->from_date)),
                'to_date' =>  date(config('constants.DATE_F'), strtotime($this->leave->to_date)),
                'days' => $this->leave->leave_days,
                'leave' => $rule_name,
                'reason' => $this->leave->reason,
                'action' => $url 
            );
            $mail = new DispatchMails($data);
        } 
        if($this->wf_level->next_level_noti){
            $url = secure_url('/');
            $approver =  GlobalHelpers::EmpConfirmationBasic($nextLevel->approval_by);
            $data = array(
                'instance' => 'AppliedAtLevel',
                'to' => $this->user->oemail_id,
                'cc' => '',
                'bcc' => '',
                'req_code' => $this->user->emp_code,
                'req_name' => $this->user->emp_name, 
                'approver_code' => $approver->emp_code,
                'approver_name' => $approver->emp_name,
                'from_date' =>  date(config('constants.DATE_F'), strtotime($this->leave->from_date)),
                'to_date' =>  date(config('constants.DATE_F'), strtotime($this->leave->to_date)),
                'days' => $this->leave->leave_days,
                'leave' => $rule_name,
                'reason' => $this->leave->reason,
                'action' => $url 
            );
            $mail = new DispatchMails($data);
        }

    }

    public function NotifyRejectionToRequester($user, $trx, $rule_name)
    {
        $this->user = $user ;
        $this->leave = $trx ;
        $this->wf_id = $this->leave->workflow_id;
        $workflow = new Workflow($this->wf_id);
        $this->wf_level  = $workflow->getLevelInfo($this->leave->wf_level);
        if( $this->wf_level->requ_noti){
            $url = secure_url('/');
            $approver =  GlobalHelpers::EmpConfirmationBasic($this->leave->approval_by);
            $data = array(
                'instance' => 'RejectedRequest',
                'to' => $this->user->oemail_id,
                'cc' => '',
                'bcc' => '',
                'req_code' => $this->user->emp_code,
                'req_name' => $this->user->emp_name, 
                'approver_code' => $approver->emp_code,
                'approver_name' => $approver->emp_name,
                'from_date' =>  date(config('constants.DATE_F'), strtotime($this->leave->from_date)),
                'to_date' =>  date(config('constants.DATE_F'), strtotime($this->leave->to_date)),
                'days' => $this->leave->leave_days,
                'leave' => $rule_name,
                'reason' => $this->leave->reason,
                'action' => $url 
            );
            $mail = new DispatchMails($data);
        }
        // if( $this->wf_level->approver_noti){
        //     $url = secure_url('/');
        //     $approver =  GlobalHelpers::EmpConfirmationBasic($this->leave->approval_by);
        //     $data = array(
        //         'instance' => 'RejectedRequest',
        //         'to' => $this->user->oemail_id,
        //         'cc' => '',
        //         'bcc' => '',
        //         'req_code' => $this->user->emp_code,
        //         'req_name' => $this->user->emp_name, 
        //         'approver_code' => $approver->emp_code,
        //         'approver_name' => $approver->emp_name,
        //         'from_date' =>  date(config('constants.DATE_F'), strtotime($this->leave->from_date)),
        //         'to_date' =>  date(config('constants.DATE_F'), strtotime($this->leave->to_date)),
        //         'days' => $this->leave->leave_days,
        //         'leave' => $rule_name,
        //         'reason' => $this->leave->reason,
        //         'action' => $url 
        //     );
        //     $mail = new DispatchMails($data);
        // }
    }

    public function CancelNotifications($user,$trx){
        $this->user = $user ;
        $this->mark = $trx ;
        $url = secure_url('/');
        $approver =  GlobalHelpers::EmpConfirmationBasic($this->mark->approval_by);
        $data = array(
            'instance' => 'LeaveApprovedLevel',
            'to' => $this->user->oemail_id,
            'cc' => '',
            'bcc' => '',
            'req_code' => $this->user->emp_code,
            'req_name' => $this->user->emp_name, 
            'approver_code' => $approver->emp_code,
            'approver_name' => $approver->emp_name,
            'from_date' =>  date(config('constants.DATE_F'), strtotime($this->mark->from_date)),
            'to_date' =>  date(config('constants.DATE_F'), strtotime($this->mark->to_date)),
            'reason' => $this->mark->reason,
            'action' => $url 
        );
        $mail = new DispatchMails($data);
    }
}