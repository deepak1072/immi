<?php
/** 
 * User: Sandeep Maurya
 * Date: 15/12/2019
 * Time: 8:26 PM
 */
namespace App\libraries\agents;
use GlobalHelpers;   
use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
class AgentsTransactions 
{
    public function Transaction(Request $request)
    { 
        $limit = ($request->get('pageSize')) ? $request->get('pageSize') : 5;
        $offset = $limit * (($request->get('page')) ? $request->get('page') : 0);
        $filter = ($request->get('filter')) ? $request->get('filter') : [];
        $sort = ($request->get('sort')) ? $request->get('sort') : [];
        
        $query=DB::table("emp_master as em");
        $query->join('users as u','u.id','=','em.user_id');
        $query->selectRaw("em_id, user_id, access_code, emp_code, first_name, middle_name, last_name, emp_name, em.oemail_id, mobile_number,em.created_at,u.is_access_allowed");
        if($request->filled('role') && $request->role != 1 ){
            if( $request->role == 2){
                $query->where('user_id',"!=",1);
            }
        }else{  
        } 
        
        // applying filter 
        if(count($filter) > 0){

            foreach ($filter as $key => $value) {
                
               $filtered = json_decode($value);
                $searchValue =  strtolower(htmlspecialchars(stripcslashes(trim($filtered->value))));
                if( $filtered->id != 'is_access_allowed'){
                    if(in_array($filtered->id , ['oemail_id','created_at'])){
                        $id  =  " lower(em.".$filtered->id.") " ;
                    }else{
                        $id  =  " lower(".$filtered->id.") " ;
                    }
                    

                    $where  = "".$id ." like ('%$searchValue%') " ;
                    if($key == 0){
                        $query->whereRaw($where);
                    }else{
                        $query->orWhere($where); 
                    }
                }
                
            }
        }

        // applying sorting
        if(count($sort) > 0){
            foreach ($sort as $key => $value) {
                $sorted = json_decode($value);
                $query->orderBy($sorted->id,($sorted->desc)?'desc':'asc');
            }
        } 
        $count = $query->count();
        $query->offset($offset);
        $query->limit($limit);
        $data= $query->get();
        // echo $query->toSql();
        return array('count'=>$count,'data'=>$data);
    }
    
    public function _request(Request $request){
        
        $res = array(
            'result'=>false,
            'message'=>'',
            'type' => 'warn',
            'status' => 200
        );
        $date_str = strtotime($request->post('date'));
         
        $this->od['created_at'] = date(config('constants.DATE_TIME'));
        $this->od['updated_at'] = date(config('constants.DATE_TIME')); 
        $this->od['created_by'] = $request->post('user_id');
        $this->od['emp_name'] = $request->post('emp_name');
        $this->od['mobile_number'] = $request->post('mobile_number');
        $this->od['oemail_id'] = $request->post('oemail_id'); 
        $this->od['access_code'] = $request->post('oemail_id'); 
        $this->od['emp_code'] = $request->post('mobile_number'); 
        $this->od['first_name'] = "F"; 
        $this->od['middle_name'] = "M"; 
        $this->od['last_name'] = "L"; 
        $this->od['profile_image'] = ''; 
        $this->od['gender'] = 1 ;  
        $this->od['confirm_req'] = 0; 
        $this->od['confirm_policy_id'] = 1;  
        $this->od['marital_status'] = 1;  
        $this->od['confirm_status'] = 1;  
        $this->od['blood_group'] = 1;  
        $this->od['nationality'] = 1;  
        $this->od['religion'] = 1;  
        $this->od['updated_by'] = $request->post('user_id');  
        
        $user = array(
           "company_code"=>'maurya',
           "user_code"=> $this->od['mobile_number'] ,
           "oemail_id"=> $this->od['oemail_id'] ,
           "api_token"=> '',
           "pass_reset_token"=>'',
           "pass_reset_attempt"=>0,
           "failed_attempt"=>0,
           "is_access_allowed"=>1,
           "is_locked"=>0,
           "locked_reason"=>0, 
           "remember_token"=>'',
           "is_deleted"=>'0', 
           "created_by"=>$request->post('user_id'), 
           "updated_by"=>$request->post('user_id')

        );
 
        if(!empty($request->password)){
            $user['password'] = Hash::make($request->password);
        } 

        $query=DB::table("emp_master");
        $query->selectRaw(" mobile_number "); 
        $query->where('mobile_number',$request->post('mobile_number'));
        $data = $query->first();
       
         
        if($request->filled('action') && $request->post('action') == 'Update'){
            // update 
            unset( $user['created_by'] );
            DB::beginTransaction();
            try {
                if(empty($request->password)){
                    unset( $user['password'] );
                }
                DB::table('users') 
                    ->where('user_code',$request->post('mobile_number'))
                    ->update( $user );
                DB::commit();
                $res['result'] = true;
            } catch (\Exception $e) {  
                // echo $e->getMessage();die;
                DB::rollback(); 
                $res['result'] = false;
            }

            DB::beginTransaction();
            try {
                DB::table('emp_master') 
                    ->where('em_id',$request->post('em_id'))
                    ->update(  $this->od  );
                DB::commit();
                $res['result'] = true;
            } catch (\Exception $e) { 
                // echo $e->getMessage();die; 
                DB::rollback(); 
                $res['result'] = false;
            }
                    
            if( $res['result'] ){
                
                $res['message'] = 'Updated Successfully';
                $res['type'] = 'success';
                return $res;
            }else{
                $res['message'] = 'Could not update, Please try after sometime';
                return $res;
            }
        }else{
            //create new request
            if($data){
                $res['message'] = 'Agent with same mobile number already registered !';
                return $res;
            }
            DB::beginTransaction();
            try {
               $id = DB::table('users')
                    ->insertGetId($user);
                DB::commit(); 
                $res['result'] = true;

                DB::beginTransaction();
                try {
                    $this->od['user_id'] =  $id ;
                    DB::table('emp_master')
                        ->insert($this->od);
                    DB::commit(); 
                    $res['result'] = true;

                    $role = array(
                       "role_masters_id"=>3 ,
                       "user_id"=>  $id, 
                       "created_by"=> $request->post('user_id'), 
                       "updated_by"=> $request->post('user_id'),
                       "is_default"=>'1',
                       "is_active"=>'1',
                       "is_deleted"=>'0'
                    ); 
 
                    DB::beginTransaction();
                    try { 
                        DB::table('user_roles')
                            ->insert($role);
                        DB::commit(); 
                        $res['result'] = true;
     
                    } catch (\Exception $e) {
                        // echo $e->getMessage();die;
                        DB::rollback();  
                        $res['result'] = false; 
                    } 
                } catch (\Exception $e) {
                    // echo $e->getMessage();die;
                    DB::rollback();  
                    $res['result'] = false; 
                }

            } catch (\Exception $e) {
                DB::rollback(); 
                // echo $e->getMessage();die;
                $res['result'] = false; 
            } 
            if($res['result']){
                $res['message'] = 'Created Successfully';
                $res['type'] = 'success';
                return $res;
            }else{
                $res['message'] = 'Could not create, Please try after sometime';
                return $res;
            }
        } 
    }

    public function TransactionDetails(Request $request){
        $query=DB::table("emp_master");
        $query->selectRaw("em_id,user_id,emp_name,oemail_id,mobile_number"); 
        $query->where('user_id',$request->get('id'));
        $data= $query->first();
        // dd($request->all(), $data);
        return $data;
       
    }
    public function DeadLineDetails(Request $request){
        $query=DB::table("deadline");
        $query->selectRaw("`dl_doc`, `dl_crm`, `dl_online`, `dl_oncall`, `dl_cic`, `dl_mail`, `dl_approval`, `dl_visit`");
        $data= $query->first();
        return $data;
       
    }

    
    public function DeadLine(Request $request){
        $res = array(
            'result'=>false,
            'message'=>'',
            'type' => 'warn',
            'status' => 200
        );
        $this->od['dl_doc'] = $request->post('dl_doc');
        $this->od['dl_crm'] = $request->post('dl_crm');
        $this->od['dl_online'] = $request->post('dl_online');
        $this->od['dl_oncall'] = $request->post('dl_oncall');
        $this->od['dl_cic'] = $request->post('dl_cic');
        $this->od['dl_mail'] = $request->post('dl_mail');
        $this->od['dl_approval'] = $request->post('dl_approval');
        $this->od['dl_visit'] = $request->post('dl_visit');
        DB::beginTransaction();
        try {
            DB::table('deadline')  
                ->update(  $this->od  );
            DB::commit();
            $res['result'] = true;
        } catch (\Exception $e) {  
            DB::rollback(); 
            $res['result'] = false;
        }
                
        if( $res['result'] ){
            
            $res['message'] = 'Updated Successfully';
            $res['type'] = 'success';
            return $res;
        }else{
            $res['message'] = 'Could not update, Please try after sometime';
            return $res;
        }
       
    }

    
    public function AgentStatus(Request $request){
        $res = array(
            'result'=>false,
            'message'=>'',
            'type' => 'warn',
            'status' => 200
        );
        // dd($request->all());
        DB::beginTransaction();
        try {
            DB::table('users')
                 ->where('id',$request->id)
                ->update([
                    'is_access_allowed'=>(int)($request->status == 0)  ? 1 : 0 ,
                    'updated_by' => $request->user_id,
                    'updated_at' => date('Y-m-d H:i:s')
                ]);
            DB::commit();
            $res['result'] = true;
            $res['type'] = 'success';
            $res['message'] = " Agent status updated successfully";
        } catch (\Exception $e) {
            DB::rollback(); 
            $res['message'] = " Agent status could not be updated! Please try after sometime ";
        }
        return $res;
    }

}
