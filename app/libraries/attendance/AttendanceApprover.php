<?php  
/**
 * created by Sandeep Kumar Maurya 
 * @2:47pm 2nd March 2019
 */

namespace App\libraries\attendance;
use GlobalHelpers;
use App\models\attendance\AttendanceApprovalModel;  

class AttendanceApprover{
    function __construct(){

    } 

    public function _approvers( $request ){
        $approvers = array();
        $events = $request['events'];
        $workflow = AttendanceApprovalModel::Workflow($events);
        $id = $request['req_user_id'];
        if(count($workflow) > 0){
            $_flow = [];
            foreach($workflow as $flow){
                $ou = json_decode($flow->applicable_ou,true);
                $is_applicable = GlobalHelpers::EmpOuApplicable($id,$ou);
                if($is_applicable){
                    $_flow['wf_id'] = $flow->wf_id;
                    $_flow['flow_type'] = $flow->flow_type;
                    $_flow['wf_events'] = $flow->wf_events;
                    $_flow['workflow_code'] = $flow->workflow_code;  
                    break ;
                } 
            } 
            if(!empty($_flow)){
                $total_approver = 0 ;
                $retrieved = 0;
                $required = 0;
                $approvers['flow_type'] = $_flow['flow_type'];
                 
                if($_flow['flow_type'] == 1){
                    // manager
                    $levels = GlobalHelpers::WorkflowLevels($_flow['wf_id']);
                    
                    if(!empty($levels)){
                        $approvers['list'] = array();
                        $_previous_limit = 0;
                        foreach($levels as $level){ 
                            $appr = GlobalHelpers::AuthorityDetails($level->approver,$id,1);
                            $total_approver++;
                            if(!empty($appr)){
                                $appr['wf_id']= $_flow['wf_id'];
                                $appr['wf_level']= $level->wf_level;
                                if($level->wf_level == 1){
                                    $approvers['list'][] = $appr;
                                    $retrieved++;
                                    $required++; 
                                }else{
                                    $approvers['list'][] = $appr;
                                    $retrieved++;
                                    $required++;
                                } 
                            }else{
                                $required++; 
                            }
                            $_previous_limit = $level->days_limit ;
                        }
                    } 
                    $approvers['total_approvers'] = $total_approver;
                    $approvers['retrieved'] = $retrieved;
                    $approvers['required'] = $required;
                }else{
                    // automatic 
                    $approvers['flow_type'] = $_flow['flow_type'];
                    $approvers['total_approvers'] = $total_approver;
                }
            }
        }
        return $approvers;
    } 
}
?>