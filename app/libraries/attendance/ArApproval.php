<?php
/** 
 * User: Sandeep Maurya
 * Date: 15/12/2019
 * Time: 8:26 PM
 */
namespace App\libraries\attendance;
use GlobalHelpers; 
use App\libraries\notifications\AttendanceRequestNotifications;
use App\models\attendance\AttendanceApprovalModel;
use App\Jobs\AttendanceScheduler;
class ArApproval 
{
    private $mark_id ;
    private $level_id;
    private $requester ; 
    private $status ; 
    private $level ;
    private $levels ;
    private $transaction ;
    public $res = array(
                'result'=>false,
                'type' => 'warn',
                'message'=>'',
                'status' => 200
            );

    // approve new request
    public function ApproveRequest(array $options)
    {
        $mark = AttendanceApprovalModel::TransactionData($options);
        $this->transaction = $mark ;
        if(empty($mark)){
            $this->res['message'] = __('attendance.error_mark_transaction_invalid');
            return $this->res;
        }
        $this->requester = GlobalHelpers::EmpConfirmationBasic($this->transaction->user_id);
        $trx_level = array(
            'approved_by' => $options['user_id'],
            'status'=> $options['status'],
            'approver_remarks' => TRIM($options['remarks']),
            'approved_at' =>  $options['activity_time']
        );
        $approve = AttendanceApprovalModel::UpdateLevel($trx_level,$this->transaction->trx_id);
        if(! $approve ){
            $this->res['message'] = __('attendance.error_update_trx_error');
            return $this->res;
        }
        $this->transaction->approved_by =  $options['req_user_id'];
        $nextLevelExists = AttendanceApprovalModel::NextLevel($this->transaction->mark_id,$this->transaction->level);
        $mark_noti = new AttendanceRequestNotifications();
        
        if($nextLevelExists){
            AttendanceApprovalModel::NotifyNextLevel($nextLevelExists->trx_id);
            AttendanceApprovalModel::UpdateLevelActiveness($this->transaction->trx_id);
            
            $mark_noti->ApproversNotifications(
                $this->requester, 
                $this->transaction,
                $nextLevelExists
            );
        }  else{
            $final_update = array(
                'status' => $options['status'],
                'approved_at'=>  $options['activity_time']
            );
            AttendanceApprovalModel::FinalStatus($this->transaction->mark_id,$final_update);
            $mark_noti->FinalNotificationToRequest(
                $this->requester, 
                $this->transaction
            );
            AttendanceScheduler::dispatch(
                $this->transaction->user_id,
                $this->transaction->mark_date,
                $this->transaction->mark_date
            );
        }
        $this->res['message'] = __('attendance.success_mark_approval',[
            'name' => $this->requester->emp_name,
            'code' => $this->requester->emp_code,
            'date' => date(config('constants.DATE_F'),strtotime($this->transaction->mark_date))
            ]);
        $this->res['type'] = 'success';
        $this->res['result'] = true;
        return $this->res;

    }

    public function CancelRequest(array $options)
    {
        $mark = AttendanceApprovalModel::TransactionData($options);
        $this->transaction = $mark ;
        if(empty($mark)){
            $this->res['message'] = __('attendance.error_mark_transaction_invalid');
            return $this->res;
        } 
        $this->requester = GlobalHelpers::EmpConfirmationBasic($this->transaction->user_id);
        
        if(in_array($this->transaction->final_status,[1,2]) ){
            if(strtotime($mark->trx_date. ' + 1 months ' ) >= strtotime(date('Y-m-d')) ){
                $mark_noti = new AttendanceRequestNotifications();
                switch($this->transaction->status){
                    case 1 : 
                            if($this->transaction->level == 1){
                                $final_update = array(
                                    'status' => $options['status'],
                                    'approved_at'=>  $options['activity_time'],
                                    'user_remarks' => $options['remarks']
                                );
                                AttendanceApprovalModel::FinalStatus($this->transaction->mark_id,$final_update);
                                $mark_noti->CancelNotifications(
                                    $this->requester, 
                                    $this->transaction
                                );
                                AttendanceScheduler::dispatch(
                                    $this->transaction->user_id,
                                    $this->transaction->mark_date,
                                    $this->transaction->mark_date
                                );
                                $this->res['message'] = __('attendance.success_od_cancel',[
                                    'name' => $this->requester->emp_name,
                                    'code' => $this->requester->emp_code,
                                    'date' => date(config('constants.DATE_F'),strtotime($this->transaction->mark_date))
                                    ]);
                            }else{
                                $this->res['message'] = __('attendance.error_ar_approval_in_process');
                                return $this->res;
                            }
                        break ;
                    case 2 : 
                        // $final_update = array(
                        //     'status' => 5,
                        //     'approved_at'=>  $options['activity_time'],
                        //     'user_remarks' => $options['remarks']
                        // );
                        // OdApprovalModel::FinalStatus($this->transaction->od_id,$final_update);
                        // OdApprovalModel::InitCancellation($this->transaction->od_id);
                        // $mark_noti->CancelNotifications(
                        //     $this->requester, 
                        //     $this->transaction
                        // );
                        // AttendanceScheduler::dispatch(
                        //     $this->transaction->user_id,
                        //     $this->transaction->od_date,
                        //     $this->transaction->od_date
                        // );
                        // $this->res['message'] = __('attendance.success_od_cancellation',[
                        //     'name' => $this->requester->emp_name,
                        //     'code' => $this->requester->emp_code,
                        //     'date' => date(config('constants.DATE_F'),strtotime($this->transaction->od_date))
                        //     ]);
                        $this->res['message'] = __('attendance.error_ar_cancel_not_allowed');
                        return $this->res;
                        break ;
                    default : 
                        break;
                }
            }else{
                $this->res['message'] = __('attendance.error_ar_cancel_period_exceeds');
                return $this->res;
            }
        }else{
            $this->res['message'] = __('attendance.error_ar_cancel_not_allowed');
            return $this->res;
        }
        $this->res['type'] = 'success';
        $this->res['result'] = true;
        return $this->res;
    }

    public function RejectRequest(array $options)
    {
        $mark = AttendanceApprovalModel::TransactionData($options);
        $this->transaction = $mark ;
        if(empty($mark)){
            $this->res['message'] = __('attendance.error_mark_transaction_invalid');
            return $this->res;
        } 
        $trx_level = array(
            'approved_by' => $options['user_id'],
            'status'=> $options['status'],
            'approver_remarks' => $options['remarks'],
            'approved_at' =>  $options['activity_time']
        );
        $approve = AttendanceApprovalModel::UpdateLevel($trx_level,$this->transaction->trx_id);
        if(! $approve ){
            $this->res['message'] = __('attendance.error_update_trx_error');
            return $this->res;
        }

        $final_update = array(
            'status' => $options['status'],
            'approved_at'=>  $options['activity_time']
        );
        $this->transaction->approved_by = $options['user_id'] ;
        AttendanceApprovalModel::FinalStatus($this->transaction->mark_id,$final_update);
        $this->requester = GlobalHelpers::EmpConfirmationBasic($this->transaction->user_id);
        $mark_noti = new AttendanceRequestNotifications();
        $mark_noti->NotifyRejectionToRequester(
            $this->requester, 
            $this->transaction
        );
        $this->res['result'] = true;
        $this->res['type'] = 'success';
        $this->res['message'] = __('attendance.success_request_rejected');

        return $this->res ;
    }

    public function ReconsiderRequest(array $options)
    {

    }
    public function EnqueueActivity(array $options)
    {

    }
}