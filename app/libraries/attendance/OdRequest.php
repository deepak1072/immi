<?php
/**
 * Created by PhpStorm.
 * User: Sandeep Maurya
 * Date: 5/1/2019
 * Time: 8:26 AM
 */
namespace App\libraries\attendance;
use GlobalHelpers;
use Illuminate\Http\Request;
use App\libraries\attendance\AttendanceApprover;
use App\models\attendance\OdRequestModel; 
use App\libraries\notifications\OdRequestNotifications;
use App\Jobs\AttendanceScheduler;
class OdRequest
{
    private $rule_id ;
    private $request ;
    private $od = [];
    private $levels = [];
    private $rule;
    private $user;
    function __construct()
    {
        $this->activity_time = date('Y-m-d H:i:s');
    }

    /**
     * add update od request 
     */
    public function _request(Request $request){
        
        $res = array(
            'result'=>false,
            'message'=>'',
            'type' => 'warn',
            'status' => 200
        );
        $date_str = strtotime($request->post('date'));
        $rule = array(
            'commencement' => 60,
            'frequency' => 2
        );
        $this->rule = $rule;
        $this->user = GlobalHelpers::EmpConfirmationBasic($request->req_user_id);
        if(empty($this->user)){
            $res['message'] = __('attendance.error_invalid_user',['id'=>$request->req_user_id]);
            return $res;
        }
        $this->od['trx_date'] = date(config('constants.DATE_TIME'));
        $this->od['created_at'] = date(config('constants.DATE_TIME'));
        $this->od['created_by'] = $request->post('user_id');
        $this->od['user_id'] = $request->post('req_user_id');
        $this->od['trx_code'] = $request->post('source');
        $this->od['reason_type'] = $request->post('reason_type');
        $this->od['reason'] = $request->post('reason');
        $this->od['od_date'] = date(config('constants.DATE'),$date_str);
        $this->od['od_type'] = 1 ;
        $this->od['from_type'] = $request->post('from_type');
        $this->od['to_type'] = $request->post('from_type');
        if($this->od['from_type'] == '2FD' || $this->od['from_type'] == '2FH' ){
            $this->od['to_type'] = $this->od['from_type'];
        }
        AttendanceScheduler::dispatchNow(
            $this->od['user_id'],
            $this->od['od_date'],
            $this->od['od_date']
        );
        // $this->od['mark_in'] = date(config('constants.DATE_TIME'),strtotime($request->in_time));
        // $this->od['mark_out'] = date(config('constants.DATE_TIME'),strtotime($request->out_time));
        $this->od['status'] = 1;
        $this->od['user_remarks'] = '';
        
        // commencement days 
        $_commnecement_str = strtotime(' -'.$this->rule['commencement'].' days') ;
        if($date_str < $_commnecement_str){
            $date = date(config('constants.DATE_F'), $_commnecement_str); 
            $res['message'] = __('attendance.error_commencement_days',['date'=>$date]);
            return $res; 
        }
        if($date_str > strtotime($this->activity_time) ){
            $res['message'] = __('attendance.error_od_date');
            return $res; 
        }
        $info = array(
            'code' =>  $this->user->emp_code,
            'name' => $this->user->emp_name,
            'date' =>    date(config('constants.DATE_F'), strtotime($this->od['od_date']))
        );
        $approver = new AttendanceApprover();
        $request_model = new OdRequestModel();
        $notification = new OdRequestNotifications();

        // duplicate check 
        $duplicate = $request_model->isDuplicate($this->od);
        if($duplicate){
            $res['message'] = __('attendance.error_od_duplicate',$info);
            return $res;  
        }
        // get approvers 
        $flow_data = array(
            'req_user_id' => $request->req_user_id,
            'events' => 2
        );
        $approvers = $approver->_approvers($flow_data);
        if(!empty($approvers)){
            if($approvers['retrieved'] != $approvers['required']){
                $res['message'] = __('attendance.error_approver_missing');
                return $res;  
            }
            $this->od['workflow_id'] = $approvers['flow_type'];
            $last_appr = '';
            foreach($approvers['list'] as $key=>$approver){
                if($last_appr == $approver['id']){
                   continue; 
                } 
                $flag = ($key == 0) ? '1' : '0' ;
                $this->levels[] = array(
                    'approver_role' => $approver['authority'],
                    'approval_by' => $approver['id'],
                    'status' => '1',
                    'wf_level' => $approver['wf_level'],
                    'level' => ( $key +1 ),
                    'flag' => $flag,
                    'active' => $flag,
                    'expected_approval' => null,
                    'od_id' => ''
                );
                $last_appr = $approver['id'];
            } 
        }else{
            $res['message'] = __('attendance.error_no_workflow');
            return $res;
        }

        if($request->filled('action') && $request->post('action') == 'Update'){
            // update old leave 
            // $resp = $request_model->update($this->mark,1);
            AttendanceScheduler::dispatch(
                $this->od['user_id'],
                $this->od['od_date'],
                $this->od['od_date']
            );            
            // $notification->markUpdate($this->user,$this->leave,$this->levels);
            // if($resp){
            //     $res['result'] = true;
            //     $res['message'] = __('attendance.success_mark_update',$info);
            //     $res['type'] = 'success';
            //     return $res;
            // }else{
            //     $res['message'] = __('attendance.error_mark_update',$info);
            //     return $res;
            // }
        }else{
            //create new request
            $resp = $request_model->create($this->od);
            if($resp){
                foreach($this->levels as $key=>$level){
                    $this->levels[$key]['od_id'] = $resp;
                }
                $this->od['od_id'] = $resp;
                $level = $request_model->create_levels($this->levels);
                AttendanceScheduler::dispatch(
                    $this->od['user_id'],
                    $this->od['od_date'],
                    $this->od['od_date']
                );
                $notification->Create($this->user,$this->od,$this->levels);
                $res['result'] = true;
                $res['message'] = __('attendance.success_od_create',$info);
                $res['type'] = 'success';
                return $res;
            }else{
                $res['message'] = __('attendance.error_mark_create',$info);
                return $res;
            }
        } 
    }
     
}