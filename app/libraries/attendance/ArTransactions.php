<?php
/** 
 * User: Sandeep Maurya
 * Date: 15/12/2019
 * Time: 8:26 PM
 */
namespace App\libraries\attendance;
use GlobalHelpers;  
use App\models\leave\LeaveApprovalModel; 
use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;
class ArTransactions 
{
    public function ArTransaction(Request $request)
    { 
        $limit = ($request->get('pageSize')) ? $request->get('pageSize') : 5;
        $offset = $limit * (($request->get('page')) ? $request->get('page') : 0);
        $filter = ($request->get('filter')) ? $request->get('filter') : [];
        $sort = ($request->get('sort')) ? $request->get('sort') : [];
        
        $query = DB::table("mark_attendance  as ar");
        $query->selectRaw("ar.id as mark_id,ar.trx_date,ar.mark_date,ar.mark_in,ar.mark_out,ar.reason,em.emp_name,em.emp_code,ar.user_id,art.id,ar.status as final_status,ts.status_text as final_status_text,txs.status_text, art.status, art.flag");
        $query->join('mark_transactions as art','ar.id','=','art.mark_id');
        $query->join('emp_master as em','em.user_id','=','ar.user_id');
        $query->join('trx_status as ts','ts.status_value','=','ar.status');
        $query->join('trx_status as txs','txs.status_value','=','art.status');
        
        if($request->filled('role') && $request->role != 1 ){
            if($request->role == 4 || $request->role == 3){
                // $query->selectRaw("l.leave_id,l.trx_date,lv.leave_name,em.emp_name,em.emp_code,l.user_id,l.leave_type,l.from_date,l.to_date,l.from_period,l.to_period,l.leave_days,lt.ltrx_id,l.status as final_status,ts.status_text as final_status_text,txs.status_text, lt.status, lt.flag");
                $query->where("art.flag","=",1);
                $query->where("art.active","=",1);
            }else if($request->role == 2 ){
                // managers
                $query->where("art.approval_by",$request->req_user_id);
                $query->where("art.flag","=",1);
                // $query->where("art.active","=",1); 
            } 
        }else{
            $query->where("ar.user_id","=",$request->req_user_id);
            $query->where("art.active","=",1);  
        }
        $query->where("ar.is_deleted","=",0);
        
        // applying filter 
        if(count($filter) > 0){

            foreach ($filter as $key => $value) {
               $filtered = json_decode($value);
                // $searchValue =  strtolower(htmlspecialchars(stripcslashes(trim($filtered->value))));

                // $id  = ($filtered->id == 'leave_master_name') ? " lower(".$filtered->id.") " : " lower(lv.".$filtered->id.") ";

                // $where  = "".$id ." like ('%$searchValue%') " ;
                // if($key == 0){
                //     $query->whereRaw($where);
                // }else{
                //     $query->orWhere($where); 
                // }
            }
        }

        // applying sorting
        if(count($sort) > 0){
            foreach ($sort as $key => $value) {
                $sorted = json_decode($value);
                $query->orderBy($sorted->id,($sorted->desc)?'desc':'asc');
            }
        } 
        $count = $query->count();
        $query->offset($offset);
        $query->limit($limit);
        $data= $query->get();
        return array('count'=>$count,'data'=>$data);
    }

    public function ArDetails(Request $request){
        $query = DB::table("mark_attendance  as ma");
        $query->selectRaw("ma.id as mark_id,mt.id as trx_id,workflow_id,ma.trx_date,ma.mark_date,ma.mark_in,ma.mark_out,ma.reason,em.emp_name,em.emp_code,ma.user_id,ma.user_remarks,mt.id,ma.status as final_status,ts.status_text as final_status_text,txs.status_text, mt.status, mt.flag,ma.reason_type");
        $query->join('mark_transactions as mt','ma.id','=','mt.mark_id');
        $query->join('emp_master as em','em.user_id','=','ma.user_id');
        $query->join('trx_status as ts','ts.status_value','=','ma.status');
        $query->join('trx_status as txs','txs.status_value','=','mt.status');
        $query->where("ma.id",$request->id); 
        $query->where('mt.id',$request->trx_id);
        $mark = $query->first();
        $data = [];
        if($mark){

            $_query = DB::table("configurations");
            $_query->selectRaw("_label");
            $_query->where('_code','mark_reason');
            $_query->where('_value',$mark->reason_type);
            $reason_type = $_query->first();

            $query_1 = DB::table("mark_transactions  as mt");
            $query_1->selectRaw("approver_role,em.emp_code,mt.approver_remarks,em.profile_image,emx.profile_image as profile_image1,approval_by,emx.emp_name as approval_by_name,approved_by,em.emp_name as approved_by_name,status,flag,ts.status_text");
            $query_1->join('trx_status as ts','ts.status_value','=','mt.status');
            $query_1->join('emp_master as emx','emx.user_id','=','mt.approval_by');
            $query_1->leftJoin('emp_master as em','em.user_id','=','mt.approved_by');
            $query_1->where('mark_id',$request->id);
            $query_1->orderBy('mt.id','ASC');
            $all = $query_1->get();
            $approvers = [];
            foreach($all as $key => $levels){
                $actions  =  array();
                if(in_array($levels->status,[2,3,4,6,7])){
                    $actions['id'] =  $levels->approved_by;
                    $actions['short_name'] = GlobalHelpers::ShortName($levels->approved_by_name);
                    $actions['name'] = $levels->approved_by_name; 
                    $actions['pr_image'] = $levels->profile_image; 
                    $actions['remarks'] = (TRIM($levels->approver_remarks)) ? TRIM($levels->approver_remarks) : '';
                }else{
                    $actions['id'] =  $levels->approval_by;
                    $actions['short_name'] = GlobalHelpers::ShortName($levels->approval_by_name);
                    $actions['name'] = $levels->approval_by_name; 
                    $actions['pr_image'] = $levels->profile_image1;  
                    $actions['remarks'] = '';  
                }
                $actions['status'] = $levels->status_text;
                array_push($approvers ,$actions);
            }
            $data['apprs'] = $approvers ;
            $data['mark_id'] = $mark->mark_id;
            $data['trx_id'] = $mark->trx_id;
            $data['workflow_id'] = $mark->workflow_id;
            $data['trx_date'] = date(config('constants.DATE_TIME_F'), strtotime($mark->trx_date));
            $data['mark_date'] = date(config('constants.DATE_F'), strtotime($mark->mark_date));
            $data['mark_in'] = $mark->mark_in;
            $data['mark_out'] = $mark->mark_out;
            $data['user_remarks'] = $mark->user_remarks;
            $data['reason'] = $mark->reason;
            $data['emp_code'] = $mark->emp_code;
            $data['emp_name'] = $mark->emp_name;
            $data['user_id'] = $mark->user_id;
            $data['final_status'] = $mark->final_status ;
            $data['final_status_text'] = $mark->final_status_text ;
            $data['reason_type'] = $reason_type->_label ;
            
            // $request->request->add(['role'=>2]) ; 
            $data['actions'] = [];
            if($request->filled('role') && $request->role != 1 ){
                // manager and others  
                if( $mark->final_status == 1 ){
                    if( $mark->status == 1 ){ 
                        $data['actions'][] = array('name'=>'Approve','type'=>'cyan','status' => 2 );
                        $data['actions'][] = array('name'=>'Reject','type'=>'default', 'status' => 3);
                        // $data['actions'][] = array('name'=>'Reconsider','type'=>'default','status'=>8);
                    } 
                }  
                if($mark->final_status == 5){
                    if(in_array($mark->status,[1,2])){ 
                        $data['actions'][] = array('name'=>'Approve','type'=>'cyan','status' => 6 );
                        $data['actions'][] = array('name'=>'Reject','type'=>'default', 'status' => 7);
                        // $data['actions'][] = array('name'=>'Reconsider','type'=>'default','status'=>8);
                    } 
                }  
    
                if($mark->final_status >= 5){
                    $data['user_remarks'] = $mark->user_remarks;
                } 
            }else{
                // employee 
                if(in_array($mark->final_status,[1])){
                    if(strtotime($mark->trx_date. ' + 1 months ' ) >= strtotime(date('Y-m-d')) ){
                        $data['actions'][] = array('name'=>'Cancel','type'=>'default','status'=> 4);
                    } 
                } 
            }
        }
         
        return $data ; 
    }
}
