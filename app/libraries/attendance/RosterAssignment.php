<?php
/**
 * Created by PhpStorm.
 * User: Sandeep Maurya
 * Date: 19/1/2020
 * Time: 10:56 AM
 */
namespace App\libraries\attendance;
use GlobalHelpers;  
use App\models\attendance\RosterModel; 

class RosterAssignment
{   
    private $user_id ;
    private $start_date;
    private $end_date;
    private $roster_id ;
    private $start_date_str ;
    private $end_date_str ;
    private $roster_date ;
    private $mapping = [];
    private $roster_model;
    private $batch = [];
    private $activity_time ;
    private $activist;
    function __construct(){
        $this->activity_time = date('Y-m-d H:i:s');
    } 

    public function assign($user,$roster,$start_date,$end_date,$activist)
    {
        $this->user_id = $user ;
        $this->activist = $activist;
        $this->roster_id = $roster ;
        $this->start_date = $start_date;
        $this->end_date = $end_date;
        $this->start_date_str = strtotime($start_date);
        $this->end_date_str = strtotime($end_date);
        
        if($this->start_date_str > $this->end_date_str){
            return true ;
        }
        $this->roster_model = new RosterModel();
        
        $roster = $this->roster_model->getRosterDetails($this->roster_id);
        if(count($roster) < 1){
            return true ;
        }

        $start_month = date('Y-m-01',$this->start_date_str);
        $start_month_str = strtotime($start_month);
        $end_month = date('Y-m-t',$this->end_date_str);
        $end_month_str = strtotime($end_month);
        $dates = [];
        while($start_month_str <= $end_month_str){
            $special_month = $start_month_str ;
            $special_month_end_str = strtotime(date('Y-m-t',$special_month));
            while($special_month <= $special_month_end_str ){
                $week = intval(date("W", $special_month)) - intval(date("W", $start_month_str)) + 1;
                $dates[date("m", $special_month)][$week][date('N',$special_month)] = date('Y-m-d',$special_month);
                $special_month += 86400; 
            }
            $start_month_str += (86400 * date('t',$start_month_str)) ;
        }

        $week_roster = array();
        foreach($roster as $key => $value){
            $week_roster[1][$value->day] = $value->first_week;
            $week_roster[2][$value->day] = $value->second_week;
            $week_roster[3][$value->day] = $value->third_week;
            $week_roster[4][$value->day] = $value->fourth_week;
            $week_roster[5][$value->day] = $value->fifth_week;
        }
        if(count($dates) < 1){
            return false;
        }
        $mapping = array();
        foreach($dates as $key => $weeks){
            foreach($weeks as $k => $week){
                foreach($week as $kk => $day){
                    if(isset($week_roster[$k])){
                        $mapping[$day] =  $week_roster[$k][$kk];
                    }else{ 
                        $mapping[$day] =  $week_roster[1][$kk];
                    }
                }
            }
        }
        
        $this->mapping = $mapping;
        if(count($this->mapping) < 1){
            return false;
        } 
        if($this->start_date_str == $this->end_date_str){
            $this->roster_date = date('Y-m-d',$this->start_date_str);
            $this->rosterMap();
        }else{
            while($this->start_date_str <= $this->end_date_str){
                $this->roster_date = date('Y-m-d',$this->start_date_str);
                $this->rosterMap();
                $this->start_date_str += 86400;
            }
        }
        if(count($this->batch) > 0){
            $this->roster_model->insertUserRoster($this->batch);
        }
        return true ;
    }

    private function rosterMap(){
        $mapped = $this->roster_model->isRosterAlreadyMapped($this->user_id, $this->roster_date);
        if($mapped){
            // update 
            $update = array(
                'roster_id' => $this->roster_id, 
                'user_id' => $this->user_id , 
                'shift_id' => $this->mapping[$this->roster_date],
                'roster_date' => $this->roster_date, 
                'updated_at' => $this->activity_time,
                'updated_by' => $this->activist 
            );
            $this->roster_model->updateUserRoster($update,$mapped->id);
        }else{
            // mapping  
            $this->batch[] = array(
                'roster_id' => $this->roster_id, 
                'user_id' => $this->user_id , 
                'shift_id' => $this->mapping[$this->roster_date],
                'roster_date' => $this->roster_date, 
                'created_at' => $this->activity_time, 
                'created_by' => $this->activist , 
                'updated_at' => $this->activity_time,
                'updated_by' => $this->activist 
            );
        }
        return true;
    }
}
