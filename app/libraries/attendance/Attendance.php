<?php
/**
 * Created by PhpStorm.
 * User: Sandeep Maurya
 * Date: 19/1/2020
 * Time: 10:56 AM
 */
namespace App\libraries\attendance;
use GlobalHelpers;  
use App\models\attendance\AttendanceModel; 

class Attendance
{
    private $user ;
    private $start_date;
    private $start_date_str;
    private $end_date;
    private $end_date_str;
    private $date ;
    private $roster ;
    private $shift ;
    private $_id;
    private $attendance_model ;
    private $attendance = [];
    function __construct(){
        $this->attendance_model = new AttendanceModel();
    } 


    public function runScheduler($user,$start_date,$end_date)
    {
        $this->user = $user ;
        $this->start_date = $start_date;
        $this->end_date = $end_date;
        $this->start_date_str = strtotime($this->start_date);
        $this->end_date_str = strtotime($this->end_date);
        while($this->start_date_str <= $this->end_date_str){
            $this->start_date = date('Y-m-d',$this->start_date_str);
            $this->runSchedulerPerDay();
            $this->start_date_str += 86400;
        }
    }

    private function runSchedulerPerDay(){
        $shift = $this->attendance_model->getShiftOfDay($this->user,$this->start_date);
        if(!is_null($shift)){
            // update
            $this->shift = $shift;
            $this->_id = $shift->_id;
            $this->mapAttendance();
            $this->attendance_model->updateAttendance($this->attendance,$this->_id);
        }else{
            // insert
            $roster = $this->attendance_model->getRosterOfDate($this->user,$this->start_date);
            if(!is_null($roster)){
                $this->roster = $roster ;
                $shift_details = $this->attendance_model->getShiftDetails($this->roster->shift_id);
                if(!is_null($shift_details)){
                    $this->shift = $shift_details;
                    $this->mapAttendance();
                    $this->attendance_model->insertAttendance($this->attendance);
                }
            }
        }
    }

    private function mapAttendance(){
        $punches = $this->attendance_model->getPunchesOfDay($this->user,$this->start_date);
        $leave = $this->attendance_model->getLeaveOfDay($this->user,$this->start_date);
        $mark = $this->attendance_model->getMarkOfDay($this->user,$this->start_date);
        $od = $this->attendance_model->getOdOfDay($this->user,$this->start_date);
        // dd($punches,$leave,$mark,$od);
        $this->attendance['user_id'] = $this->user;
        $this->attendance['shift_id'] = $this->shift->shift_id;
        $this->attendance['att_date'] = $this->start_date;
        
        if(count($punches) > 0){
            $first_punch = $punches->first();
            $last_punch = $punches->last();
            $this->attendance['in_time'] = $first_punch->punch_time;
            $this->attendance['out_time'] = $last_punch->punch_time;
        }
        if(count($od) > 0){
            $this->attendance['is_od']  = 1 ;
        }
        if(count($mark) > 0){
            $this->attendance['is_mark']  = 1 ;
        }
        if(count($leave) > 0){
            $this->attendance['is_leave']  = 1 ;
        }
        dd($this->attendance);
    }
 
}
