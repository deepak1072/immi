<?php
/** 
 * User: Sandeep Maurya
 * Date: 15/12/2019
 * Time: 8:26 PM
 */
namespace App\libraries\attendance;
use GlobalHelpers; 
use App\libraries\notifications\OdRequestNotifications;
use App\models\attendance\OdApprovalModel;
use App\Jobs\AttendanceScheduler;
class OdApproval 
{
    private $od_id ;
    private $level_id;
    private $requester ; 
    private $status ; 
    private $level ;
    private $levels ;
    private $transaction ;
    public $res = array(
                'result'=>false,
                'message'=>'',
                'status' => 200,
                'type' => 'warn'
            );

    // approve new request
    public function ApproveRequest(array $options)
    {
        $mark = OdApprovalModel::TransactionData($options);
        $this->transaction = $mark ;
        if(empty($mark)){
            $this->res['message'] = __('attendance.error_od_transaction_invalid');
            return $this->res;
        }
        $this->requester = GlobalHelpers::EmpConfirmationBasic($this->transaction->user_id);
        $trx_level = array(
            'approved_by' => $options['user_id'],
            'status'=> $options['status'],
            'approver_remarks' => TRIM($options['remarks']),
            'approved_at' =>  $options['activity_time']
        );
        $approve = OdApprovalModel::UpdateLevel($trx_level,$this->transaction->trx_id);
        if(! $approve ){
            $this->res['message'] = __('attendance.error_update_trx_error');
            return $this->res;
        }
        $this->transaction->approved_by =  $options['req_user_id'];
        $nextLevelExists = OdApprovalModel::NextLevel($this->transaction->od_id,$this->transaction->level);
        $mark_noti = new OdRequestNotifications();
        
        if($nextLevelExists){
            OdApprovalModel::NotifyNextLevel($nextLevelExists->trx_id);
            OdApprovalModel::UpdateLevelActiveness($this->transaction->trx_id);
            
            $mark_noti->ApproversNotifications(
                $this->requester, 
                $this->transaction,
                $nextLevelExists
            );
        }  else{
            $final_update = array(
                'status' => $options['status'],
                'approved_at'=>  $options['activity_time']
            );
            OdApprovalModel::FinalStatus($this->transaction->od_id,$final_update);
            $mark_noti->FinalNotificationToRequest(
                $this->requester, 
                $this->transaction
            );
            AttendanceScheduler::dispatch(
                $this->transaction->user_id,
                $this->transaction->od_date,
                $this->transaction->od_date
            );
        }
        $this->res['message'] = __('attendance.success_od_approval',[
            'name' => $this->requester->emp_name,
            'code' => $this->requester->emp_code,
            'date' => date(config('constants.DATE_F'),strtotime($this->transaction->od_date))
            ]);
        $this->res['result'] = true;
        $this->res['type'] = 'success';
        return $this->res;

    }

    public function CancelRequest(array $options)
    {
        $mark = OdApprovalModel::TransactionData($options);
        $this->transaction = $mark ;
        if(empty($mark)){
            $this->res['message'] = __('attendance.error_od_transaction_invalid');
            return $this->res;
        }
        $this->requester = GlobalHelpers::EmpConfirmationBasic($this->transaction->user_id);
        
        if(in_array($this->transaction->final_status,[1,2]) ){
            if(strtotime($mark->trx_date. ' + 1 months ' ) >= strtotime(date('Y-m-d')) ){
                $mark_noti = new OdRequestNotifications();
                switch($this->transaction->status){
                    case 1 : 
                            if($this->transaction->level == 1){
                                $final_update = array(
                                    'status' => $options['status'],
                                    'approved_at'=>  $options['activity_time'],
                                    'user_remarks' => $options['remarks']
                                );
                                OdApprovalModel::FinalStatus($this->transaction->od_id,$final_update);
                                $mark_noti->CancelNotifications(
                                    $this->requester, 
                                    $this->transaction
                                );
                                AttendanceScheduler::dispatch(
                                    $this->transaction->user_id,
                                    $this->transaction->od_date,
                                    $this->transaction->od_date
                                );
                                $this->res['message'] = __('attendance.success_od_cancel',[
                                    'name' => $this->requester->emp_name,
                                    'code' => $this->requester->emp_code,
                                    'date' => date(config('constants.DATE_F'),strtotime($this->transaction->od_date))
                                    ]);
                            }else{
                                $this->res['message'] = __('attendance.error_od_approval_in_process');
                                return $this->res;
                            }
                        break ;
                    case 2 : 
                        // $final_update = array(
                        //     'status' => 5,
                        //     'approved_at'=>  $options['activity_time'],
                        //     'user_remarks' => $options['remarks']
                        // );
                        // OdApprovalModel::FinalStatus($this->transaction->od_id,$final_update);
                        // OdApprovalModel::InitCancellation($this->transaction->od_id);
                        // $mark_noti->CancelNotifications(
                        //     $this->requester, 
                        //     $this->transaction
                        // );
                        // AttendanceScheduler::dispatch(
                        //     $this->transaction->user_id,
                        //     $this->transaction->od_date,
                        //     $this->transaction->od_date
                        // );
                        // $this->res['message'] = __('attendance.success_od_cancellation',[
                        //     'name' => $this->requester->emp_name,
                        //     'code' => $this->requester->emp_code,
                        //     'date' => date(config('constants.DATE_F'),strtotime($this->transaction->od_date))
                        //     ]);
                        $this->res['message'] = __('attendance.error_od_cancel_not_allowed');
                        return $this->res;
                        break ;
                    default : 
                        break;
                }
            }else{
                $this->res['message'] = __('attendance.error_od_cancel_period_exceeds');
                return $this->res;
            }
        }else{
            $this->res['message'] = __('attendance.error_od_cancel_not_allowed');
            return $this->res;
        }
        $this->res['result'] = true;
        $this->res['type'] = 'success';
        return $this->res;
    }

    public function RejectRequest(array $options)
    {
        $mark = OdApprovalModel::TransactionData($options);
        $this->transaction = $mark ;
        if(empty($mark)){
            $this->res['message'] = __('attendance.error_od_transaction_invalid');
            return $this->res;
        } 
        $trx_level = array(
            'approved_by' => $options['user_id'],
            'status'=> $options['status'],
            'approver_remarks' => $options['remarks'],
            'approved_at' =>  $options['activity_time']
        );
        $approve = OdApprovalModel::UpdateLevel($trx_level,$this->transaction->trx_id);
        if(! $approve ){
            $this->res['message'] = __('attendance.error_update_trx_error');
            return $this->res;
        }

        $final_update = array(
            'status' => $options['status'],
            'approved_at'=>  $options['activity_time']
        );
        $this->transaction->approved_by = $options['user_id'] ;
        OdApprovalModel::FinalStatus($this->transaction->od_id,$final_update);
        $this->requester = GlobalHelpers::EmpConfirmationBasic($this->transaction->user_id);
        $mark_noti = new OdRequestNotifications();
        $mark_noti->NotifyRejectionToRequester(
            $this->requester, 
            $this->transaction
        );
        $this->res['result'] = true;
        $this->res['message'] = __('attendance.success_od_request_rejected');
        $this->res['type'] = 'success';
        return $this->res ;
    }

    public function ReconsiderRequest(array $options)
    {

    }
    public function EnqueueActivity(array $options)
    {

    }
}