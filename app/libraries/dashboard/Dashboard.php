<?php
namespace App\libraries\dashboard;   
use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;
class Dashboard 
{
    public function ClientCount(Request $request)
    { 
      
        $start_date = '';
        $end_date = '';
        $today = date('Y-m-d');
        $data = array();
        $pie_data = array();
        // today
        if($request->get('period') == 1 ){
            $start_date = $today.' 00:00:00' ;
            $end_date = $today.' 23:59:59' ;
        }
        // last seven days
        if($request->get('period') == 7 ){
            $start_date = $today.' 00:00:00' ;
            $start_date = date('Y-m-d H:i:s',strtotime(' -7 days'));
            $end_date = $today.' 23:59:59' ;
        }
        // last 15 days 
        if($request->get('period') == 15 ){
            $start_date = $today.' 00:00:00' ;
            $start_date = date('Y-m-d H:i:s',strtotime(' -15 days'));
            $end_date = $today.' 23:59:59' ;
        }
        // last 30 days 
        if($request->get('period') == 30 ){
            $start_date = $today.' 00:00:00' ;
            $start_date = date('Y-m-d H:i:s',strtotime(' -30 days'));
            $end_date = $today.' 23:59:59' ;
        }
        if( empty($start_date) || empty($end_date) ){
            return $data;
        }
        $query=DB::table("customer");
        $query->selectRaw("id,result,doc_updated, crm_update, visit, online_councelling, oncall_councelling, cic, verification_mail, approval, hr_finalization"); 
        $query->where('is_deleted',0);
        if($request->get('active_role') == 3 ){
            $query->where('created_by', "=",$request->get('user_id') ); 
        }
        $query->whereBetween("created_at",[$start_date,$end_date]);
        $res = $query->get();
        if($res){
            
            $client = array(
                'form' => 0,
                'doc' => 0,
                'crm' => 0,
                'visit' => 0,
                'online' => 0,
                'oncall' => 0,
                'cic' => 0 ,
                'mail' => 0,
                'approval' => 0,
                'hr' => 0,
                'approved' => 0,
                'pending' => 0,
                'rejected' => 0
            );
            foreach($res as $key => $value){
                $client['form'] += 1;

                if( $value->result == 'Pending' ){
                    if( $value->doc_updated == 1 ){
                        $client['doc'] += 1;
                    }
                    if( $value->crm_update == 1 ){
                        $client['crm'] += 1;
                    }
                    if( $value->visit == 1 ){
                        $client['visit'] += 1;
                    }
                    if( $value->online_councelling == 1 ){
                        $client['online'] += 1;
                    }
                    if( $value->oncall_councelling == 1 ){
                        $client['oncall'] += 1;
                    }
                    if( $value->cic == 1 ){
                        $client['cic'] += 1;
                    }
                    if( $value->verification_mail == 1 ){
                        $client['mail'] += 1;
                    }
                    if( $value->approval == 1 ){
                        $client['approval'] += 1;
                    }
                    if( $value->hr_finalization == 1 ){
                        $client['hr'] += 1;
                    }
                }
                
                
                
                
                if( $value->result == 'Approved' ){
                    $client['approved'] += 1;
                }
                if( $value->result == 'Rejected' ){
                    $client['rejected'] += 1;
                }
                if( $value->result == 'Pending' ){
                    $client['pending'] += 1;
                }
                
            }
            $pie_data[]  =  array( 
                'label' => 'Completed',
                'value' => $client['rejected'] + $client['approved']  ,
            );
            $pie_data[]  =  array( 
                'label' => 'Pending',
                'value' => $client['pending'],
            );
            $data[] = array(
                'title' => 'Total Form Fillup',
                'label' => 'Total Form Fillup',
                'count' => $client['form'],
                'value' => $client['form'],
                'code' => '',
                'link' => '/customer?id='.$request->user_id."&type=total&period=".$request->get('period') 
            );
            $data[] = array(
                'title' => 'Approved',
                'label' => 'Approved',
                'count' => $client['approved'],
                'value' => $client['approved'],
                'code' => '',
                'link' => '/customer?id='.$request->user_id."&type=approved&period=".$request->get('period') 
            );
            $data[] = array(
                'title' => 'Rejected',
                'label' => 'Rejected',
                'count' => $client['rejected'],
                'value' => $client['rejected'],
                'code' => '',
                'link' => '/customer?id='.$request->user_id."&type=rejected&period=".$request->get('period') 
            );
            $data[] = array(
                'title' => 'Pending',
                'label' => 'Pending',
                'count' => $client['pending'],
                'value' => $client['pending'],
                'code' => '',
                'link' => '/customer?id='.$request->user_id."&type=pending&period=".$request->get('period') 
            );
            $data[] = array(
                'title' => 'DOC Updated',
                'label' => 'DOC Updated',
                'count' => $client['doc'],
                'value' => $client['doc'],
                'code' => '',
                'link' => '/customer?id='.$request->user_id."&type=doc&period=".$request->get('period') 
            );
            $data[] = array(
                'title' => 'CRM Updated',
                'label' => 'CRM Updated',
                'count' => $client['crm'],
                'value' => $client['crm'],
                'code' => '',
                'link' => '/customer?id='.$request->user_id."&type=crm&period=".$request->get('period') 
            );
            $data[] = array(
                'title' => ' Visit ',
                'label' => ' Visit ',
                'count' => $client['visit'],
                'value' => $client['visit'],
                'code' => '',
                'link' => '/customer?id='.$request->user_id."&type=visit&period=".$request->get('period') 
            );
            $data[] = array(
                'title' => ' Online Councelling ',
                'label' => ' Online Councelling ',
                'count' => $client['online'],
                'value' => $client['online'],
                'code' => '',
                'link' => '/customer?id='.$request->user_id."&type=online&period=".$request->get('period') 
            );
            $data[] = array(
                'title' => ' Oncall Councelling ',
                'label' => ' Oncall Councelling ',
                'count' => $client['oncall'],
                'value' => $client['oncall'],
                'code' => '',
                'link' => '/customer?id='.$request->user_id."&type=oncall&period=".$request->get('period') 
            );
            $data[] = array(
                'title' => ' CIC ',
                'label' => ' CIC ',
                'count' => $client['cic'],
                'value' => $client['cic'],
                'code' => '',
                'link' => '/customer?id='.$request->user_id."&type=cic&period=".$request->get('period') 
            );
            $data[] = array(
                'title' => ' Mail Verification ',
                'label' => ' Mail Verification ',
                'count' => $client['mail'],
                'value' => $client['mail'],
                'code' => '',
                'link' => '/customer?id='.$request->user_id."&type=mail&period=".$request->get('period') 
            );
            $data[] = array(
                'title' => ' Approval ',
                'label' => ' Approval ',
                'count' => $client['approval'],
                'value' => $client['approval'],
                'code' => '',
                'link' => '/customer?id='.$request->user_id."&type=approval&period=".$request->get('period') 
            );
            $data[] = array(
                'title' => ' HR Final Call ',
                'label' => ' HR Final Call ',
                'count' => $client['hr'],
                'value' => $client['hr'],
                'code' => '',
                'link' => '/customer?id='.$request->user_id."&type=hr&period=".$request->get('period') 
            );

        }
        $data = array(
            'bar' => $data,
            'pie' => $pie_data
        );
        
        return $data;
    }
 
    public function AgentsHistory(Request $request)
    { 
         
        $query=DB::table("customer");
        $query->selectRaw("id,result"); 
        $query->where('created_by',$request->id); 
        $query->where('is_deleted',0);
        // echo $request->id;die; 
        // echo $query->toSql();die;
        $res = $query->get();
        $pie_data = array();
        $data = array();
        if($res){
            
            $client = array(
                'form' => 0,  
                'approved' => 0,
                'pending' => 0, 
                'rejected' => 0
            );
            foreach($res as $key => $value){
                $client['form'] += 1;
                if( $value->result == 'Approved' ){
                    $client['approved'] += 1;
                }
                if( $value->result == 'Pending' ){
                    $client['pending'] += 1;
                }
                if( $value->result == 'Rejected' ){
                    $client['rejected'] += 1;
                }
            }
             
            $data[] = array(
                'title' => 'Total Visit',
                'count' => $client['form'],
                'code' => 'green-2',
                'link' => '/customer?id='.$request->id."&type=total"
            );
            $data[] = array(
                'title' => 'Completed',
                'count' => $client['approved'] + $client['rejected'],
                'code' => 'green-6',
                'link' => '/customer?id='.$request->id."&type=completed"
            );
            
            $data[] = array(
                'title' => 'Pending',
                'count' => $client['pending'],
                'code' => 'green-3',
                'link' => '/customer?id='.$request->id."&type=pending"
            );
            $pie_data[] = array(
                'label' => 'Completed',
                'value' => $client['approved'] + $client['rejected'],
                'code' => 'green-6',
                'link' => '/customer?id='.$request->id."&type=approved"
            );
            $pie_data[] = array(
                'label' => 'Pending',
                'value' => $client['pending'],
                'code' => 'green-3',
                'link' => '/customer?id='.$request->id."&type=pending"
            );

        }
        $data = array(
            'bar' => $data,
            'pie' => $pie_data
        );
        
        return $data;
    }
    public function TransactionDetails(Request $request){
        $query=DB::table("customer");
        $query->selectRaw("id,name,email,mobile,result,doc_comments,crm_comments,visit_comments,online_comments,oncall_comments,cic_comments,mail_comments,approval_comments,hr_comments,result_comments,created_at, doc_updated, crm_update, visit, online_councelling, oncall_councelling, cic, verification_mail, approval, hr_finalization, result, created_by, updated_at, is_deleted"); 
        $query->where('id',$request->get('id'));
        $data= $query->first();
        // dd($request->all(), $data);
        return $data;
       
    }
}
