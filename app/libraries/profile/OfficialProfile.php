<?php
/**
 * Created by PhpStorm.
 * User: Sandeep Maurya
 * Date: 21/7/2019
 * Time: 10:00 PM
 */
namespace App\libraries\profile;
use GlobalHelpers;

use Illuminate\Support\Facades\DB;

class OfficialProfile
{
    function __construct()
    {
        
    }
    // official details
	public function  OfficialDetails($options){
		$data=DB::table("emp_official as off")
            ->selectRaw("eo_id,off.user_id, mast.emp_code, mast.access_code, mngr_code,rm.emp_name as reporting, mngr_code2,fm.emp_name as functional,mast.confirm_req,   DATE_FORMAT(mast.confirm_due_date, '%d %b %Y') as confirm_due_date,DATE_FORMAT(mast.dob,'%d %b %Y') as  dob,DATE_FORMAT(mast.confirm_start_date,'%d %b %Y') as confirm_start_date,DATE_FORMAT(mast.confirmation_date,'%d %b %Y') as confirmation_date,mast.confirm_status,cstatus_mast.cstatus_name, off.band_code,band_mast.band_name ,off.designation_code,dsg.designation_name, off.function_code,funct.function_name, off.subfunction_code,subfunct.sub_function_name, off.busi_code,bmast.buss_name, off.subbusi_code,sbmast.sub_buss_name, off.comp_code,comp_mast.sub_company_name, off.loc_code,locmast.loc_name, off.wloc_code,wlocmast.wloc_name, off.cost_code,ccmast.cost_center_name, off.process_code,pcmast.process_name, off.type_code,tpmast.type_name, off.role_code,rmast.role_name, off.grade_code,gmast.grade_name, off.division_code,divimast.division_name, off.region_code,regmast.regions_name, off.ero, off.domicile_code,DATE_FORMAT(effective_date,'%d %b %Y') as effective_date, work_phone,  off.status_code,status_mast.status_name,DATE_FORMAT(doj,'%d %b %Y') as doj, DATE_FORMAT(dol,'%d %b %Y') as dol, mast.oemail_id")
            ->join("emp_master as mast","mast.user_id","=","off.user_id")
            ->leftJoin("designation_masters as dsg","dsg.designation_id","=","off.designation_code")
            ->leftJoin("function_masters as funct","funct.function_id","=","off.function_code")
            ->leftJoin("sub_function_masters as subfunct","subfunct.sub_function_id","=","off.subfunction_code")
            ->leftJoin("bussiness_masters as bmast","bmast.buss_id","=","off.busi_code")
            ->leftJoin("sub_bussiness_masters as sbmast","sbmast.sub_buss_id","=","off.subbusi_code")
            ->leftJoin("sub_company_masters as comp_mast","comp_mast.sub_company_id","=","off.comp_code")
            ->leftJoin("loc_masters as locmast","locmast.loc_id","=","off.loc_code")
            ->leftJoin("wloc_masters as wlocmast","wlocmast.wloc_id","=","off.wloc_code")
            ->leftJoin("cost_center_masters as ccmast","ccmast.cost_center_id","=","off.cost_code")
            ->leftJoin("process_masters as pcmast","pcmast.process_id","=","off.process_code")
            ->leftJoin("type_masters as tpmast","tpmast.type_id","=","off.type_code")
            ->leftJoin("rolemast  as rmast","rmast.role_id","=","off.role_code")
            ->leftJoin("grade_masters  as gmast","gmast.grade_id","=","off.grade_code")
            ->leftJoin("division_masters  as divimast","divimast.division_id","=","off.division_code")
            ->leftJoin("regions_masters  as regmast","regmast.regions_id","=","off.region_code")
            ->leftJoin("status_masters  as status_mast","status_mast.status_id","=","off.status_code")
            ->leftJoin("band_masters  as band_mast","band_mast.band_id","=","off.band_code")
            ->leftJoin("confirmation_status_master  as cstatus_mast","cstatus_mast.cstatus_id","=","mast.confirm_status")
            ->leftJoin('emp_master as rm','rm.user_id','=','off.mngr_code')
            ->leftJoin('emp_master as fm','fm.user_id','=','off.mngr_code2')
            ->where("off.user_id",$options['req_user_id']) 
            ->first(); 
            // dd($data);
        return  $data; 		  
	}

	public function BankDetails($options){
		$data=DB::table("emp_bank_details as e_bank")
            ->selectRaw("ebd_id,salary_bank_id,bm.bank_name as salary_bank,s_ifsc,salary_account,reim_bank_id,bmm.bank_name as reim_bank,reim_ifsc,reim_account") 
            ->leftJoin('bank_masters as bm',"bm.bank_id","=","e_bank.salary_bank_id")
            ->leftJoin('bank_masters as bmm',"bmm.bank_id","=","e_bank.reim_bank_id")
            ->where("e_bank.user_id",$options['req_user_id']) 
            ->first(); 
        return  $data; 
	}

	public function PersonalDetails($options){
		$data=DB::table("emp_bank_details as e_bank")
            ->selectRaw("ebd_id,salary_bank_id,bm.bank_name as salary_bank,s_ifsc,salary_account,reim_bank_id,bmm.bank_name as reim_bank,reim_ifsc,reim_account") 
            ->leftJoin('bank_masters as bm',"bm.bank_id","=","e_bank.salary_bank_id")
            ->leftJoin('bank_masters as bmm',"bmm.bank_id","=","e_bank.reim_bank_id")
            ->where("e_bank.user_id",$options['req_user_id']) 
            ->first(); 
        return  $data;
	}

	public function ContactDetails($options){
		$data=DB::table("contact_details as cd")
            ->selectRaw("c_id,mobile_num,phone_num, alt_num, email_id, c_address_1, c_address_2, c_address_3,n.nation_name as c_nation_name , s.state_name as c_state_name, c.city_name as c_city_name, c_pin_code, c_address_proof, p_address_1, p_address_2, p_address_3, nn.nation_name as p_nation_name, ss.state_name as p_state_name, cc.city_name as p_city_name, p_pin_code, p_address_proof") 
            ->leftJoin('nations as n',"n.nation_id","=","cd.c_nation")
            ->leftJoin('nations as nn',"nn.nation_id","=","cd.p_nation")
            ->leftJoin('states as s',"s.state_id","=","cd.c_state")
            ->leftJoin('states as ss',"ss.state_id","=","cd.p_state")
            ->leftJoin('cities as c',"c.city_id","=","cd.c_city")
            ->leftJoin('cities as cc',"cc.city_id","=","cd.p_city")
            ->where("cd.user_id",$options['req_user_id']) 
            ->first(); 
        return  $data;
    }
    
    
}