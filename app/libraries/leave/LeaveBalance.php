<?php  
/**
 * created by Sandeep Kumar Maurya 
 * @2:47pm 2nd March 2019
 */

namespace App\libraries\leave;
use GlobalHelpers;
use App\models\leave\CreditBalanceModel; 

class LeaveBalance{
	private $leave_rule = '' ;
    private $leave_settings = '';
	private $id ;
	private $entitlement ;
	private $entitlement_final ;
	private $monthly_ent = 0 ;
	private $fin_start ;
	private $fin_end ;
    private $finyear ; 
    private $effective_date ;   
    private $is_deduction_liable = false ;
    private $noofmonthstillyrend= 0;
    private $remaining_month = 0;
    private $noofmonths = 0;
    private $usermast ;
    private $umast ;
    private $user_id ;



	function __construct(){

    }
    public function LeaveBalanceProvider(string $emp_code,string $type = '',string $current_date = '' ){	
    	$leave_balance = array(); 
    	$this->user_id = $emp_code ;
    	$emp_data = GlobalHelpers::EmpOfficialBasic($emp_code);
        $this->usermast = $this->umast = $emp_data ; 
        $this->effective_date = ($current_date == '') ? date('Y-m-d') : $current_date ;
        $credit_balance = new CreditBalanceModel();
    	if(!empty($emp_data) > 0){ 
    		$leave_types = GlobalHelpers::ActiveLeaveTypes();
    		if(count($leave_types) > 0){
 
                $finyear = GlobalHelpers::ActivityYear('L');
                $this->fin_start = $finyear->activity_start_date;
                $this->finyear = $finyear->activity_year;
                $this->fin_end = date('Y-m-d',strtotime($this->fin_start.' +1 years -1 days')); 
  
    			foreach ($leave_types as $id => $leave) {
                     
                    $this->leave_rule = $leave;
                    $this->leave_settings = json_decode($leave->leave_settings,true);
                    $_balance = array();
                    $_balance['lv_id'] = $leave->leave_id;
                    $_balance['lv_name'] = $leave->leave_name;
                    $_balance['master_id'] = $leave->leave_master_id;
                    $_balance['user_id'] = $emp_code;
    				$ou = json_decode($leave->leave_settings,true);  
    				$is_applicable = GlobalHelpers::EmpOuApplicable($emp_code,$ou['applicable_ou']);
                    if($is_applicable){ 
                        
                        if(3 == $leave->leave_master_id){
                            $_balance['opening'] = 0;
                            $_balance['adjustment'] = 0;
                            $_balance['credit'] = $credit_balance->CreditCompoffBalance($emp_code,$this->leave_settings,$this->fin_start,$this->fin_end,$this->effective_date);  
                            $_balance['debit'] = $credit_balance->CoffDebitBalance($emp_code,$this->leave_settings,$this->fin_start,$this->fin_end,$this->effective_date);   
                        }else{
                            $_balance['opening'] = $credit_balance->OpeningBalance($emp_code,$leave->leave_id,$this->fin_start,$this->fin_end,$this->effective_date);
                        
                            $_balance['adjustment'] = $credit_balance->Adjustment($emp_code,$leave->leave_id,$this->fin_start,$this->fin_end,$this->effective_date);
                            $_balance['credit'] = $credit_balance->CreditBalance($emp_code,$leave->leave_id,$this->fin_start,$this->fin_end,$this->effective_date);  
                            $_balance['debit'] = $credit_balance->DebitBalance($emp_code,$leave->leave_id,$this->fin_start,$this->fin_end,$this->effective_date);   
                        }                     
                        
                        $_balance['balance'] = ($_balance['opening'] + $_balance['adjustment'] + $_balance['credit'] - $_balance['debit']); 
                        $_balance['yearendbalance'] = $_balance['balance'];
                        $_balance['yearendlapsed'] = 0;
                        
                        switch ($this->leave_settings['credit_leave_rule']) {
                            case 1 : 
                                array_push($leave_balance,$_balance); 
                            break; 
                            case 2:
                                # on confirmation
                                $confirmation = GlobalHelpers::EmpConfirmationBasic($this->user_id);
                                if(4 == $confirmation->confirm_status){
                                    array_push($leave_balance,$_balance);   
                                }
                                break;
                            case 3 : 
                                # after specified month 
                                if(strtotime(date('Y-m-d')) >= strtotime($this->usermast->doj." +".$this->leave_settings['credit_leave_after']." months")){ 
                                    array_push($leave_balance,$_balance); 
                                } 
                                break;
                            case 4 : 
                                # after specified days 
                                if(strtotime(date('Y-m-d')) >= strtotime($this->usermast->doj." +".$this->leave_settings['credit_leave_after']." days")){ 
                                    array_push($leave_balance,$_balance); 
                                }   
                                break;
                            
                            default:
                                array_push($leave_balance,$_balance); 
                                break;
                        } 
                    } 
    			}
            } 
        } 
        if(strtolower($type) == 'report'){
            if(!empty($leave_balance)){
                $credit_balance->UpdateBalanceReports($leave_balance);
            }
            return 'Completed';  
        }else{
            return $leave_balance ;
        } 
    }
 

     

    // function for round off values 
    private function RoundOffValue($value){
        if(isset($this->leave_settings['round_off_rule'])){
            switch($this->leave_settings['round_off_rule']){
                case 1 :
                    // Round down to ... decimal places
                    $round_off_digit = ($this->leave_settings['round_off_value']) ? trim($this->leave_settings['round_off_value']) : 2 ;
                    return round($value,$round_off_digit);
                    break;
                case 2 :
                    // Round off to the lowest ... days
                    $lowest = (isset($this->leave_settings['round_off_value'])) ? trim($this->leave_settings['round_off_value']) : 1;
                    switch($lowest){
                        case 1 :
                            // full
                            return floor($value);
                            break;
                        case 2 :
                            // half
                            return  floor($value * 2) / 2 ;
                            break;
                        case 3:
                            // quarter
                            return floor($value * 4) / 4 ;
                            break;
                    }
                    break;
                case 3 :
                    // Round off to the highest ... days
                    $highest = (isset($this->leave_settings['round_off_value'])) ? trim($this->leave_settings['round_off_value']) : 1;
                    switch($highest){
                        case 1 :
                            // full
                            return ceil($value);
                            break;
                        case 2 :
                            // half
                            return  ceil($value * 2) / 2 ;
                            break;
                        case 3:
                            // quarter
                            return ceil($value * 4) / 4 ;
                            break;
                    }
                    break;
                case 4 :
                    // Round off to the nearest ... days
                    $nearest = (isset($this->leave_settings['round_off_value'])) ? trim($this->leave_settings['round_off_value']) : 1;
                    switch($nearest){
                        case 1 :
                            // full
                            return round($value);
                            break;
                        case 2 :
                            // half
                            return  round($value * 2) / 2;
                            break;
                        case 3:
                            // quarter
                            return round($value * 4) / 4;
                            break;
                    }
                    break;
                default :
                    // default round off to 2 decimal places 
                    return round($value,2);

            }
        }else{
            // default round off to two decimal places 
            return round($value,2);
        }
    }

   
 
}