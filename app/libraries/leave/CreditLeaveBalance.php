<?php  
/**
 * created by Sandeep Kumar Maurya 
 * @6:47pm 19th Nov 2019
 */

namespace App\libraries\leave;
use GlobalHelpers; 
use App\models\leave\CreditBalanceModel;
use DateTime;
use DateInterval;
use DatePeriod;
class CreditLeaveBalance{
	private $leave_rule = '' ;
    private $leave_settings = '';
	private $id ;
	private $entitlement ;
	private $entitlement_final ;
	private $monthly_ent = 0 ;
	private $fin_start ;
	private $fin_end ;
    private $finyear ; 
    private $effective_date ; 
    private $current_date ; 
    private $current_date_str ; 
    private $current_date_parse ;
	private $start_date_str ;
	private $start_date ;
	private $start_date_parse ;
	private $end_date ;
	private $end_date_str ;
	private $end_date_parse ;
	private $doj_parse ;
    private $doj_str ;
    private $doj;
    private $is_deduction_liable = false ;
    private $noofmonthstillyrend= 0;
    private $remaining_month = 0;
    private $noofmonths = 0;
    private $usermast ;
    private $umast ;
    private $user_id ;
    private $users_list = array();
    private $credit_effective = '';

	function __construct(){

    }

    /**
     * 
     * @params company code ,
     * @parms employee_id,
     * @date for balance
     * return only true or false 
     */
    public function CreditBalance(String $company_code = '',String $user_id = '',String $current_date = '' , String $cycle = '' ){
        
        $response = array(
            'result'=>true,
            'message'=>__('leave.credit_success')
        ); 
        $this->users_list = GlobalHelpers::ActiveUsers($user_id);
        $leave_types = GlobalHelpers::ActiveLeaveTypes();
        if(! $leave_types){
            $response['result']= false;
            $response['message']= __('leave.no_policy');
            return $response;
        }
        
        $finyear = GlobalHelpers::ActivityYear('L');
        if(!$finyear){
            $response['result']= false;
            $response['message']= __('leave.no_leave_year');
            return $response;
        }
        $this->fin_start = $finyear->activity_start_date;
        $this->finyear = $finyear->activity_year;
        $this->fin_end = date('Y-m-d',strtotime($this->fin_start.' +1 years -1 days'));
        $this->effective_date = date('Y-m-d',strtotime($finyear->effective_date));
        
        // leave balance period decision for calculations 
        $this->start_date = (strtotime($this->effective_date) > strtotime($this->fin_start)) ? $this->effective_date : $this->fin_start ;
        $this->current_date = ($current_date != '') ? $current_date : date('Y-m-d');
        $this->current_date_str = strtotime($this->current_date);
        $credit_balance = new CreditBalanceModel();
        if($this->current_date_str < strtotime($this->fin_start)){
            $response['result']= false;
            $response['message']= __('leave.credit_not_allowed');
            return $response;
        }
        if(count($this->users_list) > 0){ 
            foreach($this->users_list as $user){
                $this->user_id = $user->user_id;
                $this->usermast = $user ;
                $this->doj= $this->usermast->doj;
                $this->doj_str =  strtotime($this->usermast->doj) ; 
                // for new joiners 
                $this->start_date = ( $this->doj_str > strtotime($this->start_date)) ? $this->usermast->doj : $this->start_date  ;
                $this->end_date = (in_array($this->usermast->status_code, array(2,3,4))) ? $this->usermast->dol : $this->fin_end ;
                $this->end_date = (strtotime($this->end_date) > strtotime($this->current_date)) ? $this->current_date : $this->end_date ; 
                $this->start_date_parse = date_parse($this->start_date);
                $this->end_date_parse = date_parse($this->end_date);
                $this->doj_parse = date_parse($this->usermast->doj);
                $this->start_date_str = strtotime($this->start_date);
                $this->end_date_str = strtotime($this->end_date);
                                
    			foreach ($leave_types as $id => $leave) {
                    $this->entitlement = 0 ;  
                    $this->leave_rule = $leave;
                    $this->leave_settings = json_decode($leave->leave_settings,true);
                   
                    $_balance = array();
                    $leave_balance = array(); 
                    $_balance['user_id'] = $this->user_id ;
                    $_balance['leave_type'] = $this->leave_rule->leave_id;
                    
                    
                    $is_applicable = GlobalHelpers::EmpOuApplicable($this->user_id,$this->leave_settings['applicable_ou']); 
                   
                    if($is_applicable === true){  
                         
                        if(in_array($this->leave_rule->leave_master_id,array(8,9,10,11,3,6,4))){
                            continue;
                        }
                        switch ($this->leave_settings['annual_ent_base']) {
                            case 0 :
                                    # annual entilement 
                                    $this->entitlement = $this->AnnualEntitlement(); 
                                break;
                            case 1 : 
                                # tenure based entitle ment
                            case 2 : 

                                # age based entitlement 
                            default:
                                # annual entitlement
                                break;
                        } 
                        $_balance['balance'] = $this->entitlement; 
                        $_balance['lb_type'] = 'Credit'; 
                        $_balance['lb_key'] = md5(uniqid()); 
                        // $_balance['effective_date'] = date('Y-m-01',strtotime($this->current_date));
                        $_balance['effective_date'] = $this->credit_effective;
                         
                        # gender applicable
                        switch ($this->leave_settings['gender_applicable']) {
                            case 0:
                                # all applicable
                                array_push($leave_balance, $_balance);
                                break;
                            case 1:
                                # male applicable
                                if($this->usermast->gender == 1){
                                    array_push($leave_balance, $_balance);
                                }
                                break;
                            case 2:
                                # female applicable
                                if($this->usermast->gender == 2){
                                    array_push($leave_balance, $_balance);
                                }
                                break; 
                            case 3:
                                # male & female applicable
                                if(in_array($this->usermast->gender,[1,2])){
                                    array_push($leave_balance, $_balance);
                                }
                                break;
                            case 4:
                                # transgender applicable
                                if($this->usermast->gender == 3){
                                    array_push($leave_balance, $_balance);
                                }
                                break;
                            default:
                                # code...
                                break;
                        }  
                    } 
                    
                    if(!empty($leave_balance)){
                        $check_credit =  $credit_balance->checkCreditBalance($leave_balance[0]);
                        if(!empty($check_credit)){ 
                            $leave_balance[0]['updated_at'] = date('Y-m-d h:i:s');
                            $credit_balance->updateCreditBalance($leave_balance[0],$check_credit);
                        }else{ 
                            $credit_balance->addCreditBalance($leave_balance[0]);   
                        }  
                    }
                } 
            }
            return $response;
        }else{
            $response['result']= false;
            $response['message']= __('leave.no_user');
            return $response;
        } 
    }

    /** annual entitlement calculations
     * leave accrued 
     */ 
    private function AnnualEntitlement($foryettoaccruve = 0){
     
        $this->entitlement = 0;
        $this->short_leave = 0;
        $deduction_balance = 0; 
        switch ($this->leave_settings['leave_credit_period']) {
            case 1:
                // monthly credit 
                $this->monthlyCredit();
                break;
            case 2 :
                // bi-monthly calculation
                $this->biMonthlyCredit();
                break;
            case 3 :
                // quarterly
                $this->quarterlyCredit();
                break;
            case 6 :
                // half yearly
                $this->halfYearlyCredit();
                
                break;
            case 12 : 
                // annual credit 
                $this->annualCredit();
                break;
        }
        return $this->RoundOffValue($this->entitlement);
    
    }

    // tenure based entitlement calculations 
    private function TenureEntitlement(){

    }

    // age based entitlement calculations 
    private function AgeEntitlement(){

    }

    // function for round off values 
    private function RoundOffValue($value){
        if(isset($this->leave_settings['round_off_rule'])){
            switch($this->leave_settings['round_off_rule']){
                case 1 :
                    // Round down to ... decimal places
                    $round_off_digit = ($this->leave_settings['round_off_value']) ? trim($this->leave_settings['round_off_value']) : 2 ;
                    return round($value,$round_off_digit);
                    break;
                case 2 :
                    // Round off to the lowest ... days
                    $lowest = (isset($this->leave_settings['round_off_value'])) ? trim($this->leave_settings['round_off_value']) : 1;
                    switch($lowest){
                        case 1 :
                            // full
                            return floor($value);
                            break;
                        case 2 :
                            // half
                            return  floor($value * 2) / 2 ;
                            break;
                        case 3:
                            // quarter
                            return floor($value * 4) / 4 ;
                            break;
                    }
                    break;
                case 3 :
                    // Round off to the highest ... days
                    $highest = (isset($this->leave_settings['round_off_value'])) ? trim($this->leave_settings['round_off_value']) : 1;
                    switch($highest){
                        case 1 :
                            // full
                            return ceil($value);
                            break;
                        case 2 :
                            // half
                            return  ceil($value * 2) / 2 ;
                            break;
                        case 3:
                            // quarter
                            return ceil($value * 4) / 4 ;
                            break;
                    }
                    break;
                case 4 :
                    // Round off to the nearest ... days
                    $nearest = (isset($this->leave_settings['round_off_value'])) ? trim($this->leave_settings['round_off_value']) : 1;
                    switch($nearest){
                        case 1 :
                            // full
                            return round($value);
                            break;
                        case 2 :
                            // half
                            return  round($value * 2) / 2;
                            break;
                        case 3:
                            // quarter
                            return round($value * 4) / 4;
                            break;
                    }
                    break;
                default :
                    // default round off to 2 decimal places 
                    return round($value,2);

            }
        }else{
            // default round off to two decimal places 
            return round($value,2);
        }
    }

    # monthly credit 
    private function monthlyCredit(){
        $this->credit_effective = date('Y-m-01',strtotime($this->current_date));
        $this->monthly_ent = $this->leave_settings['annual_entitlement'] / 12 ;
        if($this->doj_str >= $this->start_date_str){
            $this->credit_effective = $this->doj;
            if(strtotime(date('Y-m',$this->current_date_str)) > strtotime(date('Y-m',$this->doj_str))){ 
                $this->entitlement = $this->monthly_ent;
            
            }elseif(strtotime(date('Y-m',$this->current_date_str)) == strtotime(date('Y-m',$this->doj_str))){
                switch($this->leave_settings['first_period_credit_rule']){
                    case 1 :  
                            if(in_array($this->start_date_parse['day'],array('1','01'))){  
                                $this->entitlement = $this->monthly_ent;
                            } else{
                                $this->entitlement = 0; 
                            }
                        break ;
                    case 2 : 
                            $this->entitlement = $this->monthly_ent;
                        break ;
                    case 3 : 
                            $days_in_first_month = date('t',$this->doj_str) ; 
                            $no_of_days_worked = $days_in_first_month - $this->doj_parse['day']+1;
                            $this->entitlement = ($this->monthly_ent/ $days_in_first_month )* $no_of_days_worked;
                        break ;
                    case 4 : 
                            if($this->start_date_parse['day']>=16){ 
                                switch($this->leave_settings['credit_for_second_half_months']){
                                    case 0 : $this->entitlement = 0; 
                                        break;
                                    case 1 : $this->entitlement = ($this->monthly_ent/2);  
                                        break;
                                    case 2 : $this->entitlement = $this->monthly_ent;  
                                        break;
                                }
                            }else{ 
                                switch($this->leave_settings['credit_for_first_half_months']){
                                    case 0 : $this->entitlement = 0;  
                                        break;
                                    case 1 : $this->entitlement = ($this->monthly_ent/2); 
                                        break;
                                    case 2 : $this->entitlement = $this->monthly_ent;  
                                        break;
                                }
                            }
                        break ; 
                    case 5 : 
                        break;
                    default : 
                            $this->entitlement = $this->monthly_ent; 
                        break ;
                }
            }else{
                $this->entitlement = 0 ;  
            }  
        }else{
            $this->entitlement = $this->monthly_ent ; 
        } 
        // Special leave accrual
        if($this->leave_settings['first_period_credit_rule'] == 5){
            $this->entitlement = 0;
            $attendance_all = $this->getPresentDaysPeriodwise($this->finyear['FY_Start'],$this->finyear);
            $current_months =  date('n');
            $period_starting = date('Y-m-d',strtotime($this->finyear['FY_Start']));
            $period_last  = date('Y-m-t',strtotime($this->finyear['FY_Start'].' +1 years -1 days'));
            $total_leave = $this->getTotalLeavesPeriodWise($period_starting,$period_last,$this->finyear,$this->LeaveRule);
            $total_leave_data = array();


            if(sizeof($total_leave) > 0){
                foreach($total_leave as $_leave){
                    $total_leave_data[$_leave['Year']][$_leave['Month']]['Month'] = $_leave['Month'];
                    $total_leave_data[$_leave['Year']][$_leave['Month']]['Year'] = $_leave['Year'];
                    $total_leave_data[$_leave['Year']][$_leave['Month']]['Days'] = $_leave['Days'];
                }
            }

            if(sizeof($attendance_all) > 0){
                foreach($attendance_all as $key=>$attendance){
                    $total_days = 0;
                    $days_deductions = 0;
                    $days_deductions = $attendance['woff'] +  $attendance['Holiday'];
                    if(trim($attendance['Month']) == trim($current_months)){
                        break;
                    }

                    if(trim($this->LeaveRule['accrual_for_working_days']) == 1 ){
                        $total_days += $attendance['totalPresent'] - $attendance['leaveDays'];
                    }

                    if(trim($this->LeaveRule['accrual_for_weekly_off']) == 1){
                        $total_days += $attendance['woff'];
                        $days_deductions -= $attendance['woff'];
                    }

                    if(trim($this->LeaveRule['accrual_for_holidays']) == 1){
                        $total_days += $attendance['Holiday'];
                        $days_deductions -= $attendance['Holiday'];
                    }

                    $datestr = $attendance['Year'].'-'.$attendance['Month'].'-01';
                    if(trim($this->LeaveRule['accrual_for_leave_days']) == 1){
                        $total_days += (isset($total_leave_data[$attendance['Year']][$attendance['Month']]['Days'])) ? $total_leave_data[$attendance['Year']][$attendance['Month']]['Days'] : 0;
                    }
                    if(isset($this->LeaveRule['accrual_day_basis']) && $this->LeaveRule['accrual_day_basis'] == 1){
                        // Actual working days ->roster days
                        $accrual_basis_days  = date('t',strtotime($datestr)) - $days_deductions;
                        $this->entitlement +=  ($this->monthly_ent / $accrual_basis_days  ) * $total_days ;
                    }else{
                        // calendar days
                        $this->entitlement +=  ($this->monthly_ent / date('t',strtotime($datestr)) ) * $total_days ;
                    }
                }



            }else{
                $this->entitlement = 0;
            }
        }

        if($this->leave_settings['leave_credit_at'] != 0){
            $this->credit_effective = date('Y-m-d',strtotime(date('Y-m-t'.strtotime($this->credit_effective)).' +1 days'));
        }  

    }

    # bi-monthly credit 
    private function biMonthlyCredit(){
        $bi_month_cycle = array();
        $fin = $this->fin_start;
        // bi-monthly period generation from finyear start
        for($i = 1 ;$i<=6;$i++) {
            $m = date('n',strtotime($fin));
            $mm = date('n',strtotime('+1 months',strtotime($fin)));
            $fin = date('Y-m-d',strtotime('+2 months',strtotime($fin)));
            $bi_month_cycle[$i]= $m.','.$mm;
        }

        $period_start = 0;
        $periof_end = 0;
        $first_period_months = array();
        foreach ($bi_month_cycle as $key => $value) {
            $period = explode(',',$value);
            if(in_array($this->start_date_parse['month'], $period)){
                $first_period_months = $period;
                $period_start = $key;
                // if($this->start_date_parse['month'] == $period[0])
                // $this->entitlement += $this->monthly_ent;
            }
            $month_lies = ($foryettoaccruve == 1) ?$this->end_date_parse['month'] : $this->current_date_parse['month'];
            if(in_array($month_lies ,$period)){
                $period_end = $key;
            }
        }

        // doe condition after 1st no credit
        if($this->leave_settings['first_period_credit_rule'] == 0){
            if($this->start_date_parse['day']==1){
                // all  credit  joined on 1st of the month
                $this->entitlement = $this->monthly_ent;
                if($first_period_months[0] == $this->start_date_parse['month']){
                    $this->entitlement += $this->monthly_ent;
                }
            }else{
                // joined other days of the month except 1
                $this->entitlement = 0;
                if($first_period_months[0] == $this->start_date_parse['month']){
                    $this->entitlement += $this->monthly_ent;
                }
            }
        }

        //Credit complete period's leave irrespective of DoE
        if($this->leave_settings['first_period_credit_rule'] == 1){
            $this->entitlement += ($this->monthly_ent * 2);
        }

        // Treat DoE as it falls and compute first period leave on a prorated days basis
        if($this->leave_settings['first_period_credit_rule'] == 2){
            // no of days in current period
            $no_of_days = date('t',strtotime($this->usermast['DOJ']))-$this->start_date_parse['day']+1;
            if($first_period_months[0] == $this->start_date_parse['month']){
                $no_of_days += date('t',strtotime('+1 month',strtotime($this->usermast['DOJ'])));
            }
            // prorated day basis of that period
            $this->entitlement += (($this->tempdata['entitlement']/365)*$no_of_days);
        }

        if($this->leave_settings['first_period_credit_rule'] == 3){

            // first half
            $doe3 = $this->getDOEDetails3($this->LeaveRule['LvType']);
            if($this->start_date_parse['day']>=16){
                //joined between 16 & ends of month
                switch($doe3['doe_credit_mid']){
                    case 0 : $this->entitlement += 0; // no credit
                        break;
                    case 1 : $this->entitlement += ($this->monthly_ent/2); //half month credit
                        break;
                    case 2 : $this->entitlement += $this->monthly_ent; // full month credit
                        break;
                }
                // credit ent for second month of period
                if($this->start_date_parse['month'] == $first_period_months[0]){
                    $this->entitlement += $this->monthly_ent;
                }
            }else{
                // joined before mid of month
                switch($doe3['doe_credit_start']){
                    case 1 : $this->entitlement += ($this->monthly_ent/2); //half month credit
                        break;
                    case 2 : $this->entitlement += $this->monthly_ent; // full month credit
                        break;
                }
                // second month of period
                if($this->start_date_parse['month'] == $first_period_months[0]){
                    $this->entitlement += $this->monthly_ent;
                }
            }
        }

        // for old employee
        // if(strtotime($this->umast['DOJ']) < strtotime($this->finyear['FY_Start'])){
        //     $this->entitlement = ($this->monthly_ent*2);
        // }

        // credit time for rest period
        if($this->leave_settings['leave_credit_at'] == 0){
            // leave credit on start of the period
            $this->entitlement +=  (($period_end-$period_start)*($this->monthly_ent*2));
        } else{
            // at end of the end of period
            if($this->diff_month <2){
                $this->entitlement = 0;
            }
            $this->entitlement +=  ((($period_end-$period_start)-(($foryettoaccruve == 1)?0:1))*($this->monthly_ent*2));
        }

        // leave deduction on seperation
        if($this->leave_settings['first_period_credit_rule'] == 3){
            // leave deduction rules
            if(!$this->isActive){
                if($this->dol_parse['day']>=16){
                    //separates between 16th and end of the month
                    switch($doe3['doe_deduction_mid']){
                        case 0 : $this->entitlement -= 0; // no credit
                            break;
                        case 1 : $this->entitlement -= ($this->monthly_ent/2); //half month credit
                            break;
                        case 2 : $this->entitlement -= $this->monthly_ent; // full month credit
                            break;
                    }
                }else{
                    //separates between 1st and 15th of the month
                    switch($doe3['doe_deduction_start']){
                        case 0 : $this->entitlement -= 0; // no credit
                            break;
                        case 1 : $this->entitlement -= ($this->monthly_ent/2); //half month credit
                            break;
                        case 2 : $this->entitlement -= $this->monthly_ent; // full month credit
                            break;
                    }
                }
            }
        }

        $this->short_leave = $this->entitlement;

        // Special leave accrual
        if($this->leave_settings['first_period_credit_rule'] == 5){
            $this->entitlement = 0;
            $attendance_all = $this->getPresentDaysPeriodwise($this->finyear['FY_Start'],$this->finyear);
            $current_months =  date('n');
            $period_starting = date('Y-m-d',strtotime($this->finyear['FY_Start']));
            $period_last  = date('Y-m-t',strtotime($this->finyear['FY_Start'].' +1 years -1 days'));
            $total_leave = $this->getTotalLeavesPeriodWise($period_starting,$period_last,$this->finyear,$this->LeaveRule);
            $total_leave_data = array();
            if(sizeof($total_leave) > 0){
                foreach($total_leave as $_leave){
                    $total_leave_data[$_leave['Year']][$_leave['Month']]['Month'] = $_leave['Month'];
                    $total_leave_data[$_leave['Year']][$_leave['Month']]['Year'] = $_leave['Year'];
                    $total_leave_data[$_leave['Year']][$_leave['Month']]['Days'] = $_leave['Days'];
                }
            }

            $month_list = array();
            foreach($bi_month_cycle as $key=>$months ){
                $mm = explode(",",$months);
                if(in_array($current_months,$mm)){
                    break;
                }
                $month_list[] = $mm[0];
                $month_list[] = $mm[1];
            }

            if(sizeof($attendance_all) > 0){
                foreach($attendance_all as $key=>$attendance){
                    $total_days = 0;
                    $days_deductions = 0;
                    $days_deductions = $attendance['woff'] +  $attendance['Holiday'];
                    if(!in_array(trim($attendance['Month']),$month_list)){
                        break;
                    }

                    if(trim($this->LeaveRule['accrual_for_working_days']) == 1 ){
                        $total_days += $attendance['totalPresent'] - $attendance['leaveDays'];
                    }

                    if(trim($this->LeaveRule['accrual_for_weekly_off']) == 1){
                        $total_days += $attendance['woff'];
                        $days_deductions -= $attendance['woff'];
                    }

                    if(trim($this->LeaveRule['accrual_for_holidays']) == 1){
                        $total_days += $attendance['Holiday'];
                        $days_deductions -= $attendance['Holiday'];
                    }

                    $datestr = $attendance['Year'].'-'.$attendance['Month'].'-01';
                    if(trim($this->LeaveRule['accrual_for_leave_days']) == 1){
                        $total_days += (isset($total_leave_data[$attendance['Year']][$attendance['Month']]['Days'])) ? $total_leave_data[$attendance['Year']][$attendance['Month']]['Days'] : 0;
                    }

                    if(isset($this->LeaveRule['accrual_day_basis']) && $this->LeaveRule['accrual_day_basis'] == 1){
                        // Actual working days ->roster days
                        $accrual_basis_days  = date('t',strtotime($datestr)) - $days_deductions;
                        $this->entitlement +=  ($this->monthly_ent / $accrual_basis_days  ) * $total_days ;
                    }else{
                        // calendar days
                        $this->entitlement +=  ($this->monthly_ent / date('t',strtotime($datestr)) ) * $total_days ;
                    }
                }
            }else{
                $this->entitlement = 0;
            }
        }
    } 
    
    # quarterly credit 
    private function quarterlyCredit(){
        $quarter_cycle = array();
        $fin = $this->finyear['FY_Start'];
        // bi-monthly period generation from finyear start
        for($i = 1 ;$i<=4;$i++) {
            $m = date('n',strtotime($fin));
            $mm = date('n',strtotime('+1 months',strtotime($fin)));
            $mmm = date('n',strtotime('+2 months',strtotime($fin)));
            $fin = date('Y-m-d',strtotime('+3 months',strtotime($fin)));
            $quarter_cycle[$i]= $m.','.$mm.','.$mmm;
        }

        $period_start = 0;
        $periof_end = 0;
        $first_period_months = array();
        foreach ($quarter_cycle as $key => $value) {
            $period = explode(',',$value);
            if(in_array($this->start_date_parse['month'], $period)){
                $first_period_months = $period;
                $period_start = $key;
                // if($this->start_date_parse['month'] == $period[0])
                // $this->entitlement += $this->monthly_ent;
            }

            $month_lies = ($foryettoaccruve == 1) ?$this->finlastdateparse['month'] : $this->currentMonth;
            if(in_array($month_lies,$period)){
                $period_end = $key;
            }
        }
        // echo "<pre>";
        // echo $period_start ." : ".$period_end;
        // print_r($quarter_cycle);die;

        // If DoE is after 1st of month, do not credit that month's leave required
        if($this->leave_settings['first_period_credit_rule'] == 0){
            if($this->start_date_parse['day'] == 1){
                $this->entitlement += $this->monthly_ent;
            }else{
                $this->entitlement += 0;
            }
            $no_of_r_months = 0;
            if($this->start_date_parse['month'] == $first_period_months[0]){
                $no_of_r_months = 2;
            }else if($this->start_date_parse['month'] == $first_period_months[1]){
                $no_of_r_months = 1;
            }

            $this->entitlement+= ($no_of_r_months * $this->monthly_ent);
        }

        //Credit complete period's leave irrespective of DoE
        if($this->leave_settings['first_period_credit_rule'] == 1){
            $this->entitlement += ($this->monthly_ent * 3);
        }

        

        // Treat DoE as it falls and compute first period leave on a prorated days basis
        if($this->leave_settings['first_period_credit_rule'] == 2){
            $no_of_days = date('t',strtotime($this->usermast['DOJ']))-$this->start_date_parse['day']+1;
                
            if($first_period_months[0] == $this->start_date_parse['month']){
                $no_of_days += date('t',strtotime('+1 month',strtotime($this->usermast['DOJ'])));
                $no_of_days += date('t',strtotime('+2 month',strtotime($this->usermast['DOJ'])));
            }

            if($first_period_months[1] == $this->start_date_parse['month']){
                $no_of_days += date('t',strtotime('+1 month',strtotime($this->usermast['DOJ'])));
            }

            // prorated day basis of that period
            $this->entitlement += (($this->tempdata['entitlement']/365)*$no_of_days);
            // leave balance deductions 
            if($this->is_deduction_liable){
                $current_period = explode(',',$quarter_cycle[$period_end]);
                $days_in_first_month = cal_days_in_month(CAL_GREGORIAN,$current_period[0],$this->dol_parse['year']);
                $days_in_second_month = cal_days_in_month(CAL_GREGORIAN,$current_period[1],$this->dol_parse['year']);
                $days_in_last_month = cal_days_in_month(CAL_GREGORIAN,$current_period[2],$this->dol_parse['year']);
                if($this->leave_settings['leave_credit_at'] == 0){
                    // deduct at start of the period 
                    
                    if($this->dol_parse['month'] == $current_period[0]){
                        // first month of the period 
                        $no_of_days_ = date('t',strtotime($this->usermast['DOL'])) - $this->dol_parse['day'] ;
                        $no_of_days_ += date('t',strtotime('+1 month',strtotime($this->usermast['DOL'])));
                        $no_of_days_ += date('t',strtotime('+2 month',strtotime($this->usermast['DOL'])));
                    }
                    if($this->dol_parse['month'] == $current_period[1]){
                        // second month of the period  
                        $no_of_days_ = date('t',strtotime($this->usermast['DOL'])) - $this->dol_parse['day'] ;
                        $no_of_days_ += date('t',strtotime('+1 month',strtotime($this->usermast['DOL']))); 
                    }
                    if($this->dol_parse['month'] == $current_period[2]){
                        // last month of the period 
                        $no_of_days_ = date('t',strtotime($this->usermast['DOL'])) - $this->dol_parse['day'] ;
                    }
                    
                    $deduction_balance = ($this->monthly_ent*3 / ($days_in_first_month + $days_in_second_month + $days_in_last_month) ) * $no_of_days_ ;
                }else{ 
                    // deduct at end of period 
                    if($this->dol_parse['month'] == $current_period[0]){
                        // last month of the period 
                        $no_of_days_ =  $this->dol_parse['day'] ;
                    }
                    if($this->dol_parse['month'] == $current_period[1]){
                        // second month of the period  
                        $no_of_days_ =  $this->dol_parse['day'] ;
                        $no_of_days_ += date('t',strtotime('-1 month',strtotime($this->usermast['DOL']))); 
                    }
                    if($this->dol_parse['month'] == $current_period[2]){
                        // first month of the period 
                        $no_of_days_ =  $this->dol_parse['day'] ;
                        $no_of_days_ += date('t',strtotime('-1 month',strtotime($this->usermast['DOL'])));
                        $no_of_days_ += date('t',strtotime('-2 month',strtotime($this->usermast['DOL'])));
                    } 
                        
                    $deduction_balance = ($this->monthly_ent*3 / ($days_in_first_month + $days_in_second_month + $days_in_last_month) ) * $no_of_days_ ;

                } 
            } 
        }

        // doe depends
        if($this->leave_settings['first_period_credit_rule'] == 3){
            // conditions of doe
            $doe3 = $this->getDOEDetails3($this->LeaveRule['LvType']);
            if($this->start_date_parse['day']>=16){
                //joined between 16 & ends of month
                switch($doe3['doe_credit_mid']){
                    case 0 : $this->entitlement += 0; // no credit
                        break;
                    case 1 : $this->entitlement += ($this->monthly_ent/2); //half month credit
                        break;
                    case 2 : $this->entitlement += $this->monthly_ent; // full month credit
                        break;
                }
                // credit ent for second month of period
                if($this->start_date_parse['month'] == $first_period_months[0]){
                    $this->entitlement += ($this->monthly_ent*2);
                }
                if($this->start_date_parse['month'] == $first_period_months[1]){
                    $this->entitlement += $this->monthly_ent;
                }
            }else{
                // joined before mid of month
                switch($doe3['doe_credit_start']){

                    case 1 : $this->entitlement += ($this->monthly_ent/2); //half month credit
                        break;
                    case 2 : $this->entitlement += $this->monthly_ent; // full month credit
                        break;
                }
                // second month of period
                if($this->start_date_parse['month'] == $first_period_months[0]){
                    $this->entitlement += ($this->monthly_ent*2);
                }
                if($this->start_date_parse['month'] == $first_period_months[1]){
                    $this->entitlement += $this->monthly_ent;
                }
            }
        }

        // for old employee
        // if(strtotime($this->umast['DOJ']) < strtotime($this->finyear['FY_Start'])){
        //     $this->entitlement = ($this->monthly_ent*3);
        // }

        if($this->leave_settings['leave_credit_at'] == 0){
            // credit at the start of the period

            $this->entitlement += ($period_end - $period_start)* ($this->monthly_ent*3);

            $this->entitlement -= $deduction_balance;

        } else{
            // at end of the period
            if($this->diff_month <3){
                $this->entitlement = 0;
            }
            $this->entitlement += (($period_end - $period_start)-(($foryettoaccruve == 1)?0:1)) *($this->monthly_ent*3);
            $this->entitlement += $deduction_balance;
        }

            

        $this->short_leave = $this->entitlement;
        // Special leave accrual
        if($this->leave_settings['first_period_credit_rule'] == 5){
            $this->entitlement = 0;
            $attendance_all = $this->getPresentDaysPeriodwise($this->finyear['FY_Start'],$this->finyear);
            $current_months =  date('n');
            $period_starting = date('Y-m-d',strtotime($this->finyear['FY_Start']));
            $period_last  = date('Y-m-t',strtotime($this->finyear['FY_Start'].' +1 years -1 days'));
            $total_leave = $this->getTotalLeavesPeriodWise($period_starting,$period_last,$this->finyear,$this->LeaveRule);
            $total_leave_data = array();
            if(sizeof($total_leave) > 0){
                foreach($total_leave as $_leave){
                    $total_leave_data[$_leave['Year']][$_leave['Month']]['Month'] = $_leave['Month'];
                    $total_leave_data[$_leave['Year']][$_leave['Month']]['Year'] = $_leave['Year'];
                    $total_leave_data[$_leave['Year']][$_leave['Month']]['Days'] = $_leave['Days'];
                }
            }

            $month_list = array();
            foreach($quarter_cycle as $key=>$months ){
                $mm = explode(",",$months);
                if(in_array($current_months,$mm)){
                    break;
                }
                $month_list[] = $mm[0];
                $month_list[] = $mm[1];
                $month_list[] = $mm[2];
            }

            if(sizeof($attendance_all) > 0){
                foreach($attendance_all as $key=>$attendance){
                    $total_days = 0;
                    $days_deductions = 0;
                    $days_deductions = $attendance['woff'] + $attendance['Holiday'];
                    if(!in_array(trim($attendance['Month']),$month_list)){
                        break;
                    }

                    if(trim($this->LeaveRule['accrual_for_working_days']) == 1 ){
                        $total_days += $attendance['totalPresent'] - $attendance['leaveDays'];
                    }

                    if(trim($this->LeaveRule['accrual_for_weekly_off']) == 1){
                        $total_days += $attendance['woff'];
                        $days_deductions -= $attendance['woff'];
                    }

                    if(trim($this->LeaveRule['accrual_for_holidays']) == 1){
                        $total_days += $attendance['Holiday'];
                        $days_deductions -= $attendance['Holiday'];
                    }

                    $datestr = $attendance['Year'].'-'.$attendance['Month'].'-01';
                    if(trim($this->LeaveRule['accrual_for_leave_days']) == 1){
                        $total_days += (isset($total_leave_data[$attendance['Year']][$attendance['Month']]['Days'])) ? $total_leave_data[$attendance['Year']][$attendance['Month']]['Days'] : 0;
                    }

                    if(isset($this->LeaveRule['accrual_day_basis']) && $this->LeaveRule['accrual_day_basis'] == 1){
                        // Actual working days ->roster days
                        $accrual_basis_days  = date('t',strtotime($datestr)) - $days_deductions;
                        $this->entitlement +=  ($this->monthly_ent / $accrual_basis_days  ) * $total_days ;
                    }else{
                        // calendar days
                        $this->entitlement +=  ($this->monthly_ent / date('t',strtotime($datestr)) ) * $total_days ;
                    }
                }
            }else{
                $this->entitlement = 0;
            }
        }
    }

    # half yearly credit 
    private function halfYearlyCredit(){
        $half_cycle = array();
        $fin = $this->fin_start;
        $this->monthly_ent = $this->leave_settings['annual_entitlement'] / 2 ;
        // monthly period generation from finyear start
        for($i = 1 ;$i<=2;$i++) {
            $m = date('n',strtotime($fin));
            $mm = date('n',strtotime('+1 months',strtotime($fin)));
            $mmm = date('n',strtotime('+2 months',strtotime($fin)));
            $l = date('n',strtotime('+3 months',strtotime($fin)));
            $ll = date('n',strtotime('+4 months',strtotime($fin)));
            $lll = date('n',strtotime('+5 months',strtotime($fin)));
            
            $half_cycle[$i]['months']= array($m,$mm,$mmm,$l,$ll,$lll);
            $half_cycle[$i]['balance']= $this->monthly_ent;
            $half_cycle[$i]['credit_at']= $fin;
            $half_cycle[$i]['credit_till']= date('Y-m-t',strtotime('+5 months',strtotime($fin)));
            $fin = date('Y-m-d',strtotime('+6 months',strtotime($fin)));
        }
        $n_months = date('n',$this->doj_str);
        $__p = (in_array($n_months,$half_cycle[1]['months']))? 1:2 ; 
        
        if($this->doj_str >= $this->start_date_str){ 
            $this->credit_effective = $this->doj ;  
            switch($this->leave_settings['first_period_credit_rule']){
                case 1 : 
                    $this->entitlement = $this->monthly_ent ; 
                    break ;
                case 2 : 
                    $this->entitlement = $this->monthly_ent ; 
                    break ;
                case 3 : 
                        
                        if($this->doj_str < strtotime($half_cycle[$__p]['credit_at'])){
                            $this->entitlement = $this->monthly_ent ; 
                        }else{
                            $_end = strtotime($half_cycle[$__p]['credit_till']); 
                            $_start = strtotime($half_cycle[$__p]['credit_at']);
                            $total_diff = $_end - $_start ;
                            $total = round($total_diff / 86400)+1;
                            $diff = $_end - $this->doj_str;
                            $no_of_days  =  round($diff / 86400)+1;
                            $this->entitlement = ($this->monthly_ent * $no_of_days /$total) ; 
                        }
                    break;
                case 4 :   
                        if($this->doj_str < strtotime($half_cycle[$__p]['credit_at'])){
                            $this->entitlement = $this->monthly_ent ; 
                        }else{  
                            $this->monthly_ent = $this->leave_settings['annual_entitlement'] / 12 ;
                            if($this->doj_parse['day']>=16){ 
                                switch($this->leave_settings['credit_for_second_half_months']){
                                    case 0 : $this->entitlement = 0; 
                                        break;
                                    case 1 : $this->entitlement = ($this->monthly_ent/2);  
                                        break;
                                    case 2 : $this->entitlement = $this->monthly_ent;  
                                        break;
                                }
                            }else{ 
                                switch($this->leave_settings['credit_for_first_half_months']){
                                    case 0 : $this->entitlement = 0;  
                                        break;
                                    case 1 : $this->entitlement = ($this->monthly_ent/2); 
                                        break;
                                    case 2 : $this->entitlement = $this->monthly_ent;  
                                        break;
                                }
                            } 
                            $selected_period = $half_cycle[$__p]['months'] ; 
                            
                            switch($n_months){
                                case $selected_period[0] : 
                                    $this->entitlement += ($this->monthly_ent*5); 
                                break;
                                case $selected_period[1] : 
                                    $this->entitlement += ($this->monthly_ent*4);
                                break;
                                case $selected_period[2]: 
                                    $this->entitlement += ($this->monthly_ent *3);
                                break;
                                case $selected_period[3] : 
                                    $this->entitlement += ($this->monthly_ent *2);
                                break;
                                case $selected_period[4] :
                                    $this->entitlement += ($this->monthly_ent);
                                break;
                            }  
                        } 
                    break;
                case 5 : 
                    $this->entitlement = $this->monthly_ent ;
                    break ;
                default : 
                    $this->entitlement = $this->monthly_ent ; 
                    break;
            }
        }else{ 
            $this->credit_effective = $half_cycle[$__p]['credit_at'] ;        
            $this->entitlement = $this->monthly_ent ; 
        }
         
        
           
        // Special leave accrual
        if($this->leave_settings['first_period_credit_rule'] == 5){
            $this->entitlement = 0;
            $attendance_all = $this->getPresentDaysPeriodwise($this->finyear['FY_Start'],$this->finyear);
            $current_months =  date('n');
            $period_starting = date('Y-m-d',strtotime($this->finyear['FY_Start']));
            $period_last  = date('Y-m-t',strtotime($this->finyear['FY_Start'].' +1 years -1 days'));
            $total_leave = $this->getTotalLeavesPeriodWise($period_starting,$period_last,$this->finyear,$this->LeaveRule);
            $total_leave_data = array();
            if(sizeof($total_leave) > 0){
                foreach($total_leave as $_leave){
                    $total_leave_data[$_leave['Year']][$_leave['Month']]['Month'] = $_leave['Month'];
                    $total_leave_data[$_leave['Year']][$_leave['Month']]['Year'] = $_leave['Year'];
                    $total_leave_data[$_leave['Year']][$_leave['Month']]['Days'] = $_leave['Days'];
                }
            }

            $month_list = array();
            foreach($half_cycle as $key=>$months ){
                $mm = explode(",",$months);
                if(in_array($current_months,$mm)){
                    break;
                }
                $month_list[] = $mm[0];
                $month_list[] = $mm[1];
                $month_list[] = $mm[2];
                $month_list[] = $mm[3];
                $month_list[] = $mm[4];
                $month_list[] = $mm[5];
            }
            if(sizeof($attendance_all) > 0){
                foreach($attendance_all as $key=>$attendance){
                    $total_days = 0;
                    $days_deductions = 0;
                    $days_deductions = $attendance['woff']+$attendance['Holiday'];
                    if(!in_array(trim($attendance['Month']),$month_list)){
                        break;
                    }

                    if(trim($this->LeaveRule['accrual_for_working_days']) == 1 ){
                        $total_days += $attendance['totalPresent'] - $attendance['leaveDays'];
                    }

                    if(trim($this->LeaveRule['accrual_for_weekly_off']) == 1){
                        $total_days += $attendance['woff'];
                        $days_deductions -= $attendance['woff'];
                    }

                    if(trim($this->LeaveRule['accrual_for_holidays']) == 1){
                        $total_days += $attendance['Holiday'];
                        $days_deductions -= $attendance['Holiday'];
                    }

                    $datestr = $attendance['Year'].'-'.$attendance['Month'].'-01';

                    if(trim($this->LeaveRule['accrual_for_leave_days']) == 1){
                        $total_days += (isset($total_leave_data[$attendance['Year']][$attendance['Month']]['Days'])) ? $total_leave_data[$attendance['Year']][$attendance['Month']]['Days'] : 0;
                    }

                    if(isset($this->LeaveRule['accrual_day_basis']) && $this->LeaveRule['accrual_day_basis'] == 1){
                        // Actual working days ->roster days
                        $accrual_basis_days  = date('t',strtotime($datestr)) - $days_deductions;
                        $this->entitlement +=  ($this->monthly_ent / $accrual_basis_days  ) * $total_days ;
                    }else{
                        // calendar days
                        $this->entitlement +=  ($this->monthly_ent / date('t',strtotime($datestr)) ) * $total_days ;
                    }
                }
            }else{
                $this->entitlement = 0;
            }
        }

        if($this->leave_settings['leave_credit_at'] != 0){
            $this->credit_effective = date('Y-m-d',strtotime( $half_cycle[$__p]['credit_till'] .' +1 days'));
        } 
    }

    #yearly credit 
    private function annualCredit(){
        //  yearly
        $this->monthly_ent = $this->leave_settings['annual_entitlement'] / 12 ;
        if($this->doj_str >= $this->start_date_str){ 
            $this->credit_effective = $this->doj ; 
            switch($this->leave_settings['first_period_credit_rule']){
                case 1 : 
                    $this->entitlement = $this->leave_settings['annual_entitlement'] ; 
                    break ;
                case 2 : 
                    $this->entitlement = $this->leave_settings['annual_entitlement'] ; 
                    break ;
                case 3 : 
                        $fin_end = strtotime($this->fin_end); 
                        $fin_start = strtotime($this->fin_start);
                        $total_diff = $fin_end - $fin_start ;
                        $total = round($total_diff / 86400)+1;
                        $diff = $fin_end - $this->doj_str;
                        $no_of_days  =  round($diff / 86400)+1;
                        $this->entitlement = ($this->leave_settings['annual_entitlement']/$total) * $no_of_days;

                    break;
                case 4 : 
                        if($this->start_date_parse['day']>=16){ 
                            switch($this->leave_settings['credit_for_second_half_months']){
                                case 0 : $this->entitlement = 0; 
                                    break;
                                case 1 : $this->entitlement = ($this->monthly_ent/2);  
                                    break;
                                case 2 : $this->entitlement = $this->monthly_ent;  
                                    break;
                            }
                        }else{ 
                            switch($this->leave_settings['credit_for_first_half_months']){
                                case 0 : $this->entitlement = 0;  
                                    break;
                                case 1 : $this->entitlement = ($this->monthly_ent/2); 
                                    break;
                                case 2 : $this->entitlement = $this->monthly_ent;  
                                    break;
                            }
                        } 
                        $start    = (new DateTime($this->start_date));
                        $end      = (new DateTime($this->fin_end));
                        $interval = DateInterval::createFromDateString('1 month');
                        $period   = new DatePeriod($start, $interval, $end);
                        $periods = array();
                        foreach ($period as $key=>$month) { 
                            $periods[$month->format("Y-m")]['balance'] = ($key == 0) ? $this->entitlement: $this->monthly_ent;  
                        }
                        
                        $this->entitlement =  array_sum(array_column($periods,'balance'));
                         
                    break;
                case 5 : 
                    break;
                default : 
                    $this->entitlement = $this->leave_settings['annual_entitlement'] ; 
                break;
            }
             
     
             
    
            //Treat DoE as it falls and compute first period leave on a prorated days basis
            
    
            if($this->leave_settings['first_period_credit_rule'] == 3){
                $doe3 = $this->getDOEDetails3($this->LeaveRule['LvType']);
                if($this->start_date_parse['day']>=16){
                    //joined between 16 & ends of month
                    switch($doe3['doe_credit_mid']){
                        case 0 : $this->entitlement += 0; // no credit
                            break;
                        case 1 : $this->entitlement += ($this->monthly_ent/2); //half month credit
                            break;
                        case 2 : $this->entitlement += $this->monthly_ent; // full month credit
                            break;
                    }
                }else{
                    // joined before mid of month
                    switch($doe3['doe_credit_start']){
    
                        case 1 : $this->entitlement += ($this->monthly_ent/2); //half month credit
                            break;
                        case 2 : $this->entitlement += $this->monthly_ent; // full month credit
                            break;
                    }
                }
    
                $this->entitlement += ( $this->remaining_month * $this->monthly_ent);
            }
    
            // Special leave accrual
            if($this->leave_settings['first_period_credit_rule'] == 5){
                if($this->leave_settings['leave_credit_at'] == 0){
                    $total_days = 0;
                    $days_deductions = 0;
                    $this->entitlement = 0;
                    $attendance = $this->getPresentDays($this->finyear['FY_Start'],$this->finyear);
                    $days_deductions = $attendance['woff'] + $attendance['Holiday'];
                    if(trim($this->LeaveRule['accrual_for_working_days']) == 1 ){
                        $total_days += $attendance['totalPresent'] - $attendance['leaveDays'];
                    }
    
                    if(trim($this->LeaveRule['accrual_for_weekly_off']) == 1){
                        $total_days += $attendance['woff'];
                        $days_deductions -= $attendance['woff'];
                    }
    
                    if(trim($this->LeaveRule['accrual_for_holidays']) == 1){
                        $total_days += $attendance['Holiday'];
                        $days_deductions -= $attendance['Holiday'];
                    }
    
                    if(trim($this->LeaveRule['accrual_for_leave_days']) == 1){
                        //$total_days += $this->getTotalLeaves($this->finyear['FY_Start'],$this->finyear,$this->LeaveRule);
                        $total_days += $attendance['leaveDays'];
                    }
    
                    if(isset($this->LeaveRule['accrual_day_basis']) && $this->LeaveRule['accrual_day_basis'] == 1){
                        // Actual working days ->roster days
                        $accrual_basis_days  = 365 - $days_deductions;
                        $this->entitlement +=  ($this->tempdata['entitlement'] / $accrual_basis_days  ) * $total_days ;
                    }else{
                        // calendar days
                        $this->entitlement +=  ($this->tempdata['entitlement'] / 365 ) * $total_days ;
                    }
                }else{
    
                    $total_days = 0;
                    $this->entitlement = 0;
                    $prevFinYear = date('Y-m-d',strtotime($this->finyear['FY_Start'].' -1 years'));
                    $period_last  = date('Y-m-t',strtotime($prevFinYear.' +1 years -1 days'));
                    $attendance_all = $this->getPresentDaysPeriodwise($prevFinYear,$this->finyear);
                    $total_leave = $this->getTotalLeavesPeriodWise($prevFinYear,$period_last,$this->finyear,$this->LeaveRule);
                    $total_leave_days = 0;
                    if(sizeof($total_leave) > 0){
                        foreach($total_leave as $_leave){
                            $total_leave_days += $_leave['Days'];
                        }
                    }
    
                    if(sizeof($attendance_all) > 0){
                        foreach($attendance_all as $key=>$attendance){
                            $days_deductions = 0;
                            $days_deductions = $attendance['Holiday'] + $attendance['woff'];
                            if(trim($this->LeaveRule['accrual_for_working_days']) == 1 ){
                                $total_days += $attendance['totalPresent'];
                            }
    
                            if(trim($this->LeaveRule['accrual_for_weekly_off']) == 1){
                                $total_days += $attendance['woff'];
                                $days_deductions -= $attendance['woff'];
                            }
    
                            if(trim($this->LeaveRule['accrual_for_holidays']) == 1){
                                $total_days += $attendance['Holiday'];
                                $days_deductions -= $attendance['Holiday'];
                            }
                        }
                        if(trim($this->LeaveRule['accrual_for_leave_days']) == 1){
                            $total_days += $total_leave_days;
                        }
    
                        if(isset($this->LeaveRule['accrual_day_basis']) && $this->LeaveRule['accrual_day_basis'] == 1){
                            // Actual working days ->roster days
                            $accrual_basis_days  = 365 - $days_deductions;
                            $this->entitlement +=  ($this->tempdata['entitlement'] / $accrual_basis_days  ) * $total_days ;
                        }else{
                            // calendar days
                            $this->entitlement +=  ($this->tempdata['entitlement'] / 365 ) * $total_days ;
                        }
                    }else{
                        $this->entitlement = 0;
                    }
                }
            }
        } else{
            $this->credit_effective = $this->start_date ;  
            $this->entitlement =  $this->leave_settings['annual_entitlement'] ;
        }  
    }
    
}