<?php
/** 
 * User: Sandeep Maurya
 * Date: 15/12/2019
 * Time: 8:26 PM
 */
namespace App\libraries\leave;
use GlobalHelpers;
use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;
class OpeningBalance 
{
    public function Transactions(Request $request)
    { 
        $limit = ($request->get('pageSize')) ? $request->get('pageSize') : 5;
        $offset = $limit * (($request->get('page')) ? $request->get('page') : 0);
        $filter = ($request->get('filter')) ? $request->get('filter') : [];
        $sort = ($request->get('sort')) ? $request->get('sort') : [];
        
        $query=DB::table("leave_balance  as lb");
        $query->selectRaw("lb.lb_id,em.emp_name,emx.emp_name as created_by,lv.leave_name,lb.balance,lb.effective_date,lb.remarks,lb.created_at");
        $query->join('leave_types as lv','lv.leave_id','=','lb.leave_type');
        $query->join('emp_master as em','em.user_id','=','lb.user_id');
        $query->join('emp_master as emx','emx.user_id','=','lb.created_by');
        
        if($request->filled('role') && $request->role != 1 ){
            if($request->role == 4 || $request->role == 3){
                // $query->selectRaw("l.leave_id,l.trx_date,lv.leave_name,em.emp_name,em.emp_code,l.user_id,l.leave_type,l.from_date,l.to_date,l.from_period,l.to_period,l.leave_days,lt.ltrx_id,l.status as final_status,ts.status_text as final_status_text,txs.status_text, lt.status, lt.flag");
                 
            }else if($request->role == 2 ){

            }
        }else{ 
            $query->where("bl.user_id","=",$request->req_user_id);
        }
        $query->where("lb.user_id","=",$request->req_user_id);
        $query->where("lb.lb_type","=",'Opening');
        
        // applying filter 
        if(count($filter) > 0){
            $filter_key = array(
                'emp_name' => 'em.emp_name',
                'created_by' => 'emx.emp_name',
                'leave_name' => 'lv.leave_name',
                'balance' => 'lb.balance',
                'effective_date' => 'lb.effective_date',
                'remarks' => 'lb.remarks',
                'created_at' => 'lb.created_at'
            );
            foreach ($filter as $key => $value) {
               $filtered = json_decode($value);
                $searchValue =  strtolower(htmlspecialchars(stripcslashes(trim($filtered->value))));
                $id  =     " lower(".$filter_key[$filtered->id].") ";
                $where  = "".$id ." like ('%$searchValue%') " ;
                if($key == 0){
                    $query->whereRaw($where);
                }else{
                    $query->orWhere($where); 
                }
            }
        }

        // applying sorting
        if(count($sort) > 0){
            foreach ($sort as $key => $value) {
                $sorted = json_decode($value);
                $query->orderBy($sorted->id,($sorted->desc)?'desc':'asc');
            }
        }
        $count = $query->count();
        $query->offset($offset);
        $query->limit($limit);
        $data= $query->get();
        // echo $query->toSql();
        return array('count'=>$count,'data'=>$data);
    }
    
    // credit opening balance 
    public function CreditOpeningBalance($balance){
        DB::beginTransaction();
        try {
            DB::table('leave_balance') 
                ->insert($balance);
            DB::commit();
        } catch (\Exception $e) { 
            DB::rollback();
            return false;
        }
    }
      
}
