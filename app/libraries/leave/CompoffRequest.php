<?php
/**
 * Created by PhpStorm.
 * User: Sandeep Maurya
 * Date: 5/1/2019
 * Time: 8:26 AM
 */
namespace App\libraries\leave;
use GlobalHelpers;
use Illuminate\Http\Request;
use App\libraries\leave\CompoffApprover;
use App\models\leave\CoffRequestModel; 
use App\libraries\notifications\CompoffRequestNotifications;
use App\Jobs\AttendanceScheduler;
class CompoffRequest
{
    private $rule_id ;
    private $request ;
    private $mark = [];
    private $levels = [];
    private $rule;
    private $user;
    function __construct()
    {
        $this->activity_time = date('Y-m-d H:i:s');
    }

    /**
     * add update mark request 
     */
    public function _request(Request $request){
        
        $res = array(
            'result'=>false,
            'message'=>'',
            'type' => 'warn',
            'status' => 200
        );
        $date_str = strtotime($request->post('for_date'));
        $rule = array(
            'commencement' => 60,
            'frequency' => 2
        );
        $this->rule = $rule;
        $this->user = GlobalHelpers::EmpConfirmationBasic($request->req_user_id);
        if(empty($this->user)){
            $res['message'] = __('attendance.error_invalid_user',['id'=>$request->req_user_id]);
            return $res;
        }
        $this->mark['trx_date'] = date(config('constants.DATE_TIME'));
        $this->mark['created_at'] = date(config('constants.DATE_TIME'));
        $this->mark['created_by'] = $request->post('user_id');
        $this->mark['user_id'] = $request->post('req_user_id');
        $this->mark['trx_code'] = $request->post('source');
        $this->mark['reason'] = $request->post('reason');
        $this->mark['work_date'] = date(config('constants.DATE'),$date_str);
        $this->mark['effective_date'] = date(config('constants.DATE'),$date_str);
        // $this->mark['mark_in'] = date(config('constants.DATE_TIME'),strtotime($request->in_time));
        // $this->mark['mark_out'] = date(config('constants.DATE_TIME'),strtotime($request->out_time));
        $this->mark['status'] = 1;
        $this->mark['user_remarks'] = '';
        $this->mark['credit_days'] = 0;
        $this->mark['debit_days'] = 0;
        $this->mark['work_day_type'] = 'Weekly-Off';
        $this->mark['actual_in'] = '';
        $this->mark['actual_out'] = '';
        $this->mark['planned_in'] = '';
        $this->mark['planned_out'] = '';

        
        // commencement days 
        $_commnecement_str = strtotime(' -'.$this->rule['commencement'].' days') ;
        
        if($date_str < $_commnecement_str){
            $date = date(config('constants.DATE_F'), $_commnecement_str); 
            $res['message'] = __('attendance.error_commencement_days',['date'=>$date]);
            return $res; 
        }
        if($date_str > strtotime($this->activity_time) ){
            $res['message'] = __('leave.error_request_date');
            return $res; 
        }
        
        $options = array(
            'user_id' => $request->req_user_id,
            'date' => $request->for_date
        );
        $credit = $this->_credit($options);
        if($credit['result']){
            dd($credit , $this->mark);
        }else{
            $res['message'] = $credit['message'];
            return $res;
        }
        $info = array(
            'code' =>  $this->user->emp_code,
            'name' => $this->user->emp_name,
            'date' =>    date(config('constants.DATE_F'), strtotime($this->mark['work_date']))
        );
        $approver = new CompoffApprover();
        $request_model = new CoffRequestModel();
        $notification = new CompoffRequestNotifications();
        // duplicate check 
        $duplicate = $request_model->isDuplicate($this->mark);
        if($duplicate){
            $res['message'] = __('leave.error_coff_duplicate',$info);
            return $res;  
        }
        // get approvers 
        $flow_data = array(
            'req_user_id' => $request->req_user_id,
            'events' => 1
        );
        $approvers = $approver->_approvers($flow_data);
        if(!empty($approvers)){
            if($approvers['retrieved'] != $approvers['required']){
                $res['message'] = __('leave.error_coff_approver_missing');
                return $res;  
            }
            $this->mark['workflow_id'] = $approvers['flow_type'];
            $last_appr = '';
            foreach($approvers['list'] as $key=>$approver){
                if($last_appr == $approver['id']){
                   continue; 
                } 
                $flag = ($key == 0) ? '1' : '0' ;
                $this->levels[] = array(
                    'approver_role' => $approver['authority'],
                    'approval_by' => $approver['id'],
                    'status' => '1',
                    'wf_level' => $approver['wf_level'],
                    'level' => ( $key +1 ),
                    'flag' => $flag,
                    'active' => $flag,
                    'coff_id' => '',
                    'approved_by' => 0,
                    'approver_remarks' => ''
                );
                $last_appr = $approver['id'];
            } 
        }else{
            $res['message'] = __('leave.error_no_workflow');
            return $res;
        }

        if($request->filled('action') && $request->post('action') == 'Update'){
            // update old leave 
            // $resp = $request_model->update($this->mark,1);
            AttendanceScheduler::dispatch(
                $this->mark['user_id'],
                $this->mark['work_date'],
                $this->mark['work_date']
            );            
            // $notification->markUpdate($this->user,$this->leave,$this->levels);
            // if($resp){
            //     $res['result'] = true;
            //     $res['message'] = __('attendance.success_mark_update',$info);
            //     $res['type'] = 'success';
            //     return $res;
            // }else{
            //     $res['message'] = __('attendance.error_mark_update',$info);
            //     return $res;
            // }
        }else{
            //create new request
            dd($this->mark,$this->levels);
            $resp = $request_model->create($this->mark);
            if($resp){
                foreach($this->levels as $key=>$level){
                    $this->levels[$key]['coff_id'] = $resp;
                }
                $this->mark['coff_id'] = $resp;
                $level = $request_model->create_levels($this->levels);
                
                $notification->markCreate($this->user,$this->mark,$this->levels);
                $res['result'] = true;
                $res['message'] = __('leave.success_coff_create',$info);
                $res['type'] = 'success';
                return $res;
            }else{
                $res['message'] = __('leave.error_coff_create',$info);
                return $res;
            }
        } 
    }

    public function _credit($options){
        $credit = array(
            'day_type' => 'Weekly-Off',
            'credit' => 1,
            'punch_in' => '09:30:22',
            'punch_out' => '20:30:23',
            'result' => true,
            'message' => 'No record found'
        );
        return $credit;
    }
     
}