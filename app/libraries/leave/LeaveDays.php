<?php  
/**
 * created by Sandeep Kumar Maurya 
 * @2:47pm 5th Jan 2020
 */

namespace App\libraries\leave;
use GlobalHelpers;
use App\models\leave\LeaveApprovalModel; 
use Illuminate\Http\Request;

class LeaveDays{
    function __construct(){

    } 

    public function _days(Request $request){
        $id = $request->get('id');
        $lvtype = $request->get('lvtype');
        $from_date = $request->get('from_date');
        $to_date = $request->get('to_date');
        $from_type = $request->get('from_type');
        $to_type = $request->get('to_type');
        $days = 0;
        // days calculations will be on shift basis attendance
        $emp_data = GlobalHelpers::EmpOfficialBasic($id);
        $rule = GlobalHelpers::LeaveRule($lvtype);
        return $days;
    }
}