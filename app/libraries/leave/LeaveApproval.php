<?php
/** 
 * User: Sandeep Maurya
 * Date: 15/12/2019
 * Time: 8:26 PM
 */
namespace App\libraries\leave;
use GlobalHelpers; 
use App\libraries\notifications\LeaveRequestNotifications;
use App\models\leave\LeaveApprovalModel;
use App\Jobs\AttendanceScheduler;
class LeaveApproval 
{
    private $leave_id ;
    private $level_id;
    private $requester ; 
    private $status ; 
    private $level ;
    private $levels ;
    private $leave_rule ;
    private $transaction ;
    public $res = array(
                'result'=>false,
                'message'=>'',
                'status' => 200
            );
    function __construct()
    {

    }

    // create new leave request
    public function ApproveRequest(array $options)
    {
         
        $leave = LeaveApprovalModel::LeaveTransactionData($options);
        $this->transaction = $leave ;
        if(empty($leave)){
            $this->res['message'] = __('leave.error_leave_transaction_invalid');
            return $this->res;
        } 
        $leave_rule = GlobalHelpers::LeaveRule($leave->user_id); 
        $this->leave_rule = json_decode($leave_rule->leave_settings,true);
        $this->requester = GlobalHelpers::EmpConfirmationBasic($this->transaction->user_id);

        if($this->leave_rule['approve_pre_yr_leave'] ==  0 ){
            $this->res['message'] = __('leave.error_prev_yr_leave_not_allowed');
            return $this->res;
        }   
        $trx_level = array(
            'approved_by' => $options['req_user_id'],
            'status'=> $options['status'],
            'approver_remarks' => $options['remarks'],
            'approved_at' =>  date(config('constants.DATE_TIME'))
        ); 
        $approve = LeaveApprovalModel::UpdateLevel($trx_level,$this->transaction->ltrx_id);
        if(! $approve ){
            $this->res['message'] = __('leave.error_update_trx_error');
            return $this->res;
        }
        $this->transaction->approved_by =  $options['req_user_id'] ; 
        $nextLevelExists = LeaveApprovalModel::NextLevel($this->transaction->leave_id,$this->transaction->level);
        $leave_noti = new LeaveRequestNotifications();
        
        if($nextLevelExists){
            LeaveApprovalModel::NotifyNextLevel($nextLevelExists->ltrx_id);
            LeaveApprovalModel::UpdateLevelActiveness($this->transaction->ltrx_id);
            
            $leave_noti->ApproversNotifications(
                $this->requester, 
                $this->transaction, 
                $this->leave_rule['leave_name'] ,
                $nextLevelExists
            );
        }  else{
            $final_update = array(
                'status' => $options['status'],
                'approved_at'=>  date(config('constants.DATE_TIME'))
            );
            LeaveApprovalModel::FinalStatus($this->transaction->leave_id,$final_update);
            $leave_noti->FinalNotificationToRequest(
                $this->requester, 
                $this->transaction, 
                $this->leave_rule['leave_name'] 
            );
            AttendanceScheduler::dispatch(
                $this->transaction->user_id,
                $this->transaction->from_date,
                $this->transaction->to_date
            );
        }
        $this->res['message'] = __('leave.success_leave_approval',[
            'leave' => $this->leave_rule['leave_name'],
            'name' => $this->requester->emp_name,
            'code' => $this->requester->emp_code,
            'day' => $this->transaction->leave_days,
            'from' => date(config('constants.DATE_F'),strtotime($this->transaction->from_date)),
            'to' => date(config('constants.DATE_F'),strtotime($this->transaction->to_date))
            ]);
        $this->res['result'] = true;
        return $this->res;

    }

    public function CancelRequest(array $options)
    {
        $leave = LeaveApprovalModel::LeaveTransactionData($options);
        $this->transaction = $leave ;
        if(empty($leave)){
            $this->res['message'] = __('leave.error_leave_transaction_invalid');
            return $this->res;
        } 
        $this->requester = GlobalHelpers::EmpConfirmationBasic($this->transaction->user_id);
        
        if(in_array($this->transaction->final_status,[1,2]) ){
            if(strtotime($leave->trx_date. ' + 1 months ' ) >= strtotime(date('Y-m-d')) ){
                $_noti = new LeaveRequestNotifications();
                switch($this->transaction->status){
                    case 1 : 
                            if($this->transaction->level == 1){
                                $final_update = array(
                                    'status' => $options['status'],
                                    'approved_at'=>  $options['activity_time'],
                                    'user_remarks' => $options['remarks']
                                );
                                LeaveApprovalModel::FinalStatus($this->transaction->leave_id,$final_update);
                                $_noti->CancelNotifications(
                                    $this->requester, 
                                    $this->transaction
                                );
                                AttendanceScheduler::dispatch(
                                    $this->transaction->user_id,
                                    $this->transaction->from_date,
                                    $this->transaction->to_date
                                );
                                $this->res['message'] = __('leave.success_leave_cancel',[
                                    'name' => $this->requester->emp_name,
                                    'code' => $this->requester->emp_code,
                                    'from' => date(config('constants.DATE_F'),strtotime($this->transaction->from_date)),
                                    'to' => date(config('constants.DATE_F'),strtotime($this->transaction->to_date))
                                    ]);
                            }else{
                                $this->res['message'] = __('leave.error_leave_approval_in_process');
                                return $this->res;
                            }
                        break ;
                    case 2 : 
                        // $final_update = array(
                        //     'status' => 5,
                        //     'approved_at'=>  $options['activity_time'],
                        //     'user_remarks' => $options['remarks']
                        // );
                        // OdApprovalModel::FinalStatus($this->transaction->od_id,$final_update);
                        // OdApprovalModel::InitCancellation($this->transaction->od_id);
                        // $mark_noti->CancelNotifications(
                        //     $this->requester, 
                        //     $this->transaction
                        // );
                        // AttendanceScheduler::dispatch(
                        //     $this->transaction->user_id,
                        //     $this->transaction->od_date,
                        //     $this->transaction->od_date
                        // );
                        // $this->res['message'] = __('attendance.success_od_cancellation',[
                        //     'name' => $this->requester->emp_name,
                        //     'code' => $this->requester->emp_code,
                        //     'date' => date(config('constants.DATE_F'),strtotime($this->transaction->od_date))
                        //     ]);
                        $this->res['message'] = __('leave.error_leave_cancel_not_allowed');
                        return $this->res;
                        break ;
                    default : 
                        break;
                }
            }else{
                $this->res['message'] = __('leave.error_leave_cancel_period_exceeds');
                return $this->res;
            }
        }else{
            $this->res['message'] = __('leave.error_leave_cancel_not_allowed');
            return $this->res;
        }
        $this->res['result'] = true;
        $this->res['type'] = 'success';
        return $this->res;
    }

    public function RejectRequest(array $options)
    {
        $leave = LeaveApprovalModel::LeaveTransactionData($options);
        $this->transaction = $leave ;
        if(empty($leave)){
            $this->res['message'] = __('leave.error_leave_transaction_invalid');
            return $this->res;
        } 
        $trx_level = array(
            'approved_by' => $options['req_user_id'],
            'status'=> $options['status'],
            'approver_remarks' => $options['remarks'],
            'approved_at' =>  date(config('constants.DATE_TIME'))
        );
        $approve = LeaveApprovalModel::UpdateLevel($trx_level,$this->transaction->ltrx_id);
        if(! $approve ){
            $this->res['message'] = __('leave.error_update_trx_error');
            return $this->res;
        }

        $final_update = array(
            'status' => $options['status'],
            'approved_at'=>  date(config('constants.DATE_TIME'))
        );
        $this->transaction->approved_by = $options['req_user_id'] ;
        LeaveApprovalModel::FinalStatus($this->transaction->leave_id,$final_update);
        $this->requester = GlobalHelpers::EmpConfirmationBasic($this->transaction->user_id);
        $leave_rule = GlobalHelpers::LeaveRule($leave->leave_type);
        
        $this->leave_rule = json_decode($leave_rule->leave_settings,true);
        $leave_noti = new LeaveRequestNotifications();
        $leave_noti->NotifyRejectionToRequester(
            $this->requester, 
            $this->transaction, 
            $this->leave_rule['leave_name'] 
        );
        $this->res['result'] = true;
        $this->res['type'] = 'success';
        $this->res['message'] = __('leave.success_request_rejected');

        return $this->res ;
    }

    public function ReconsiderRequest(array $options)
    {

    }
    public function EnqueueActivity(array $options)
    {

    }
}