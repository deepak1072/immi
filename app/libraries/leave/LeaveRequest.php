<?php
/**
 * Created by PhpStorm.
 * User: Sandeep Maurya
 * Date: 5/1/2019
 * Time: 8:26 AM
 */
namespace App\libraries\leave;
use GlobalHelpers;
use Illuminate\Http\Request;
use App\libraries\leave\LeaveApprover;
use App\libraries\leave\LeaveBalance;
use App\models\leave\LeaveRequestModel; 
use App\libraries\notifications\LeaveRequestNotifications;
use App\Jobs\AttendanceScheduler;
class LeaveRequest
{
    private $rule_id ;
    private $setup_rule = [];
    private $request ;
    private $leave = [];
    private $levels = [];
    private $user ;
    function __construct()
    {

    }

    /**
     * number of days
     */

    public function leaveDays($data){
        $days = 0;

        $from_str = strtotime($data['from_date']);
        $to_str = strtotime($data['to_date']);
        if($from_str == $to_str){
            if($data['from_period'] == '2FH'){
                $days = 0.5;
            }else{
                $days = 1; 
            }  
             
        }else{
            if($data['from_period'] == '2FH'){
                $days = $days - 0.5 ;
            }
            if($data['to_period'] == '2FH'){
                $days = $days - 0.5 ;
            }
        }

        $diff = $to_str - $from_str ;
        $days = ($days + $diff / 86400) ;
        return $days;
    }

    /**
     * add update leave request 
     */
    public function _request(Request $request){
        
        $res = array(
            'result'=>false,
            'message'=>'',
            'status' => 200
        );
         
        // AttendanceScheduler::dispatch(
        //     1,
        //     '2020-02-13',
        //     '2020-02-13'
        // );
        $from_date_str = strtotime($request->post('from_date'));
        $to_date_str = strtotime($request->post('to_date'));
        if($from_date_str > $to_date_str){
            $res['message'] = __('leave.error_from_date_gte');
            return $res;
        }
        $period = array('2FD','1FH','2FH');
        if(!in_array($request->post('from_type'),$period)){
            $res['message'] = __('leave.error_from_type');
            return $res;
        }
        if(!in_array($request->post('to_type'),$period)){
            $res['message'] = __('leave.error_to_type');
            return $res;
        }
        $rule = GlobalHelpers::LeaveRule($request->post('leave_type'));
        if(empty($rule)){
            $res['message'] = __('leave.error_leave_type');
            return $res;
        } 
        $this->user = GlobalHelpers::EmpConfirmationBasic($request->req_user_id);
        if(empty($this->user)){
            $res['message'] = __('leave.error_invalid_user',['id'=>$request->req_user_id]);
            return $res;
        } 

        $this->rule_id = $rule->leave_id;
        $this->leave['leave_type'] = $rule->leave_id;
        $this->leave['trx_date'] = date(config('constants.DATE_TIME'));
        $this->leave['created_at'] = date(config('constants.DATE_TIME'));
        $this->leave['created_by'] = $request->post('user_id');
        $this->leave['user_id'] = $request->post('req_user_id');
        $this->leave['trx_code'] = $request->post('source');
        $this->leave['from_date'] = date(config('constants.DATE'),strtotime($request->post('from_date')));
        $this->leave['to_date'] = date(config('constants.DATE'),strtotime($request->post('to_date')));
        $this->leave['leave_days'] = $request->post('days');
        $this->leave['from_period'] = $request->post('from_type');
        $this->leave['to_period'] = $request->post('to_type');
        $this->leave['status'] = '1'; 
        $this->leave['effective_date'] = date(config('constants.DATE'));
        $this->leave['attachments'] = '';
        $this->leave['plan_unplan'] = 0 ;
        $this->leave['pup_reason'] = 0;
        $this->setup_rule = json_decode($rule->leave_settings,true); 
        
        if($this->setup_rule['leave_track'] == 1){   
            if(! $request->filled('p_u_p')){ 
                $res['message'] = __('leave.error_planned_unplanned');
                return $res; 
            }
            $this->leave['plan_unplan'] = $request->post('p_u_p');
            if($request->post('p_u_p') != 1){ 
               if(! $request->filled('p_u_p_reason')){
                    $res['message'] = __('leave.error_unplanned_reason');
                    return $res; 
               } 
               $this->leave['pup_reason'] = $request->post('p_u_p_reason');
            }
        }
        $this->setup_rule['reason']  = 1; 
        if($this->setup_rule['reason'] == 1){
            if(! $request->filled('reason')){
                $res['message'] = __('leave.error_reason');
                return $res; 
            }
            $this->leave['reason'] = $request->post('reason');
        }
        
        $this->setup_rule['attachments_required'] = 2;
        if($this->setup_rule['attachments_required'] == 1 ){ 
            if($request->hasFile('files')) {
                if ($request->file('files')->isValid()) {
                    $name = $request->file('files')->getClientOriginalName();
                    $extension = $request->file('files')->extension();
                    $path = $request->file('files')->storeAs('documents/leave/'.$request->company_code, $name);
                    $this->leave['attachments'] = $path;
                }else{
                    $res['message'] = __('leave.error_attachments');
                    return $res; 
                }
            } 
        }

        if($this->setup_rule['attachments_required'] == 2 ){
            if($request->hasFile('files')) {
                if ($request->file('files')->isValid()) {
                    $extension = $request->file('files')->extension();
                    if(!in_array($extension,['docs','pdf','jpg','jpeg','png','csv','xlxs'])){
                        $res['message'] = __('leave.error_invalid_extensions');
                        return $res;  
                    }
                    $name = $request->file('files')->getClientOriginalName();
                    $path = $request->file('files')->storeAs('documents/leave/'.$request->company_code, $name);
                    $this->leave['attachments'] = $path;
                }else{
                    $res['message'] = __('leave.error_attachment_invalid');
                    return $res; 
                }
            }else{
                $res['message'] = __('leave.error_attachments');
                return $res; 
            } 
        }

        if($this->setup_rule['declaration_required'] == 1 ){
            if($request->filled('declaration')){
                $this->leave['declarations'] = $request->post('declaration');
            }
        }
        if($this->setup_rule['declaration_required'] == 2 ){
            if(! $request->filled('declaration')){
                $res['message'] = __('leave.error_declaration');
                return $res; 
            }
            $this->leave['declarations'] = $request->post('declaration');
        }
        // dd($this->leave);
        //  leave commencement days 
        $_commnecement_str = strtotime(' -'.$this->setup_rule['leave_commencement_days'].' days') ;
        if($from_date_str < $_commnecement_str){
            $date = date(config('constants.DATE_F'), $_commnecement_str); 
            $res['message'] = __('leave.error_commencement_days',['date'=>$date]);
            return $res; 
        }

        // leave days 
        $this->leave['leave_days'] = $this->leaveDays($this->leave);
        if($this->leave['leave_days'] < 0.5){ 
            $res['message'] = __('leave.error_leave_days');
            return $res; 
        }

        // leave balance 
        $balance = new LeaveBalance();
        $leave_balance = $balance->LeaveBalanceProvider($request->req_user_id,'',date(config('constants.DATE')));
        if(empty($leave_balance)){
            $res['message'] = __('leave.error_leave_balance');
            return $res; 
        }

        $isBalance =false ; 
        foreach($leave_balance as $key => $balance){
            if($balance['lv_id'] = $this->leave['leave_type']){
                if($this->leave['leave_days'] <= $balance['balance']){
                    $isBalance = true;
                }
            }
        }
        if(! $isBalance ){
            $res['message'] = __('leave.error_leave_balance');
            return $res; 
        } 

        // get leave approvers 
        $approver = new LeaveApprover();
        $flow_data = array(
            'req_user_id' => $request->req_user_id,
            'days'=>$request->days,
            'events' =>$request->leave_type
        );
        $approvers = $approver->_approvers($flow_data);
        if(!empty($approvers)){
            if($approvers['retrieved'] != $approvers['required']){
                $res['message'] = __('leave.error_approver_missing');
                return $res;  
            }
            $this->leave['workflow_id'] = $approvers['flow_type'];
            $last_appr = '';
            foreach($approvers['list'] as $key=>$approver){
                if($last_appr == $approver['id']){
                   continue; 
                } 
                $flag = ($key == 0) ? '1' : '0' ;
                $this->levels[] = array(
                    'approver_role' => $approver['authority'],
                    'approval_by' => $approver['id'],
                    'status' => '1',
                    'wf_level' => $approver['wf_level'],
                    'level' => ( $key +1 ),
                    'flag' => $flag,
                    'active' => $flag,
                    'exp_approval_date' => null,
                    'leave_id' => ''
                );
                $last_appr = $approver['id'];
            }
               
        }else{
            $res['message'] = __('leave.error_no_workflow');
            return $res;
        }
        $info = array(
            'code' =>  $this->user->emp_code,
            'name' => $this->user->emp_name,
            'day' => $this->leave['leave_days'],
            'from' =>    date(config('constants.DATE_F'), strtotime($this->leave['from_date'])),
            'to' =>    date(config('constants.DATE_F'), strtotime($this->leave['to_date'])),
        );
        $request_model = new LeaveRequestModel();
        
        $notification = new LeaveRequestNotifications();
        if($request->filled('action') && $request->post('action') == 'Update'){
            // update old leave 
            // $resp = $request_model->update($this->leave,1);
            AttendanceScheduler::dispatch(
                $this->leave['user_id'],
                $this->leave['from_date'],
                $this->leave['to_date']
            );            
            $notification->leaveUpdate($this->user,$this->leave,$this->levels,$this->setup_rule);
            if($resp){
                $res['result'] = true;
                $res['message'] = __('leave.success_leave_update',$info);
                return $res;
            }else{
                $res['message'] = __('leave.error_leave_update',$info);
                return $res;
            }
        }else{
            //create new request
            $resp = $request_model->create($this->leave); 
            if($resp){  
                foreach($this->levels as $key=>$level){
                    $this->levels[$key]['leave_id'] = $resp;
                } 

                $this->leave['leave_id'] = $resp;
                $level = $request_model->create_levels($this->levels);
                 
                AttendanceScheduler::dispatch(
                    $this->leave['user_id'],
                    $this->leave['from_date'],
                    $this->leave['to_date']
                );
                $notification->leaveCreate($this->user,$this->leave,$this->levels,$this->setup_rule);
                $res['result'] = true;
                $res['message'] = __('leave.success_leave_create',$info);
                return $res;
            }else{
                $res['message'] = __('leave.error_leave_create',$info);
                return $res;
            }
        } 
    }
     
}