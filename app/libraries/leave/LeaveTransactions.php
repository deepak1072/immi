<?php
/** 
 * User: Sandeep Maurya
 * Date: 15/12/2019
 * Time: 8:26 PM
 */
namespace App\libraries\leave;
use GlobalHelpers;  
use App\models\leave\LeaveApprovalModel; 
use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;
class LeaveTransactions 
{
    public function LeaveTransaction(Request $request)
    { 
        $limit = ($request->get('pageSize')) ? $request->get('pageSize') : 5;
        $offset = $limit * (($request->get('page')) ? $request->get('page') : 0);
        $filter = ($request->get('filter')) ? $request->get('filter') : [];
        $sort = ($request->get('sort')) ? $request->get('sort') : [];
        
        $query=DB::table("leave  as l");
        $query->selectRaw("l.leave_id,l.trx_date,lv.leave_name,em.emp_name,em.emp_code,l.user_id,l.leave_type,l.from_date,l.to_date,l.from_period,l.to_period,l.leave_days,lt.ltrx_id,l.status as final_status,ts.status_text as final_status_text,txs.status_text, lt.status, lt.flag");
        $query->join('leave_transactions as lt','l.leave_id','=','lt.leave_id');
        $query->join('leave_types as lv','lv.leave_id','=','l.leave_type');
        $query->join('emp_master as em','em.user_id','=','l.user_id');
        $query->join('trx_status as ts','ts.status_value','=','l.status');
        $query->join('trx_status as txs','txs.status_value','=','lt.status');
        
        if($request->filled('role') && $request->role != 1 ){
            if($request->role == 4 || $request->role == 3){
                // $query->selectRaw("l.leave_id,l.trx_date,lv.leave_name,em.emp_name,em.emp_code,l.user_id,l.leave_type,l.from_date,l.to_date,l.from_period,l.to_period,l.leave_days,lt.ltrx_id,l.status as final_status,ts.status_text as final_status_text,txs.status_text, lt.status, lt.flag");
                $query->where("lt.flag","=",1);
                $query->where("lt.active","=",1);
            }else if($request->role == 2 ){ 
                $query->where("lt.flag","=",1); 
                $query->where("lt.approval_by",$request->req_user_id);
            }
        }else{ 
            $query->where("l.user_id","=",$request->req_user_id);
            $query->where("lt.active","=",1);  
        }
        $query->where("l.is_deleted","=",0);
        
        // applying filter 
        if(count($filter) > 0){

            foreach ($filter as $key => $value) {
               $filtered = json_decode($value);
                // $searchValue =  strtolower(htmlspecialchars(stripcslashes(trim($filtered->value))));

                // $id  = ($filtered->id == 'leave_master_name') ? " lower(".$filtered->id.") " : " lower(lv.".$filtered->id.") ";

                // $where  = "".$id ." like ('%$searchValue%') " ;
                // if($key == 0){
                //     $query->whereRaw($where);
                // }else{
                //     $query->orWhere($where); 
                // }
            }
        }

        // applying sorting
        if(count($sort) > 0){
            foreach ($sort as $key => $value) {
                $sorted = json_decode($value);
                $query->orderBy($sorted->id,($sorted->desc)?'desc':'asc');
            }
        } 
        $count = $query->count();
        $query->offset($offset);
        $query->limit($limit);
        $data= $query->get();
        // echo $query->toSql();
        return array('count'=>$count,'data'=>$data);
    }

    public function LeaveTransactionDetails(Request $request){
        $query=DB::table("leave  as l");
        $query->selectRaw("l.leave_id,workflow_id,l.trx_date,l.user_remarks,lv.leave_name,l.reason,l.attachments,em.emp_name,em.emp_code,l.user_id,l.leave_type,l.from_date,l.to_date,l.from_period,l.to_period,l.leave_days,l.plan_unplan,l.pup_reason,lt.ltrx_id,l.status as final_status,ts.status_text as final_status_text,txs.status_text,lt.wf_level,lt.level,l.approved_at as final_approved_at, lt.status, lt.flag ,lv.leave_settings");
        $query->join('leave_transactions as lt','l.leave_id','=','lt.leave_id');
        $query->join('leave_types as lv','lv.leave_id','=','l.leave_type');
        $query->join('emp_master as em','em.user_id','=','l.user_id');
        $query->join('trx_status as ts','ts.status_value','=','l.status');
        $query->join('trx_status as txs','txs.status_value','=','lt.status');
        $query->where("l.leave_id",$request->l_id); 
        $query->where('lt.ltrx_id',$request->trx_id);
        $leave = $query->first();
        $data = [];
        if($leave){
            $query_1 = DB::table("leave_transactions  as lt");
            $query_1->selectRaw("approver_role,lt.approver_remarks,em.profile_image,emx.profile_image as profile_image1,approval_by,emx.emp_name as approval_by_name,approved_by,em.emp_name as approved_by_name,status,flag,ts.status_text");
            $query_1->join('trx_status as ts','ts.status_value','=','lt.status'); 
            $query_1->join('emp_master as emx','emx.user_id','=','lt.approval_by');            
            $query_1->leftJoin('emp_master as em','em.user_id','=','lt.approved_by');
            $query_1->where('leave_id',$request->l_id);
            $query_1->orderBy('lt.ltrx_id','ASC');
            $all = $query_1->get();
            $approvers = [];
            foreach($all as $key => $levels){
                $actions  =  array();
                if(in_array($levels->status,[2,3,4,6,7])){
                    $actions['id'] =  $levels->approved_by;
                    $actions['short_name'] = GlobalHelpers::ShortName($levels->approved_by_name);
                    $actions['name'] = $levels->approved_by_name; 
                    $actions['pr_image'] = $levels->profile_image; 
                    $actions['remarks'] = (trim($levels->approver_remarks)) ? $levels->approver_remarks : '';

                }else{
                    $actions['id'] =  $levels->approval_by;
                    $actions['short_name'] = GlobalHelpers::ShortName($levels->approval_by_name);
                    $actions['name'] = $levels->approval_by_name; 
                    $actions['pr_image'] = $levels->profile_image1;  
                    $actions['remarks'] = '';  
                }
                $actions['status'] = $levels->status_text; 
                
                array_push($approvers ,$actions);
            }
            $data['apprs'] = $approvers ;
        }
        $data['leave_id'] = $leave->leave_id;
        $data['ltrx_id'] = $leave->ltrx_id;
        $data['workflow_id'] = $leave->workflow_id;
        $data['trx_date'] = date(config('constants.DATE_TIME_F'), strtotime($leave->trx_date));
        $data['from_date'] = date(config('constants.DATE_F'), strtotime($leave->from_date));
        $data['to_date'] = date(config('constants.DATE_F'), strtotime($leave->to_date));
        $data['leave_name'] = $leave->leave_name;
        $data['reason'] = $leave->reason;
        $data['emp_code'] = $leave->emp_code;
        $data['emp_name'] = $leave->emp_name;
        $data['user_id'] = $leave->user_id;
        $data['days'] = $leave->leave_days;
        $data['plan_unplan'] = $leave->plan_unplan;
        $data['pup_reason'] = $leave->pup_reason;
        $data['wf_level'] = $leave->wf_level;
        $data['from_period'] = GlobalHelpers::getFullPeriod($leave->from_period);
        $data['to_period'] = GlobalHelpers::getFullPeriod($leave->to_period); 
        $rule = json_decode($leave->leave_settings,true);
        $data['attachments_req'] = $rule['attachments_required'];
        $data['leave_track'] = $rule['leave_track'];
        $data['final_status'] = $leave->final_status ; 
        $data['final_status_text'] = $leave->final_status_text ; 
        // $request->request->add(['role'=>2]) ; 
        $data['actions'] = [];
        if($request->filled('role') && $request->role != 1 ){
            // manager and others  
            if( $leave->final_status == 1 ){
                if( $leave->status == 1 ){ 
                    $data['actions'][] = array('name'=>'Approve','type'=>'cyan','status' => 2 );
                    $data['actions'][] = array('name'=>'Reject','type'=>'default', 'status' => 3);
                    // $data['actions'][] = array('name'=>'Reconsider','type'=>'default','status'=>8);
                } 
            }  
            if($leave->final_status == 5){
                if(in_array($leave->status,[1,2])){ 
                    $data['actions'][] = array('name'=>'Approve','type'=>'cyan','status' => 6 );
                    $data['actions'][] = array('name'=>'Reject','type'=>'default', 'status' => 7);
                    // $data['actions'][] = array('name'=>'Reconsider','type'=>'default','status'=>8);
                } 
            }  

            if($leave->final_status >= 5){
                $data['user_remarks'] = $leave->user_remarks;
            } 
        }else{
            // employee 
            if(in_array($leave->final_status,[1])){
                if(strtotime($leave->trx_date. ' + 1 months ' ) >= strtotime(date('Y-m-d')) ){
                    $data['actions'][] = array('name'=>'Cancel','type'=>'default','status'=> 4);
                } 
            } 
        } 
        return $data ; 
    }

     

    public function TeamLeaveTransactions(Request $request){
        $limit =   10;
        $offset =   0;  
        
        $query=DB::table("leave  as l");
        $query->selectRaw(" em.emp_name  ,em.emp_code  ,em.profile_image,l.user_id,l.from_date,l.to_date");
        $query->join('emp_master as em','em.user_id','=','l.user_id'); 
        $query->where("l.is_deleted","=",0);
        $query->whereIn("l.status",[2,5,7]);
        
        $query->offset($offset);
        $query->limit($limit);
        $data= $query->get();
        // echo $query->toSql();die;
        $team = [];
        if($data){
            foreach($data as $key => $leave){
                $team[] = array(
                    'name' => $leave->emp_name,
                    'code' => $leave->emp_code,
                    'user_id' => $leave->user_id,
                    'from_date' => date(config('constants.DATE_F'), strtotime($leave->from_date)),
                    'to_date' => date(config('constants.DATE_F'), strtotime($leave->to_date)),
                    'short_name' => GlobalHelpers::ShortName($leave->emp_name),
                    'pr_image' => $leave->profile_image
                );
            }
        } 
        return array('team'=>$team);
    }
}
