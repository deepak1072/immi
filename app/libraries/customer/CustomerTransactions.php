<?php
/** 
 * User: Sandeep Maurya
 * Date: 15/12/2019
 * Time: 8:26 PM
 */
namespace App\libraries\customer;
use GlobalHelpers;   
use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;
class CustomerTransactions 
{
    public function Transaction(Request $request)
    { 
        $limit = ($request->get('pageSize')) ? $request->get('pageSize') : 5;
        $offset = $limit * (($request->get('page')) ? $request->get('page') : 0);
        $filter = ($request->get('filter')) ? $request->get('filter') : [];
        $sort = ($request->get('sort')) ? $request->get('sort') : [];
        
        $query=DB::table("customer");
        $query->selectRaw("id,name,email,mobile,result,customer.created_at,emp_name,`doc_updated`, `crm_update`, `visit`, `online_councelling`, `oncall_councelling`, `cic`, `verification_mail`, `approval`, `hr_finalization`"); 
        $query->join('emp_master','user_id','=','customer.created_by');
       
        if($request->filled('role') && $request->role != 1 ){
            if(  $request->role == 3){  
                $query->where("customer.created_by","=",$request->user_id);
           
            }
        }
        if($request->filled('id') &&  $request->active_role == 3){
            $query->where("customer.created_by","=",$request->id);
        }
        if($request->filled('type')){
            if($request->type != 'total'){
                if($request->type == 'completed'){
                    $query->whereIn("result",['Approved','Rejected']);
                }
                 
                if($request->type == 'pending' || $request->type == 'rejected' || $request->type == 'approved' ){
                    $query->where("result","=",ucfirst($request->type));
                } 
                if($request->type == 'doc'){
                    $query->where("doc_updated","=",1);
                    $query->where("result","=",'Pending');
                }
                if($request->type == 'crm'){
                    $query->where("crm_update","=",1);
                    $query->where("result","=",'Pending');
                }
                if($request->type == 'visit'){
                    $query->where("visit","=",1);
                    $query->where("result","=",'Pending');
                }
                if($request->type == 'online'){
                    $query->where("online_councelling","=",1);
                    $query->where("result","=",'Pending');
                }
                if($request->type == 'oncall'){
                    $query->where("oncall_councelling","=",1);
                    $query->where("result","=",'Pending');
                }
                if($request->type == 'cic'){
                    $query->where("cic","=",1);
                    $query->where("result","=",'Pending');
                }
                if($request->type == 'mail'){
                    $query->where("verification_mail","=",1);
                    $query->where("result","=",'Pending');
                }
                if($request->type == 'approval'){
                    $query->where("approval","=",1);
                    $query->where("result","=",'Pending');
                }
                if($request->type == 'hr'){
                    $query->where("hr_finalization","=",1);
                    $query->where("result","=",'Pending');
                }
            }
        }
        $query->where("is_deleted","=",0);
        
        $start_date = '';
        $end_date = '';
        $today = date('Y-m-d'); 
        $start_date = $today.' 00:00:00' ;
        $end_date = $today.' 23:59:59' ;
        // today
        if($request->get('period') == 1 ){
            $start_date = $today.' 00:00:00' ;
            $end_date = $today.' 23:59:59' ;
        }
        // last seven days
        if($request->get('period') == 7 ){
            $start_date = $today.' 00:00:00' ;
            $start_date = date('Y-m-d H:i:s',strtotime(' -7 days'));
            $end_date = $today.' 23:59:59' ;
        }
        // last 15 days 
        if($request->get('period') == 15 ){
            $start_date = $today.' 00:00:00' ;
            $start_date = date('Y-m-d H:i:s',strtotime(' -15 days'));
            $end_date = $today.' 23:59:59' ;
        }
        // last 30 days 
        if($request->get('period') == 30 ){
            $start_date = $today.' 00:00:00' ;
            $start_date = date('Y-m-d H:i:s',strtotime(' -30 days'));
            $end_date = $today.' 23:59:59' ;
        }
        if($request->filled('period')){
            $query->whereBetween("customer.created_at",[$start_date,$end_date]);
        }
        
        // applying filter 
        if(count($filter) > 0){

            foreach ($filter as $key => $value) {
                
               $filtered = json_decode($value);
                if($filtered->id != 'pending_at'){
                    $searchValue =  strtolower(htmlspecialchars(stripcslashes(trim($filtered->value))));
                    $id  =  " lower(".$filtered->id.") ";
                    $where  = "".$id ." like ('%$searchValue%') " ;
                    if($key == 0){
                        $query->whereRaw($where);
                    }else{
                        $query->orWhere($where); 
                    }
                }
            }
        }

        // applying sorting
        if(count($sort) > 0){
            foreach ($sort as $key => $value) {
                $sorted = json_decode($value);
                $query->orderBy($sorted->id,($sorted->desc)?'desc':'asc');
            }
        } 
        $count = $query->count();
        $query->offset($offset);
        $query->limit($limit);
        $data= $query->get();
        // echo $query->toSql();
        return array('count'=>$count,'data'=>$data);
    }
    
    public function _request(Request $request){
        
        $res = array(
            'result'=>false,
            'message'=>'',
            'type' => 'warn',
            'status' => 200
        );
        $date_str = strtotime($request->post('date'));
         
        
        $this->od['updated_at'] = date(config('constants.DATE_TIME')); 
        
        $this->od['name'] = $request->post('name');
        $this->od['mobile'] = $request->post('mobile');
        $this->od['email'] = $request->post('email');
        $this->od['location'] = $request->post('location');
        $this->od['doc_updated'] = $request->post('doc_updated');
        if( $request->post('doc_updated') != 1){
            if( $request->post('action') != 'Update' ){
                $this->od['doc_time'] = date(config('constants.DATE_TIME'));
            }
        }
        $this->od['crm_update'] = $request->post('crm_update');
        if( $request->post('crm_update') != 1){
            if( $request->post('action') != 'Update' ){
                $this->od['crm_time'] = date(config('constants.DATE_TIME'));
            }
        }
        $this->od['visit'] = $request->post('visit');
        if( $request->post('visit') != 1){
            if( $request->post('action') != 'Update' ){
                $this->od['visit_time'] = date(config('constants.DATE_TIME'));
            }
        }
        $this->od['online_councelling'] = $request->post('online_councelling');
        if( $request->post('online_councelling') != 1){
            if( $request->post('action') != 'Update' ){
                $this->od['online_time'] = date(config('constants.DATE_TIME'));
            }
        }
        $this->od['oncall_councelling'] = $request->post('oncall_councelling');
        if( $request->post('oncall_councelling') != 1){
            if( $request->post('action') != 'Update' ){
                $this->od['oncall_time'] = date(config('constants.DATE_TIME'));
            }
        }
        $this->od['cic'] = $request->post('cic');
        if( $request->post('cic') != 1){
            if( $request->post('action') != 'Update' ){
                $this->od['cic_time'] = date(config('constants.DATE_TIME'));
            }
        }
        $this->od['verification_mail'] = $request->post('verification_mail');
        if( $request->post('verification_mail') != 1){
            if( $request->post('action') != 'Update' ){
                $this->od['mail_time'] = date(config('constants.DATE_TIME'));
            }
        }
        $this->od['approval'] = $request->post('approval');
        if( $request->post('approval') != 1){
            if( $request->post('action') != 'Update' ){
                $this->od['approval_time'] = date(config('constants.DATE_TIME'));
            }
        }
        $this->od['hr_finalization'] = $request->post('hr_finalization');
        if( $request->post('hr_finalization') != 1){
            if( $request->post('action') != 'Update' ){
                $this->od['hr_time'] = date(config('constants.DATE_TIME'));
            }
        }
        $this->od['result'] = $request->post('result'); 
        if( $request->post('result') != 1){
            if( $request->post('action') != 'Update' ){
                $this->od['result_time'] = date(config('constants.DATE_TIME'));
            }
        }
        $this->od['doc_comments'] = $request->post('doc_comments') ?? '';
        $this->od['crm_comments'] = $request->post('crm_comments') ?? '';
        $this->od['visit_comments'] = $request->post('visit_comments') ?? '';
        $this->od['online_comments'] = $request->post('online_comments') ?? '';
        $this->od['oncall_comments'] = $request->post('oncall_comments') ?? '';
        $this->od['cic_comments'] = $request->post('cic_comments') ?? '';
        $this->od['mail_comments'] = $request->post('mail_comments') ?? '';
        $this->od['approval_comments'] = $request->post('approval_comments') ?? '';
        $this->od['hr_comments'] = $request->post('hr_comments') ?? '';
        $this->od['result_comments'] = $request->post('result_comments') ?? '';
        // dd($request->all());
        $this->od['is_deleted'] = 0;
        if($request->filled('action') && $request->post('action') == 'Update'){
            // update 
             
            DB::beginTransaction();
            try {
                DB::table('customer') 
                    ->where('id',$request->post('id'))
                    ->update($this->od);
                DB::commit();
                $res['result'] = true;
            } catch (\Exception $e) {
                DB::rollback(); 
                $res['result'] = false;
            }
                    
            if( $res['result'] ){
                
                $res['message'] = 'Updated Successfully';
                $res['type'] = 'success';
                return $res;
            }else{
                $res['message'] = 'Could not update, Please try after sometime';
                return $res;
            }
        }else{
            //create new request
            $this->od['created_at'] = date(config('constants.DATE_TIME'));
            $this->od['created_by'] = $request->post('user_id');
            DB::beginTransaction();
            try {
                DB::table('customer')
                    ->insert($this->od);
                DB::commit(); 
                $res['result'] = true;
            } catch (\Exception $e) {
                DB::rollback();
                echo $e->getMessage();die;
                $res['result'] = false; 
            } 
            if($res['result']){
                
                $res['message'] = 'Created Successfully';
                $res['type'] = 'success';
                return $res;
            }else{
                $res['message'] = 'Could not create, Please try after sometime';
                return $res;
            }
        } 
    }

    public function TransactionDetails(Request $request){
        $query=DB::table("customer");
        $query->selectRaw("id,name,email,mobile,location,result,doc_comments,crm_comments,visit_comments,online_comments,oncall_comments,cic_comments,mail_comments,approval_comments,hr_comments,result_comments,created_at, doc_updated, crm_update, visit, online_councelling, oncall_councelling, cic, verification_mail, approval, hr_finalization, result, created_by, updated_at, is_deleted,doc_time,crm_time,cic_time,visit_time,oncall_time,online_time,mail_time,approval_time"); 
        $query->where('id',$request->get('id'));
        $data= $query->first();
        // dd($request->all(), $data);
        return $data;
       
    }

    public function GetDeadLineRules(Request $request){
        $query=DB::table("deadline");
        $query->selectRaw(" dl_doc,dl_crm,dl_cic,dl_visit,dl_oncall,dl_online,dl_mail,dl_approval"); 
        $data= $query->first();
        return $data;
       
    }
    
}
