<?php
/** 
 * User: Sandeep Maurya
 * Date: 15/12/2019
 * Time: 8:26 PM
 */
namespace App\libraries\reports;
use GlobalHelpers; 
use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;
class ImportExports 
{
    public function Transaction(Request $request)
    { 
        $limit = ($request->get('pageSize')) ? $request->get('pageSize') : 5;
        $offset = $limit * (($request->get('page')) ? $request->get('page') : 0);
        $filter = ($request->get('filter')) ? $request->get('filter') : [];
        $sort = ($request->get('sort')) ? $request->get('sort') : [];

        $query = DB::table("import_export  as ie");
        $query->selectRaw("ie.id,ie.name,ie.type,em.emp_name,ie.status,ie.module,ie._file,ie.file_link,ie.created_at,ie.updated_at");
        $query->join('emp_master as em','em.user_id','=','ie.user_id');
        $query->where("ie.user_id","=",$request->user_id);
        
        // applying filter 
        if(count($filter) > 0){
            foreach ($filter as $key => $value) {
                $filtered = json_decode($value);
                 $searchValue =  strtolower(htmlspecialchars(stripcslashes(trim($filtered->value))));
 
                 $id  = ($filtered->id == 'emp_name') ? " lower(em.".$filtered->id.") " : " lower(ie.".$filtered->id.") ";
                 $where  = "".$id ." like ('%$searchValue%') " ;
                 if($key == 0){
                     $query->whereRaw($where);
                 }else{
                     $query->orWhere($where); 
                 }
            }
        }

        // applying sorting
        if(count($sort) > 0){
            foreach ($sort as $key => $value) {
                $sorted = json_decode($value);
                $query->orderBy($sorted->id,($sorted->desc)?'desc':'asc');
            }
        } 
        $count = $query->count();
        $query->offset($offset);
        $query->limit($limit);
        $data= $query->get();
        return array('count'=>$count,'data'=>$data);
    }

    public function Latest(Request $request){
        $query = DB::table("import_export  as ie");
        $query->selectRaw("ie.id,ie.name,ie.type,em.emp_name,ie.status,ie.module,ie._file,ie.file_link,ie.created_at,ie.updated_at");
        $query->join('emp_master as em','em.user_id','=','ie.user_id');
        $query->where("ie.user_id","=",$request->user_id);
        $query->orderBy('created_at','desc');
        $query->limit(5);
        $data= $query->get();
        return array('data'=>$data);
    }

    public function Submit($options){
        DB::beginTransaction();
        try {
            $id = DB::table('import_export') 
                ->insertGetId($options);
            DB::commit();
            return $id ;
        } catch (\Exception $e) { 
            DB::rollback();
            return false;
        }
    }

    public function updateStatus($id,$status,$desc = '_',$error_link =''){
        $options = array(
            'status' => $status,
            'descriptions' => $desc,
            'error_link' => $error_link
        );
        DB::beginTransaction();
        try {
            $id = DB::table('import_export') 
                ->where('id',$id)
                ->update($options);
            DB::commit();
            return true ;
        } catch (\Exception $e) {
            DB::rollback();
            return false;
        }
    }
    public function updateStatusSuccess($id,$status,$desc = '_',$file_link =''){
        $options = array(
            'status' => $status,
            'descriptions' => $desc,
            'file_link' => $file_link

        );
        DB::beginTransaction();
        try {
            $id = DB::table('import_export') 
                ->where('id',$id)
                ->update($options);
            DB::commit();
            return true ;
        } catch (\Exception $e) {
            DB::rollback();
            return false;
        }
    }
}
