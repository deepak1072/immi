<?php
/**
 * Created by PhpStorm.
 * User: Sandeep Maurya
 */
namespace App\libraries\workflows;
use GlobalHelpers; 
use Illuminate\Support\Facades\DB;  
class Workflow
{
    private $wf_id ;
    function __construct($wf_id)
    {
        $this->wf_id = $wf_id ;
    }

    public function getLevelInfo($level)
    {
        return DB::table('workflow_levels')
            ->selectRaw('`wf_level_id`, `wf_id`, `wf_level`, `approver`, `requ_noti`, `approver_noti`, `next_level_noti`, `days_limit`, `fixed_cc`, `fixed_bcc`, `dynamic_cc`, `dynamic_bcc`')
            ->where('wf_id',$this->wf_id)
            ->where('wf_level',$level)
            ->first();
    } 
}