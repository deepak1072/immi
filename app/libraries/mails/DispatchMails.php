<?php  
/**
 * created by Sandeep Kumar Maurya 
 * @2:47pm 24nd March 2020
 */
namespace App\libraries\mails;
use Illuminate\Support\Facades\Mail;
use App\Mail\leave\CreateLeaveRequest;
class DispatchMails{
    private $data ;
    function __construct($data)
    {
        $this->data = $data;
    }

    function __invoke(){
        // $mail_instance = new 
        $instance = $this->getMailInstance() ;

        $mail = Mail::to($this->data['to']);
        if(isset($this->data['cc']) && !empty($this->data['cc'])){
            $mail->cc($this->data['cc']);
        }
        if(isset($this->data['bcc']) && !empty($this->data['bcc'])){
            $mail->bcc($this->data['bcc']);
        }
        if(isset($this->data['send']) && $this->data['send'] == 1){
            // $mail->send(new OrderShipped($this->data));
            // $mail->send($instance);
        }else{
            // $mail->queue($instance);
        } 
    }

    function getMailInstance(){
        $instance = null ; 
        switch($this->data['instance']){
            case 'CreateLeaveRequest' : 
                $instance = new CreateLeaveRequest($this->data);
                break ;
            default :
                $instance = null;
                break ;    
        }   
        return $instance ;
    }
}