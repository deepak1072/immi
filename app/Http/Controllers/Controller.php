<?php

namespace App\Http\Controllers;

use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
date_default_timezone_set('Asia/Kolkata');
class Controller extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;
    private $activity_time = '';
    public $res  = [
        'result'=> false,
        'message'=>'', 
        'status'=> 200,
        'type' => 'warn'
    ];
}
