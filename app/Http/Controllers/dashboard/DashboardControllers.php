<?php
/**
 * Created by PhpStorm.
 * User: Sandeep Maurya
 * Date: 19/1/2020
 * Time: 4:27 PM
 */
namespace App\Http\Controllers\dashboard;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;  
use App\libraries\dashboard\Dashboard;
/**
 * @Written by Sandeep Kumar Maurya
 * @ 4:03 PM 2nd March 2019
 */

class DashboardControllers extends Controller {
    public $res  = [
        'result'=> false,
        'message'=>'', 
        'status'=> 200,
        'type' => 'warn'
    ];
    function __construct(){
        $this->activity_time = date('Y-m-d H:i:s');
    }
    
    /**
     *   dashboard count 
     */
    public function ClientCount(Request $request){
        $leave = new Dashboard();
        $data['data'] = $leave->ClientCount($request);
        $data['result'] = true;
        return response()->json($data);
    }
    public function AgentsHistory(Request $request){
        $leave = new Dashboard();
        $data['data'] = $leave->AgentsHistory($request);
        $data['result'] = true;
        return response()->json($data);
    }
}