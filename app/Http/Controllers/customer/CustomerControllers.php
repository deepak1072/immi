<?php
/**
 * Created by PhpStorm.
 * User: Sandeep Maurya
 * Date: 19/1/2020
 * Time: 4:27 PM
 */
namespace App\Http\Controllers\customer;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;  
use App\libraries\customer\CustomerTransactions;
/**
 * @Written by Sandeep Kumar Maurya
 * @ 4:03 PM 2nd March 2019
 */

class CustomerControllers extends Controller {
    public $res  = [
        'result'=> false,
        'message'=>'', 
        'status'=> 200,
        'type' => 'warn'
    ];
    function __construct(){
        $this->activity_time = date('Y-m-d H:i:s');
    }

 

    /**
     *   transactions
     */
    public function Transactions(Request $request){
        $leave = new CustomerTransactions();
        $data = $leave->Transaction($request);
        $data['result'] = true;
        if($data['data']){
            $new_data = array();
            foreach ( $data['data'] as $key => $leave ) { 
                //id,name,email,mobile,result,
                $pending_at = '';
                if( $leave->doc_updated != 1 ){
                    $pending_at = 'DOC Update';
                }else if( $leave->crm_update != 1 ){
                    $pending_at = 'CRM Update';
                }else if( $leave->visit != 1 ){
                    $pending_at = 'Visit';
                }else if( $leave->online_councelling != 1 ){
                    $pending_at = 'Online Counselling';
                }else if( $leave->oncall_councelling != 1 ){
                    $pending_at = 'Oncall Counselling';
                }else if( $leave->cic != 1 ){
                    $pending_at = 'CIC';
                }else if( $leave->cic != 1 ){
                    $pending_at = 'CIC';
                }else if( $leave->verification_mail != 1 ){
                    $pending_at = 'Mail Verification ';
                }else if( $leave->approval != 1 ){
                    $pending_at = 'Approval ';
                }else if( $leave->hr_finalization != 1 ){
                    $pending_at = 'HR Finalization ';
                }                  
                
                array_push($new_data,array(
                    'id'=> $leave->id,
                    'name' => $leave->name,
                    'email' => $leave->email,
                    'emp_name' => $leave->emp_name,
                    'mobile' => $leave->mobile,  
                    'result' => $leave->result,
                    'pending_at' => $pending_at,  
                    'created_at' => date(config('constants.DATE_F'), strtotime($leave->created_at))
                ));
            }
            $data['data'] = $new_data;
        }
        return response()->json($data);
    }

    /**
     *   transaction details 
     */
    public function ClientsData(Request $request){
        $validator = Validator::make($request->all(), [
            'req_user_id' => 'required' , 
            'id' => 'required' ,
        ]);
 
        if($validator->fails()) {
            $error = $validator->errors()->all();
            $this->res['message'] =$error[0] ;
            return response()->json($this->res);
        }else{ 
            $leave = new CustomerTransactions();
            $data = $leave->TransactionDetails($request);
            $rules =  $leave->GetDeadLineRules($request);
            
            if($data){
                $data->doc_status  = ' green-2 btn-floating note_btn ';
                if( $data->doc_updated == 0){
                    
                    if(strtotime($data->doc_time.' +'.$rules->dl_doc.' hours') < strtotime($this->activity_time)){
                        $data->doc_status  = ' saffron btn-floating note_btn ';
                    }
                }
                $data->crm_status  = ' green-2 btn-floating note_btn ';
                if( $data->crm_update == 0){
                    if(strtotime($data->crm_time.' +'.$rules->dl_crm.' hours') < strtotime($this->activity_time)){
                        $data->crm_status  = ' saffron btn-floating note_btn ';
                    }
                }
                $data->visit_status  = ' green-2 btn-floating note_btn ';
                if( $data->visit == 0){
                    if(strtotime($data->visit_time.' +'.$rules->dl_visit.' hours') < strtotime($this->activity_time)){
                        $data->visit_status  = ' saffron btn-floating note_btn ';
                    }
                }
                $data->online_status  = ' green-2 btn-floating note_btn ';
                if( $data->online_councelling == 0){
                    if(strtotime($data->online_time.' +'.$rules->dl_online.' hours') < strtotime($this->activity_time)){
                        $data->online_status  = ' saffron btn-floating note_btn ';
                    }
                }
                $data->oncall_status  = ' green-2 btn-floating note_btn ';
                if( $data->oncall_councelling == 0){
                    if(strtotime($data->oncall_time.' +'.$rules->dl_oncall.' hours') < strtotime($this->activity_time)){
                        $data->oncall_status  = ' saffron btn-floating note_btn ';
                    }
                }
                $data->cic_status  = ' green-2 btn-floating note_btn ';
                if( $data->	cic == 0){
                    if(strtotime($data->cic_time.' +'.$rules->dl_cic.' hours') < strtotime($this->activity_time)){
                        $data->cic_status  = ' saffron btn-floating note_btn ';
                    }
                }
                $data->mail_status  = ' green-2 btn-floating note_btn ';
                if( $data->verification_mail == 0){
                    if(strtotime($data->mail_time.' +'.$rules->dl_mail.' hours') < strtotime($this->activity_time)){
                        $data->mail_status  = ' saffron btn-floating note_btn ';
                    }
                }
                $data->app_status  = ' green-2 btn-floating note_btn ';
                if( $data->approval == 0){
                    if(strtotime($data->approval_time.' +'.$rules->dl_approval.' hours') < strtotime($this->activity_time)){
                        $data->app_status  = ' saffron btn-floating note_btn ';
                    }
                }
               
                $this->res['data'] = $data ;
                $this->res['result'] = true;
            }else{
                $this->res['message'] = 'No record(s) found ! ';
            } 
            return response()->json($this->res);
        }
    }

   

    public function Clients(Request $request){
        $validator = Validator::make($request->all(), [
            'req_user_id' => 'required' , 
            'name' => 'required|max:255' , 
            'mobile' => 'required|numeric|digits_between:10,12',
            'email' => 'email',
            'location' => 'required|max:255',
            'doc_comments' => 'max:255',
            'crm_comments' => 'max:255',
            'visit_comments' => 'max:255',
            'online_comments' => 'max:255',
            'oncall_comments' => 'max:255',
            'cic_comments' => 'max:255',
            'mail_comments' => 'max:255',
            'approval_comments' => 'max:255',
            'hr_comments' => 'max:255'
        ]);
 
        if($validator->fails()) {
            $error = $validator->errors()->all();
            $this->res['message'] =$error[0] ;
            return response()->json($this->res);
        }else{
            // condition for approval
            if($request->post('result') == 'Approved'){
                if( $request->post('doc_updated') != 1){
                    $this->res['message'] = 'Application could not be Approved as Document Upload is not done!';
                    return response()->json($this->res);
                }
                if( $request->post('crm_update') != 1){
                    $this->res['message'] = 'Application could not be Approved as CRM is not done!';
                    return response()->json($this->res);
                }
                if( $request->post('cic') != 1){
                    $this->res['message'] = 'Application could not be Approved as CIC is not done!';
                    return response()->json($this->res);
                }
                if( $request->post('verification_mail') != 1){
                    $this->res['message'] = 'Application could not be Approved as Mail Verification is not done!';
                    return response()->json($this->res);
                }
                if( $request->post('approval') != 1){
                    $this->res['message'] = 'Application could not be Approved as Approval is not done!';
                    return response()->json($this->res);
                }
                if( $request->post('hr_finalization') != 1){
                    $this->res['message'] = 'Application could not be Approved as HR Final Call is not done!';
                    return response()->json($this->res);
                }
            }
            // condition for rejections 
            if($request->post('result') == 'Rejected'){
                if(empty($request->post('result_comments'))){
                    $this->res['message'] = 'Comment for Result is Mandatory for Rejection !';
                    return response()->json($this->res);
                }
            }
            $leave  = new CustomerTransactions();
            $this->res =  $leave->_request($request);
            return response()->json($this->res);
        }
    }
 
    
}