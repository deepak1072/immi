<?php
/**
 * Created by PhpStorm.
 * User: Sandeep Maurya
 * Date: 21 march 2020
 * Time: 4:27 PM
 */
namespace App\Http\Controllers\attendance;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator; 
use App\libraries\attendance\OdApproval; 
use App\libraries\attendance\OdTransactions;
/**
 * @Written by Sandeep Kumar Maurya
 * @ 11:03 AM 21st March 2020
 */

class OdApprovalControllers extends Controller {
    public $res  = [
        'result'=> false,
        'message'=>'', 
        'status'=> 200,
        'type'=>'warn'
    ];
    function __construct(){
        $this->activity_time = date('Y-m-d H:i:s');
    }

    public function Approve(Request $request){
        $valid_input = array( 
            'id' => 'required',
            'status' => 'required', 
            'trx_id' => 'required',
            'wf_id' => 'required' 
        );
        if(in_array($request->status,[3,4,6,8,7])){
            $valid_input['remarks'] = 'required';
        }
        $validator = Validator::make($request->all(), $valid_input);
         
        if($validator->fails()) {
            $error = $validator->errors()->all();
            $this->res['message'] = $error[0];
            return response()->json($this->res);
        }else{
            $approval = new OdApproval();
            $options = array(
                'od_id' => $request->id, 
                'remarks' => $request->remarks,
                'status' => $request->status ,
                'trx_id' => $request->trx_id ,
                'wf_id' => $request->wf_id,
                'req_user_id' => $request->req_user_id,
                'user_id' => $request->user_id,
                'activity_time'=>$this->activity_time
            );
            switch($request->status){
                case 2 :  
                        $res = $approval->ApproveRequest($options);
                        break ; 
                case 3 : 
                        $res = $approval->RejectRequest($options);
                        break ;
                case 4 : 
                        $res = $approval->CancelRequest($options);
                        break ;
                case 6 :  
                        $res = $approval->ApproveRequest($options);
                        break;
                case 7 :  
                        $res = $approval->RejectRequest($options);
                        break ;
                default : 
                    $res = $approval->ApproveRequest($options);
            }
            return response()->json($res);
        }
    }

    /**
     * leave transactions
     */
    public function Transactions(Request $request){
        $leave = new OdTransactions();
        $data = $leave->Transaction($request);
        $data['result'] = true;
        if($data['data']){
            $new_data = array();
            foreach ( $data['data'] as $key => $mark ) { 
                array_push($new_data,array(
                    'od_id'=> $mark->od_id,
                    'trx_date' =>date(config('constants.DATE_TIME_F'), strtotime($mark->trx_date)) ,
                    'emp_name' => $mark->emp_name,
                    'emp_code' => $mark->emp_code,
                    'user_id' => $mark->user_id,
                    'od_date' => date(config('constants.DATE_F'), strtotime($mark->od_date)),
                    'id' => $mark->id,
                    'reason' => $mark->reason,
                    'final_status_text' => $mark->final_status_text 
                ));
            }
            $data['data'] = $new_data;
        }

        return response()->json($data);
    }

    /**
     * leave transaction details 
     */
    public function Details(Request $request){
        $validator = Validator::make($request->all(), [
            'req_user_id' => 'required' , 
            'id' => 'required' , 
            'trx_id' => 'required'   , 
        ]);
 
        if($validator->fails()) {
            $error = $validator->errors()->all();
            $this->res['message'] =$error[0] ;
            return response()->json($this->res);
        }else{ 
            $leave = new OdTransactions();
            $data = $leave->Details($request);
            if($data){
                $this->res['data'] = $data ;
                $this->res['result'] = true;
            }else{
                $this->res['message'] = __('attendance.error_no_od_details');
            } 
            return response()->json($this->res);
        }
    }    
}