<?php

	namespace App\Http\Controllers\attendance;
    use App\models\attendance\RosterModel;
	use App\Http\Controllers\Controller;
    use Illuminate\Http\Request; 
    use Illuminate\Support\Facades\Validator; 
        
    /**
     * @Written by Sandeep Kumar Maurya 
     * @ 4:03 PM 8th Feb 2020
     */

    class Roster extends Controller {
        public $res = [
            'result' => false,
            'message' => '',
            'data' => [],
            'status' => 200
        ];
        private $date = '';
        private $roster_model ;
        function __construct()
        {
            $this->date = date('Y-m-d H:i:s');
            $this->roster_model = new RosterModel();
        }

        public function index(Request $request){
            $validator = Validator::make($request->all(), [
                'month'     =>  'required',
                'year'     =>  'required',
                // 'ids' => 'required',
                'page' => 'required'
            ]); 
            if($validator->fails()){
                $this->res['message'] = $validator->errors()->all();
                return response()->json($this->res);
            }else{
                $page = 1; 
                $user = 1;
                $chunk_number = $page -1 ; 
                $month = $request->month + 1;
                $year = $request->year;
                $data = array(
                    'month' => $month,
                    'users' => $user,
                    'year' => $year
                );
                $rost = $this->roster_model->assignedRoster($data);
                $roster = [];
                if(count($rost) > 0){
                    foreach($rost as $key => $ros){
                        // $roster['user_id'] = $ros->user_id;
                        // $roster['name'] = $ros->emp_name;
                        // $roster['code'] = $ros->emp_code;
                        // $roster['text'] = $ros->emp_name."(".$ros->emp_code.")";
                        // $roster['roster'][$key]['title'] = "".date(config('constants.TIME_2'),strtotime($ros->shift_start_mandatory))."-".date(config('constants.TIME_2'),strtotime($ros->shift_end_mandatory))."" ;
                        $roster['roster'][$key]['title'] =  $ros->shift_name ;
                        // $roster['roster'][$key]['general'] = $ros->general_shift;
                        // $roster['roster'][$key]['name'] = $ros->shift_name;
                        // $roster['roster'][$key]['id'] = $ros->shift_id;
                        $roster['roster'][$key]['start'] = $ros->roster_date;
                        if($ros->general_shift == 1){
                            $roster['roster'][$key]['textColor'] = '#f7f7f7';
                            $roster['roster'][$key]['backgroundColor'] = '#0098a6';
                            $roster['roster'][$key]['borderColor'] = '#0098a6';
                        }else{
                            $roster['roster'][$key]['textColor'] = '#aaa';
                            $roster['roster'][$key]['backgroundColor'] = '#f7f7f7';
                            $roster['roster'][$key]['borderColor'] = '#f7f7f7';
                        }   
                    }
                    $this->res['result'] = true;
                    $this->res['data'] = $roster;
                }else{
                    $this->res['message'] = __('settings.error_no_records');
                }
                return response()->json($this->res);
            }
        }   
    }