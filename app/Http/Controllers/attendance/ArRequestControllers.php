<?php
/**
 * Created by PhpStorm.
 * User: Sandeep Maurya
 * Date: 29 feb 2020
 * Time: 8:30 AM
 */
namespace App\Http\Controllers\attendance;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use GlobalHelpers;
use ConfigHelpers;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use App\libraries\attendance\AttendanceApprover;
use App\libraries\attendance\AttendanceRequest;

/**
 * @Written by Sandeep Kumar Maurya
 * @ 4:03 PM 2nd March 2019
 */

class ArRequestControllers extends Controller {
    function __construct(){

    }

    public function SubmitRequest(Request $request){
        $validator = Validator::make($request->all(), [
            'req_user_id' => 'required',
            'date' => 'required|date',
            'in_time' => 'required|date',
            'out_time' => 'required|date',
            'action' => 'required',
            'type' =>'required',
            'reason' => 'required|max:255',
            'reason_type' => 'required|numeric'
        ]);
        if($validator->fails()) {
            $error = $validator->errors()->all();
            return response()->json([
                'result'=> false,
                'message'=>$error[0],
                'type' => 'warn',
                'status'=> 200
            ]);
        }else{
            // $date = date('Y-m-d h:i:s',strtotime($request->in_time));
            // dd($request->all(),$date);
            $requ = new AttendanceRequest();
            $res = $requ->_request($request); 
            return response()->json($res);
        }
    }

    /**
     * get approver 
     */
    public function Approvers(Request $request){

        $validator = Validator::make($request->all(), [
            'req_user_id' => 'required|int' ,
            'events' => 'required|int'   
        ]);
        if($validator->fails()) {
            return response()->json([
                'result'=> false,
                'error'=>__('notifications.user_code_error'),
                'error_details'=>$validator->errors() ,
                'status'=> 200
            ]);
        }else{ 
            $approver = new AttendanceApprover();
            $data = array(
                'req_user_id' => $request->get('req_user_id'),
                'events' =>$request->get('events')
            );
            return response()->json([
                'result'=> true,
                'approvers'=>$approver->_approvers($data) ,
                'status' => 200
            ]);
        }
    }

    /**
     * rule 
     */
    public function Rule(Request $request){
        $validator = Validator::make($request->all(), [
            'req_user_id' => 'required|int' ,   
        ]);
        $type  = ($request->type) ? $request->type : 'mark_reason';
        if($validator->fails()) {
            return response()->json([
                'result'=> false,
                'error'=>__('notifications.user_code_error'),
                'error_details'=>$validator->errors() ,
                'status'=> 200
            ]);
        }else{
            $_rule = array();
            $_rule['commencement'] = 60;
            $reason = ConfigHelpers::configAll($type);
            $_rule['reason_type'] = $reason;
            // mark_reason
            if(!empty($_rule)){
                return response()->json([
                    'result'=> true,
                    'data'=>$_rule ,
                    'status' => 200
                ]);
            } 
            return response()->json([
                'result'=> false,
                'error'=>__('attendance.no_rule') ,
                'status' => 200
            ]);
        }
    }
}