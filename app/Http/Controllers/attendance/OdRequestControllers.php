<?php
/**
 * Created by PhpStorm.
 * User: Sandeep Maurya
 * Date: 29 feb 2020
 * Time: 8:30 AM
 */
namespace App\Http\Controllers\attendance;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use GlobalHelpers;
use ConfigHelpers;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use App\libraries\attendance\AttendanceApprover;
use App\libraries\attendance\OdRequest;

/**
 * @Written by Sandeep Kumar Maurya
 * @ 4:03 PM 2nd March 2019
 */

class OdRequestControllers extends Controller {
    function __construct(){

    }

    public function SubmitRequest(Request $request){
        $validator = Validator::make($request->all(), [
            'req_user_id' => 'required',
            'date' => 'required|date',
            'from_type' => 'required',
            'to_type' => 'required',
            'action' => 'required',
            'type' =>'required',
            'reason' => 'required|max:255',
            'reason_type' => 'required|numeric'
        ]);
        if($validator->fails()) {
            $error = $validator->errors()->all();
            return response()->json([
                'result'=> false,
                'message'=>$error[0],
                'type' => 'warn',
                'status'=> 200
            ]);
        }else{
            
            $requ = new OdRequest();
            $res = $requ->_request($request); 
            return response()->json($res);
        }
    }
}