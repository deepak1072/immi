<?php

// Created By Sandeep Maurya @ 15th Dec 2018
namespace App\Http\Controllers\admin_settings;

use App\models\admin_settings\RegionsSettingsModel;
use Validator;
use Illuminate\Validation\Rule;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class RegionsSettings extends Controller
{
    //
    function __construct()
    {

    }

    public function AddUpdateRegions(Request $request){
    	 


        $validator = Validator::make($request->all(), [
            'regions_name'   =>'required',
            'company_code'     =>  'required',
            'regions_code'      =>  'required|max:255' ,
            'action' => 'required'
        ]);

      

        if($validator->fails()){
            return response()->json(array('result'=>false,'error'=>$validator->errors()->all()));
        }  else{
        	if($request->post('action') == 'Create'){
        		// create new regions 
        		$result = RegionsSettingsModel::CheckRegionsAvailability($request);
        		
	            if($result === true){
	            	$result1 = RegionsSettingsModel::AddRegions($request);
	            	if($result1){
	            		$message = "Regions created successfully";
	            	}else{
	            		$message = "Regions could not be created , please try after sometime ";
	            	}
	               	
	               	return response()->json(array('result'=>true,'message'=>$message));
	            }else{
	               	$message = array(" Regions with same code have been created already , please try with different data set  ");
	               	return response()->json(array('result'=>false,'error'=>$message));
	            }
	            
        	}else{
        		// update regionss 
        		$result = RegionsSettingsModel::UpdateRegions($request);
	            if($result){
	               $message = "Regions updated successfully";
	            }else{
	               $message = "Regions could not be updated , please try after some time  ";
	            }
	            return response()->json(array('result'=>true,'message'=>$message));
        	}
            
        }
    }


    /**
     * regionss list
     */
    public function fetchRegionsList(Request $request){
    	$data = RegionsSettingsModel::fetchRegionsList($request);
        return response()->json($data);
    }

    /**
     * update regions status 
     */
    public function UpdateRegionsStatus(Request $request){
    	$data = RegionsSettingsModel::UpdateRegionsStatus($request);
        return response()->json($data);
    }

    /**
     * get regions details 
     */
    public function GetRegionsDetails(Request $request){
    	$data  = RegionsSettingsModel::GetRegionsDetails($request);
    	return response()->json($data);
    }
}
