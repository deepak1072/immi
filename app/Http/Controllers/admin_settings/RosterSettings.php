<?php

// Created By Sandeep Maurya @ 08th Feb 2020
namespace App\Http\Controllers\admin_settings;
use App\models\admin_settings\RosterSettingsModel;
use Validator;
use Illuminate\Validation\Rule;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\interfaces\ActivityInterface;
use App\Jobs\RosterScheduler;
class RosterSettings extends Controller implements ActivityInterface
{
	public $res = [
		'result' => false,
		'message' => '',
		'data' => ''
	];
    private $date = '';
    function __construct()
    {
        $this->date = date('Y-m-d H:i:s');
    }
	public function UpdateStatus(Request $request){
		$data = RosterSettingsModel::updateRosterStatus($request);
        return response()->json($data);
	}
	public function AddUpdate(Request $request){
		$validator = Validator::make($request->all(), [
            'name'   =>'required|max:255',
			'is_fixed'     =>  'required',
			'pattern' => 'required'
        ]); 
        if($validator->fails()){
			$this->res['message'] = $validator->errors()->all();
            return response()->json($this->res);
        }else{
			$name =  RosterSettingsModel::checkRoster($request->name);
			if(! $name ){
				$this->res['message'] = __('settings.roster_duplicate');
				return response()->json($this->res);
			}
			$flag = false;  
			$roster = array(
				'rp_name' => $request->name,
				'is_fixed' => $request->is_fixed,
				'created_by' => $request->user_id,
				'created_at' => $this->date,
				'updated_by' => $request->user_id,
				'updated_at' => $this->date
			); 
			if($request->is_fixed == 1){
				foreach($request->pattern as $key=>$pattern){
					if($key == 0){
						continue;
					}
					if(empty($pattern) || is_null($pattern)){
						$flag = true ;
					}
				} 
			}else{
				foreach($request->pattern as $key => $days){
					if($key == 0){
						continue;
					}
					foreach($days as $key=> $val){
						if(empty($val) || is_null($val)){
							$flag = true ;
						}
					}
				}
			}
			if( ! $flag ){
				$rp_id = RosterSettingsModel::createRoster($roster);
				if($rp_id){
					$pattern = [];
					if($request->is_fixed == 1){
						foreach($request->pattern as $key=>$pat){
							if($key == 0){
								continue;
							}
							$pattern[] = array(
								'rp_id' => $rp_id,
								'day' => $key, 
								'first_week'=> $pat, 
								'second_week' => $pat, 
								'third_week' => $pat, 
								'fourth_week' => $pat, 
								'fifth_week' => $pat, 
								'created_at' => $this->date
							);
						}
					}else{
						foreach($request->pattern as $key => $days){
							if($key == 0){
								continue;
							}
							$pattern[] = array(
								'rp_id' => $rp_id,
								'day' => $key, 
								'first_week'=> $days[0], 
								'second_week' =>$days[1], 
								'third_week' => $days[2], 
								'fourth_week' => $days[3], 
								'fifth_week' => $days[4], 
								'created_at' => $this->date
							);
						}
					}
					$rsch = RosterSettingsModel::createPattern($pattern);
					if($rsch){
						$this->res['result'] =true;
						$this->res['message'] = __('settings.success_creation');
					}else{
						RosterSettingsModel::deleteInvalidRoster($rp_id);
						$this->res['message'] = __('settings.error_creation');
					}
				}else{
					$this->res['message'] = __('settings.error_creation');
				}
				return response()->json($this->res);
			}else{
				$this->res['message'] = __('settings.incomplete');
			}
			return response()->json($this->res);
		}
	}

	public function FetchList(Request $request){
        $data = RosterSettingsModel::getRoster($request);
        return response()->json($data);
	}

	public function GetDetails(Request $request){
		$data  = RosterSettingsModel::getRosterDetails($request);
		$roster = [];
		if(count($data) > 0){
			if($data[0]->is_fixed == 1){
				$roster['shift'][] = '';
				$roster['action'] = 'Update';
				foreach($data as $key => $rost){
					$roster['p_name'] = $rost->rp_name;
					$roster['is_fixed'] = $rost->is_fixed;
					$roster['id'] = $rost->id;
					$roster['shift'][$rost->day] = $rost->first_week ; 
				}
			}else{
				$roster['shift_v'][] = ['','','','',''];
				$roster['action'] = 'Update';
				foreach($data as $key => $rost){
					$roster['p_name'] = $rost->rp_name;
					$roster['is_fixed'] = $rost->is_fixed;
					$roster['id'] = $rost->id;
					$roster['shift_v'][$rost->day] = [
						$rost->first_week,
						$rost->second_week,
						$rost->third_week,
						$rost->fourth_week,
						$rost->fifth_week
					];
				}
			} 
		}
        return response()->json($roster);
	}

	public function assignRoster(Request $request){
		$validator = Validator::make($request->all(), [
            'roster'   =>'required|max:255',
			'start_date'     =>  'required|date',
			'end_date'     =>  'required|date',
			'applicable_ou' => 'required'
        ]); 
        if($validator->fails()){
			$this->res['message'] = $validator->errors()->all();
            return response()->json($this->res);
        }else{
			$start_date = date('Y-m-d',strtotime($request->start_date));
			$end_date = date('Y-m-d',strtotime($request->end_date));
			if(strtotime($start_date) > strtotime($end_date)){
				$this->res['message'] = __('settings.error_date_selections');
				return response()->json($this->res);
			}
			RosterScheduler::dispatchNow(
				'',
                $request->roster,
				$start_date,
				$end_date,
				$request->applicable_ou,
				$request->user_id
			); 
			$this->res['result'] = true;
			$this->res['message'] = __('settings.success_roster_map');
			return response()->json($this->res);
		}
    }
}