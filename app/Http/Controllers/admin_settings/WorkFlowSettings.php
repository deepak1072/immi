<?php

// Created By Sandeep Maurya @ 25th Dec 2019
namespace App\Http\Controllers\admin_settings;

use App\models\admin_settings\WorkFlowSettingsModel;
use Validator;
use Illuminate\Validation\Rule;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Helpers\GlobalHelpers;
class WorkFlowSettings extends Controller
{
    //
    function __construct()
    {

    }

    public function AddUpdate(Request $request){
         
        $validator = Validator::make($request->all(), [
            'wf_name'   =>'required',
            'wf_type'     =>  'required',
            'wf_code'      =>  'required|max:255' ,
            'wf_module' => 'required',
            'wf_events' => 'required', 
            'action' => 'required'
        ]);
            
        if($validator->fails()){
            return response()->json(array('result'=>false,'error'=>$validator->errors()->all()));
        }  else{
            if($request->post('wf_type') == 1){
                $levels = $request->post('wf_levels') ;
                $check = array_map(function($level){
                    return (!empty($level['approver'])) ? 1 : 2;
                },$levels);
                if(in_array(2,$check)){
                    return response()->json(array('result'=>false,'error'=>[__('settings.approver_not_selected')]));
                }
            }
        	if($request->post('action') == 'Create'){
        		// create new workflow 
        		$result = WorkFlowSettingsModel::CheckAvailability($request);
        	 
	            if($result === true){
	            	$result1 = WorkFlowSettingsModel::AddWorkflow($request);
	            	if($result1){
	            		$message = __('settings.workflow_success');
	            	}else{
	            		$message = __('settings.workflow_error');
	            	} 
	               	return response()->json(array('result'=>true,'message'=>$message));
	            }else{
	               	$message = array(__('settings.workflow_duplicate'));
	               	return response()->json(array('result'=>false,'error'=>$message));
	            }
	            
        	}else{
        		// update workflow 
        		$result = WorkFlowSettingsModel::UpdateWorkflow($request);
	            if($result){
	               $message = __('settings.workflow_update');
	            }else{
	               $message = __('settings.workflow_update_error');
	            }
	            return response()->json(array('result'=>true,'message'=>$message));
        	}
            
        }
    }


    /**
     * roles list
     */
    public function fetchList(Request $request){
    	$data = WorkFlowSettingsModel::fetchList($request);
        return response()->json($data);
    }

    /**
     * update role status 
     */
    public function UpdateStatus(Request $request){
    	$data = WorkFlowSettingsModel::UpdateStatus($request);
        return response()->json($data);
    }

    /**
     * get role details 
     */
    public function GetDetails(Request $request){
    	$data  = WorkFlowSettingsModel::GetDetails($request);
    	return response()->json($data);
    }

    /**
     * get events 
     */
    public function GetEvents(Request $request){
        $data = array();
        $data[] = array('name'=>'All Events','value'=>100);
        $id = $request->get('id');
        switch($id){
             
            case 'PRLL' : 
                // payroll
                break;
            case 'PR' : 
                //profile
                $pr_events  = WorkFlowSettingsModel::GetEvents($id);
                if(!empty($pr_events)){
                    foreach($pr_events as $key=>$value){
                        array_push($data,['name'=>$value->name,'value'=>$value->value]);
                    }
                }
                break;
            case 'ATT' : 
                // attendance
                $att_events  = WorkFlowSettingsModel::GetEvents($id);
                if(!empty($att_events)){
                    foreach($att_events as $key=>$value){
                        array_push($data,['name'=>$value->name,'value'=>$value->value]);
                    }
                }
                break;
            case 'LV' :  
                    $leaves = GlobalHelpers::ActiveLeaveTypes();
                    if(!empty($leaves)){
                        foreach($leaves as $leave){
                            array_push($data,['name'=>$leave->leave_name,'value'=>$leave->leave_id]);
                        }
                    } 
                // leave
                break;
            case 'CNFM' : 
                //confirmation
                break;
            case 'SEP' : 
                // separation
                break;
            case 'ONB' : 
                // onboarding
                break;
            case 'RC' : 
                // recruitment
                break;
            case 'TE' : 
                // travel
                break;
            case 'COFF' : 
                // comp-off
                $att_events  = WorkFlowSettingsModel::GetEvents($id);
                if(!empty($att_events)){
                    foreach($att_events as $key=>$value){
                        array_push($data,['name'=>$value->name,'value'=>$value->value]);
                    }
                }
                break;
            default :
                // all events 
                break;
        } 
        

    	return response()->json($data);
    }
    /**
     * get authorities for approval process
     */
    public function GetAuthorities(){
        $data  = WorkFlowSettingsModel::GetAuthorities();
        $appr = array();
        if(!empty($data)){
            foreach($data as $key=>$value){
                array_push($appr,['name'=>$value->appr_name,'value'=>$value->appr_code]);
            }
        }
    	return response()->json($appr);
    }

    /**
     * get modules which requires approval flow
     */
    public function GetModules(){
        $data  = WorkFlowSettingsModel::GetModules();
        $modules = array();
        if(!empty($data)){
            foreach($data as $key=>$value){
                array_push($modules,['name'=>$value->module_name,'value'=>$value->module_code]);
            }
        }
    	return response()->json($modules);
    } 
   
}
