<?php

// Created By Sandeep Maurya @ 15th Dec 2018
namespace App\Http\Controllers\admin_settings;

use App\models\admin_settings\QualificationSettingsModel;
use Validator;
use Illuminate\Validation\Rule;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class QualificationSettings extends Controller
{
    //
    function __construct()
    {

    }

    public function AddUpdateQualification(Request $request){
    	 


        $validator = Validator::make($request->all(), [
            'qualification_name'   =>'required',
            'company_code'     =>  'required',
            'qualification_code'      =>  'required|max:255' ,
            'action' => 'required'
        ]);

      

        if($validator->fails()){
            return response()->json(array('result'=>false,'error'=>$validator->errors()->all()));
        }  else{
        	if($request->post('action') == 'Create'){
        		// create new process 
        		$result = QualificationSettingsModel::CheckQualificationAvailability($request);
        		
	            if($result === true){
	            	$result1 = QualificationSettingsModel::AddQualification($request);
	            	if($result1){
	            		$message = "Qualification created successfully";
	            	}else{
	            		$message = "Qualification could not be created , please try after sometime ";
	            	}
	               	
	               	return response()->json(array('result'=>true,'message'=>$message));
	            }else{
	               	$message = array(" Qualification with same code have been created already , please try with different data set  ");
	               	return response()->json(array('result'=>false,'error'=>$message));
	            }
	            
        	}else{
        		// update processs 
        		$result = QualificationSettingsModel::UpdateQualification($request);
	            if($result){
	               $message = "Qualification updated successfully";
	            }else{
	               $message = "Qualification could not be updated , please try after some time  ";
	            }
	            return response()->json(array('result'=>true,'message'=>$message));
        	}
            
        }
    }


    /**
     * processs list
     */
    public function fetchQualificationList(Request $request){
    	$data = QualificationSettingsModel::fetchQualificationList($request);
        return response()->json($data);
    }

    /**
     * update process status 
     */
    public function UpdateQualificationStatus(Request $request){
    	$data = QualificationSettingsModel::UpdateQualificationStatus($request);
        return response()->json($data);
    }

    /**
     * get process details 
     */
    public function GetQualificationDetails(Request $request){
    	$data  = QualificationSettingsModel::GetQualificationDetails($request);
    	return response()->json($data);
    }
}
