<?php

// Created By Sandeep Maurya @ 14th Dec 2018
namespace App\Http\Controllers\admin_settings;

use App\models\admin_settings\DivisionSettingsModel;
use Validator;
use Illuminate\Validation\Rule;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class DivisionSettings extends Controller
{
    //
    function __construct()
    {

    }

    public function AddUpdateDivisions(Request $request){
    	 


        $validator = Validator::make($request->all(), [
            'division_name'   =>'required',
            'company_code'     =>  'required',
            'division_code'      =>  'required|max:255' ,
            'action' => 'required'
        ]);

      

        if($validator->fails()){
            return response()->json(array('result'=>false,'error'=>$validator->errors()->all()));
        }  else{
        	if($request->post('action') == 'Create'){
        		// create new roles 
        		$result = DivisionSettingsModel::CheckDivisionsAvailability($request);
        		
	            if($result === true){
	            	$result1 = DivisionSettingsModel::AddDivisions($request);
	            	if($result1){
	            		$message = "Division created successfully";
	            	}else{
	            		$message = "Division could not be created , please try after sometime ";
	            	}
	               	
	               	return response()->json(array('result'=>true,'message'=>$message));
	            }else{
	               	$message = array(" Division with same code have been created already , please try with different data set  ");
	               	return response()->json(array('result'=>false,'error'=>$message));
	            }
	            
        	}else{
        		// update roles 
        		$result = DivisionSettingsModel::UpdateDivisions($request);
	            if($result){
	               $message = "Division updated successfully";
	            }else{
	               $message = "Division could not be updated , please try after some time  ";
	            }
	            return response()->json(array('result'=>true,'message'=>$message));
        	}
            
        }
    }


    /**
     * roles list
     */
    public function fetchDivisionList(Request $request){
    	$data = DivisionSettingsModel::fetchDivisionList($request);
        return response()->json($data);
    }

    /**
     * update role status 
     */
    public function UpdateDivisionStatus(Request $request){
    	$data = DivisionSettingsModel::UpdateDivisionStatus($request);
        return response()->json($data);
    }

    /**
     * get role details 
     */
    public function GetDivisionDetails(Request $request){
    	$data  = DivisionSettingsModel::GetDivisionDetails($request);
    	return response()->json($data);
    }
}
