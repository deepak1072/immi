<?php

// Created By Sandeep Maurya @ 15th Dec 2018
namespace App\Http\Controllers\admin_settings;

use App\models\admin_settings\GradeSettingsModel;
use Validator;
use Illuminate\Validation\Rule;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class GradeSettings extends Controller
{
    //
    function __construct()
    {

    }

    public function AddUpdateGrade(Request $request){
    	 


        $validator = Validator::make($request->all(), [
            'grade_name'   =>'required',
            'company_code'     =>  'required',
            'grade_code'      =>  'required|max:255' ,
            'action' => 'required'
        ]);

      

        if($validator->fails()){
            return response()->json(array('result'=>false,'error'=>$validator->errors()->all()));
        }  else{
        	if($request->post('action') == 'Create'){
        		// create new grade 
        		$result = GradeSettingsModel::CheckGradeAvailability($request);
        		
	            if($result === true){
	            	$result1 = GradeSettingsModel::AddGrade($request);
	            	if($result1){
	            		$message = "Grade created successfully";
	            	}else{
	            		$message = "Grade could not be created , please try after sometime ";
	            	}
	               	
	               	return response()->json(array('result'=>true,'message'=>$message));
	            }else{
	               	$message = array(" Grade with same code have been created already , please try with different data set  ");
	               	return response()->json(array('result'=>false,'error'=>$message));
	            }
	            
        	}else{
        		// update grades 
        		$result = GradeSettingsModel::UpdateGrade($request);
	            if($result){
	               $message = "Grade updated successfully";
	            }else{
	               $message = "Grade could not be updated , please try after some time  ";
	            }
	            return response()->json(array('result'=>true,'message'=>$message));
        	}
            
        }
    }


    /**
     * grades list
     */
    public function fetchGradeList(Request $request){
    	$data = GradeSettingsModel::fetchGradeList($request);
        return response()->json($data);
    }

    /**
     * update grade status 
     */
    public function UpdateGradeStatus(Request $request){
    	$data = GradeSettingsModel::UpdateGradeStatus($request);
        return response()->json($data);
    }

    /**
     * get grade details 
     */
    public function GetGradeDetails(Request $request){
    	$data  = GradeSettingsModel::GetGradeDetails($request);
    	return response()->json($data);
    }
}
