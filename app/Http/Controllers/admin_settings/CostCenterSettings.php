<?php

// Created By Sandeep Maurya @ 15th Dec 2018
namespace App\Http\Controllers\admin_settings;

use App\models\admin_settings\CostCenterSettingsModel;
use Validator;
use Illuminate\Validation\Rule;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class CostCenterSettings extends Controller
{
    //
    function __construct()
    {

    }

    public function AddUpdateCostCenter(Request $request){
    	 


        $validator = Validator::make($request->all(), [
            'cost_center_name'   =>'required',
            'company_code'     =>  'required',
            'cost_center_code'      =>  'required|max:255' ,
            'action' => 'required'
        ]);

      

        if($validator->fails()){
            return response()->json(array('result'=>false,'error'=>$validator->errors()->all()));
        }  else{
        	if($request->post('action') == 'Create'){
        		// create new designation 
        		$result = CostCenterSettingsModel::CheckCostCenterAvailability($request);
        		
	            if($result === true){
	            	$result1 = CostCenterSettingsModel::AddCostCenter($request);
	            	if($result1){
	            		$message = "Cost Center created successfully";
	            	}else{
	            		$message = "Cost Center could not be created , please try after sometime ";
	            	}
	               	
	               	return response()->json(array('result'=>true,'message'=>$message));
	            }else{
	               	$message = array(" Cost Center with same code have been created already , please try with different data set  ");
	               	return response()->json(array('result'=>false,'error'=>$message));
	            }
	            
        	}else{
        		// update designations 
        		$result = CostCenterSettingsModel::UpdateCostCenter($request);
	            if($result){
	               $message = "Cost Center updated successfully";
	            }else{
	               $message = "Cost Center could not be updated , please try after some time  ";
	            }
	            return response()->json(array('result'=>true,'message'=>$message));
        	}
            
        }
    }


    /**
     * designations list
     */
    public function fetchCostCenterList(Request $request){
    	$data = CostCenterSettingsModel::fetchCostCenterList($request);
        return response()->json($data);
    }

    /**
     * update designation status 
     */
    public function UpdateCostCenterStatus(Request $request){
    	$data = CostCenterSettingsModel::UpdateCostCenterStatus($request);
        return response()->json($data);
    }

    /**
     * get designation details 
     */
    public function GetCostCenterDetails(Request $request){
    	$data  = CostCenterSettingsModel::GetCostCenterDetails($request);
    	return response()->json($data);
    }
}
