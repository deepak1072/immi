<?php

// Created By Sandeep Maurya @ 15th Dec 2018
namespace App\Http\Controllers\admin_settings;

use App\models\admin_settings\DesignationSettingsModel;
use Validator;
use Illuminate\Validation\Rule;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class DesignationSettings extends Controller
{
    //
    function __construct()
    {

    }

    public function AddUpdateDesignation(Request $request){
    	 


        $validator = Validator::make($request->all(), [
            'designation_name'   =>'required',
            'company_code'     =>  'required',
            'designation_code'      =>  'required|max:255' ,
            'action' => 'required'
        ]);

      

        if($validator->fails()){
            return response()->json(array('result'=>false,'error'=>$validator->errors()->all()));
        }  else{
        	if($request->post('action') == 'Create'){
        		// create new designation 
        		$result = DesignationSettingsModel::CheckDesignationAvailability($request);
        		
	            if($result === true){
	            	$result1 = DesignationSettingsModel::AddDesignation($request);
	            	if($result1){
	            		$message = "Designation created successfully";
	            	}else{
	            		$message = "Designation could not be created , please try after sometime ";
	            	}
	               	
	               	return response()->json(array('result'=>true,'message'=>$message));
	            }else{
	               	$message = array(" Designation with same code have been created already , please try with different data set  ");
	               	return response()->json(array('result'=>false,'error'=>$message));
	            }
	            
        	}else{
        		// update designations 
        		$result = DesignationSettingsModel::UpdateDesignation($request);
	            if($result){
	               $message = "Designation updated successfully";
	            }else{
	               $message = "Designation could not be updated , please try after some time  ";
	            }
	            return response()->json(array('result'=>true,'message'=>$message));
        	}
            
        }
    }


    /**
     * designations list
     */
    public function fetchDesignationList(Request $request){
    	$data = DesignationSettingsModel::fetchDesignationList($request);
        return response()->json($data);
    }

    /**
     * update designation status 
     */
    public function UpdateDesignationStatus(Request $request){
    	$data = DesignationSettingsModel::UpdateDesignationStatus($request);
        return response()->json($data);
    }

    /**
     * get designation details 
     */
    public function GetDesignationDetails(Request $request){
    	$data  = DesignationSettingsModel::GetDesignationDetails($request);
    	return response()->json($data);
    }
}
