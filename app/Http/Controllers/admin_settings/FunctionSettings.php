<?php

// Created By Sandeep Maurya @ 15th Dec 2018
namespace App\Http\Controllers\admin_settings;

use App\models\admin_settings\FunctionSettingsModel;
use Validator;
use Illuminate\Validation\Rule;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class FunctionSettings extends Controller
{
    //
    function __construct()
    {

    }

    public function AddUpdateFunction(Request $request){
    	 


        $validator = Validator::make($request->all(), [
            'function_name'   =>'required',
            'function_head'   =>'required',
            'company_code'     =>  'required',
            'function_code'      =>  'required|max:255' ,
            'action' => 'required'
        ]);

      

        if($validator->fails()){
            return response()->json(array('result'=>false,'error'=>$validator->errors()->all()));
        }  else{
        	if($request->post('action') == 'Create'){
        		// create new process 
        		$result = FunctionSettingsModel::CheckFunctionAvailability($request);
        		
	            if($result === true){
	            	$result1 = FunctionSettingsModel::AddFunction($request);
	            	if($result1){
	            		$message = "Function created successfully";
	            	}else{
	            		$message = "Function could not be created , please try after sometime ";
	            	}
	               	
	               	return response()->json(array('result'=>true,'message'=>$message));
	            }else{
	               	$message = array(" Function with same code have been created already , please try with different data set  ");
	               	return response()->json(array('result'=>false,'error'=>$message));
	            }
	            
        	}else{
        		// update processs 
        		$result = FunctionSettingsModel::UpdateFunction($request);
	            if($result){
	               $message = "Function updated successfully";
	            }else{
	               $message = "Function could not be updated , please try after some time  ";
	            }
	            return response()->json(array('result'=>true,'message'=>$message));
        	}
            
        }
    }


    /**
     * processs list
     */
    public function fetchFunctionList(Request $request){
    	$data = FunctionSettingsModel::fetchFunctionList($request);
        return response()->json($data);
    }

    /**
     * update process status 
     */
    public function UpdateFunctionStatus(Request $request){
    	$data = FunctionSettingsModel::UpdateFunctionStatus($request);
        return response()->json($data);
    }

    /**
     * get process details 
     */
    public function GetFunctionDetails(Request $request){
    	$data  = FunctionSettingsModel::GetFunctionDetails($request);
    	return response()->json($data);
    }
}
