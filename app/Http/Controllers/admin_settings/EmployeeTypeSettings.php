<?php

// Created By Sandeep Maurya @ 15th Dec 2018
namespace App\Http\Controllers\admin_settings;

use App\models\admin_settings\EmployeeTypeSettingsModel;
use Validator;
use Illuminate\Validation\Rule;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class EmployeeTypeSettings extends Controller
{
    //
    function __construct()
    {

    }

    public function AddUpdateEmployeeType(Request $request){
    	 


        $validator = Validator::make($request->all(), [
            'employee_type_name'   =>'required',
            'company_code'     =>  'required',
            'employee_type_code'      =>  'required|max:255' ,
            'action' => 'required'
        ]);

      

        if($validator->fails()){
            return response()->json(array('result'=>false,'error'=>$validator->errors()->all()));
        }  else{
        	if($request->post('action') == 'Create'){
        		// create new EmployeeType 
        		$result = EmployeeTypeSettingsModel::CheckEmployeeTypeAvailability($request);
        		
	            if($result === true){
	            	$result1 = EmployeeTypeSettingsModel::AddEmployeeType($request);
	            	if($result1){
	            		$message = "Employee Type created successfully";
	            	}else{
	            		$message = "Employee Type could not be created , please try after sometime ";
	            	}
	               	
	               	return response()->json(array('result'=>true,'message'=>$message));
	            }else{
	               	$message = array(" Employee Type with same code have been created already , please try with different data set  ");
	               	return response()->json(array('result'=>false,'error'=>$message));
	            }
	            
        	}else{
        		// update EmployeeTypes 
        		$result = EmployeeTypeSettingsModel::UpdateEmployeeType($request);
	            if($result){
	               $message = "Employee Type updated successfully";
	            }else{
	               $message = "Employee Type could not be updated , please try after some time  ";
	            }
	            return response()->json(array('result'=>true,'message'=>$message));
        	}
            
        }
    }


    /**
     * EmployeeTypes list
     */
    public function fetchEmployeeTypeList(Request $request){
    	$data = EmployeeTypeSettingsModel::fetchEmployeeTypeList($request);
        return response()->json($data);
    }

    /**
     * update EmployeeType status 
     */
    public function UpdateEmployeeTypeStatus(Request $request){
    	$data = EmployeeTypeSettingsModel::UpdateEmployeeTypeStatus($request);
        return response()->json($data);
    }

    /**
     * get EmployeeType details 
     */
    public function GetEmployeeTypeDetails(Request $request){
    	$data  = EmployeeTypeSettingsModel::GetEmployeeTypeDetails($request);
    	return response()->json($data);
    }
}
