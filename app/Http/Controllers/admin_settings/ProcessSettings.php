<?php

// Created By Sandeep Maurya @ 15th Dec 2018
namespace App\Http\Controllers\admin_settings;

use App\models\admin_settings\ProcessSettingsModel;
use Validator;
use Illuminate\Validation\Rule;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class ProcessSettings extends Controller
{
    //
    function __construct()
    {

    }

    public function AddUpdateProcess(Request $request){
    	 


        $validator = Validator::make($request->all(), [
            'process_name'   =>'required',
            'company_code'     =>  'required',
            'process_code'      =>  'required|max:255' ,
            'action' => 'required'
        ]);

      

        if($validator->fails()){
            return response()->json(array('result'=>false,'error'=>$validator->errors()->all()));
        }  else{
        	if($request->post('action') == 'Create'){
        		// create new process 
        		$result = ProcessSettingsModel::CheckProcessAvailability($request);
        		
	            if($result === true){
	            	$result1 = ProcessSettingsModel::AddProcess($request);
	            	if($result1){
	            		$message = "Process created successfully";
	            	}else{
	            		$message = "Process could not be created , please try after sometime ";
	            	}
	               	
	               	return response()->json(array('result'=>true,'message'=>$message));
	            }else{
	               	$message = array(" Process with same code have been created already , please try with different data set  ");
	               	return response()->json(array('result'=>false,'error'=>$message));
	            }
	            
        	}else{
        		// update processs 
        		$result = ProcessSettingsModel::UpdateProcess($request);
	            if($result){
	               $message = "Process updated successfully";
	            }else{
	               $message = "Process could not be updated , please try after some time  ";
	            }
	            return response()->json(array('result'=>true,'message'=>$message));
        	}
            
        }
    }


    /**
     * processs list
     */
    public function fetchProcessList(Request $request){
    	$data = ProcessSettingsModel::fetchProcessList($request);
        return response()->json($data);
    }

    /**
     * update process status 
     */
    public function UpdateProcessStatus(Request $request){
    	$data = ProcessSettingsModel::UpdateProcessStatus($request);
        return response()->json($data);
    }

    /**
     * get process details 
     */
    public function GetProcessDetails(Request $request){
    	$data  = ProcessSettingsModel::GetProcessDetails($request);
    	return response()->json($data);
    }
}
