<?php

// Created By Sandeep Maurya @ 11th Dec 2018
namespace App\Http\Controllers\admin_settings;

use App\models\admin_settings\RolesSettingsModel;
use Validator;
use Illuminate\Validation\Rule;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class RolesSettings extends Controller
{
    //
    function __construct()
    {

    }

    public function AddUpdateRoles(Request $request){
        $validator = Validator::make($request->all(), [
            'super_role_id'   =>'required',
            'company_code'     =>  'required',
            'role_name'      =>  'required|max:255' ,
            'checked' => 'required',
            'action' => 'required'
        ]);
   

        if($validator->fails()){
            return response()->json(array('result'=>false,'error'=>$validator->errors()->all()));
        }  else{
        	if($request->post('action') == 'Create'){
        		// create new roles 
        		$result = RolesSettingsModel::CheckRolesAvailability($request);
        		
	            if($result === true){
	            	$result1 = RolesSettingsModel::AddRoles($request);
	            	if($result1){
	            		$message = "Role created successfully";
	            	}else{
	            		$message = "Role could not be created , please try after sometime ";
	            	}
	               	
	               	return response()->json(array('result'=>true,'message'=>$message));
	            }else{
	               	$message = array(" Same Role have been created already , please try with different data set  ");
	               	return response()->json(array('result'=>false,'error'=>$message));
	            }
	            
        	}else{
        		// update roles 
        		$result = RolesSettingsModel::UpdateRoles($request);
	            if($result){
	               $message = "Role updated successfully";
	            }else{
	               $message = "Role could not be updated , please try after some time  ";
	            }
	            return response()->json(array('result'=>true,'message'=>$message));
        	}
            
        }
    }


    /**
     * roles list
     */
    public function fetchRolesList(Request $request){
    	$data = RolesSettingsModel::fetchRolesList($request);
        return response()->json($data);
    }

    /**
     * update role status 
     */
    public function UpdateRoleStatus(Request $request){
    	$data = RolesSettingsModel::UpdateRoleStatus($request);
        return response()->json($data);
    }

    /**
     * get role details 
     */
    public function GetRoleDetails(Request $request){
    	$data  = RolesSettingsModel::GetRoleDetails($request);
    	return response()->json($data);
    }

    /**
     * roles assigned to user's list
     */
    public function getAssignedRolesList(Request $request){
        $data  = RolesSettingsModel::getAssignedRolesList($request);
        return response()->json($data);
    }

    /*
     * available users and roles list 
     */
    public function getUsersRolesList(Request $request){
        $data  = RolesSettingsModel::getUsersRolesList($request);
        return response()->json($data);
    }

    public function AssignedRolesStatusUpdate(Request $request){
        $data = RolesSettingsModel::AssignedRolesStatusUpdate($request);
        return response()->json($data);
    }
}
