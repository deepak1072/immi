<?php

namespace App\Http\Controllers\admin_settings;

use App\models\admin_settings\CompanySettingsModel;
use Validator;
use Illuminate\Validation\Rule;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class CompanySettings extends Controller
{
    //
    function __construct()
    {

    }

    public function createNewCompany(Request $request){

    	
    	$validator = Validator::make($request->all(), [
            'company_name'   =>'required|unique:company_masters|max:255',
            'company_url'     =>  'required|unique:company_masters|max:255',
            'company_code'      =>  'required|unique:company_masters|max:255' ,
            'company_ip_address'=>  'required|max:255|ip'
        ]);
         

        if($validator->fails()){
            return response()->json(array('result'=>false,'error'=>$validator->errors()->all()));
        }  else{
            $result = CompanySettingsModel::createNewCompany($request);
            if($result){
               $message = "Company created successfully";
            }else{
               $message = "Company could not be created ";
            }
            return response()->json(array('result'=>true,'message'=>$message));
        }
    }


     /**
     * get company list
     */
    public function fetchCompanyList(Request $request){
        $data = CompanySettingsModel::fetchCompanyList($request);
        return response()->json($data);
    }

    /**
     * company details 
     */
    public function fetchCompanyDetails(Request $request){
    	$data = CompanySettingsModel::fetchCompanyDetails($request);
        return response()->json($data);
    }

    /**
     * update company 
     */
    public function updateCompany(Request $request){
    	$validator = Validator::make($request->all(), [
            'company_name'   =>'required|max:255',
            'company_url'     =>  'required|max:255',
            'company_code'      =>  'required|max:255' ,
            'company_ip_address'=>  'required|max:255|ip'
        ]);
         

        if($validator->fails()){
            return response()->json(array('result'=>false,'error'=>$validator->errors()->all()));
        }  else{
            $result = CompanySettingsModel::UpdateCompany($request);
            if($result){
               $message = "Company updated successfully";
            }else{
               $message = "Company could not be updated ";
            }
            return response()->json(array('result'=>true,'message'=>$message));
        }
    }

    /**
     * get active company
     */
    public function getActiveCompany(){
    	$data = CompanySettingsModel::getActiveCompany();
        return response()->json($data);
    }


}