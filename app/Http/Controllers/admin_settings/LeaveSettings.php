<?php

// Created By Sandeep Maurya @ 25th Dec 2018
namespace App\Http\Controllers\admin_settings;

use App\models\admin_settings\LeaveSettingsModel;
use Validator;
use Illuminate\Validation\Rule;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class LeaveSettings extends Controller
{
    //
    function __construct()
    {

    }

    public function AddUpdateLeave(Request $request){
    	 


        $validator = Validator::make($request->all(), [
            'leave_name'   =>'required',
            'leave_master_id'     =>  'required',
            'leave_short_name'     =>  'required',
             
            'action' => 'required'
        ]);

      

        if($validator->fails()){
            return response()->json(array('result'=>false,'error'=>$validator->errors()->all()));
        }  else{
        	if($request->post('action') == 'Create'){
        		// create new leave 
        		$result = LeaveSettingsModel::CheckLeaveAvailability($request);

                
        		
	            if($result === true){
	            	$result1 = LeaveSettingsModel::AddLeave($request);
	            	if($result1){
	            		$message = "Leave created successfully";
	            	}else{
	            		$message = "Leave could not be created , please try after sometime ";
	            	}
	               	
	               	return response()->json(array('result'=>true,'message'=>$message));
	            }else{
	               	$message = array(" Leave with same code have been created already , please try with different data set  ");
	               	return response()->json(array('result'=>false,'error'=>$message));
	            }
	            
        	}else{
        		// update leaves 
        		$result = LeaveSettingsModel::UpdateLeave($request);
	            if($result){
	               $message = "Leave updated successfully";
	            }else{
	               $message = "Leave could not be updated , please try after some time  ";
	            }
	            return response()->json(array('result'=>true,'message'=>$message));
        	}
            
        }
    }


    /**
     * leaves list
     */
    public function fetchLeaveList(Request $request){
    	$data = LeaveSettingsModel::fetchLeaveList($request);
        return response()->json($data);
    }

    /**
     * update leave status 
     */
    public function UpdateLeaveStatus(Request $request){
    	$data = LeaveSettingsModel::UpdateLeaveStatus($request);
        return response()->json($data);
    }

    /**
     * get leave details 
     */
    public function GetLeaveDetails(Request $request){
        $data  = LeaveSettingsModel::GetLeaveDetails($request);
        return response()->json($data);
    }

    /**
     * get leave masters 
     */
    public function fetchLeaveMasters(Request $request){
        $data  = LeaveSettingsModel::fetchLeaveMasters();
        return response()->json($data);
    }

    
}
