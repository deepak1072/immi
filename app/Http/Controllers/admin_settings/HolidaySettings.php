<?php

// Created By Sandeep Maurya @ 20th April 2019
namespace App\Http\Controllers\admin_settings;

use App\models\admin_settings\HolidaySettingsModel;
use Validator;
use Illuminate\Validation\Rule;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class HolidaySettings extends Controller
{
    //
    function __construct()
    {

    }

    public function AddUpdateHoliday(Request $request){
        $validator = Validator::make($request->all(), [
            'h_date'   =>'required',
            'h_name'     =>  'required|max:255',
            'h_type'      =>  'required|max:255' ,
            'h_status' => 'required',
            'is_additional' => 'required', 
            'h_applicable' => 'required'
        ]);
   

        if($validator->fails()){
            return response()->json(array('result'=>false,'error'=>$validator->errors()->all()));
        }  else{
            
        	if($request->post('action') == 'Create'){
        		// create new roles 
        		$result = HolidaySettingsModel::CheckAvailability($request);
        		
	            if($result === true){
	            	$result1 = HolidaySettingsModel::Create($request);
	            	if($result1){
	            		$message = "Holiday created successfully";
	            	}else{
	            		$message = "Holiday could not be created , please try after sometime ";
	            	}
	               	
	               	return response()->json(array('result'=>true,'message'=>$message));
	            }else{
	               	$message = array(" Holiday with same name have been created already , please try with different data set  ");
	               	return response()->json(array('result'=>false,'error'=>$message));
	            }
	            
        	}else{
        		// update roles 
        		$result = HolidaySettingsModel::UpdateHoliday($request);
	            if($result){
	               $message = "Holiday updated successfully";
	            }else{
	               $message = "Holiday could not be updated , please try after some time  ";
	            }
	            return response()->json(array('result'=>true,'message'=>$message));
        	}
            
        }
    }


    /**
     * holiday list
     */
    public function fetchList(Request $request){
    	$data = HolidaySettingsModel::fetchList($request);
        return response()->json($data);
    }

    /**
     * update holiday status 
     */
    public function UpdateStatus(Request $request){
    	$data = HolidaySettingsModel::UpdateStatus($request);
        return response()->json($data);
    }

    /**
     * get holiday details 
     */
    public function GetDetails(Request $request){
    	$data  = HolidaySettingsModel::GetDetails($request);
    	return response()->json($data);
    }

     
}
