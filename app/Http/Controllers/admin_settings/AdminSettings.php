<?php

namespace App\Http\Controllers\admin_settings;

use App\models\admin_settings\AdminSettingsModel;
use Validator;
use Illuminate\Validation\Rule;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class AdminSettings extends Controller
{
    //
    function __construct()
    {

    }

    /**
     * get parent menu for menu creation
     */
    public function getParentMenu(Request $request){
        $data = AdminSettingsModel::getParentMenu();
        return response()->json($data);
    }

    /**
     * Create new  menu
     */
    public function createNewMenu(Request $request){
        $validator = Validator::make($request->all(), [
            'parent_menu'   =>'required',
            'menu_name'     =>  'required|unique:menu_masters',
            'menu_url'      =>  'sometimes' ,
            'menu_attribute'=>  'required'
        ]);
        $validator->sometimes('menu_url', 'required|unique:menu_masters', function ($input) {
            return ($input->parent_menu ==0 ) ? false : true;
        });

        if($validator->fails()){
            return response()->json(array('result'=>false,'error'=>$validator->errors()->all()));
        }  else{
            $result = AdminSettingsModel::createNewMenu($request);
            if($result){
               $message = "Menu created successfully";
            }else{
               $message = "Menu could not be created ";
            }
            return response()->json(array('result'=>true,'message'=>$message));
        }
    }

    /**
     * get menu list
     */
    public function fetchMenuList(Request $request){
        $data = AdminSettingsModel::fetchMenuList($request);
        return response()->json($data);
    }

    /**
     * update menu status 
     */
    public function updateMenuStatus(Request $request){
        $data = AdminSettingsModel::updateMenuStatus($request);
        return response()->json($data);
    }

    /**
     * get menu details  
     */
    public function getMenuDetails(Request $request)
    {
        $data  = AdminSettingsModel::getMenuDetails($request);
        return response()->json($data);
    }

    /**
     *
     *update meu 
     */
    public  function UpdateMenu(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'parent_menu'   =>'required',
            'menu_name'     =>  'required',
            'menu_url'      =>  'required' ,
            'menu_attribute'=>  'required'
        ]);
        // $validator->sometimes('menu_url', 'required|unique:menu_masters', function ($input) {
        //     return ($input->parent_menu ==0 ) ? false : true;
        // });

        if($validator->fails()){
            return response()->json(array('result'=>false,'error'=>$validator->errors()->all()));
        }  else{
            $result = AdminSettingsModel::UpdateMenu($request);
            if($result){
               $message = "Menu updated successfully";
            }else{
               $message = "Menu could not be updated , please try after some time  ";
            }
            return response()->json(array('result'=>true,'message'=>$message));
        }
    }


    // get menu ou masters
    public function getMenuMasters(){
        $data = AdminSettingsModel::getMenuMasters();
        return response()->json($data);
    }

    // get ou details 
    public function getOuDetails(Request $request){
         $data = AdminSettingsModel::getOuDetails($request);
        return response()->json($data);
    }



}
