<?php

// Created By Sandeep Maurya @ 25th Dec 2018
namespace App\Http\Controllers\admin_settings;

use App\models\admin_settings\ShiftSettingsModel;
use Validator;
use Illuminate\Validation\Rule;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\interfaces\ActivityInterface;
use App\Jobs\RosterScheduler;
class ShiftSettings extends Controller implements ActivityInterface
{
    //
    function __construct()
    {

    }
	public $res = [
		'result' => false,
		'message' => '',
		'data' => ''
	];
	private $date = '';
    public function AddUpdate(Request $request){

    	$validator = Validator::make($request->all(), [
            'shift_name'   =>'required|max:255',
            'code'     =>  'required|max:255',
            'round_clock'      =>  'required|max:1|min:1' ,
            'mark_out' => 'required|max:1|min:1',
            'shift_start' => 'required', 
            'shift_end' => 'required',
            'mandate_start' => 'required',
            'mandate_end' => 'required'
        ]);
   

        if($validator->fails()){
            return response()->json(array('result'=>false,'error'=>$validator->errors()->all()));
        }  else{
            
        	if($request->post('action') == 'Create'){
        		// create new roles 
        		$result = ShiftSettingsModel::CheckAvailability($request);
        		
	            if($result === true){
	            	$result1 = ShiftSettingsModel::Create($request);
	            	if($result1){
	            		$message = "Shift created successfully";
	            	}else{
	            		$message = "Shift could not be created , please try after sometime ";
	            	}
	               	
	               	return response()->json(array('result'=>true,'message'=>$message));
	            }else{
	               	$message = array(" Shift with same name have been created already , please try with different data set  ");
	               	return response()->json(array('result'=>false,'error'=>$message));
	            }
	            
        	}else{
        		// update roles 
        		$result = ShiftSettingsModel::UpdateShift($request);
	            if($result){
	               $message = "Shift updated successfully";
	            }else{
	               $message = "Shift could not be updated , please try after some time  ";
	            }
	            return response()->json(array('result'=>true,'message'=>$message));
        	}
            
        }
    }

	//get list 
	public function FetchList(Request $request){
        $data = ShiftSettingsModel::fetchList($request);
        return response()->json($data);
	}

	// update status 
	public function UpdateStatus(Request $request){
        $data = ShiftSettingsModel::UpdateStatus($request);
        return response()->json($data);
	}

	

	// get details 
	public function GetDetails(Request $request){
        $data  = ShiftSettingsModel::GetDetails($request);
        return response()->json($data);
	}

	public function getActiveShift(Request $request)
	{
		$data  = ShiftSettingsModel::getActiveShift();
		$shift = [];
		if(count($data) > 0){
			foreach($data as $key => $value){
				array_push($shift,[
					'name' => $value->shift_name."(".date('H:i',strtotime($value->shift_start_mandatory))."-".date('H:i',strtotime($value->shift_end_mandatory)) .")",
					'value'=> $value->shift_id
					]);
			}
		} 
        return response()->json($shift);
	}
}