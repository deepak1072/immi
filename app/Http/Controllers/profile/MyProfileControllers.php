<?php

	namespace App\Http\Controllers\profile;

	use App\Http\Controllers\Controller;
    use Illuminate\Http\Request;
    use Illuminate\Support\Facades\Hash;
    use Illuminate\Support\Facades\Validator;
    use Tymon\JWTAuth\Exceptions\JWTException;
    use App\libraries\profile\OfficialProfile;
        
    /**
     * @Written by Sandeep Kumar Maurya 
     * @ 7:23 PM 21st July 2019
     */

    class MyProfileControllers extends Controller {
        
        // official details  
        public function OfficialDetails(Request $request){
            $validator = Validator::make($request->all(), [
                    'req_user_id' => 'required|int|max:11' 
                ]);

            if ($validator->fails()) {
                return response()->json([
                    'result'=> false,
                    'error'=>'Please provide employee code ',
                    'error_details'=>$validator->errors() ,
                    'status'=> 200
                ]);
            }else{
                $req_user_id = $request->get('req_user_id');
                $profile = new OfficialProfile();
                $options = array(
                    'req_user_id' =>$req_user_id
                );
               
                $official_details = $profile->OfficialDetails($options);

                return response()->json([
                    'result'=> true,
                    'data'=>$official_details ,
                    'status' => 200
                ]);
            }
        }

        // bank details 
        public function BankDetails(Request $request){
            $validator = Validator::make($request->all(), [
                    'req_user_id' => 'required|int|max:11' 
                ]);

            if ($validator->fails()) {
                return response()->json([
                    'result'=> false,
                    'error'=>'Please provide employee code ',
                    'error_details'=>$validator->errors() ,
                    'status'=> 200
                ]);
            }else{
                $req_user_id = $request->get('req_user_id');
                $profile = new OfficialProfile();
                $options = array(
                    'req_user_id' =>$req_user_id
                );
               
                $bank_details = $profile->BankDetails($options);

                return response()->json([
                    'result'=> true,
                    'data'=>$bank_details ,
                    'status' => 200
                ]);
            }
        }

        // personal details 
        public function PersonalDetails(Request $request){
            $validator = Validator::make($request->all(), [
                    'req_user_id' => 'required|int|max:11' 
                ]);

            if ($validator->fails()) {
                return response()->json([
                    'result'=> false,
                    'error'=>'Please provide employee code ',
                    'error_details'=>$validator->errors() ,
                    'status'=> 200
                ]);
            }else{
                $req_user_id = $request->get('req_user_id');
                $profile = new OfficialProfile();
                $options = array(
                    'req_user_id' =>$req_user_id
                );
               
                $_details = $profile->PersonalDetails($options);

                return response()->json([
                    'result'=> true,
                    'data'=>$_details ,
                    'status' => 200
                ]);
            }
        }

        // contact details 
        public function ContactDetails(Request $request){
            $validator = Validator::make($request->all(), [
                    'req_user_id' => 'required|int|max:11' 
                ]);

            if ($validator->fails()) {
                return response()->json([
                    'result'=> false,
                    'error'=>'Please provide employee code ',
                    'error_details'=>$validator->errors() ,
                    'status'=> 200
                ]);
            }else{
                $req_user_id = $request->get('req_user_id');
                $profile = new OfficialProfile();
                $options = array(
                    'req_user_id' =>$req_user_id
                );
               
                $_details = $profile->ContactDetails($options);

                return response()->json([
                    'result'=> true,
                    'data'=>$_details ,
                    'status' => 200
                ]);
            }
        }
    }