<?php

	namespace App\Http\Controllers\leave;

	use App\Http\Controllers\Controller;
    use Illuminate\Http\Request;
    use Illuminate\Support\Facades\Hash;
    use Illuminate\Support\Facades\Validator;
    use Tymon\JWTAuth\Exceptions\JWTException;
    use App\libraries\leave\LeaveBalance;
    use App\libraries\leave\CreditLeaveBalance;
        
    /**
     * @Written by Sandeep Kumar Maurya 
     * @ 4:03 PM 2nd March 2019
     */

    class LeaveBalanceControllers extends Controller {
        
        // Leave balance 
        public function LeaveBalance(Request $request)  {

            $validator = Validator::make($request->all(), [
                    'req_user_id' => 'required|int|max:11' 
                ]);
                 
            if($validator->fails()) {
                return response()->json([
                    'result'=> false,
                    'error'=>__('notifications.user_code_error'),
                    'error_details'=>$validator->errors() ,
                    'status'=> 200
                ]);
            }else{  
                $id = $request->get('req_user_id'); 
                $leave_balance = new LeaveBalance(); 
                $balance = $leave_balance->LeaveBalanceProvider($id);
                return response()->json([
                    'result'=> true,
                    'leave_balance'=>$balance ,
                    'status' => 200
                ]);
            }
        }

        public function CreditBalance(Request $request){
            $credit_balance = new CreditLeaveBalance();
            $credit = $credit_balance->CreditBalance('sandeep','','2019-02-03','');
            
            return response()->json($credit);
        }
    }