<?php
/**
 * Created by PhpStorm.
 * User: Sandeep Maurya
 * Date: 19/1/2020
 * Time: 4:27 PM
 */
namespace App\Http\Controllers\leave;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use App\libraries\leave\OpeningBalance;
use App\libraries\reports\ImportExports;
use App\Jobs\ImportOpeningBalance;

class OpeningBalanceControllers extends Controller {
    public $res  = [
        'result'=> false,
        'message'=>'', 
        'status'=> 200,
        'type' => 'warn'
    ];
    function __construct(){
        $this->activity_time = date('Y-m-d H:i:s');
    }
 

    /**
     * leave master
     */
    public function index(Request $request){
        $leave = new OpeningBalance();
        $data = $leave->Transactions($request);
        $data['result'] = true;
        if($data['data']){
            $new_data = array();
            foreach ( $data['data'] as $key => $balance ) { 
                array_push($new_data,array(
                    'lb_id'=> $balance->lb_id,
                    'created_at' =>date(config('constants.DATE_TIME_F'), strtotime($balance->created_at)) ,
                    'leave_name' => $balance->leave_name,
                    'emp_name' => $balance->emp_name,
                    'created_by' => $balance->created_by,
                    'balance' =>$balance->balance,
                    'effective_date' => date(config('constants.DATE_F'), strtotime($balance->effective_date)),
                    'remarks' => $balance->remarks
                ));
            }
            $data['data'] = $new_data;
        }
        return response()->json($data);
    }

    public function BalanceUpload(Request $request){
        $validator = Validator::make($request->all(), [
            'req_user_id' => 'required',
            'uploaded_file' => 'required',
        ]);
        if($validator->fails()) {
            $error = $validator->errors()->all();
            $this->res['message'] = $error[0]; 
        }else{               
            if($request->hasFile('uploaded_file')) {
                if ($request->file('uploaded_file')->isValid()) {
                    $name = $request->file('uploaded_file')->getClientOriginalName();
                    $extension = $request->file('uploaded_file')->extension();
                    $path = $request->file('uploaded_file')->storeAs(
                        'uploads/'.$request->company_code,
                        date('YmdHis')."_". $name
                    );
                    $import = array(
                        'type' => 'Import',
                        'user_id' => $request->user_id,
                        'name' => 'Leave Opening Balance',
                        'module' => 'leave',
                        '_file' => $name,
                        'file_link' => $path,
                        'error_link' => '',
                        'descriptions' => 'Opening balance upload',
                        'status' => 'New',
                        'created_at' => $this->activity_time ,
                        'updated_at' => $this->activity_time 
                    );
                    $import_obj = new ImportExports();
                    $res = $import_obj->Submit( $import );
                    if($res){
                        ImportOpeningBalance::dispatchNow($res,$path,$request->user_id);
                        $this->res['result'] = true;
                        $this->res['type'] = 'success';
                        $this->res['message'] = 'You file have been successfully uploaded, data will be proccessed soon';
                    }else{
                        $this->res['message'] = 'We could not accept your request, please try after sometime';
                    }
                }else{
                    $this->res['message'] = "It seems you are trying to upload a corrupt file ";
                }
            }else{
                $this->res['message'] = "Please select file for upload";
            }
        }
        return response()->json($this->res); 
    }
}