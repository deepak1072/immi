<?php
/**
 * Created by PhpStorm.
 * User: Sandeep Maurya
 * Date: 19/1/2020
 * Time: 4:27 PM
 */
namespace App\Http\Controllers\leave;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator; 
use App\libraries\leave\LeaveApproval; 
use App\libraries\leave\LeaveTransactions;
/**
 * @Written by Sandeep Kumar Maurya
 * @ 4:03 PM 2nd March 2019
 */

class LeaveApprovalController extends Controller {
    public $res  = [
        'result'=> false,
        'message'=>'', 
        'status'=> 200
    ];
    function __construct(){
        $this->activity_time = date('Y-m-d H:i:s');
    }

    public function Approve(Request $request){
        $validator = Validator::make($request->all(), [
            'req_user_id' => 'required' , 
            'leave_id' => 'required' ,
            'status' => 'required'  , 
            'ltrx_id' => 'required'   ,
            'wf_id' => 'required' 
        ]);
        if($validator->fails()) {
            $error = $validator->errors()->all();
            $this->res['message'] = $error[0];
            return response()->json($this->res);
        }else{ 
            $approval = new LeaveApproval();
            $options = array(
                'leave_id' => $request->leave_id, 
                'remarks' => $request->remarks,
                'status' => $request->status ,
                'ltrx_id' => $request->ltrx_id ,
                'wf_id' => $request->wf_id,
                'req_user_id' => $request->req_user_id,
                'activity_time' => $this->activity_time
            );   
            
            switch($request->status){
                case 2 :  
                        $res = $approval->ApproveRequest($options);
                        break ; 
                case 3 : 
                        $res = $approval->RejectRequest($options);
                        break ;
                case 4 : 
                        $res = $approval->CancelRequest($options);
                        break ;
                case 6 :  
                        $res = $approval->ApproveRequest($options);
                        break;
                case 7 :  
                        $res = $approval->RejectRequest($options);
                        break ;
                default : 
                    $res = $approval->ApproveRequest($options);
            }
            return response()->json($res);
        }
    }

    /**
     * leave transactions
     */
    public function LeaveTransactions(Request $request){
        $leave = new LeaveTransactions();
        $data = $leave->LeaveTransaction($request);
        $data['result'] = true;
        if($data['data']){
            $new_data = array();
            foreach ( $data['data'] as $key => $leave ) { 
                array_push($new_data,array(
                    'leave_id'=> $leave->leave_id,
                    'trx_date' =>date(config('constants.DATE_TIME_F'), strtotime($leave->trx_date)) ,
                    'leave_name' => $leave->leave_name,
                    'emp_name' => $leave->emp_name,
                    'emp_code' => $leave->emp_code,
                    'user_id' => $leave->user_id,
                    'leave_type' =>$leave->leave_type,
                    'from_date' => date(config('constants.DATE_F'), strtotime($leave->from_date)),
                    'to_date' => date(config('constants.DATE_F'), strtotime($leave->to_date)),
                    'leave_days' => $leave->leave_days,
                    'ltrx_id' => $leave->ltrx_id,
                    'final_status_text' => $leave->final_status_text 
                ));
            }
            $data['data'] = $new_data;
        }
        return response()->json($data);
    }

    /**
     * leave transaction details 
     */
    public function LeaveTransactionDetails(Request $request){
        $validator = Validator::make($request->all(), [
            'req_user_id' => 'required' , 
            'l_id' => 'required' , 
            'trx_id' => 'required'   , 
        ]);
 
        if($validator->fails()) {
            $error = $validator->errors()->all();
            $this->res['message'] =$error[0] ;
            return response()->json($this->res);
        }else{ 
            $leave = new LeaveTransactions();
            $data = $leave->LeaveTransactionDetails($request);
            if($data){
                $this->res['data'] = $data ;
                $this->res['result'] = true;
            }else{
                $this->res['message'] = __('leave.error_no_leave_details');
            } 
            return response()->json($this->res);
        }
    }

    public function TeamOnLeave(Request $request){
        $validator = Validator::make($request->all(), [
            'req_user_id' => 'required' , 
            'from_date' => 'required' , 
            'to_date' => 'required'   , 
            'role' => 'required'
        ]);
 
        if($validator->fails()) {
            $error = $validator->errors()->all();
            $this->res['message'] =$error[0] ;
            return response()->json($this->res);
        }else{ 
            $leave  = new LeaveTransactions();
            $data = $leave->TeamLeaveTransactions($request);
            $this->res['result'] = true ;
            $this->res['data'] = $data;
            return response()->json($this->res);
        }

    }
    
}