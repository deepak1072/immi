<?php
/**
 * Created by PhpStorm.
 * User: Sandeep Maurya
 * Date: 26 march 2020
 * Time: 11:30 PM
 */
namespace App\Http\Controllers\leave;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use GlobalHelpers;
use ConfigHelpers;
use Illuminate\Support\Facades\Validator;
use App\libraries\leave\CompoffApprover;
use App\libraries\leave\CompoffRequest;

class CompoffRequestControllers extends Controller {
    function __construct(){

    }

    public function SubmitRequest(Request $request){
        $validator = Validator::make($request->all(), [
            'req_user_id' => 'required',
            'for_date' => 'required|date',
            'action' => 'required',
            'type' =>'required',
            'reason' => 'required|max:255',
        ]);
        if($validator->fails()) {
            $error = $validator->errors()->all();
            $this->res['message'] = $error[0];
            return response()->json($this->res);
        }else{
            // $date = date('Y-m-d h:i:s',strtotime($request->in_time));
            // dd($request->all(),$date);
            $requ = new CompoffRequest();
            $res = $requ->_request($request); 
            return response()->json($res);
        }
    }

    /**
     * get approver 
     */
    public function Approvers(Request $request){

        $validator = Validator::make($request->all(), [
            'req_user_id' => 'required|int' ,
            'events' => 'required|int'   
        ]);
        if($validator->fails()) {
            $this->res['message'] = __('notifications.user_code_error');
            $this->res['error'] = __('notifications.user_code_error');
            return response()->json($this->res);
        }else{ 
            $approver = new CompoffApprover();
            $data = array(
                'req_user_id' => $request->get('req_user_id'),
                'events' =>$request->get('events')
            );
            $this->res['result'] = true;
            $this->res['approvers'] = $this->res['data'] = $approver->_approvers($data);
            $this->res['type'] = 'success';
            return response()->json($this->res);
        }
    }

    /**
     * rule 
     */
    public function Rule(Request $request){
        $validator = Validator::make($request->all(), [
            'req_user_id' => 'required|int' ,   
        ]);
        if($validator->fails()) {
            return response()->json([
                'result'=> false,
                'error'=>__('notifications.user_code_error'),
                'error_details'=>$validator->errors() ,
                'status'=> 200
            ]);
        }else{
            $_rule = array();
            $_rule['commencement'] = 60;
            if(!empty($_rule)){
                return response()->json([
                    'result'=> true,
                    'data'=>$_rule ,
                    'status' => 200
                ]);
            } 
            return response()->json([
                'result'=> false,
                'error'=>__('leave.no_compoff_rule') ,
                'status' => 200
            ]);
        }
    }

    public function CompoffCredit(Request $request){
        $validator = Validator::make($request->all(), [
            'req_user_id' => 'required|int' ,
            'day' => 'required'   
        ]);
        if($validator->fails()) {
            $error = $validator->errors()->all();
            $this->res['error'] = $this->res['message'] = $error[0];
            return response()->json($this->res);
        }else{
            $requ = new CompoffRequest();
            $options = array(
                'user_id' => $request->req_user_id,
                'date' => $request->day
            );
            $res = $requ->_credit($options);
            if($res['result']){
                $this->res['result'] = true;
                $this->res['type'] = 'success';
                $this->res['data'] = $res;
            }else{
                $this->res['error'] = $this->res['message'] = $res['message']; 
            }
            return response()->json($this->res);
        } 
    }
}