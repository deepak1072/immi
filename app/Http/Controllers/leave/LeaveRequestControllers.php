<?php
/**
 * Created by PhpStorm.
 * User: Sandeep Maurya
 * Date: 5/1/2019
 * Time: 8:30 AM
 */
namespace App\Http\Controllers\leave;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use GlobalHelpers;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use Tymon\JWTAuth\Exceptions\JWTException;
use App\libraries\leave\LeaveApprover;
use App\libraries\leave\LeaveDays;
use App\libraries\leave\LeaveRequest;

/**
 * @Written by Sandeep Kumar Maurya
 * @ 4:03 PM 2nd March 2019
 */

class LeaveRequestControllers extends Controller {
    function __construct(){

    }

    public function SubmitLeaveRequest(Request $request){
        $validator = Validator::make($request->all(), [
            'req_user_id' => 'required' ,
            'days' => 'required' ,
            'leave_type' => 'required'  , 
            'from_date' => 'required|date'   ,
            'to_date' => 'required|date' ,
            'from_type' => 'required|size:3' ,
            'to_type' => 'required|size:3' ,
            'type' =>'required'
        ]);
         
        if($validator->fails()) {
            $error = $validator->errors()->all();
             
            return response()->json([
                'result'=> false,
                'message'=>$error[0], 
                'status'=> 200
            ]);
        }else{
            $requ = new LeaveRequest();
            $res = $requ->_request($request); 
            return response()->json($res);
        }
    }

    /**
     * get leave approver 
     */
    public function LeaveApprovers(Request $request){

        $validator = Validator::make($request->all(), [
            'req_user_id' => 'required|int' ,
            'days' => 'required|numeric|max:255' ,
            'leave_type' => 'required|int'   
        ]);
         
        if($validator->fails()) {
            return response()->json([
                'result'=> false,
                'error'=>__('notifications.user_code_error'),
                'error_details'=>$validator->errors() ,
                'status'=> 200
            ]);
        }else{ 
            $approver = new LeaveApprover();
            $data = array(
                'req_user_id' => $request->get('req_user_id'),
                'days'=>$request->get('days'),
                'events' =>$request->get('leave_type')
            );  
            return response()->json([
                'result'=> true,
                'leave_approvers'=>$approver->_approvers($data) ,
                'status' => 200
            ]);
        }
    }

    /**
     * leave rule 
     */
    public function LeaveRule(Request $request){
        $validator = Validator::make($request->all(), [
            'id' => 'required|int' ,   
        ]);
         
        if($validator->fails()) {
            return response()->json([
                'result'=> false,
                'error'=>__('leave.leave_id_error'),
                'error_details'=>$validator->errors() ,
                'status'=> 200
            ]);
        }else{ 
            $id = $request->get('id');
            $rule = GlobalHelpers::LeaveRule($id); 
            $_rule = array();
            if(!empty($rule)){
                $rr = json_decode($rule->leave_settings,true);
                $_rule['m_id'] = $rr['leave_master_id'];
                $_rule['_id'] = $id;
                $_rule['h_d_allowed'] = $rr['half_day_allowed'];
                $_rule['l_track'] = $rr['leave_track'];
                $_rule['m_eligible'] = $rr['manager_can_apply'];
                $_rule['delegation'] = $rr['delegation_required'];
                $_rule['declaration'] = $rr['declaration_required'];
                $_rule['dec_label'] = $rr['declaration_label'];
                $_rule['attachment'] = $rr['attachments_required'];
                $_rule['att_label'] = $rr['attachments_label'];
                $_rule['commencement'] = $rr['leave_commencement_days'];
 
                return response()->json([
                    'result'=> true,
                    'rule'=>$_rule ,
                    'status' => 200
                ]);
            } 
            return response()->json([
                'result'=> false,
                'message'=>__('leave.leave_not_found') ,
                'status' => 200
            ]);
        }
    }

    /**
     * calculate leave days 
     */
    public function LeaveDays(Request $request){
        $validator = Validator::make($request->all(), [
            'id'=>'required|int',
            'lvtype' => 'required|int' , 
            'from_date' => 'required',  
            'to_date' => 'required',  
            'from_type' => 'required',  
            'to_type' => 'required',  
        ]);
         
        if($validator->fails()) {
            return response()->json([
                'result'=> false,
                'error'=>__('leave.mandate_field_error'),
                'error_details'=>$validator->errors() ,
                'status'=> 200
            ]);
        }else{ 
            $days = new LeaveDays();
            return response()->json([
                'result'=> true, 
                'days'=>$days->_days() ,
                'status'=> 200
            ]); 

        }
    }
}