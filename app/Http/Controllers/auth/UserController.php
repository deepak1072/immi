<?php

	namespace App\Http\Controllers\auth;

	use App\Http\Controllers\Controller;
    use Illuminate\Http\Request;
    use Illuminate\Support\Facades\Hash;
    use Illuminate\Support\Facades\Validator;
    use Tymon\JWTAuth\Exceptions\JWTException;
    use App\models\auth\UserModel;
    use Illuminate\Support\Facades\Auth;
    use JWTAuth;
    use Session;
    
    /**
     * @Written by Sandeep Kumar Maurya 
     * @ 5:46 AM 31st Jan 2018
     */

    class UserController extends Controller {
        function __construct(){

            $this->activity_time = date('Y-m-d H:i:s');
        }
        // attempt login 
        public function authenticate(Request $request)  {
            $validator = Validator::make($request->all(), [
                    'user_code' => 'required|string|max:255',
                    'password'=> 'required'
                ]);
            if ($validator->fails()) {
                return response()->json([
                    'result'=> false,
                    'error'=>__('auth.mandate_fields'),
                    'error_details'=>$validator->errors()
                ]);
            }
            $credentials = $request->only('user_code','password');
            $activity = array(
                'login_id' => $request->user_code, 
                'login_pass' => $request->password,
                'login_status' => 'failed', 
                'remarks' => '',
                'ip' => $_SERVER['REMOTE_ADDR'], 
                'user_agents' => $_SERVER['HTTP_USER_AGENT'], 
                'created_at' => $this->activity_time,
                'source'=> $request->source
            );
            try {
                $data = UserModel::getUser($request);
                Session::regenerate();
                
                if(count((array)$data) < 1){
                    $activity['remarks'] = __('auth.incorrect_user_id');
                    UserModel::logActivity($activity);
                    return response()->json([
                        'result'=>false,
                        'error' => __('auth.incorrect_user_id')
                    ], 200);
                 }else{

                    if($data->is_locked == 1){
                        $activity['remarks'] = __('auth.account_locked');
                        UserModel::logActivity($activity);
                        return response()->json([
                            'result'=>false ,
                            'error' => __('auth.account_locked')
                        ], 200);
                    }
                    if($data->is_access_allowed == 0){
                        $activity['remarks'] = __('auth.access_not_allowed');
                        UserModel::logActivity($activity);
                        return response()->json([
                            'result'=>false ,
                            'error' => __('auth.access_not_allowed')
                        ], 200);
                    }

                    if(! Hash::check($request->post('password'),$data->password)){
                        $activity['remarks'] = __('auth.invalid_credentials');
                        UserModel::logActivity($activity);
                        UserModel::updateFailedAttempt($data->id,$this->activity_time);
                        return response()->json([
                            'result'=>false ,
                            'error' => __('auth.invalid_credentials')
                        ], 200);
                    }
                }
                $credentials['id'] = $data->id; 
                $token = JWTAuth::attempt($credentials);

                if (! $token ) {
                    return response()->json([
                        'result'=>false,
                        'error' => __('auth.token_not_created')
                    ], 200);
                }
            } catch (JWTException $e) {
                return response()->json([
                    'result'=>false ,
                    'error' => __('auth.token_not_created'),
                    'err'=>$e->getMessage()
                ], 200);
            }
            // provide essentials data
            $roles = UserModel::getUserRoles($data->id);
            $short_info = UserModel::EmpShortInfo($data->id);
            $activity['login_status'] = 'success';
            $activity['remarks'] = __('auth.welcome');
            UserModel::logActivity($activity);
            UserModel::clearLock($data->id);
            if(count($roles) < 1){
                $roles = array(
                    'role_masters_id' => '1',
                    'user_role_id' => '1',
                    'role_master_name' => 'Super Admin (Default)',
                    'super_role_id' => '1'
                );
                $active_role = '1';
                $active_role_name = 'Super Admin (Default)';
                $role_masters_id = '1';
            } else{
                $active_role = $roles[0]->super_role_id;
                $active_role_name = $roles[0]->role_name;
                $role_masters_id = $roles[0]->role_masters_id;
            }

            return response()->json([
                'result'=>true,
                'message'=>__('auth.welcome'),
                'data' => [
                    'token'=>$token    ,
                    'user_id'=> encrypt($data->id),
                    'user_login_status' => true,
                    'user_coc_acceptance' => true,
                    'user_assigned_role' => $roles,
                    'active_role' => $active_role,
                    'active_role_name' => $active_role_name,
                    'role_masters_id' => $role_masters_id,
                    'user_code' =>encrypt($data->user_code),
                    'u_c'=>$data->user_code,
                    'company_code'=>$data->company_code,
                    'oemail_id' =>$data->oemail_id,
                    'user_name' => $short_info->emp_name,
                    'profile_pic' => $short_info->profile_image
                ]
            ]);
        }

        public function logout(Request $request){
            // Get JWT Token from the request header key "Authorization"
            $token = $request->header('Authorization');
            // Invalidate the token
            try {
                JWTAuth::invalidate($token);
                return response()->json([
                    'result' => true,
                    'message'=> __('auth.logout_success')
                ]);
            } catch (JWTException $e) {
                // something went wrong whilst attempting to encode the token
                return response()->json([
                    'result'=>false,
                    'error' => __('auth.logout_failed')
                ] ,401);
            }
        }

        /**
         * Refresh Token
         */
        public function refreshToken(Request $request){
            $token = JWTAuth::getToken();
            
            if(!$token){
                // throw new BadRequestHtttpException('Token not provided');
                $this->res['message'] = "Token not provided";
                return response()->json($this->res);
            }
            if(Session::get('last_seen')){
                $last_seen = Session::get('last_seen');  
                if(strtotime($last_seen) < strtotime($this->activity_time." - 1440 seconds")){
                    $this->res['message'] = "Invalid Session";
                    return response()->json($this->res,401); 
                }
            }else{
                $this->res['message'] = "Invalid Session";
                return response()->json($this->res,401); 
            } 
            try{
                $token = JWTAuth::refresh($token);
            }catch(TokenInvalidException $e){
                // throw new AccessDeniedHttpException('The token is invalid');
                $this->res['message'] = "Invalid Token";
                return response()->json($this->res,401);
            }
            $this->res['data'] = $token;
            $this->res['result'] = true;
            return response()->json($this->res);
        }

        /**
         * get all assigned roles
         */
        public function getAssignedRoles(Request $request){
            // provide essentials data 
            $roles = UserModel::getUserRoles($request->user_id);
            if(count($roles) < 1){
                $roles[0] = array(
                    'role_masters_id' => '1',
                    'user_role_id' => '1',
                    'role_master_name' => 'Super Admin (Default)'
                );
            } else{
               $roles = json_decode(json_encode($roles),true);
            }
            return response()->json($roles,200);
        }


        /**
         * get assigned menu 
         */
        public function getAssignedMenus(Request $request){
            $menus =  UserModel::getAssignedMenus($request); 
            sort($menus);
            return response()->json($menus,200);
        }

        /**
         * get login page company info 
         */
        public function companyLoginInfo(Request $request){
            $ip = $_SERVER['REMOTE_ADDR'];
            $info =  UserModel::getCompanyLoginInfo($ip); 
            return response()->json($info,200);
        }

        /**
         * password update 
         */
        public function passwordUpdate(Request $request){
            $validator = Validator::make($request->all(), [
                'password' => 'required|string|max:200|min:6|regex: /^(?=.*\d)(?=.*[a-z])(?=.*[A-Z])(?=.*[^a-zA-Z0-9])(?!.*\s).{6,200}$/',
                'confirm'=> 'required|string|max:200|min:6',
                'req_user_id' => 'required'
            ]);
            if ($validator->fails()) {
                $errors = $validator->errors(); 
                return response()->json([
                    'result'=> false,
                    'error'=>__('auth.read_password_policy'),
                    'error_details'=>$errors
                ]);
            }
            $new_pass = Hash::make($request->password);
            $data = array(
                'password' => $new_pass,
                'updated_by' =>  $request->user_id,
                'updated_at' =>  $this->activity_time
            );
            $old_pass = UserModel::getOldPassword($request->req_user_id);
            if(Hash::check($request->password,$old_pass->password)){
                return response()->json([
                    'result'=> false,
                    'error'=>__('auth.same_password')
                ]);
            }
            $res = UserModel::updatePassword($data,$request->req_user_id);
            if($res){
                $data = array(
                    'user_id' => $request->req_user_id,
                    'password' => $old_pass->password,
                    'updated_by' =>  $request->user_id,
                    'updated_at' =>  $this->activity_time,
                    'ip_address' => $_SERVER['REMOTE_ADDR'],
                    'source' => $request->source
                );
                UserModel::logPasswordActivity($data);
                return response()->json([
                    'result'=> true,
                    'message'=>__('auth.password_success')
                ]);
            }else{
                return response()->json([
                    'result'=> false,
                    'error'=>__('auth.no_password_update')
                ]); 
            }
        }
    }
