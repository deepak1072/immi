<?php

	namespace App\Http\Controllers\auth;

	use App\Http\Controllers\Controller;
    use Illuminate\Http\Request;
    use Illuminate\Support\Facades\Hash;
    use Illuminate\Support\Facades\Validator; 
    use App\models\auth\PasswordResetModel;  
    
   
    /**
     * @Written by Sandeep Kumar Maurya 
     * @ 12:04 AM 23rd Nov 2019
     */

    class PasswordReset extends Controller {

        /**
         * 
         * @params $request
         * return mixed
         */
        public function UpdatePassword(Request $request)  {

            $validator = Validator::make($request->all(), [
                    'user_code' => 'required|string|max:255',
                    'new_password'=> 'required|string|max:255|min:5',
                    'confirm_password'=> 'required|string|max:255|min:5'
                ]);
            if ($validator->fails()) {
                return response()->json([
                    'result'=> false,
                    'message'=>__('passwords.reset_mandate_fields'),
                    'details'=>$validator->errors()
                ]);
            }
 
            try { 

                $response = $this->PasswordResetModel->UpdatePassword($request);
                if($response){
                    return response()->json([
                        'result'=>true,
                        'message' => __('passwords.reset_success')
                    ], 200);
                }else{
                    return response()->json([
                        'result'=>false,
                        'message' => __('passwords.reset_error')
                    ], 200);
                }
            } catch (\Exception $e) {
                return response()->json([
                    'result'=>false ,
                    'error' => __('passwords.went_wrong'),
                    'details'=>$e->getMessage()
                ], 200);
            }
        }  
    }
