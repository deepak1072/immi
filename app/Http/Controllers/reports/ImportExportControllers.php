<?php
/**
 * Created by PhpStorm.
 * User: Sandeep Maurya
 * Date: 29 feb 2020
 * Time: 4:27 PM
 */
namespace App\Http\Controllers\reports;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator; 
use App\libraries\reports\ImportExports;
use Illuminate\Support\Facades\Storage;
/**
 * @Written by Sandeep Kumar Maurya
 * @ 11:03 AM 25th March 2020
 */

class ImportExportControllers extends Controller {
    public $res  = [
        'result'=> false,
        'message'=>'', 
        'status'=> 200,
        'type'=>'warn'
    ];
    function __construct(){
        $this->activity_time = date('Y-m-d H:i:s');
    }

    /**
     * leave transactions
     */
    public function Transactions(Request $request){
        $leave = new ImportExports();
        $data = $leave->Transaction($request);
        $data['result'] = true;
        if($data['data']){
            $new_data = array();
            foreach ( $data['data'] as $key => $mark ) { 
                array_push($new_data,array(
                    'created_at' =>date(config('constants.DATE_TIME_F'), strtotime($mark->created_at)) ,
                    'emp_name' => $mark->emp_name,
                    'type' => $mark->type,
                    'updated_at' => date(config('constants.DATE_F'), strtotime($mark->updated_at)),
                    'id' => $mark->id,
                    'file_link' => 'web/generated?file='.$mark->file_link,
                    '_file' => $mark->_file ,
                    'module' => $mark->module,
                    'name' => $mark->name,
                    'status' => $mark->status
                ));
            }
            $data['data'] = $new_data;
        }

        return response()->json($data);
    }

    public function Latest(Request $request){
        $leave = new ImportExports();
        $data = $leave->Latest($request);
        $data['result'] = true;
        if($data['data']){
            $new_data = array();
            foreach ( $data['data'] as $key => $mark ) { 
                 
                $name = (strlen( $mark->name) > 40) ? substr( $mark->name, 0,37) ."..." : $mark->name; 
                array_push($new_data,array(
                    'created_at' =>date(config('constants.DATE_TIME_F'), strtotime($mark->created_at)) ,
                    'updated_at' => date(config('constants.DATE_F'), strtotime($mark->updated_at)),
                    'id' => $mark->id,
                    'file_link' => 'web/generated?file='.$mark->file_link,
                    'name' =>$name ,
                    'type' => $mark->type,
                    'status' => $mark->status
                ));
            }
            $data['data'] = $new_data;
        }
        return response()->json($data);
    }

    public function getFileContent( Request $request ){
        if($request->has('file')){
            $exists = Storage::exists($request->file);
            if($exists){
                return Storage::download( $request->file );
            }
            return redirect('/welcome');
        }
        return redirect('/welcome');
    }
     
}