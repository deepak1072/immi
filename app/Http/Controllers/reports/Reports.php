<?php
/**
 * Created by PhpStorm.
 * User: Sandeep Maurya
 * Date: 29 feb 2020
 * Time: 4:27 PM
 */
namespace App\Http\Controllers\reports;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator; 
use App\libraries\reports\ImportExports;
use App\Jobs\ImmigrationReports;
/**
 * @Written by Sandeep Kumar Maurya
 * @ 11:03 AM 25th March 2020
 */

class Reports extends Controller {
    public $res  = [
        'result'=> false,
        'message'=>'', 
        'status'=> 200,
        'type'=>'warn'
    ];
    function __construct(){
        $this->activity_time = date('Y-m-d H:i:s');
    }
    
    public function GenerateReports(Request $request){
        $validator = Validator::make($request->all(), [
            'req_user_id' => 'required'
        ]);
        if($validator->fails()) {
            $error = $validator->errors()->all();
            $this->res['message'] = $error[0]; 
        }else{           
            $import = array(
                'type' => 'Export',
                'user_id' => $request->user_id,
                'name' => 'Customer Report',
                'module' => 'Immigration',
                '_file' => '',
                'file_link' => '',
                'error_link' => '',
                'descriptions' => 'Customer Report Generation',
                'status' => 'New',
                'created_at' => $this->activity_time ,
                'updated_at' => $this->activity_time 
            );
            $import_obj = new ImportExports();
            $res = $import_obj->Submit( $import );
            // $res = 1;
            if($res){
                ImmigrationReports::dispatchNow($res,$request->user_id,$request->period);
                $this->res['result'] = true;
                $this->res['type'] = 'success';
                $this->res['message'] = 'Report will be generated soon & will be available in Import/Export !';
            }else{
                $this->res['message'] = 'We could not accept your request, please try after sometime';
            }
        }
        return response()->json($this->res); 
    }    
}