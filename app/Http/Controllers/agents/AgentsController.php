<?php
/**
 * Created by PhpStorm.
 * User: Sandeep Maurya
 * Date: 19/1/2020
 * Time: 4:27 PM
 */
namespace App\Http\Controllers\agents;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;  
use App\libraries\agents\AgentsTransactions;
/**
 * @Written by Sandeep Kumar Maurya
 * @ 4:03 PM 2nd March 2019
 */

class AgentsController extends Controller {
    public $res  = [
        'result'=> false,
        'message'=>'', 
        'status'=> 200
    ];
    function __construct(){
        $this->activity_time = date('Y-m-d H:i:s');
    }

 

    /**
     *   transactions
     */
    public function Transactions(Request $request){
        $leave = new AgentsTransactions();
        $data = $leave->Transaction($request);
        $data['result'] = true;
        // dd($data );
        if($data['data']){
            $new_data = array();
            foreach ( $data['data'] as $key => $leave ) { 
                //id,name,email,mobile,result,,
                array_push($new_data,array(
                    'em_id'=> $leave->em_id,
                    'user_id' => $leave->user_id,
                    'is_access_allowed' => $leave->is_access_allowed,
                    'emp_name' => $leave->emp_name,
                    'oemail_id' => $leave->oemail_id,
                    'mobile_number' => $leave->mobile_number,    
                    'created_at' => date(config('constants.DATE_F'), strtotime($leave->created_at)),  
                ));
            }
            $data['data'] = $new_data;
        }
        return response()->json($data);
    }

    /**
     *   transaction details 
     */
    public function ClientsData(Request $request){
        $validator = Validator::make($request->all(), [
            'req_user_id' => 'required' , 
            'id' => 'required' ,
        ]);
 
        if($validator->fails()) {
            $error = $validator->errors()->all();
            $this->res['message'] =$error[0] ;
            return response()->json($this->res);
        }else{ 
            $leave = new AgentsTransactions();
            $data = $leave->TransactionDetails($request);
            if($data){
                $this->res['data'] = $data ;
                $this->res['result'] = true;
            }else{
                $this->res['message'] = 'No record(s) found ! ';
            } 
            return response()->json($this->res);
        }
    }
    
    public function DeadLineDetails(Request $request){
        $validator = Validator::make($request->all(), [
            'req_user_id' => 'required' 
        ]);
 
        if($validator->fails()) {
            $error = $validator->errors()->all();
            $this->res['message'] =$error[0] ;
            return response()->json($this->res);
        }else{ 
            $leave = new AgentsTransactions();
            $data = $leave->DeadLineDetails($request);
            if($data){
                $this->res['data'] = $data ;
                $this->res['result'] = true;
            }else{
                $this->res['message'] = 'No record(s) found ! ';
            } 
            return response()->json($this->res);
        }
    }
 

    public function Clients(Request $request){
        $messages = [];
        $customAttributes = [
            'emp_name' => 'Agent Name',
            'mobile_number' => 'Mobile Number', 
            'email' => 'Email'
        ];
        $validator = Validator::make($request->all(), [
            'req_user_id' => 'required' , 
            'emp_name' => 'required' , 
            'mobile_number' => 'required|digits_between:10,12', 
            'email' => 'email'
        ],$messages , $customAttributes  ); 
        if( ! $request->has('em_id')){
            if(empty($request->password)){
                $this->res['message'] = 'Password is Required!' ;
                return response()->json($this->res);
            }
        }
        if($validator->fails()) {
            $error = $validator->errors()->all();
            $this->res['message'] =$error[0] ;
            return response()->json($this->res);
        }else{ 
            $leave  = new AgentsTransactions();
            $this->res =  $leave->_request($request);
            return response()->json($this->res);
        }
    }

    public function DeadLine(Request $request){
        $messages = [];
        $customAttributes = [
            'dl_doc' => 'DOC Deadline',
            'dl_crm' => 'CRM Deadline',
            'dl_online' => 'Online Counselling Deadline',
            'dl_oncall' => 'Oncall Counselling Deadline',
            'dl_cic' => 'CIC Deadline',
            'dl_mail' => 'Mail Verification Deadline',
            'dl_approval' => 'Approval Deadline',
            'dl_visit' => 'Visit Deadline',
        ];
        $validator = Validator::make($request->all(), [
            'req_user_id' => 'required' , 
            'dl_doc' => 'required|numeric|digits_between:1,5' , 
            'dl_crm' => 'required|numeric|digits_between:1,5' , 
            'dl_online' => 'required|numeric|digits_between:1,5' , 
            'dl_oncall' => 'required|numeric|digits_between:1,5' , 
            'dl_cic' => 'required|numeric|digits_between:1,5' , 
            'dl_mail' => 'required|numeric|digits_between:1,5' ,
            'dl_approval' => 'required|numeric|digits_between:1,5' ,
            'dl_visit' => 'required|numeric|digits_between:1,5' ,
        ],$messages ,$customAttributes  ); 
         
        if($validator->fails()) {
            $error = $validator->errors()->all();
            $this->res['message'] =$error[0] ;
            return response()->json($this->res);
        }else{ 
            $leave  = new AgentsTransactions();
            $this->res =  $leave->DeadLine($request);
            return response()->json($this->res);
        }
    }
 
    public function UpdateAgentStatus(Request $request){
    	$leave  = new AgentsTransactions();
        $this->res =  $leave->AgentStatus($request);
        return response()->json( $this->res );
    }
}