<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;
use Tymon\JWTAuth\Contracts\JWTSubject;
use JWTAuth;
use Session;

class AuthenticateOnceWithBasicAuth implements JWTSubject
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
         
        try { 

            $user = JWTAuth::parseToken()->authenticate();

        } catch (\Tymon\JWTAuth\Exceptions\TokenInvalidException $e){
            return response()->json([
                'result' =>false,
                'error' =>'Token is Invalid'
            ],401);
        }   catch(\Tymon\JWTAuth\Exceptions\TokenExpiredException $e){
            return response()->json([
                'result' =>false,
                'error'=>'Token has Expired'
            ],401);
        } catch (\Tymon\JWTAuth\Exceptions\JWTException $e) {

            return response()->json([
                'result'=>false,
                'error'=>'Token not provided'
            ],401);

        }
        catch (Exception $e) {

            return response()->json([
                'result' =>false,
                'error'=>'Authorization Token not found'
            ],401);

        }

        $new_request = [
            'user_code' =>  $user->user_code,
            'company_code' => $user->company_code,
            'user_id' => $user->id 
        ]; 
        // decrypt request user id 
        if($request->filled('req_user_id')){  
            $new_request['req_user_id'] = decrypt($request->req_user_id) ; 
        }
        if($request->filled('active_role')){  
            $new_request['role'] = $request->active_role ; 
        }
        
        if($request->filled('source')){  
            $new_request['source'] = $request->source ; 
        }else{
            $new_request['source'] = 'W' ; 
        } 
        // add aditional info in request 
        if( strpos( $_SERVER['REQUEST_URI'] ,'refresh-token') !== false ){
            
        }else{
            Session::put('last_seen', date('Y-m-d H:i:s'));
        }

        $request->request->add($new_request); 
        return $next($request);
    }



    // get authenticated user's details
    public function getAuthenticatedUser() {
        try {

            if (! $user = JWTAuth::parseToken()->authenticate()) {
                return response()->json(['user_not_found'], 404);
            }

        } catch (\Tymon\JWTAuth\Exceptions\TokenExpiredException $e) {

            return response()->json(['token_expired'], $e->getStatusCode());

        } catch (\Tymon\JWTAuth\Exceptions\TokenInvalidException $e) {

            return response()->json(['token_invalid'], $e->getStatusCode());

        } catch (\Tymon\JWTAuth\Exceptions\JWTException $e) {

            return response()->json(['token_absent'], $e->getStatusCode());

        }
        return response()->json(compact('user'));
    }
    


    public function getJWTIdentifier()
    {
        return $this->getKey();
    }

    /**
     * Return a key value array, containing any custom claims to be added to the JWT.
     *
     * @return array
     */
    public function getJWTCustomClaims()
    {
        return [];
    }

   
    
}
