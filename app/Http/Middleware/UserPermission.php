<?php

namespace App\Http\Middleware;

use Closure;
use App\models\auth\UserModel;
class UserPermission
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * check for data permissions over portals
     * @return mixed
     */
    public function handle($request, Closure $next)
    {  
        $user_id = $request->user_id;
        $role_masters_id = $request->role_masters_id;
        $active_role = $request->active_role;
        $roles = UserModel::getUserRoles($request);
        $role = $roles->pluck('role_masters_id');
        $role = json_decode(json_encode($role),true);
        // if(!in_array($request->role_masters_id,$role)){
        //     return response()->json([
        //         'result' =>false,
        //         'error'=>'You are not allowed to perform this action ',
        //         'data'=>''
        //     ],406);
        // } 
        return $next($request);
    }
}
