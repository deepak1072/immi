<?php  

namespace App\Helpers;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
class GlobalHelpers
{
    public static function OuApplicableStatus(String $json)
    {
        return true;
    }

    public static  function getFullPeriod($sh){
        $data['1FH'] = 'First Half';
        $data['2FH'] = 'Second Half';
        $data['2FD'] = 'Full Day';
        return $data[$sh];
    }

    // get basic employee ou data 
    public static function EmpOfficialOUBasic(String $id){
    	$data=DB::table("emp_official")
            ->selectRaw("user_id,eo_id,designation_code,function_code,subfunction_code,busi_code,subbusi_code,comp_code,loc_code,wloc_code,cost_code,process_code,type_code,grade_code,division_code,region_code")
            ->where("user_id",$id)
            ->first();
        return $data; 
    }
    // get basic employee official data 
    public static function EmpOfficialBasic(String $id){
        $data=DB::table("emp_official")
            ->selectRaw("user_id,eo_id,cast(dol as date) as dol,cast(doj as date) as doj,mngr_code,mngr_code2,ero,effective_date,status_code")
            ->where("user_id",$id)
            ->first();
        return $data; 
    }    
    // employee confirmation data 
    public static function EmpConfirmationBasic(String $id){
        $data=DB::table("emp_master")
            ->selectRaw("emp_code,user_id,emp_name,oemail_id,confirm_req,confirm_policy_id,cast(confirm_start_date as date) as confirm_start_date,cast(confirm_due_date as date ) as confirm_due_date ,cast(confirmation_date as date) as confirmation_date ,confirm_status")
            ->where("user_id",$id)
            ->first();
        return $data; 
    }

    // get active leave types for specific company
    public static function ActiveLeaveTypes(){
        $data=DB::table("leave_types")
            ->selectRaw("leave_settings,leave_master_id,leave_short_name,leave_desc,leave_name,leave_id")
            ->where("leave_status",1)
            ->get();
        return $data; 

    }

    // get leave and financial year 
    public static function ActivityYear(string $activity_type){
        $data=DB::table("activity_year")
            ->selectRaw("activity_year,activity_start_date,cast(effective_date as date) as effective_date")
            ->where("activity_type",$activity_type) 
            ->orderBy("ayr_id","desc")
            ->first();
        return $data; 
    }

    // check ou applicability 
    public static function EmpOuApplicable(string $user_id,array $ou_data){
        $emp_data = self::EmpOfficialOUBasic($user_id);
        $check_point = array();
        $flag = false ;

        if(!empty($emp_data) > 0){
            if(count($ou_data) > 0){ 
                foreach ($ou_data as $key => $ou) {
                    // company applicability
                    if($ou['name'] == 'Company'){
                        $company = array();
                        foreach ($ou['children'] as $kk => $comp) {
                            if($comp['flag'] == 1){
                                $company[] = $comp['id'];
                            } 
                        }
                        if(sizeof($company) > 0){
                            if(!in_array($emp_data->comp_code, $company)){
                                $check_point[] = 'false';
                            }
                        }
                    }
                    // process applicability
                    if($ou['name'] == 'Process'){
                        $process = array();
                        foreach ($ou['children'] as $kk => $proc) {
                            if($proc['flag'] == 1){
                                $process[] = $proc['id'];
                            } 
                        }
                        if(sizeof($process) > 0){
                            if(!in_array($emp_data->process_code, $process)){
                                $check_point[] = 'false';
                            }
                        }
                    }

                    // cost center applicability
                    if($ou['name'] == 'Cost Centers'){
                        $cost = array();
                        foreach ($ou['children'] as $kk => $cos) {
                            if($cos['flag'] == 1){
                                $cost[] = $cos['id'];
                            } 
                        }
                        if(sizeof($cost) > 0){
                            if(!in_array($emp_data->cost_code, $cost)){
                                $check_point[] = 'false';
                            }
                        }
                    }
                }

                if(!in_array('false', $check_point)){
                    $flag = true;
                }
            }
        }

        return $flag;
    } 

    /**
     * get active user_id
     */
    public static function ActiveUsers(String $id = ''){
        $query =  DB::table("emp_official as eo");
        $query->selectRaw("eo.user_id,em_id,cast(dol as date) as dol,cast(doj as date) as doj,mngr_code,mngr_code2,ero,effective_date,status_code,gender,em.emp_name"); 
        $query->join('emp_master as em','em.user_id','=','eo.user_id');
        $query->whereIn("status_code",[1,2]) ;
        if($id != ''){
            $query->where('eo.user_id',$id);
        } 
        return $query->get();    
    }

      /**
     * get workflow levels 
     */
    public static function WorkflowLevels($id ){
        $query =  DB::table("workflow_levels");
        $query->selectRaw(" `wf_id`, `wf_level`, `approver`, `requ_noti`, `approver_noti`, `next_level_noti`, `days_limit`, `fixed_cc`, `fixed_bcc`, `dynamic_cc`, `dynamic_bcc` ");   
        $query->where('wf_id',$id); 
        $query->orderBy('wf_level_id','asc');
        return $query->get();    
    }
    
    /**
     * get authorities details 
     * 
     */
    public static function AuthorityDetails($authority,$id,$type = 1){
        $authority_details = array();
        $authority = strtolower($authority);
        switch($type){
            case 1 : 
                // fixed approver
                switch($authority){
                    case 'rm' : 
                        // reporting manager 
                        $query =  DB::table("emp_official as eo");
                        $query->selectRaw("mngr_code");  
                        $query->whereIn("status_code",[1,2]) ; 
                        $query->where('eo.user_id',$id); 
                        $mngr = $query->first(); 
                        if(!empty($mngr)){ 
                            $query =  DB::table("emp_official as eo");
                            $query->selectRaw("dm.designation_name,em.emp_name,em.user_id,em.profile_image,em.emp_code,em.access_code");   
                            $query->join('emp_master as em','em.user_id','=','eo.user_id');
                            $query->leftJoin('designation_masters as dm','dm.designation_id','=','eo.designation_code');
                            $query->where('em.user_id',$mngr->mngr_code); 
                            $mngr_details = $query->first();
                            if(!empty($mngr_details)){ 
                                $authority_details['name'] = $mngr_details->emp_name;
                                $authority_details['id'] = $mngr_details->user_id;
                                $authority_details['authority'] = $authority; 
                                $authority_details['code'] = $mngr_details->emp_code;
                                $authority_details['designation'] = $mngr_details->designation_name;
                                $authority_details['pr_image'] = $mngr_details->profile_image;
                                $authority_details['short_name'] = self::ShortName($mngr_details->emp_name); 
                            }
                        }  
                    break;
                    case 'fm' : 
                        // functional manager 
                        $query =  DB::table("emp_official as eo");
                        $query->selectRaw("mngr_code2");  
                        $query->whereIn("status_code",[1,2]) ; 
                        $query->where('eo.user_id',$id); 
                        $mngr = $query->first(); 
                        if(!empty($mngr)){
                            
                            $query =  DB::table("emp_official as eo");
                            $query->selectRaw("dm.designation_name,em.emp_name,em.user_id,em.profile_image,em.emp_code,em.access_code");   
                            $query->join('emp_master as em','em.user_id','=','eo.user_id');
                            $query->leftJoin('designation_masters as dm','dm.designation_id','=','eo.designation_code');
                            $query->where('eo.user_id',$mngr->mngr_code2); 
                            $mngr_details = $query->first();
                            if(!empty($mngr_details)){
                                $authority_details['name'] = $mngr_details->emp_name;
                                $authority_details['id'] = $mngr_details->user_id;
                                $authority_details['authority'] = $authority; 
                                $authority_details['code'] = $mngr_details->emp_code;
                                $authority_details['designation'] = $mngr_details->designation_name;
                                $authority_details['pr_image'] = $mngr_details->profile_image;
                                $authority_details['short_name'] = self::ShortName($mngr_details->emp_name); 
                            }
                        } 
                    break;
                    case 'ero' : 
                        // ERO
                        $query =  DB::table("emp_official as eo");
                        $query->selectRaw("ero");  
                        $query->whereIn("status_code",[1,2]) ; 
                        $query->where('eo.user_id',$id); 
                        $mngr = $query->first();
                        if(!empty($mngr)){ 
                            $query =  DB::table("emp_official as eo");
                            $query->selectRaw("dm.designation_name,em.emp_name,em.user_id,em.profile_image,em.emp_code,em.access_code");   
                            $query->join('emp_master as em','em.user_id','=','eo.user_id');
                            $query->leftJoin('designation_masters as dm','dm.designation_id','=','eo.designation_code');
                            $query->where('eo.user_id',$mngr->ero); 
                            $mngr_details = $query->first();
                            if(!empty($mngr_details)){
                                $authority_details['name'] = $mngr_details->emp_name;
                                $authority_details['id'] = $mngr_details->user_id;
                                $authority_details['authority'] = $authority; 
                                $authority_details['code'] = $mngr_details->emp_code;
                                $authority_details['designation'] = $mngr_details->designation_name;
                                $authority_details['pr_image'] = $mngr_details->profile_image;
                                $authority_details['short_name'] = self::ShortName($mngr_details->emp_name); 
                            }
                        } 
                    break;
                    case 'hr' : 
                        //HR
                        $query =  DB::table("emp_official as eo");
                        $query->selectRaw("ero");  
                        $query->whereIn("status_code",[1,2]) ; 
                        $query->where('eo.user_id',$id); 
                        $mngr = $query->first();
                        if(!empty($mngr)){ 
                            $query =  DB::table("emp_official as eo");
                            $query->selectRaw("dm.designation_name,em.emp_name,em.user_id,em.profile_image,em.emp_code,em.access_code");   
                            $query->join('emp_master as em','em.user_id','=','eo.user_id');
                            $query->leftJoin('designation_masters as dm','dm.designation_id','=','eo.designation_code');
                            $query->where('eo.user_id',$mngr->ero); 
                            $mngr_details = $query->first();
                            if(!empty($mngr_details)){
                                $authority_details['name'] = $mngr_details->emp_name;
                                $authority_details['id'] = $mngr_details->user_id;
                                $authority_details['authority'] = $authority;
                                $authority_details['code'] = $mngr_details->emp_code;
                                $authority_details['designation'] = $mngr_details->designation_name;
                                $authority_details['pr_image'] = $mngr_details->profile_image;
                                $authority_details['short_name'] = self::ShortName($mngr_details->emp_name); 
                            }
                        } 
                    break;
                    case 'l1' : 
                        // Manager 
                        $query =  DB::table("emp_official as eo");
                        $query->selectRaw("mngr_code");  
                        $query->whereIn("status_code",[1,2]) ; 
                        $query->where('eo.user_id',$id); 
                        $mngr = $query->first(); 
                        if(!empty($mngr)){ 
                            $query =  DB::table("emp_official as eo");
                            $query->selectRaw("dm.designation_name,em.emp_name,em.user_id,em.profile_image,em.emp_code,em.access_code");   
                            $query->join('emp_master as em','em.user_id','=','eo.user_id');
                            $query->leftJoin('designation_masters as dm','dm.designation_id','=','eo.designation_code');
                            $query->where('em.user_id',$mngr->mngr_code); 
                            $mngr_details = $query->first();
                            if(!empty($mngr_details)){ 
                                $authority_details['name'] = $mngr_details->emp_name;
                                $authority_details['id'] = $mngr_details->user_id;
                                $authority_details['authority'] = $authority;
                                $authority_details['code'] = $mngr_details->emp_code;
                                $authority_details['designation'] = $mngr_details->designation_name;
                                $authority_details['pr_image'] = $mngr_details->profile_image;
                                $authority_details['short_name'] = self::ShortName($mngr_details->emp_name); 
                            }
                        } 
                    break;
                    case 'l2' : 
                        // Manager of l1 
                        $query =  DB::table("emp_official as eo");
                        $query->selectRaw("mngr_code");  
                        $query->whereIn("status_code",[1,2]) ; 
                        $query->where('eo.user_id',$id); 
                        $mngr = $query->first(); 
                        if(!empty($mngr)){
                            $query =  DB::table("emp_official as eo");
                            $query->selectRaw("mngr_code");  
                            $query->whereIn("status_code",[1,2]) ; 
                            $query->where('eo.user_id',$mngr->mngr_code); 
                            $mngr = $query->first(); 
                            if(!empty($mngr)){
                                $query =  DB::table("emp_official as eo");
                                $query->selectRaw("dm.designation_name,em.emp_name,em.user_id,em.profile_image,em.emp_code,em.access_code");   
                                $query->join('emp_master as em','em.user_id','=','eo.user_id');
                                $query->leftJoin('designation_masters as dm','dm.designation_id','=','eo.designation_code');
                                $query->where('em.user_id',$mngr->mngr_code); 
                                $mngr_details = $query->first();
                                if(!empty($mngr_details)){ 
                                    $authority_details['name'] = $mngr_details->emp_name;
                                    $authority_details['id'] = $mngr_details->user_id;
                                    $authority_details['authority'] = $authority;
                                    $authority_details['code'] = $mngr_details->emp_code;
                                    $authority_details['designation'] = $mngr_details->designation_name;
                                    $authority_details['pr_image'] = $mngr_details->profile_image;
                                    $authority_details['short_name'] = self::ShortName($mngr_details->emp_name); 
                                }
                            }
                            
                        } 
                    break;
                    case 'l3' : 
                        // manager of l2
                        $query =  DB::table("emp_official as eo");
                        $query->selectRaw("mngr_code");  
                        $query->whereIn("status_code",[1,2]) ; 
                        $query->where('eo.user_id',$id); 
                        $mngr = $query->first(); 
                        if(!empty($mngr)){
                            $query =  DB::table("emp_official as eo");
                            $query->selectRaw("mngr_code");  
                            $query->whereIn("status_code",[1,2]) ; 
                            $query->where('eo.user_id',$mngr->mngr_code); 
                            $mngr = $query->first(); 
                            if(!empty($mngr)){
                                $query =  DB::table("emp_official as eo");
                                $query->selectRaw("mngr_code");  
                                $query->whereIn("status_code",[1,2]) ; 
                                $query->where('eo.user_id',$mngr->mngr_code); 
                                $mngr = $query->first(); 
                                if(!empty($mngr)){
                                    $query =  DB::table("emp_official as eo");
                                    $query->selectRaw("dm.designation_name,em.emp_name,em.user_id,em.profile_image,em.emp_code,em.access_code");   
                                    $query->join('emp_master as em','em.user_id','=','eo.user_id');
                                    $query->leftJoin('designation_masters as dm','dm.designation_id','=','eo.designation_code');
                                    $query->where('em.user_id',$mngr->mngr_code); 
                                    $mngr_details = $query->first();
                                    if(!empty($mngr_details)){ 
                                        $authority_details['name'] = $mngr_details->emp_name;
                                        $authority_details['id'] = $mngr_details->user_id;
                                        $authority_details['authority'] = $authority;
                                        $authority_details['code'] = $mngr_details->emp_code;
                                        $authority_details['designation'] = $mngr_details->designation_name;
                                        $authority_details['pr_image'] = $mngr_details->profile_image;
                                        $authority_details['short_name'] = self::ShortName($mngr_details->emp_name); 
                                    }
                                }
                                
                            } 
                        }
                    break;
                    case 'buh' : 
                        // business unit head 
                    break;
                    default : 
                    break;
                }
                break ;
                case 2 : 
                    // dynamic approver
                break;
                case 3 : 
                    // custom approver 
                break;

        } 

        return  $authority_details;
    }

    public static function ShortName($name){
        $expr = '/(?<=\s|^)[A-Z]/';
        $matches ='';
        preg_match_all($expr, ucwords($name), $matches);
        $text[] = $matches[0][0];  
        $text[] = (isset($matches[0][1])) ? $matches[0][1] : '';  
        $result = implode('', $text);
        return $result;
    }

    public static function LeaveRule($id){
        $data=DB::table("leave_types")
            ->selectRaw("leave_settings,leave_master_id,leave_short_name,leave_desc,leave_name,leave_id")
            ->where("leave_status",1)
            ->where('leave_id',$id)
            ->first();
        return $data; 
    }

    public static function getLeaveByShortName($short_name){
        $data=DB::table("leave_types")
        ->selectRaw("leave_settings,leave_master_id,leave_short_name,leave_desc,leave_name,leave_id")
        ->where("leave_status",1)
        ->where('leave_short_name',$short_name)
        ->first();
        return $data; 
    }

    public static function getUserByCode($code){
        $data=DB::table("emp_master")
        ->selectRaw("user_id,access_code,emp_code,emp_name,oemail_id")
        ->where("emp_code",$code)
        ->first();
        return $data; 
    }


}