<?php
namespace App\Helpers;
use Illuminate\Support\Facades\Storage;
class GenericHelpers
{

    public static function csvToArray($filename='', $delimiter=',')
    {
        $header = NULL;
        $data = array();
        $exists = Storage::disk('local')->exists($filename);
        $filename = config('constants.__DOCUMENT__').$filename;
        if ( $exists ){
            if(($handle = fopen($filename, 'r')) !== FALSE){
                while (($row = fgetcsv($handle, 100000, $delimiter)) !== FALSE)
                {
                    $row = array_map('trim', $row);
                    $data[] = $row;
                }
            }
            fclose($handle);
        }
        return $data;
    }

    public static function csvToArrayWithHeader($filename='', $delimiter=',')
    {
        $header = NULL;
        $data = array();
        $exists = Storage::disk('local')->exists($filename);
        $filename = config('constants.__DOCUMENT__').$filename;
        if ( $exists )
        {
            // $contents = Storage::get($filename);
            if(($handle = fopen($filename, 'r')) !== FALSE){
            // dd($exists , $contents);
                while (($row = fgetcsv($handle, 100000, $delimiter)) !== FALSE)
                {
                    $row = array_map('trim', $row);
                    if(!$header){
                        $header = $row;
                    }else{
                        if(count($header) != count($row)){
                            $count = min(count($header) , count($row));
                            $data[] = array_combine(array_slice($header, 0, $count), array_slice($row, 0, $count));
                        }
                        else{
                            $data[] = array_combine($header, $row);
                        }
                    }
                }
            }
            fclose($handle);
        }
        return $data;
    }

    public static function arrayToCsv($results,$filename = ''){
         
    
        $filename .= "_".date('YmdHis').".csv";
        $file = config('constants.__DOCUMENT__').$filename;
        try{
            $stream = fopen($file, 'a');
            if( $stream !== false ){
                if(count($results) > 0){
                    foreach ($results as $item){
                        fputcsv($stream, $item);
                    }
                }
            }
            fclose($stream);
        }catch(\Exception $e){
            return false;
        }
        return $filename ;
    }

}