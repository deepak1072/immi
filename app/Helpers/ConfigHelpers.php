<?php  

namespace App\Helpers;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class ConfigHelpers
{
    // config single
    public static function config(String $code){
    	$data=DB::table("configurations")
            ->selectRaw("_code, _value, _label")
            ->where("_code",$code)
            ->first();
        if(!empty($data)){
            return $data->_value;
        }else{ 
            return 0;
        }
    }
    // config multiple
    public static function configAll(String $code){
    	$data=DB::table("configurations")
            ->selectRaw("_code, _value, _label")
            ->where("_code",$code)
            ->get();
        return $data; 
    }  
}