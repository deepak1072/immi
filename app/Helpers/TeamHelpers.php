<?php  

namespace App\Helpers;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use GlobalHelpers ;
class TeamHelpers
{
    public function PermittedUsers(Request $request){
        switch($request->role){
            case 2 : 
                // manager
                break ;
            case 3 : 
                //ero 
        }
    }


    public static function OuTeam($ou_data = ''){
        
        if(empty($ou_data)){
            return GlobalHelpers::ActiveUsers();
        }else{
            if(count($ou_data) > 0){ 
                $query =  DB::table("emp_official as eo");
                $query->selectRaw("eo.user_id,em_id,cast(dol as date) as dol,cast(doj as date) as doj,mngr_code,mngr_code2,ero,effective_date,status_code,gender,em.emp_name"); 
                $query->join('emp_master as em','em.user_id','=','eo.user_id');
                $query->whereIn("status_code",[1,2]) ;
         
                foreach ($ou_data as $key => $ou) {
                    // company applicability
                    if($ou['name'] == 'Company'){
                        $company = array();
                        foreach ($ou['children'] as $kk => $comp) {
                            if($comp['flag'] == 1){
                                $company[] = $comp['id'];
                            } 
                        }
                        if(sizeof($company) > 0){
                            $query->whereIn("comp_code",$company) ;
                        }
                    }
                    // process applicability
                    if($ou['name'] == 'Process'){
                        $process = array();
                        foreach ($ou['children'] as $kk => $proc) {
                            if($proc['flag'] == 1){
                                $process[] = $proc['id'];
                            } 
                        }
                        if(sizeof($process) > 0){
                            $query->whereIn("process_code",$process) ;
                        }
                    }

                    // cost center applicability
                    if($ou['name'] == 'Cost Centers'){
                        $cost = array();
                        foreach ($ou['children'] as $kk => $cos) {
                            if($cos['flag'] == 1){
                                $cost[] = $cos['id'];
                            } 
                        }
                        if(sizeof($cost) > 0){
                            $query->whereIn("code_code",$cost) ;
                        }
                    }
                }

                return  $query->get(); 
            }
            return [];
        }
    }
}