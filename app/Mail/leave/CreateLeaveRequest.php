<?php

namespace App\Mail\leave;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue; 

class CreateLeaveRequest extends Mailable
{
    use Queueable, SerializesModels;

    /**
     * Create Leave Request Mail to Requester 
     *
     * @return void
     */
    private $data ;
    public function __construct($data)
    {
        $this->data = $data;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    { 
        return $this->view('view.leave.create_leave_request')->with($this->data);
    }
}
