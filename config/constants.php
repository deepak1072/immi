<?php

return [
    'DATE'    => 'Y-m-d',                   //2020-01-18
    'DATE_1' => "F j, Y",                 // March 10, 2001 
    'DATE_F' => "M j, Y",                 //Mar 10, 2001   
    'DATE_TIME'      => 'Y-m-d H:i:s',                 // 2020-01-18 05:05:00
    'DATE_TIME_1' => "F j, Y, g:i a",                 // March 10, 2001, 5:16 pm 
    'TIME'    => "H:i:s",                         // 17:16:18   
    'TIME_1'    => "h:i:s A",                         // 05:16:18   
    'TIME_2'    => "h:i a",                         // 05:16:18   
    'DATE_TIME_F'=> "M j, Y h:i A"  ,           // Mar 10,2001 17:16:18 
    'DATE_TIME_F_1'=> "M j, Y H:i"   ,          // Mar 10,2001 17:16
    '__DOCUMENT__' => 'S:/immigration/storage/app/',
    '__UPLOAD_ERROR__' => 'S:/immigration/public/upload_errors/'
]; 