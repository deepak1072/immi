<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

//Route::middleware('auth:api')->get('/user', function (Request $request) {
//    return $request->user();
//});

Route::post('/login','auth\UserController@authenticate');

Route::get('/company-login-info','auth\UserController@companyLoginInfo');



Route::group(['middleware' => ['jwt.verify','user_permission']], function() {
    // for user specific
    Route::post('/assigned-roles','auth\UserController@getAssignedRoles');
    Route::post('/assigned-menus','auth\UserController@getAssignedMenus');
	Route::get('/refresh-token','auth\UserController@refreshToken');
	Route::get('/projects',function (Request $request){
		return response()->json([
			'result'=>false,
			'error' => __('auth.incorrect_user_id')
		], 402);
	});


	Route::get('/customer','customer\CustomerControllers@Transactions');
	Route::post('/submit-customer','customer\CustomerControllers@Clients');
	Route::get('/customer-data','customer\CustomerControllers@ClientsData');
	Route::get('/dashboard','dashboard\DashboardControllers@ClientCount');
	Route::get('/agents-history','dashboard\DashboardControllers@AgentsHistory');
	Route::get('/agents','agents\AgentsController@Transactions');
	Route::put('/agents','agents\AgentsController@UpdateAgentStatus');
	Route::post('/submit-agents','agents\AgentsController@Clients');
	Route::get('/agents-data','agents\AgentsController@ClientsData');
	Route::get('/deadline-info','agents\AgentsController@DeadLineDetails');
	Route::post('/deadline','agents\AgentsController@DeadLine');
	
	
    Route::post('/password','auth\UserController@passwordUpdate');
    Route::post('/logout','auth\UserController@logout');
    Route::post('/get-user','auth\UserController@getAuthenticatedUser');
	

	Route::get('/parent-menu','admin_settings\AdminSettings@getParentMenu');
	Route::post('/create-menu','admin_settings\AdminSettings@createNewMenu');
	Route::post('/update-menu','admin_settings\AdminSettings@UpdateMenu');

	// for menu
	Route::get('/menu-list','admin_settings\AdminSettings@fetchMenuList');
	Route::put('/menu-list','admin_settings\AdminSettings@updateMenuStatus');
	Route::get('/menu-details','admin_settings\AdminSettings@getMenuDetails');
	Route::get('/menu-masters','admin_settings\AdminSettings@getMenuMasters'); 

	// for company
	Route::post('/create-company','admin_settings\CompanySettings@createNewCompany');
	Route::post('/update-company','admin_settings\CompanySettings@updateCompany');
	Route::get('/company-list','admin_settings\CompanySettings@fetchCompanyList');
	Route::get('/company-details','admin_settings\CompanySettings@fetchCompanyDetails');
	Route::get('/master-company','admin_settings\CompanySettings@getActiveCompany');

	// for roles
	Route::post('/add-update-roles','admin_settings\RolesSettings@AddUpdateRoles');
	Route::get('/roles-list','admin_settings\RolesSettings@fetchRolesList');
	Route::put('/roles-list','admin_settings\RolesSettings@UpdateRoleStatus');
	Route::get('/role-details','admin_settings\RolesSettings@GetRoleDetails');
	Route::get('/assigned-roles-list','admin_settings\RolesSettings@getAssignedRolesList');
	Route::put('/assigned-roles-list','admin_settings\RolesSettings@AssignedRolesStatusUpdate');
	Route::get('/users-roles-list','admin_settings\RolesSettings@getUsersRolesList');


	// for division
	Route::post('/add-update-division','admin_settings\DivisionSettings@AddUpdateDivisions');
	Route::get('/division-list','admin_settings\DivisionSettings@fetchDivisionList');
	Route::put('/division-list','admin_settings\DivisionSettings@UpdateDivisionStatus');
	Route::get('/division-details','admin_settings\DivisionSettings@GetDivisionDetails');

	// for workflow 
	Route::post('/add-update-workflow','admin_settings\WorkFlowSettings@AddUpdate');
	Route::get('/work-flow-list','admin_settings\WorkFlowSettings@fetchList');
	Route::put('/work-flow-list','admin_settings\WorkFlowSettings@UpdateStatus');
	Route::get('/workflow-details','admin_settings\WorkFlowSettings@GetDetails');
	Route::get('/get-events','admin_settings\WorkFlowSettings@GetEvents');
	Route::get('/authorities','admin_settings\WorkFlowSettings@GetAuthorities');
	Route::get('/modules','admin_settings\WorkFlowSettings@GetModules');
	
	// for process
	Route::post('/add-update-process','admin_settings\ProcessSettings@AddUpdateProcess');
	Route::get('/process-list','admin_settings\ProcessSettings@fetchProcessList');
	Route::put('/process-list','admin_settings\ProcessSettings@UpdateProcessStatus');
	Route::get('/process-details','admin_settings\ProcessSettings@GetProcessDetails');

	// for grade
	Route::post('/add-update-grade','admin_settings\GradeSettings@AddUpdateGrade');
	Route::get('/grade-list','admin_settings\GradeSettings@fetchGradeList');
	Route::put('/grade-list','admin_settings\GradeSettings@UpdateGradeStatus');
	Route::get('/grade-details','admin_settings\GradeSettings@GetGradeDetails');

	// for employee type 
	Route::post('/add-update-employee-type','admin_settings\EmployeeTypeSettings@AddUpdateEmployeeType');
	Route::get('/employee-type-list','admin_settings\EmployeeTypeSettings@fetchEmployeeTypeList');
	Route::put('/employee-type-list','admin_settings\EmployeeTypeSettings@UpdateEmployeeTypeStatus');
	Route::get('/employee-type-details','admin_settings\EmployeeTypeSettings@GetEmployeeTypeDetails');

	// for regions
	Route::post('/add-update-regions','admin_settings\RegionsSettings@AddUpdateRegions');
	Route::get('/regions-list','admin_settings\RegionsSettings@fetchRegionsList');
	Route::put('/regions-list','admin_settings\RegionsSettings@UpdateRegionsStatus');
	Route::get('/regions-details','admin_settings\RegionsSettings@GetRegionsDetails');

	// for designation
	Route::post('/add-update-designation','admin_settings\DesignationSettings@AddUpdateDesignation');
	Route::get('/designation-list','admin_settings\DesignationSettings@fetchDesignationList');
	Route::put('/designation-list','admin_settings\DesignationSettings@UpdateDesignationStatus');
	Route::get('/designation-details','admin_settings\DesignationSettings@GetDesignationDetails');

	// for cost centers
	Route::post('/add-update-cost-center','admin_settings\CostCenterSettings@AddUpdateCostCenter');
	Route::get('/cost-center-list','admin_settings\CostCenterSettings@fetchCostCenterList');
	Route::put('/cost-center-list','admin_settings\CostCenterSettings@UpdateCostCenterStatus');
	Route::get('/cost-center-details','admin_settings\CostCenterSettings@GetCostCenterDetails');

	// for qualification
	Route::post('/add-update-qualification','admin_settings\QualificationSettings@AddUpdateQualification');
	Route::get('/qualification-list','admin_settings\QualificationSettings@fetchQualificationList');
	Route::put('/qualification-list','admin_settings\QualificationSettings@UpdateQualificationStatus');
	Route::get('/qualification-details','admin_settings\QualificationSettings@GetQualificationDetails');

	// for function
	Route::post('/add-update-function','admin_settings\FunctionSettings@AddUpdateFunction');
	Route::get('/function-list','admin_settings\FunctionSettings@fetchFunctionList');
	Route::put('/function-list','admin_settings\FunctionSettings@UpdateFunctionStatus');
	Route::get('/function-details','admin_settings\FunctionSettings@GetFunctionDetails');

	// for leave
	Route::post('/add-update-leave','admin_settings\LeaveSettings@AddUpdateLeave');
	Route::put('/leave-list','admin_settings\LeaveSettings@UpdateLeaveStatus');
	Route::get('/leave-details','admin_settings\LeaveSettings@GetLeaveDetails');
	Route::get('/leave-list','admin_settings\LeaveSettings@fetchLeaveList');
	Route::get('/leave-masters','admin_settings\LeaveSettings@fetchLeaveMasters');

	Route::get('/leave-balance','leave\LeaveBalanceControllers@LeaveBalance');
	Route::get('/credit-balance','leave\LeaveBalanceControllers@CreditBalance');
	Route::get('/leave-approver','leave\LeaveRequestControllers@LeaveApprovers');
	Route::get('/leave-rule','leave\LeaveRequestControllers@LeaveRule');
	Route::get('/leave-days','leave\LeaveRequestControllers@LeaveDays');
	Route::post('/submit-leave-request','leave\LeaveRequestControllers@SubmitLeaveRequest'); 
	Route::post('/leave-approval','leave\LeaveApprovalController@Approve'); 
	Route::get('/leave-transactions','leave\LeaveApprovalController@LeaveTransactions'); 
	Route::get('/leave-transaction-details','leave\LeaveApprovalController@LeaveTransactionDetails'); 
	Route::get('/team-on-leave','leave\LeaveApprovalController@TeamOnLeave'); 
	Route::get('/comp-off-approver','leave\CompoffRequestControllers@Approvers');
	Route::get('/compoff-rule','leave\CompoffRequestControllers@Rule');
	Route::post('/submit-compoff-request','leave\CompoffRequestControllers@SubmitRequest');
	Route::get('/compoff-credit','leave\CompoffRequestControllers@CompoffCredit');
	Route::get('/opening-balance','leave\OpeningBalanceControllers@index');
	Route::post('/opening-balance','leave\OpeningBalanceControllers@BalanceUpload');
	
	
	// for holidays 
	Route::post('/add-update-holidays','admin_settings\HolidaySettings@AddUpdateHoliday');
	Route::get('/holiday-list','admin_settings\HolidaySettings@fetchList');
	Route::put('/holiday-list','admin_settings\HolidaySettings@UpdateStatus');
	Route::get('/holidays-details','admin_settings\HolidaySettings@GetDetails');

	// shift 
	Route::post('/add-update-shift','admin_settings\ShiftSettings@AddUpdate');
	Route::get('/shift-list','admin_settings\ShiftSettings@FetchList');
	Route::put('/shift-list','admin_settings\ShiftSettings@UpdateStatus');
	Route::get('/shift-details','admin_settings\ShiftSettings@GetDetails');
	Route::get('/active-shift','admin_settings\ShiftSettings@getActiveShift');

	Route::post('/roster','admin_settings\RosterSettings@AddUpdate');
	Route::put('/roster','admin_settings\RosterSettings@UpdateStatus');
	Route::get('/roster','admin_settings\RosterSettings@FetchList');
	Route::get('/roster-details','admin_settings\RosterSettings@GetDetails');
	Route::post('/roster-assign','admin_settings\RosterSettings@assignRoster');
	Route::get('/assigned-roster','attendance\Roster@index');
	
	// for attendance & Calendar 
	Route::get('/user-calendar','attendance\Calendar@UserCalendar');
	Route::post('/submit-ar-request','attendance\ArRequestControllers@SubmitRequest');
	Route::get('/attendance-approver','attendance\ArRequestControllers@Approvers');
	Route::get('/attendance-rule','attendance\ArRequestControllers@Rule');
	Route::get('/mark-transactions','attendance\ArApprovalControllers@ArTransactions');
	Route::get('/ar-transaction-details','attendance\ArApprovalControllers@ArDetails');
	Route::post('/ar-approval','attendance\ArApprovalControllers@Approve');
	Route::post('/submit-od-request','attendance\OdRequestControllers@SubmitRequest');
	Route::get('/od-transactions','attendance\OdApprovalControllers@Transactions');
	Route::get('/od-transaction-details','attendance\OdApprovalControllers@Details');
	Route::post('/od-approval','attendance\OdApprovalControllers@Approve');
	// for ou details applicability 
	Route::get('/ou-details','admin_settings\AdminSettings@getOuDetails');

	// profile 
	Route::get('/official-details','profile\MyProfileControllers@OfficialDetails');
	Route::get('/bank-details','profile\MyProfileControllers@BankDetails');
	Route::get('/personal-details','profile\MyProfileControllers@PersonalDetails');
	Route::get('/contact-details','profile\MyProfileControllers@ContactDetails');

	// reports , import & exports 
	Route::get('/import-exports','reports\ImportExportControllers@Transactions');
	Route::get('/import-exports-latest','reports\ImportExportControllers@Latest');
	Route::get('/get-file-contents','reports\ImportExportControllers@getFileContent');
	Route::post('/reports','reports\Reports@GenerateReports');
	
	
});
