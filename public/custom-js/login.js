/**
 * Created by Sandeep Maurya on 10/19/2018.
 */
$(function () {

    // login credentials check
    $('#login_submit').on('click',function () {

        var formdata = new FormData(document.getElementById('login_form'));

        var request =  $.ajax({
            type    : 'POST',
            url     : 'login-authentication',
            dataType : 'json',
            contentType:false,
            processData : false,
            cache : false,
            data : formdata ,
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });

        request.done(function (response) {
            alert()
        });
    });
});