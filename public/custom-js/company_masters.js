/**
 * Created by Sandeep Maurya on 10/28/2018.
 */

$(function () {
    $('#company_save').on('click',function () {

        var formdata = new FormData(document.getElementById('company_form'));
            formdata.append('action','save');
        var request =  $.ajax({
                type    : 'POST',
                url     : 'company-masters-submit',
                dataType : 'json',
                contentType:false,
                processData : false,
                cache : false,
                data : formdata ,
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
        });

        request.done(function (response) {
            if(response.result){
                Materialize.toast(response.message,2000,'success');
            }  else{
                $.each(response.error,function (index,value) {
                    Materialize.toast(value,4000,'error');
                });
            }
        });
    });
});