<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title> Rudraksh Group</title>

        <meta name="csrf-token" content="{{ csrf_token() }}">

        <!-- CORE CSS-->
        <link href="/css/materialize.min.css" type="text/css" rel="stylesheet" media="screen,projection">
        <link href="/css/style.min.css" type="text/css" rel="stylesheet" media="screen,projection">
        <link href="/css/flexisel.css" type="text/css" rel="stylesheet" media="screen,projection">
        <link href="/css/data-table.css" type="text/css" rel="stylesheet" media="screen,projection">
        <!-- Custome CSS-->
        <link href="/css/custom/default.min.css" type="text/css" rel="stylesheet" media="screen,projection">
        <!-- <link href="/css/layouts/style-horizontal.css" type="text/css" rel="stylesheet" media="screen,projection"> -->
        <link href="/css/react-select-search.css" type="text/css" rel="stylesheet" media="screen,projection">
        <link href="https://fonts.googleapis.com/css2?family=Merienda+One&display=swap" rel="stylesheet">
        <!-- INCLUDED PLUGIN CSS ON THIS PAGE -->
        <link href="/js/plugins/perfect-scrollbar/perfect-scrollbar.css" type="text/css" rel="stylesheet" media="screen,projection">
        <link href="/js/plugins/jvectormap/jquery-jvectormap.css" type="text/css" rel="stylesheet" media="screen,projection">
        <link href="https://fonts.googleapis.com/css?family=Lato:100" rel="stylesheet" type="text/css">
        <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.5.0/css/all.css" integrity="sha384-B4dIYHKNBt8Bc12p+WXckhzcICo0wtJAoU8YZTY5qE0Id1GSseTk6S+L3BlXeVIU" crossorigin="anonymous">
         

    </head>
    <body>
        <noscript>
            <div  style="display: table">
                <div style="display: table-cell">
                    <h2 >Please enable JavaScript in your Browser Setting ...</h2>
                </div>
            </div>
        </noscript>
        <div id="main-app"></div>
        <script src="/js/app.js"></script>
          <!-- jQuery Library -->
    <script type="text/javascript" src="js/plugins/jquery-1.11.2.min.js"></script>    
    <!-- <script type="text/javascript" src="/js/jquery-3.3.1.min.js"></script>     -->
    <!--materialize js-->
    <script type="text/javascript" src="/js/materialize.min.js"></script>
    <!--prism-->
    <script type="text/javascript" src="/js/plugins/prism/prism.js"></script>
    <!--scrollbar-->
    <script type="text/javascript" src="/js/plugins/perfect-scrollbar/perfect-scrollbar.min.js"></script>
    <!-- chartist -->
    <!-- <script type="text/javascript" src="/js/plugins/chartist-js/chartist.min.js"></script>    -->
    
    <!--plugins.js - Some Specific JS codes for Plugin Settings-->
    <script type="text/javascript" src="/js/plugins.min.js"></script>
    <!--custom-script.js - Add your own theme custom JS-->
    <script type="text/javascript" src="/js/custom-script.js"></script>
    </body>
</html>
