export default {
    path: {
      server: 'http://localhost:3000/',
      api_server: 'http://localhost:3001',
    },
    basename: '/', // this is used in routing so don't modify
  };