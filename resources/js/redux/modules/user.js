const CHANGE_ROLE = 'user/CHANGE_ROLE'
const LOADING = 'user/LOADING'
const LOADED = 'user/LOADED'
const ERROR = 'user/ERROR'

const init = {
    
        user_id : 'sandeep_123',
        user_name : 'Sandeep Kumar Maurya',
        active_role : 1,
        active_role_name : 'User',  
        img_url : '',
        roles : [
            {
                role_id: 1 ,
                role_name : 'User',
                active : true
            },
            {
                role_id: 2 ,
                role_name : 'Admin',
                active : false
            } 
        ]
    }

export default (state = init , action) => {
	 
 	switch(action.type){
 		case LOADED : 
 				return {
                    ...state, 
                    loading: false
                }
        case LOADING : 
            return {
                ...state, 
                loading: false
            }
        case ERROR :
         return {
                ...state, 
                loading: false
            }    
 		default : 
 			return state;

 	}
}

export function changeRole(payload) {
    
    return {
  		types : [LOADING,LOADED,ERROR],
        promise : client => client.get('/services/user_roles',{data : {name : 'sandeep '}})
 	}
}