import { TOGGLE_PROFILE_CARD } from './../action-types/profile_action';
const LOADING = 'globalSettings/LOADING'
const LOADED = 'globalSettings/LOADED'
const ERROR = 'globalSettings/ERROR'


const global_settings  = {
    profile_card  : false,
    sidebar : true,
    sidebar_parent : 1 ,
    sidebar_child : 1,
    app_logo : '',
    theme_selection : 1,
    theme_mode :  1,
    screen_mode: 1 ,
    logged_in : true,
    error : '' ,
    action_status : '',
    ip_address : '',
    network_status : '',
    activity_status : '',

}

export default (state = global_settings, action) => {
  
	switch(action.type){
		case LOADED : 
            return {
                ...state,
                profile_card:  action.payload,
                loading: false
            }
            break;
		case LOADING : 
            return {
                ...state,
                loading: true
            } 
            break;
        case ERROR:
            return {
                ...state,
                error:action.error,
                loading: false,
            }
            break;
        case TOGGLE_PROFILE_CARD : 
       
            return {...state, profile_card:  action.payload}
            break ;

		default : 
			return state;

	}
}

