const  menus =[
	{
		menu_name  : "Dashboard",
		menu_url : '/dashboard',
		menu_id : 1,
		menu_icons : '',
		children : []
	},
	{
		menu_name  : "Create Story",
		menu_url : '/create-story',
		menu_id : 2 ,
		menu_icons : '',
		children : []
	},
	{
		menu_name  : "Stories",
		menu_url : '/stories',
		menu_id : 3,
		children : []
	},
	{
		menu_name  : "Category",
		menu_url : '/create-category',
		menu_id : 4,
		menu_icons : '',
		children : []
	},
	{
		menu_name  : "Theme",
		menu_url : '/theme',
		menu_id : 5,
		menu_icons : '',
		children : [
			{
				sub_menu_name : "Theme 1",
				sub_menu_url : "/apply-leave",
				sub_menu_id : 6
			},
			{
				sub_menu_name : "Theme 2",
				sub_menu_url : "/leave-records",
				sub_menu_id : 7
			},
			{
				sub_menu_name : "Theme 3 ",
				sub_menu_url : "/apply-comp-off",
				sub_menu_id : 8
			}

		]
	}

]	
export default (state = menus, action) => { 
 	return state;
}