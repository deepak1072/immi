import { combineReducers } from 'redux';
import { routerReducer } from 'react-router-redux';

import global_settings from './globalSettings'
import menus from './menus'
import user from './user'

const appReducer = combineReducers({
  routing: routerReducer,
  global_settings,
  menus,
  user,
});

export default function rootReducer(state, action){
//   if(action.type == LOGOUT_SUCCESS){
//     const { routing } = state
//     state = { routing }
//   }
//   else if(action.type == SELECT_MERCHANT_SUCCESS){
//     const { routing, auth } = state
//     state = { routing, auth }
//   }

  return appReducer(state, action);
}
