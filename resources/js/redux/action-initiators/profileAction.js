
import axios from 'axios';
import { TOGGLE_PROFILE_CARD,CHANGE_ROLE } from './../action-types/profile_action';

import { PROJECT_LOADING,PROJECT_SUCCESS,PROJECT_ERROR,API_URL } from './../action-types/global_action';
export const toggle_profile = (payload)  => {

 	return {
  		type: TOGGLE_PROFILE_CARD,
  		payload: payload
 	}
}

export function change_role(payload) {

 	// return dispatch({
  // 		type: CHANGE_ROLE,
  // 		payload: payload
 	// })
 	 return function (dispatch) { 

	    dispatch({ type: PROJECT_LOADING, payload }) 
	    return axios(`${API_URL}/services/change-role?role=${payload.role_id}`) 
	      .then(data => dispatch({ type: PROJECT_SUCCESS, data })) 
	      .catch(err => dispatch({ type: PROJECT_ERROR, err })) 
  	} 
}

// export function loadProject (id) {
//   return function (dispatch) { 
//     dispatch({ type: 'PROJECT_LOADING', data }) 
//     return fetch(`/projects/${id}`) 
//       .then(data => dispatch({ type: 'PROJECT_LOADED', data })) 
//       .catch(err => dispatch({ type: 'PROJECT_ERROR', err })) 
//   } 

// }

// function load (dispatch) {
//   dispatch({ type: 'LOAD_START' })
//   fetch('https://httpbin.org/ip')
//     .then(res => res.json())
//     .then(data =>
//       dispatch({ type: 'LOAD_FINISH', data: data }))
//     .catch(error =>
//       dispatch({ type: 'LOAD_ERROR', error: error }))
// }

 