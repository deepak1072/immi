/**
 * for authentication set in each request 
 */

 

export default function SetLoginAuth(token){
	if(localStorage.hasOwnProperty('auth_token')){
		 
        localStorage.removeItem('auth_token');
        localStorage.setItem('auth_token',token);
	} else{
		localStorage.setItem('auth_token',token);
	}

	return true ;
	
}

export  function SetLogoutAuth(){
	 
	if(localStorage.hasOwnProperty('auth_token')){
		 
        localStorage.removeItem('auth_token'); 
	} 
	return true ;
}

