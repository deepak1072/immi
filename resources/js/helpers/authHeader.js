/**
 * Created by Sandeep Maurya on 2/1/2019.
 */
export default function authHeader() {
    // return authorization header with jwt token
    if (localStorage.hasOwnProperty('data')) {
        let user = JSON.parse(localStorage.getItem('data'));

        if (  user.token ) {
            return { 'Authorization': 'Bearer ' + user.token ,'Content-Type': 'application/json' };
        } else {
            return {'Content-Type': 'application/json'};
        }
    }  else{
          return {'Content-Type': 'application/json'};
    }

}
