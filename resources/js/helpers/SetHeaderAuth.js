/**
 * for authentication set in each request 
 */

import axios from 'axios';

export default function SetAuthorizationToken(token){
	if(token){
		 axios.defaults.headers.common['Authorization'] = 'Bearer ${localStorage.auth_token}'; 
	}else{

		delete  axios.defaults.headers.common['Authorization'] ;
	}
	
}

