/**
 * Created by Sandeep Maurya on 2/1/2019.
 */
import { ToastContainer, toast } from 'react-toastify';
// handle logout functionality
export function logout(){

    localStorage.removeItem('data')   ;
    // localStorage.setItem('user_login_status',false)  ;
    window.location.href = '/login';
}

// provide header with authenticate token
export  function authHeader() {
    // return authorization header with jwt token
    if (localStorage.hasOwnProperty('data')) {
        let user = JSON.parse(localStorage.getItem('data'));
        
        if (  user.token ) {
            return { 'Authorization': 'Bearer ' + user.token ,'Content-Type': 'application/json' };
        } else {
            return {'Content-Type': 'application/json'};
        }
    }  else{
        return {'Content-Type': 'application/json'};
    }

}
// handle server rerver response
export function handleServerResponse(response){
    switch(response.response.status){
        case 500 :
            // logout();
            toast(({ closeToast }) => <div> {response.response.data.message} </div>);
            break ;
        case 401 :
            toast(({ closeToast }) => <div> {response.response.data.error} </div>);
        // logout();
        default :
            toast(({ closeToast }) => <div>  could not process request ,please try after some time   </div>);
    }
}

export const ITEM_PER_PAGE = 10;
export const __DOCUMENT__  = 'S:/hris-polar/storage/app/';
export const COLORS =  [
    'dark',
    'dark-1',
    'dark-2',
    'dark-3',
    'dark-4',
    'dark-5',
    'dark-6',
    'light',
    'light-1',
    'green-1',
    'green-2',
    'green-3',
    'green-4',
    'green-5',
    'green-6',
    'green-7',
    'blue',
    'blue-1',
    'blue-2',
    'blue-3',
    'blue-4',
    'blue-5',
    'saffron',
    'saffron-1'
];
export const CARD =[
    'green-4',
    'green-6',
    'light',
    'green-2',
    'dark-6',
    'green',
    'green-5',
    'dark',
    'green-1',
    'green-3',
];
export const MONTHS = [
    "January",
    "February",
    "March",
    "April",
    "May",
    "June",
    "July",
    "August",
    "September",
    "October",
    "November",
    "December"
];