import React from 'react';
import { toast } from 'react-toastify'; 
import {authHeader,logout} from  "./responseAuthozisation";
import {Sandesh} from '../components/admin-settings/usable_components/Sandesh';
/**
 * @param {*} url 
 * @param {*} params 
 */ 
const message = "We are unable to process your request, please try later";

// get request
export  function getRequest(url,params ={} ){
    params = syncUserData(params);
    return new Promise((resolve,reject)=>{
        axios.get(url,{params:params,headers : authHeader()})
        .then(response=>{
            if(response.data.result){
                resolve(response);
            }else{
                Sandesh(response.data.error,'warn');
            }
        }) .catch(error=> {
            switch(error.response.status){
                case 401 :
                    Sandesh(error.response.data.error,'error');
                    setTimeout(()=>{logout()},3000);
                    break;
                case 405 : 
                    Sandesh('Request method is not allowed','error');
                    break;
                case 406 :
                    Sandesh(error.response.data.error,'error');
                    setTimeout(()=>{logout()},3000);
                    break;
                default :
                    Sandesh(message,'warn');
                    break;
            }
            reject(error);
        }) ;
    }); 
}

// post request
export  function postRequest(url,params ={} ){
    params = syncUserData(params);
    return new Promise((resolve,reject)=>{
        axios.post(
            url,
            params,
            { headers : authHeader()}
        ).then((response)=>{
            setTimeout(()=>{
                resolve(response);
            },500);
        }) .catch((error)=>{
            switch(error.response.status){
                case 401 :
                    Sandesh(error.response.data.error,'error');
                    setTimeout(()=>{logout()},3000);
                    break;
                case 406 :
                    Sandesh(error.response.data.error,'error');
                    setTimeout(()=>{logout()},3000);
                    break;
                case 405 : 
                    Sandesh('Request method is not allowed','error');
                    break;
                default :
                    Sandesh(message,'error');
                    break;
            }
        }) ;
    });   
}

// post request
export  function postRequestMultipart(url,params ={} ){
    params = syncUserDataMultipart(params);
    return new Promise((resolve,reject)=>{
        axios.post(
            url,
            params,
            { headers : authHeader()}
        ).then((response)=>{
            setTimeout(()=>{
                resolve(response);
            },500);
        }) .catch((error)=>{
            switch(error.response.status){
                case 401 :
                    Sandesh(error.response.data.error,'error');
                    setTimeout(()=>{logout()},3000);
                    break;
                case 406 :
                    Sandesh(error.response.data.error,'error');
                    setTimeout(()=>{logout()},3000);
                    break;
                case 405 : 
                    Sandesh('Request method is not allowed','error');
                    break;
                default :
                    Sandesh(message,'error');
                    break;
            }
        }) ;
    });   
}

// put request
export  function putRequest(url,params ={}){
    params = syncUserData(params);
    return new Promise((resolve,reject)=>{
        axios({
            method: 'put',
            url: url,
            data: params,
            async : true,
            headers : authHeader()
        }).then(response=>{
            resolve(response);
        }) .catch(error=> {
            switch(error.response.status){
                case 401 :
                    Sandesh(error.response.data.error,'error');
                    setTimeout(()=>{logout()},3000);
                    break;
                case 406 :
                    Sandesh(error.response.data.error,'error');
                    setTimeout(()=>{logout()},3000);
                    break;
                case 405 : 
                    Sandesh('Request method is not allowed','error');
                    break;
                default :
                    Sandesh(message,'error');
                    break;
            }
        }) ;
    }); 
}

// hydrate default data into request
function syncUserData(params){
    if(localStorage.hasOwnProperty('data')){
        let data = JSON.parse(localStorage.getItem('data'));
        for (let key in data) {
            if('user_id' == key){
                params.user_id = data[key] ;
                if(! params.hasOwnProperty('req_user_id')){
                    params.req_user_id = data[key];
                }
            }
            if('role_masters_id' == key){
                params.role_masters_id = data[key] ;
            }
            if('active_role' == key){
                params.active_role = data[key] ;
            }
        }
    }
    return params;
}

// hydrate default data into request
function syncUserDataMultipart(params){
    if(localStorage.hasOwnProperty('data')){
        let data = JSON.parse(localStorage.getItem('data'));
        for (let key in data) {
            if('user_id' == key){
                params.append('user_id',data[key]);
                if(! params.hasOwnProperty('req_user_id')){
                    params.append('req_user_id',data[key]);
                }
            }
            if('role_masters_id' == key){
                params.append('role_masters_id',data[key]);
            }
            if('active_role' == key){
                params.append('active_role',data[key]);
            }
        }
    }
    return params;
}
