/**
 * Created by Sandeep Maurya on 11/18/2018.
 */

 
import React from "react";
//1. import
import Scheduler, {SchedulerData, ViewTypes, DATE_FORMAT} from 'react-big-scheduler' ;
//include `react-big-scheduler/lib/css/style.css` for styles, link it in html or import it here 
import 'react-big-scheduler/lib/css/style.css' ;
import moment from 'moment' ;
 
let schedulerData = new SchedulerData(new moment().format(DATE_FORMAT), ViewTypes.Month);
let resources = [
                    {
                       id: 'r1',
                       name: 'Resource1'
                    },
                    {
                       id: 'r2',
                       name: 'Resource2'
                    },
                    {
                       id: 'r3',
                       name: 'Resource3'
                    }
                ];
schedulerData.setResources(resources);
let events = [
                {
                     id: 1,
                     start: '2017-12-18 09:30:00',
                     end: '2017-12-19 23:30:00',
                     resourceId: 'r1',
                     title: 'I am finished',
                     bgColor: '#D9D9D9'
                 }, 
                 {
                     id: 2,
                     start: '2017-12-18 12:30:00',
                     end: '2017-12-26 23:30:00',
                     resourceId: 'r2',
                     title: 'I am not resizable',
                     resizable: false
                 }, 
                 {
                     id: 3,
                     start: '2017-12-19 12:30:00',
                     end: '2017-12-20 23:30:00',
                     resourceId: 'r3',
                     title: 'I am not movable',
                     movable: false
                 }, 
                 {
                     id: 4,
                     start: '2017-12-19 14:30:00',
                     end: '2017-12-20 23:30:00',
                     resourceId: 'r1',
                     title: 'I am not start-resizable',
                     startResizable: false
                 }, 
                 {
                     id: 5,
                     start: '2017-12-19 15:30:00',
                     end: '2017-12-20 23:30:00',
                     resourceId: 'r2',
                     title: 'R2 has recurring tasks every week on Tuesday, Friday',
                     rrule: 'FREQ=WEEKLY;DTSTART=20171219T013000Z;BYDAY=TU,FR',
                     bgColor: '#f759ab'
                 }
             ];
schedulerData.setEvents(events);
export default class Calendar extends React.Component {
    // calendarComponentRef = React.createRef();
    constructor(props){
        super(props);
        const date = moment().format(DATE_FORMAT);
        const viewType = ViewTypes.Month;
        const showAgenda = true ;
        const isEventPerspective = false;
        const newConfig = undefined;
        const newBehaviors=undefined;
        const localeMoment = moment();
        this.state = {
             
        }
    }

   

    render() {
        return (
            <div className="dashboard">
                <div className="row">
                    <div className="col s12 m12 l12">
                        <div className="card-panel">
                            <Scheduler 
                                schedulerData={schedulerData}
                                prevClick={this.prevClick}
                                nextClick={this.nextClick}
                                onSelectDate={this.onSelectDate}
                                onViewChange={this.onViewChange}
                                eventItemClick={this.eventClicked}
                            />
                        </div>
                    </div>
                </div>
            </div>
        );
    }

    
}

