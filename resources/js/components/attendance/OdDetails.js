/**
 * Created by Sandeep Maurya on 21st march 2020.
 */

import React,{Fragment} from 'react';
import {Link} from 'react-router-dom';
import  Loader from './../admin-settings/usable_components/Loader';
import ApproverActions from './../admin-settings/usable_components/ApproverActions';
import ActionButton from './../admin-settings/usable_components/ActionButton';
import { getRequest,postRequest } from "../../helpers/ApiRequest";
import Sandesh from '../admin-settings/usable_components/Sandesh';
export default class OdDetails extends React.Component{
    constructor(props){
        super(props);
        this.state = {
            hasError : false, 
            is_loading : true,
            remarks : ''
        }
        this.handleRequest = this.handleRequest.bind(this); 
        this.handleFieldChange = this.handleFieldChange.bind(this);
    }

    componentDidMount(){
        let self = this;
        let id = this.props.match.params.od;
        let trx_id = this.props.match.params.trx;
        if((id != undefined || id != '') && (trx_id != undefined || trx_id != '') ){
            let request = getRequest('/api/od-transaction-details',{
                id : id,
                trx_id : trx_id
            }); 
            request.then(response=>{
                let  data = response.data; 
                if(data.result){
                    data.data.is_loading = false ;
                    self.setState(data.data);
                }
            })  
        } 
    }

    handleRequest(event,status){
         
        event.preventDefault();
        let {history} = this.props;
        let info = this.state;
        let mark = {
            'id' : info.od_id,
            'trx_id' : info.trx_id,
            'wf_id' : info.workflow_id,
            'status' : status,
            'remarks' : info.remarks
        };
        if(this.validateRule(status)){
            let url ='/api/od-approval';
            let request = postRequest(url,mark);
            request.then((response)=>{
                if(response.data.result){
                    Sandesh(response.data.message,response.data.type);
                    history.push('/od-transactions');
                }else{
                    Sandesh(response.data.message,response.data.type);
                }
            });
        }
    }

    static getDerivedStateFromError(error) {
        // Update state so the next render will show the fallback UI.
        return { hasError: true };
    }

    validateRule(status){
        let state = this.state;
        let remarks = state.remarks.trim();
        let valid = [3,4,6,8,7];
        if(valid.includes(status) && (remarks == '' || remarks == undefined ) ){
            Sandesh('Please enter remarks','warn');
            return false;
        }
        return true ;
    }
    handleFieldChange(event){
        this.setState({
            [event.target.name] : event.target.value
        });
    }

    render(){ 
        let info = this.state ;
        
        if(info.is_loading){
            return <Loader />;
        }  
        if(info.hasError){
           return <h4 className="header2">Something went wrong</h4>
        }   else{
            return(
                <div className="dashboard ">
                    <div className="card-panel ">
                        <div className="row">
                            <fieldset>
                                <legend>OD Details</legend>
                                <form className="col s12 m8"  >
                                    <div id="leave_htmlForm">
                                        <table className="table-details">
                                            <tbody>
                                                <tr>
                                                    <td>Requester Name</td>
                                                    <td>{info.emp_name}</td>
                                                </tr>
                                                <tr>
                                                    <td>Applied On </td>
                                                    <td>{info.trx_date}</td>
                                                </tr>
                                                <tr>
                                                    <td> Date </td>
                                                    <td>{info.od_date}</td>
                                                </tr>
                                                <tr>
                                                    <td> From Period </td>
                                                    <td>{info.from_type}</td>
                                                </tr>
                                                <tr>
                                                    <td> To Period </td>
                                                    <td>{info.to_type}</td>
                                                </tr>
                                                <tr>
                                                    <td>Reason Type  </td>
                                                    <td>{info.reason_type}</td>
                                                </tr>
                                                <tr>
                                                    <td>Reason  </td>
                                                    <td>{info.reason}</td>
                                                </tr>
                                                {
                                                    (info.final_status == 4) ? (
                                                        <tr>
                                                            <td>User Remark's  </td>
                                                            <td>{info.user_remarks}</td>
                                                        </tr>
                                                    ) : ('')
                                                }
                                                <tr>
                                                    <td>Final Status  </td>
                                                    <td>{info.final_status_text}</td>
                                                </tr>
                                                <ApproverActions apprs= {info.apprs} />
                                            </tbody>
                                        </table> 
                                        <div className="row">
                                            {
                                                (info.actions.length > 0) ? (
                                                    <div className="col s12 m12">
                                                
                                                        <label htmlFor="remarks">Remarks</label>
                                                        <textarea id="remarks" name="remarks" 
                                                            onChange={this.handleFieldChange}
                                                            value={info.remarks}
                                                            className="materialize-textarea"> 
                                                        </textarea> 
                                                    </div>
                                                ) : ('')
                                            }
                                        </div>
                                        <div className="row">                                            
                                            <div className="input-field col s12">
                                                <Link to="/od-transactions">
                                                    <button className="btn default   waves-light"   name="action">
                                                        <i className="mdi-navigation-arrow-back left "></i>
                                                        Back
                                                    </button>
                                                </Link>
                                                <ActionButton 
                                                    actions ={info.actions}
                                                    handleSubmit = {this.handleRequest}
                                                />
                                            </div>
                                        </div> 
                                    </div>
                                </form>
                                <div className="col s12 m4"></div>
                            </fieldset>
                        </div>
                    </div>
                </div>
            );
        }
    }
}