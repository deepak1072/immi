/**
 * Created by Sandeep Maurya on 21st March 2020
 */

import React from "react";
import _ from "lodash";
import axios from 'axios';
import ReactTable from "react-table";
import "react-table/react-table.css";
import {Link} from  'react-router-dom';
import {ITEM_PER_PAGE} from  "./../../helpers/responseAuthozisation";
import { getRequest } from "../../helpers/ApiRequest";

export  default class OdTransactions extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            data: [],
            pages: null,
            loading: true
        };
        this.fetchData = this.fetchData.bind(this);
    }

    fetchData(state, instance) {
        this.setState({ loading: true });
        let url = '/api/od-transactions';
        let request = getRequest(url,{
            filter : state.filtered,
            pageSize : state.pageSize,
            page : state.page,
            sort : state.sorted
        });
        request.then((response) => {
            let filteredData = response.data;
            this.setState({
                data: filteredData.data,
                pages: Math.ceil(filteredData.count / state.pageSize),
                loading: false
            });
        });
    }

    render() {
        const { data, pages, loading } = this.state;
         
        return (
            <div>
                <div className="dashboard">
                    <div className="card-panel">
                        <h4 className="header2">
                            OD Transactions
                        </h4>
                        
                        <ReactTable
                            columns={[
                                {
                                    Header: "Requester",
                                    accessor: "emp_name"
                                } ,
                                {
                                    Header: "Applied On",
                                    accessor: "trx_date"
                                }, 
                                {
                                    Header: "Date",
                                    accessor: "od_date"
                                },
                                {
                                    Header: "Reason",
                                    accessor: "reason"
                                },
                                {
                                    Header: "Status",
                                    accessor: "final_status_text"
                                },
                                {
                                    Header: "Action",
                                    accessor: "id",
                                    Cell:(row)=>(
                                            <div className="switch">
                                                <Link to={'/od-request-approval/'+ row.original.od_id+'/'+row.original.id } className="input-field ">
                                                    <button className="btn light waves-effect waves-light action_button "><i className="mdi-action-launch"></i></button>
                                                </Link>
                                            </div>
                                        )

                                }
                            ]}
                            manual // Forces table not to paginate or sort automatically, so we can handle it server-side
                            data={data}
                            pages={pages} // Display the total number of pages
                            loading={loading} // Display the loading overlay when we need it
                            onFetchData={this.fetchData} // Request new data when things change
                            filterable
                            defaultPageSize={ITEM_PER_PAGE}
                            className="-highlight" //-striped
                        />
                        <br />
                    </div>
                </div>

            </div>
        );
    }
}


