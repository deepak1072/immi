/**
 * Created by Sandeep Maurya on 8th Feb 2020
 */
import React from 'react';
import axios from 'axios';
import { toast } from 'react-toastify';
import  Loader from './../admin-settings/usable_components/Loader';
import MonthYear from './../layout/MonthYear';
import FullCalendar from "@fullcalendar/react";
import dayGridPlugin from "@fullcalendar/daygrid";
import SelectSearch from 'react-select-search';
import {authHeader,logout,ITEM_PER_PAGE,MONTHS} from  "./../../helpers/responseAuthozisation";
import "./styles.css";
import "@fullcalendar/core/main.css";
import "@fullcalendar/daygrid/main.css";
export default class Calendar extends React.Component{
    
    constructor(props){
        super(props);
        this.calendarComponentRef = React.createRef();
        let date = new Date();
        let month = date.getMonth();
        let month_name = MONTHS[month];
        let year = date.getFullYear();
        this.state = {  
            flag : true ,
            hasError:false,
            action: 'Create',
            is_loading :true,
            users : [{name:'sas',value:1},{name:'sass',value:2}],
            user : [],
            month : month,
            month_name : month_name,
            year: year,
            page : 0  ,
            calendar_date : new Date(),
            calendarWeekends: true, 
            calendarEvents: []
        }
        this.handleFieldChange   = this.handleFieldChange.bind(this);
        this.handlePrevNext = this.handlePrevNext.bind(this);
        this.loadRoster = this.loadRoster.bind(this);
    }

    componentDidMount(){
        this.loadRoster(this.state.month,this.state.year);
    }

    static getDerivedStateFromError(error) {
        return { hasError: true };
    }
    onTeamChange(value, state, props){
        
        let date = this.state.calendar_date; 
        let month = date.getMonth();
        let month_name = MONTHS[month];
        let year =  date.getFullYear();
        this.setState({
            month : month,
            year: year,
            month_name:month_name,
            user : value.value,
            is_loading:true
        });
        this.loadRoster(month,year);
    }
    handlePrevNext(val){
        let calendarApi = this.calendarComponentRef.current.getApi();
        if(val == 1){
            calendarApi.next();
        }else{
            calendarApi.prev();
        }
        let date = new Date(calendarApi.getDate());
        let month = date.getMonth();
        let month_name = MONTHS[month];
        let year =  date.getFullYear();
        this.setState({
            month : month,
            year: year,
            month_name:month_name
        });
        this.loadRoster(month,year);  
    }

    loadRoster(month,year){
        let self = this;   
        let url = "/api/assigned-roster";
        axios.get(url,{
            params:{
                ids : this.state.user,
                month: month,
                year: year,
                page:this.state.page
             },
            headers : authHeader()
        }).then(function(response){
            if(response.status == 200){
                let data = response.data;
                if(data.result){
                    let calendar_events = data.data.roster;
                    self.setState({
                        is_loading : false,
                        calendarEvents : calendar_events
                    });
                }else{
                    self.setState({
                        is_loading : false
                    });
                }
            }else{
                toast(({ closeToast }) => <div>Something went wrong , please try after some time  </div>);
                history.push('/calendar') ;   
            }
        }).catch(function(error){
            switch(error.response.status){
                case 500 :
                    // logout();
                    toast(({ closeToast }) => <div> {error.response.data.message} </div>); 
                    history.push('/roster-list') ;
                    break ;
                case 401 :
                    toast(({ closeToast }) => <div> {error.response.data.error} </div>);
                    logout();
                    break;
                default :
                    toast(({ closeToast }) => <div>Something went wrong , please try after some  time  </div>); 
                    history.push('/roster-list') ;  
                    break;
            } 
        });
    }

    handleDateClick(){
        alert();
    }
     
    handleFieldChange(event){
        if(this.state.action != 'Create'){
            return false;
        }
        this.setState({
            [event.target.name] : event.target.value
        });
    }
    
    render(){
        let info = this.state;
        if(info.is_loading){
            return <Loader />;
        } 
        if(this.state.hasError){
           return <h4 className="header2">Something went wrong</h4>
        }   else{
            return(
                
                    <div className="dashboard">
                        <div className=" card-panel">
                            <div className="row">
                                <div className="col s12 m8">
                                    <MonthYear
                                        month = {info.month_name}
                                        year = {info.year}
                                        handlePrevNext ={this.handlePrevNext}
                                    />
                                </div>
                                <div className="col s12 m3">
                                    <SelectSearch  
                                        options={info.users} 
                                        value={info.user} 
                                        name="user" 
                                        placeholder="Search Team"
                                        onChange={this.onTeamChange.bind(this)}
                                    /> 
                                </div>
                            </div>
                            <div className="row">
                                <div className="demo-app">
                                    <div className='demo-app-calendar'>
                                        <div className="col s12">
                                            <FullCalendar
                                                defaultView="dayGridMonth"
                                                header={{
                                                    left: '',
                                                    center: '',
                                                    right : ''
                                                }}
                                                plugins={[ dayGridPlugin ]}
                                                ref={ this.calendarComponentRef }
                                                weekends={ this.state.calendarWeekends }
                                                events={ this.state.calendarEvents }
                                                dateClick={ this.handleDateClick }
                                            />
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    
            );
        }
    }
}