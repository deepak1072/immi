/**
 * Created by Sandeep Maurya on 11/18/2018.
 */

import React from "react";
import FullCalendar from "@fullcalendar/react";
import dayGridPlugin from "@fullcalendar/daygrid";
import {authHeader,logout} from  "../../helpers/responseAuthozisation";
// import timeGridPlugin from "@fullcalendar/timegrid";
// import interactionPlugin from "@fullcalendar/interaction"; // needed for dayClick
import "./styles.css";
// // must manually import the stylesheets for each plugin
import "@fullcalendar/core/main.css";
import "@fullcalendar/daygrid/main.css";
 
export default class Calendar extends React.Component {
     
    constructor(props){
        
        super(props);
        this.calendarComponentRef = React.createRef();
        this.state = {
            user : 'sandeep',
            calendar_date : new Date(),
            calendarWeekends: true, 
            calendarEvents: [
                // initial event data
                { title: "Event Now", start: new Date() },
                { title: "On Leave", start: new Date() ,  end : '2019-04-25T16:00:00'}
            ]
        }
        // this.setCalendarEvents();

    }

    toggleWeekends(){
        this.setState({
            // update a property
            calendarWeekends: !this.state.calendarWeekends
        });
    };

    gotoPast(){
        let calendarApi = this.calendarComponentRef.current.getApi();
        calendarApi.gotoDate("2000-01-01"); // call a method on the Calendar object
    };

    nextMonth() {
         
        let calendarApi = this.calendarComponentRef.current.getApi();
        calendarApi.next();
        this.setCalendarEvents(); 
    }
    prevMonth() {
        let calendarApi = this.calendarComponentRef.current.getApi();
        calendarApi.prev();
        this.setCalendarEvents(); 
    }

    setCalendarEvents(){
        let calendar_date = this.calendarComponentRef.current.getApi().getDate() ;
        
        axios.get('/user-calendar',{
                params:{
                    user : this.state.user ,
                    calendar_date : this.state.calendar_date
                },
                headers: authHeader()
            }).then(response=>{
                if(response.data.status == 200){
                    const data = response.data.leave_balance;
                    this.setState({
                        'leave_balance' : data
                    });
                }else{
                    toast(({ closeToast }) => <div>Something went wrong , please try after some time  </div>);
                    history.push('/') ;   
                }
            }) .catch(error=> {
                switch(error.response.status){
                    case 500 :
                        // logout();
                        toast(({ closeToast }) => <div> {error.response.data.message} </div>);
                        history.push('/') ;
                        break ;
                    case 401 :
                        toast(({ closeToast }) => <div> {error.response.data.error} </div>);
                        logout();
                        break
                    default :
                        toast(({ closeToast }) => <div> could not fetch attendance details </div>);
                        history.push('/') ;
                        break;
                }
            }) ;
    }

    handleDateClick(arg){
        if (confirm("Would you like to add an event to " + arg.dateStr + " ?")) {
            this.setState({
                // add new event data
                calendarEvents: this.state.calendarEvents.concat({
                    // creates a new array
                    title: "New Event",
                    start: arg.date,
                    allDay: arg.allDay
                })
            });
        }
    };

    render() {
        return (
            <div className="dashboard">
                <div className="row">
                    <div className="col s12 m12 l12">
                        <div className="card-panel">
                            <div className="demo-app">
                                <div className='demo-app-calendar'>
                                    <div className="row">
                                        <div className="col s12 m6">
                                             
                                        </div>

                                        <div className="col s12 m6"  >
                                            <button className="btn cyan waves-effect waves-light  " onClick={this.prevMonth.bind(this)} type="submit" name="action">Prev Month 
                                                <i className="mdi-image-navigate-before left"></i>
                                            </button>
                                            <button className="btn cyan waves-effect waves-light  " onClick={this.nextMonth.bind(this)} type="submit" name="action"> Next Month
                                                <i className="mdi-image-navigate-next right"></i>
                                            </button>
                                        </div>
                                        
                                        
                                    </div>

                                    <FullCalendar
                                        defaultView="dayGridMonth"
                                        header={{
                                            left: '',
                                            center: 'title',
                                            right : ''
                                        }}
                                        plugins={[ dayGridPlugin ]}
                                        ref={ this.calendarComponentRef }
                                        weekends={ this.state.calendarWeekends }
                                        events={ this.state.calendarEvents }
                                        dateClick={ this.handleDateClick }
                                    />
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        );
    }

    
}

