/**
 * Created by Sandeep Maurya on 29th feb 2020.
 */

import React,{Fragment} from 'react';
import {Link} from 'react-router-dom';
import {getRequest,postRequest} from  "./../../helpers/ApiRequest";
import ApproversBox from './../layout/ApproversBox';
import Sandesh from './../admin-settings/usable_components/Sandesh';
import Loader from './../admin-settings/usable_components/Loader';
import LoadingButton from './../admin-settings/usable_components/LoadingButton';
import DatePicker from "react-datepicker";
import "react-datepicker/dist/react-datepicker.css";
export default class AR extends React.Component{
    constructor(props){
        super(props);
        this.state = {
            hasError : false,
            action : 'Create ',
            from_date:new Date(),
            in_time : new Date(),
            out_time : new Date(),
            commencement : 60,
            reason:'',
            reason_type : 1,
            _appr : {},
            reason_options : [],
            type : 'Create',
            is_loading : true,
            is_validating : false
        }
        this.handleRequest = this.handleRequest.bind(this);
        this.handleFieldChange = this.handleFieldChange.bind(this);
    }

    componentDidMount(){
        this.loadApprovers();
        this.loadRule();
    }

    handleRequest(event){
        event.preventDefault();
        let {history} = this.props;
        let info = this.state;
        let ar = {
            date : info.from_date,
            in_time : info.in_time,
            out_time : info.out_time,
            action : info.action,
            type : info.type,
            reason : info.reason,
            reason_type : info.reason_type
        };
        this.setState({
            is_validating : true
        });
        if(this.validateRule()){
            let url ='/api/submit-ar-request';
            let request = postRequest(url,ar);
            request.then(response=>{
                let data = response.data;
                Sandesh(data.message,data.type);
                if(data.result){
                    Sandesh(data.message,data.type);
                    setTimeout(()=>{history.push('/')},3000);
                }
            })
        }
        this.setState({
            is_validating : false
        });
    }

    static getDerivedStateFromError(error) {
        // Update state so the next render will show the fallback UI.
        return { hasError: true };
    }

    validateRule(){
        let state = this.state;
        let reason = state.reason.trim();
        let type = state.reason_type;
        if(type == '' || type == undefined){
            Sandesh('Please select reason type','warn');
            return false;
        }
        if(reason == '' || reason == undefined){
            Sandesh('Please enter reason','warn');
            return false;
        }
        return true ;
    }
    handleFieldChange(event){
        this.setState({
            [event.target.name] : event.target.value
        });
    }

    loadApprovers(){
        let self = this;
        let url = '/api/attendance-approver';
        let request = getRequest(url,{events : 1});
        request.then((response)=>{
            const data = response.data.approvers;
            self.setState({
                '_appr' : data
            });
        });
    }

    handleChange(field,date) {
        this.setState({
            [field]: date
        }); 
    }

    loadRule(){
        let self = this;
        let url = '/api/attendance-rule';
        let request = getRequest(url);
        request.then((response)=>{
            const data = response.data.data;
            self.setState({
                'commencement' : data['commencement'],
                'reason_options' : data['reason_type'],
                'is_loading' : false
            });
        }); 
    }

    render(){
        let info = this.state;
        if(info.is_loading){
            return <Loader />
        }
        if(info.hasError){
           return <h4 className="header2">Something went wrong</h4>
        }   else{
            return(
                <div className="dashboard">
                    <div className="card-panel">
                        <div className="row">
                            <form className="col s12 l8" onSubmit={this.handleRequest}>
                                <div id="leave_htmlForm">
                                    <div className="row">
                                        <div className="col s12 l6 ">
                                            <h4 className="header2">{info.action} Mark Attendance </h4>
                                        </div>
                                        <div className="col s12 l4 ">
                                        </div> 
                                    </div>
                                    <div className="row">
                                        <div className="input-field col s12 m4">
                                            <DatePicker
                                            selected={info.from_date}
                                            onChange={this.handleChange.bind(this,'from_date')}
                                            dateFormat="MMM d, yyyy"
                                            locale = "en-IN"
                                            />
                                            <label htmlFor="from_date" className="active"> Date 
                                            </label>
                                        </div>
                                        <div className="col s12 m4 input-field">
                                            <select
                                                value={info.reason_type}
                                                onChange={this.handleFieldChange}
                                                name="reason_type"
                                                id="reason_type"
                                                className="browser-default"
                                            >
                                                <option value=''> Reason Type</option>
                                                {
                                                    info.reason_options.map((reason,index)=>{
                                                        return(
                                                            <option key={index} value={reason._value} >{reason._label}</option>
                                                        )
                                                    })
                                                }
                                            </select>
                                            <label htmlFor="reason_type" className="active">Select Reason Type
                                            </label>
                                        </div>
                                    </div>
                                    <div className="row">
                                        <div className="input-field col s12 m4">
                                            <DatePicker
                                                selected={info.in_time}
                                                onChange={this.handleChange.bind(this,'in_time')}
                                                showTimeSelect
                                                dateFormat="hh:mm aa"
                                                timeIntervals={1}
                                                timeCaption="In Time"
                                                showTimeSelectOnly
                                                locale = "en-IN"
                                            />
                                            <label htmlFor="from_date" className="active"> In Time 
                                            </label>
                                        </div>
                                        <div className="input-field col s12 m4">
                                            <DatePicker
                                                selected={info.out_time}
                                                onChange={this.handleChange.bind(this,'out_time')}
                                                showTimeSelect
                                                dateFormat="hh:mm aa"
                                                timeIntervals={1}
                                                timeCaption="Out Time"
                                                showTimeSelectOnly
                                                locale = "en-IN"
                                            />
                                            <label htmlFor="from_date" className="active"> Out Time 
                                            </label>
                                        </div>
                                    </div>
                                    <div className="row">
                                        
                                        <div className="col s12 m8">
                                            <label htmlFor="reason">Reason</label>
                                            <textarea id="reason" name="reason" 
                                            onChange={this.handleFieldChange}
                                            value={info.reason}
                                            className="materialize-textarea">
                                            </textarea>
                                        </div>
                                    </div>
                                    <div className="row">
                                        <div className="col s12 m5">
                                            <ApproversBox 
                                                approvers = {info._appr}
                                            />
                                        </div>
                                    </div>
                                    <div className="row">                                            
                                        <div className="input-field col s12">
                                            {
                                                (info.is_validating) ? (
                                                    <div className="row col m4" > <LoadingButton /> </div>
                                                ) : (
                                                    <Fragment>
                                                        <Link to="/welcome">
                                                            <button className="btn default   waves-light"   name="action">
                                                                <i className="mdi-navigation-arrow-back left "></i>
                                                                cancel
                                                            </button>
                                                        </Link>
                                                        <button className="btn cyan waves-effect waves-light  " type="submit" name="action">{info.action}
                                                            <i className="mdi-content-send right"></i>
                                                        </button>
                                                    </Fragment>
                                                )
                                            }
                                        </div>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            );
        }
    }
}