/**
 * Created by Sandeep Maurya on 20th April 2019.
 */

import React from 'react';
import axios from 'axios';
import { toast } from 'react-toastify';
import {Link} from 'react-router-dom';
import {authHeader,logout,ITEM_PER_PAGE} from  "./../../helpers/responseAuthozisation";
import  Loader from './../admin-settings/usable_components/Loader';
import Sandesh from './../admin-settings/usable_components/Sandesh'; 
import DatePicker from "react-datepicker"; 
import "react-datepicker/dist/react-datepicker.css";
import TimeField from 'react-simple-timefield';
 
import {getRequest,postRequest} from  "./../../helpers/ApiRequest";
export default class CustomerDetails1 extends React.Component{
    constructor(props){
        super(props);
        var d_date = '00:00'  ;
        this.state = {
            id : '',
            name : '', 
            mobile : '', 
            email : '', 
            location : '',
            doc_updated : 0, 
            crm_update : 0, 
            visit : 0, 
            online_councelling : 0, 
            oncall_councelling : 0, 
            cic : 0, 
            verification_mail : 0, 
            approval : 0, 
            hr_finalization : 0, 
            result : 'Pending',  
            doc_comments : '',
            crm_comments : '',
            visit_comments : '',
            online_comments : '',
            oncall_comments : '',
            cic_comments : '',
            mail_comments : '',
            approval_comments : '',
            hr_comments : '',
            result_comments : '',
            doc_status : ' green-2 btn-floating ',
            crm_status : 'green-2 btn-floating',
            visit_status : 'green-2 btn-floating',
            online_status : 'green-2 btn-floating',
            oncall_status : 'green-2 btn-floating',
            cic_status : 'green-2 btn-floating',
            mail_status : 'green-2 btn-floating',
            app_status : 'green-2 btn-floating',
            date : '', 
            updated_at : '', 
            is_deleted : '',
            create_at : '',
            created_by : '',
            message : '',
            flag : true ,
            hasError:false,
            action: 'Create' ,
            is_loading:true 
        }


        this.handleClient = this.handleClient.bind(this);
        this.handleFieldChange   = this.handleFieldChange.bind(this);
        this.handleRadioChange = this.handleRadioChange.bind(this);
        this.handleChange = this.handleChange.bind(this);

    }


    componentDidMount(){

        let self = this;
        const { id } = this.props.match.params;
        
        if(id != undefined){
            
            this.loadDetails(id);
             
        }else{
            this.setState({
                name : '',
                mobile : '',
                email: '',
                is_loading : false 
            });
        }
    }

    static getDerivedStateFromError(error) {
        // Update state so the next render will show the fallback UI.
        return { hasError: true };
    }

    loadDetails(id){
        let self = this;
        let url = '/api/customer-data';
        let request = getRequest(url,{id : id});
        request.then((response)=>{
            const data = response.data; 
            
            if(data.result){ 
                let info = data.data;
                self.setState({
                    id : info.id,
                    name : info.name, 
                    mobile : info.mobile, 
                    email : info.email,
                    location : info.location, 
                    doc_updated : info.doc_updated, 
                    crm_update : info.crm_update, 
                    visit : info.visit, 
                    online_councelling : info.online_councelling, 
                    oncall_councelling : info.oncall_councelling, 
                    cic : info.cic, 
                    verification_mail : info.verification_mail, 
                    approval : info.approval, 
                    hr_finalization : info.hr_finalization, 
                    result : info.result, 
                    doc_comments : info.doc_comments,
                    crm_comments : info.crm_comments,
                    visit_comments : info.visit_comments,
                    online_comments : info.online_comments,
                    oncall_comments : info.oncall_comments,
                    cic_comments : info.cic_comments,
                    mail_comments : info.mail_comments,
                    approval_comments : info.approval_comments,
                    hr_comments : info.hr_comments,
                    result_comments : info.result_comments,
                    doc_status : info.doc_status,
                    crm_status : info.crm_status,
                    visit_status : info.visit_status,
                    online_status : info.online_status,
                    oncall_status : info.oncall_status,
                    cic_status : info.cic_status,
                    mail_status : info.mail_status,
                    app_status : info.app_status,
                    date : '', 
                    updated_at : '', 
                    is_deleted : '',
                    create_at : '',
                    created_by : '',
                    message : '',
                    flag : true ,
                    hasError:false,
                    action: 'Update' ,
                    is_loading:false 
                });
            }
            
        });
    }

    /**
     * form validation
     * @returns {boolean|*}
     */
    validateRule(){
        
        if(this.state.name =='' || this.state.name == 'undefined'){
            Sandesh('Please enter Client Name ','warn');
            return  false;
        }
        if(this.state.mobile =='' || this.state.mobile == 'undefined'){
            Sandesh('Please enter Mobile Number','warn');
            return  false;
        }
        if(this.state.location =='' || this.state.location == 'undefined'){
            Sandesh('Please enter Location','warn');
            return  false;
        }
        
        return this.state.flag;
    }



    handleClient(event){
        event.preventDefault();
        let {history} = this.props;
        let info = this.state;
        
        this.setState({
            is_validating : true
        });
        if(this.validateRule()){
            let url ='/api/submit-customer';
            let request = postRequest(url,info);
            request.then(response=>{
                let data = response.data;
                Sandesh(data.message,data.type);
                if(data.result){
                    Sandesh(data.message,data.type);
                    setTimeout(()=>{history.push('/customer')},3000);
                }
            })
        }
        this.setState({
            is_validating : false
        });

    }
    handleFieldChange(event){
        
        this.setState({
            [event.target.name] : event.target.value
        });
    }
    
    handleRadioChange(event){
        this.setState({
            [event.target.name] :  event.target.value
        }); 
    }
     
    handleChange(name,date) {  
         
        this.setState({
          [name]: date
        });
    }
 
    render(){
        if(this.state.is_loading){
            return <Loader />
        }
        if(this.state.hasError){
           return <h4 className="header2">Something went wrong</h4>
        }   else{
            const is_break_shift = (this.state.break == 1) ? true : false;
            return(
                <div className="dashboard">
                    <div className="card-panel">
                        <div className="row">
                            <form className="col s12" onSubmit={this.handleClient}>
                                <div id="leave_htmlForm">
                                    <div className="row">                                                
                                        
                                        <div className="col s12 l12 center-align">
                                            <h4 className="header2">{this.state.action} Client </h4>
                                        </div>
                                    </div>
                                    <div className="row"> 
                                            
                                        <div className="input-field col s12 m3">
                                            <input
                                                id="name"
                                                type="text"
                                                name="name"
                                                value={this.state.name}
                                                onChange={this.handleFieldChange}
                                            /> 
                                            <label htmlFor="name" className="active">Name Of Client
                                            </label> 
                                                
                                        </div>
                                        <div className="input-field col s12 m3">
                                            <input
                                                id="mobile"
                                                type="text"
                                                name="mobile"
                                                value={this.state.mobile}
                                                onChange={this.handleFieldChange}
                                            /> 
                                            <label htmlFor="mobile" className="active">Mobile No.
                                            </label>
                                        </div>  
                                        <div className="input-field col s12 m3">
                                            <input
                                                id="email"
                                                type="text"
                                                name="email"
                                                value={this.state.email}
                                                onChange={this.handleFieldChange}
                                            /> 
                                            <label htmlFor="email" className="active">Email
                                            </label>
                                        </div>
                                        <div className="input-field col s12 m3">
                                            <input
                                                id="location"
                                                type="text"
                                                name="location"
                                                value={this.state.location}
                                                onChange={this.handleFieldChange}
                                            /> 
                                            <label htmlFor="location" className="active">Location
                                            </label>
                                        </div>
                                         
                                        
                                    </div> 
                                   
                                 
                                    <fieldset>
                                        <legend> Other Details
                                        </legend> 
                                        <div className="row"> 
                                            <div className="col s12 m3"> 
                                                    <label   className="active">
                                                    <small> Documents Uploaded  </small>
                                                </label>
                                                <input
                                                    id="doc_updated_yes"
                                                    type="radio"
                                                    className="with-gap"
                                                    name="doc_updated"
                                                    value='1'
                                                    checked = {this.state.doc_updated == '1'}
                                                    onChange={this.handleRadioChange }
                                                        
                                                /> 
                                                <label htmlFor="doc_updated_yes">Yes</label>
                                                <input
                                                    id="doc_updated_no"
                                                    type="radio" 
                                                    name="doc_updated"
                                                    className="with-gap"
                                                    value= '0'
                                                    checked = {this.state.doc_updated == '0'}
                                                    onChange={this.handleRadioChange }
                                                />  
                                                <label htmlFor="doc_updated_no">No</label>
                                            </div> 
                                            <div className="input-field col s12 m2">
                                                <input
                                                    id="doc_comments"
                                                    type="text"
                                                    name="doc_comments"
                                                    value={this.state.doc_comments}
                                                    onChange={this.handleFieldChange}
                                                /> 
                                                <label htmlFor="doc_comments" className="active">Comment If Applicable
                                                </label>
                                            </div>
                                             
                                            {
                                                (this.state.action == 'Update') ? (
                                                    <div className="input-field col s6 m1">
                                                        <button className= {this.state.doc_status} type="button"  > 
                                                        </button>
                                                    </div>
                                                ) : ('')
                                            }
                                         
                                            <div className="col s12 m3"> 
                                                    <label   className="active">
                                                    <small>CRM update </small>
                                                </label>
                                                <input
                                                    id="yes"
                                                    type="radio"
                                                    className="with-gap"
                                                    name="crm_update"
                                                    value='1'
                                                    checked = {this.state.crm_update == '1'}
                                                    onChange={this.handleRadioChange }
                                                        
                                                /> 
                                                <label htmlFor="yes">Yes</label>
                                                <input
                                                    id="no"
                                                    type="radio" 
                                                    name="crm_update"
                                                    className="with-gap"
                                                    value= '0'
                                                    checked = {this.state.crm_update == '0'}
                                                    onChange={this.handleRadioChange }
                                                />  
                                                <label htmlFor="no">No</label>
                                            </div>
                                            <div className="input-field col s12 m2">
                                                <input
                                                    id="crm_comments"
                                                    type="text"
                                                    name="crm_comments"
                                                    value={this.state.crm_comments}
                                                    onChange={this.handleFieldChange}
                                                /> 
                                                <label htmlFor="crm_comments" className="active">Comment If Applicable
                                                </label>
                                            </div>
                                            {
                                                (this.state.action == 'Update') ? (
                                                    <div className="input-field col s6 m1">
                                                        <button className= {this.state.crm_status} type="button"  > 
                                                        </button>
                                                    </div>
                                                ) : ('')
                                            }
                                            
                                        </div>
                                        <div className="row"> 
                                            <div className="col s12 m3"> 
                                                    <label   className="active">
                                                    <small> Online Counselling </small>
                                                </label>
                                                <input
                                                    id="break_yes"
                                                    type="radio"
                                                    className="with-gap"
                                                    name="online_councelling"
                                                    value='1'
                                                    checked = {this.state.online_councelling == '1'}
                                                    onChange={this.handleRadioChange }
                                                        
                                                /> 
                                                <label htmlFor="break_yes">Yes</label>
                                                <input
                                                    id="break_no"
                                                    type="radio" 
                                                    name="online_councelling"
                                                    className="with-gap"
                                                    value= '0'
                                                    checked = {this.state.online_councelling == '0'}
                                                    onChange={this.handleRadioChange }
                                                />  
                                                <label htmlFor="break_no">No</label>
                                            </div>
                                            <div className="input-field col s12 m2">
                                                <input
                                                    id="online_comments"
                                                    type="text"
                                                    name="online_comments"
                                                    value={this.state.online_comments}
                                                    onChange={this.handleFieldChange}
                                                /> 
                                                <label htmlFor="online_comments" className="active">Comment If Applicable
                                                </label>
                                            </div> 
                                             
                                            {
                                                (this.state.action == 'Update') ? (
                                                    <div className="input-field col s6 m1">
                                                        <button className= {this.state.online_status} type="button"  > 
                                                        </button>
                                                    </div>
                                                ) : ('')
                                            }
                                           
                                            <div className="col s12 m3"> 
                                                    <label   className="active">
                                                    <small> On Call Counselling  </small>
                                                </label>
                                                <input
                                                    id="doc_updated_yes1"
                                                    type="radio"
                                                    className="with-gap"
                                                    name="oncall_councelling"
                                                    value='1'
                                                    checked = {this.state.oncall_councelling == '1'}
                                                    onChange={this.handleRadioChange }
                                                        
                                                /> 
                                                <label htmlFor="doc_updated_yes1">Yes</label>
                                                <input
                                                    id="doc_updated_no1"
                                                    type="radio" 
                                                    name="oncall_councelling"
                                                    className="with-gap"
                                                    value= '0'
                                                    checked = {this.state.oncall_councelling == '0'}
                                                    onChange={this.handleRadioChange }
                                                />  
                                                <label htmlFor="doc_updated_no1">No</label>
                                            </div>  
                                            <div className="input-field col s12 m2">
                                                <input
                                                    id="oncall_comments"
                                                    type="text"
                                                    name="oncall_comments"
                                                    value={this.state.oncall_comments}
                                                    onChange={this.handleFieldChange}
                                                /> 
                                                <label htmlFor="oncall_comments" className="active">Comment If Applicable
                                                </label>
                                            </div> 
                                             
                                            {
                                                (this.state.action == 'Update') ? (
                                                    <div className="input-field col s6 m1">
                                                        <button className= {this.state.oncall_status} type="button"  > 
                                                        </button>
                                                    </div>
                                                ) : ('')
                                            }
                                        </div>
                                        <div className="row"> 
                                            
                                            <div className="col s12 m3"> 
                                                    <label   className="active">
                                                    <small>CIC </small>
                                                </label>
                                                <input
                                                    id="yes1"
                                                    type="radio"
                                                    className="with-gap"
                                                    name="cic"
                                                    value='1'
                                                    checked = {this.state.cic == '1'}
                                                    onChange={this.handleRadioChange }
                                                        
                                                /> 
                                                <label htmlFor="yes1">Yes</label>
                                                <input
                                                    id="no1"
                                                    type="radio" 
                                                    name="cic"
                                                    className="with-gap"
                                                    value= '0'
                                                    checked = {this.state.cic == '0'}
                                                    onChange={this.handleRadioChange }
                                                />  
                                                <label htmlFor="no1">No</label>
                                            </div>
                                            <div className="input-field col s12 m2">
                                                <input
                                                    id="cic_comments"
                                                    type="text"
                                                    name="cic_comments"
                                                    value={this.state.cic_comments}
                                                    onChange={this.handleFieldChange}
                                                /> 
                                                <label htmlFor="cic_comments" className="active">Comment If Applicable
                                                </label>
                                            </div> 
                                             
                                            {
                                                (this.state.action == 'Update') ? (
                                                    <div className="input-field col s6 m1">
                                                        <button className= {this.state.cic_status} type="button"  > 
                                                        </button>
                                                    </div>
                                                ) : ('')
                                            }
                                          
                                            <div className="col s12 m3"> 
                                                    <label   className="active">
                                                    <small> Verification Mail Sent </small>
                                                </label>
                                                <input
                                                    id="break_yes1"
                                                    type="radio"
                                                    className="with-gap"
                                                    name="verification_mail"
                                                    value='1'
                                                    checked = {this.state.verification_mail == '1'}
                                                    onChange={this.handleRadioChange }
                                                        
                                                /> 
                                                <label htmlFor="break_yes1">Yes</label>
                                                <input
                                                    id="break_no1"
                                                    type="radio" 
                                                    name="verification_mail"
                                                    className="with-gap"
                                                    value= '0'
                                                    checked = {this.state.verification_mail == '0'}
                                                    onChange={this.handleRadioChange }
                                                />  
                                                <label htmlFor="break_no1">No</label>
                                            </div>
                                            <div className="input-field col s12 m2">
                                                <input
                                                    id="mail_comments"
                                                    type="text"
                                                    name="mail_comments"
                                                    value={this.state.mail_comments}
                                                    onChange={this.handleFieldChange}
                                                /> 
                                                <label htmlFor="mail_comments" className="active">Comment If Applicable
                                                </label>
                                            </div> 
                                             
                                            {
                                                (this.state.action == 'Update') ? (
                                                    <div className="input-field col s6 m1">
                                                        <button className= {this.state.mail_status} type="button"  > 
                                                        </button>
                                                    </div>
                                                ) : ('')
                                            }
                                        </div>
                                        <div className="row"> 
                                            <div className="col s12 m3"> 
                                                    <label   className="active">
                                                    <small> Approval </small>
                                                </label>
                                                <input
                                                    id="doc_updated_yes2"
                                                    type="radio"
                                                    className="with-gap"
                                                    name="approval"
                                                    value='1'
                                                    checked = {this.state.approval == '1'}
                                                    onChange={this.handleRadioChange }
                                                        
                                                /> 
                                                <label htmlFor="doc_updated_yes2">Yes</label>
                                                <input
                                                    id="doc_updated_no2"
                                                    type="radio" 
                                                    name="approval"
                                                    className="with-gap"
                                                    value= '0'
                                                    checked = {this.state.approval == '0'}
                                                    onChange={this.handleRadioChange }
                                                />  
                                                <label htmlFor="doc_updated_no2">No</label>
                                            </div>  
                                            <div className="input-field col s12 m2">
                                                <input
                                                    id="approval_comments"
                                                    type="text"
                                                    name="approval_comments"
                                                    value={this.state.approval_comments}
                                                    onChange={this.handleFieldChange}
                                                /> 
                                                <label htmlFor="approval_comments" className="active">Comment If Applicable
                                                </label>
                                            </div> 
                                             
                                            {
                                                (this.state.action == 'Update') ? (
                                                    <div className="input-field col s6 m1">
                                                        <button className= {this.state.app_status} type="button"  > 
                                                        </button>
                                                    </div>
                                                ) : ('')
                                            }
                                             
                                            <div className="col s12 m3"> 
                                                    <label   className="active">
                                                    <small>HR Final Call Done </small>
                                                </label>
                                                <input
                                                    id="yes11"
                                                    type="radio"
                                                    className="with-gap"
                                                    name="hr_finalization"
                                                    value='1'
                                                    checked = {this.state.hr_finalization == '1'}
                                                    onChange={this.handleRadioChange }
                                                        
                                                /> 
                                                <label htmlFor="yes11">Yes</label>
                                                <input
                                                    id="no11"
                                                    type="radio" 
                                                    name="hr_finalization"
                                                    className="with-gap"
                                                    value= '0'
                                                    checked = {this.state.hr_finalization == '0'}
                                                    onChange={this.handleRadioChange }
                                                />  
                                                <label htmlFor="no11">No</label>
                                            </div>
                                            <div className="input-field col s12 m2">
                                                <input
                                                    id="hr_comments"
                                                    type="text"
                                                    name="hr_comments"
                                                    value={this.state.hr_comments}
                                                    onChange={this.handleFieldChange}
                                                /> 
                                                <label htmlFor="hr_comments" className="active">Comment If Applicable
                                                </label>
                                            </div> 
                                        </div>
                                        <div className="row"> 
                                            <div className="col s12 m3"> 
                                                    <label   className="active">
                                                    <small> Visit </small>
                                                </label>
                                                <input
                                                    id="break_yes111"
                                                    type="radio"
                                                    className="with-gap"
                                                    name="visit"
                                                    value='1'
                                                    checked = {this.state.visit == '1'}
                                                    onChange={this.handleRadioChange }
                                                        
                                                /> 
                                                <label htmlFor="break_yes111">Yes</label>
                                                <input
                                                    id="break_no111"
                                                    type="radio" 
                                                    name="visit"
                                                    className="with-gap"
                                                    value= '0'
                                                    checked = {this.state.visit == '0'}
                                                    onChange={this.handleRadioChange }
                                                />  
                                                <label htmlFor="break_no111">No</label>
                                            </div>
                                            <div className="input-field col s12 m2">
                                             
                                                <input
                                                    id="visit_comments"
                                                    type="text"
                                                    name="visit_comments"
                                                    value={this.state.visit_comments}
                                                    onChange={this.handleFieldChange}
                                                /> 
                                                <label htmlFor="visit_comments" className="active">Comment If Applicable
                                                </label>
                                            </div>
                                            {
                                                (this.state.action == 'Update') ? (
                                                    <div className="input-field col s6 m1">
                                                        <button className= {this.state.visit_status} type="button"  > 
                                                        </button>
                                                    </div>
                                                ) : ('')
                                            } 
                                         
                                            <div className="col s12 m3 input-field">
                                                <label   className="active">
                                                    <small> Result </small>
                                                </label>
                                                <input
                                                    id="result1"
                                                    type="radio"
                                                    className="with-gap"
                                                    name="result"
                                                    value='Approved'
                                                    checked = {this.state.result == 'Approved'}
                                                    onChange={this.handleRadioChange }
                                                        
                                                /> 
                                                <label htmlFor="result1">Approved</label>
                                                <input
                                                    id="result2"
                                                    type="radio"
                                                    className="with-gap"
                                                    name="result"
                                                    value='Rejected'
                                                    checked = {this.state.result == 'Rejected'}
                                                    onChange={this.handleRadioChange }
                                                        
                                                /> 
                                                <label htmlFor="result2">Rejected</label>
                                                <input
                                                    id="result3"
                                                    type="radio"
                                                    className="with-gap"
                                                    name="result"
                                                    value='Pending'
                                                    checked = {this.state.result == 'Pending'}
                                                    onChange={this.handleRadioChange }
                                                        
                                                /> 
                                                <label htmlFor="result3">Pending</label>
                                            </div>
                                            <div className="input-field col s12 m2">
                                                <input
                                                    id="result_comments"
                                                    type="text"
                                                    name="result_comments"
                                                    value={this.state.result_comments}
                                                    onChange={this.handleFieldChange}
                                                /> 
                                                <label htmlFor="result_comments" className="active">Comment If Applicable
                                                </label>
                                            </div> 
                                        </div>
                                        
                                    </fieldset>
                                         
                                </div>
                                <div className="row">                                            
                                    <div className="input-field col s12">
                                        <Link to="/customer">
                                            <button className="btn default   waves-light"   shift_name="action">
                                                <i className="mdi-navigation-arrow-back left "></i>
                                                cancel
                                            </button>
                                        </Link>
                                        <button className="btn cyan waves-effect waves-light  " type="submit" shift_name="action">{(this.state.action)}
                                            <i className="mdi-content-send right"></i>
                                        </button>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            );
        }
    }
}