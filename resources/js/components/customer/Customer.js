/**
 * Created by Sandeep Maurya on 2nd Jan 2020
 */

import React from "react";
import _ from "lodash";
 // Import React Table
import ReactTable from "react-table";
import "react-table/react-table.css";
import {Link} from  'react-router-dom';
import {ITEM_PER_PAGE} from  "./../../helpers/responseAuthozisation";
import {getRequest} from  "./../../helpers/ApiRequest";

export  default class Customer extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            data: [],
            pages: null,
            loading: true
        };
        this.fetchData = this.fetchData.bind(this);
    }

    fetchData(state, instance) {
        this.setState({ loading: true });
        let url = '/api/customer';
        let param = this.props.location.search;
        if(param){
            url += param;
        }
        let request = getRequest(url,{
            filter : state.filtered,
            pageSize : state.pageSize,
            page : state.page,
            sort : state.sorted
        });
        request.then((response) => {
            let filteredData = response.data;
            this.setState({
                data: filteredData.data,
                pages: Math.ceil(filteredData.count / state.pageSize),
                loading: false
            });
        });
    }

    render() {
        const { data, pages, loading } = this.state;
        return (
            <div>
                <div className="dashboard">
                    <div className="card-panel">
                    <div className="row">
                            <div className=" col s6 m6 l3">
                                <Link to="/customer-new">
                                    <button className="btn cyan waves-effect waves-light  ">
                                        <i className="mdi-content-create left"></i>
                                        Create Client
                                    </button>
                                </Link>
                            </div>
                            <div className="col s6 m6 l4 center-align">
                                <h4 className="header2">
                                    Clients
                                </h4>
                            </div>
                        </div>
                        
                        <ReactTable
                            columns={[
                                {
                                    Header: "Client  Name",
                                    accessor: "name"
                                } ,
                                {
                                    Header: "Mobile No.",
                                    accessor: "mobile"
                                },
                                {
                                    Header: "Email",
                                    accessor: "email"
                                },
                                {
                                    Header: "Agent",
                                    accessor: "emp_name"
                                },
                                {
                                    Header: "Result",
                                    accessor: "result"
                                },
                                {
                                    Header: "Pending At",
                                    accessor: "pending_at"
                                },
                                {
                                    Header : "Created At" ,
                                    accessor : "created_at"
                                },
                                {
                                    Header: "Action",
                                    accessor: "id",
                                    Cell:(row)=>(
                                            <div className="switch">
                                                <Link to={'/customer-details/'+ row.original.id} className="input-field ">
                                                    <button className="btn light waves-effect waves-light action_button "><i className="mdi-action-launch"></i></button>
                                                </Link>
                                            </div>
                                        )

                                }
                            ]}
                            manual // Forces table not to paginate or sort automatically, so we can handle it server-side
                            data={data}
                            pages={pages} // Display the total number of pages
                            loading={loading} // Display the loading overlay when we need it
                            onFetchData={this.fetchData} // Request new data when things change
                            filterable
                            defaultPageSize={ITEM_PER_PAGE}
                            className="-highlight" //-striped
                        />
                        <br />
                    </div>
                </div>

            </div>
        );
    }
}


