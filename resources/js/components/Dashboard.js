/**
 * Created by Sandeep Maurya on 29th dec 2018.
 */

import React from 'react';
import  Loader from './admin-settings/usable_components/Loader'; 
import {CARD} from  "./../helpers/responseAuthozisation";
import {getRequest} from  "./../helpers/ApiRequest";
import {Link} from  'react-router-dom';
import ReactFC from "react-fusioncharts";
import FusionCharts from "fusioncharts";
import Pie2D from "fusioncharts/fusioncharts.charts";
import Column2D from "fusioncharts/fusioncharts.charts";
import FusionTheme from "fusioncharts/themes/fusioncharts.theme.fusion";
 
ReactFC.fcRoot(FusionCharts,Column2D, FusionTheme);
ReactFC.fcRoot(FusionCharts, Pie2D, FusionTheme);
 
  
export default class Dashboard extends React.Component{
    constructor(props){
        super(props);
        this.state = {
            hasError : false ,
            is_loading : true,
            period : 1,
            client_data :[],
            chartConfigs : {
                type: "column2d",
                width: "100%",
                height: "400",
                dataFormat: "json",
                dataSource: {
                  chart: {
                    caption: "Customer Forms With Status",
                    subCaption: "",
                    xAxisName: "Status",
                    yAxisName: "Total Counts",
                    numberSuffix: "",
                    theme: "fusion"
                  },
                  data: []
                }
              },
              chartConfigs1 : {
                type: "pie2d",
                width: "100%",
                height: "400",
                dataFormat: "json",
                dataSource:  {
                    "chart": {
                        "caption": "Completeness Of Forms",
                        "subCaption": "",
                        "use3DLighting": "0",
                        "showPercentValues": "1",
                        "decimals": "1",
                        "useDataPlotColorForLabels": "1",
                        "theme": "fusion"
                    },
                    "data": [ 
                    ]
                }
              }
        }
        this.handleFieldChange = this.handleFieldChange.bind(this);
    }

    componentDidMount(){
        this.loadClientsCount(1);
    }

    handleFieldChange(event){
      this.setState({
          [event.target.name] : event.target.value
      });
      this.loadClientsCount(event.target.value);
  }

    static getDerivedStateFromError(error) {
        // Update state so the next render will show the fallback UI.
        return { hasError: true };
    }

    loadClientsCount( period = 1 ){
      let self = this;
        var url = "/api/dashboard";
        let request = getRequest(url,{period:period});
        request.then((response)=>{
            const data = response.data.data;
            self.setState({
                'client_data' : data.bar,
                chartConfigs : {
                    type: "column2d",
                    width: "100%",
                    height: "400",
                    dataFormat: "json",
                    dataSource: {
                      chart: {
                        caption: "Customer Forms With Status",
                        subCaption: "",
                        xAxisName: "Status",
                        yAxisName: "Total Counts",
                        "showPercentInTooltip": "0",
                        "decimals": "1",
                        "useDataPlotColorForLabels": "1",
                        numberSuffix: "",
                        theme: "fusion"
                      },
                      data: data.bar
                    }
                  },
                  chartConfigs1 : {
                    type: "pie2d",
                    width: "100%",
                    height: "400",
                    dataFormat: "json",
                    dataSource:  {
                        "chart": {
                            "caption": "Completeness Of Forms",
                            "subCaption": "",
                            "use3DLighting": "0",
                            "showPercentValues": "1",
                            "decimals": "1",
                            "useDataPlotColorForLabels": "1",
                            "theme": "fusion"
                        },
                        "data": data.pie
                    }
                  }, 
                is_loading:false
            });
        });
    }

    ClientsCount(){
        const client_data = [];

        this.state.client_data.map((balance,index)=>{
            let color = CARD[parseInt(Math.random()*10)];
            let c_l = "card-content white-text " +color ;
            let b_l = "card-action "+ color+" darken-2 white-text";
            client_data.push(
                <div key={index} className="col s12 m4 l3">
                    <div className="card">
                        <div className={c_l}>
                            {/* <p className="card-stats-title center-align">  {balance.title.toUpperCase()}  </p> */}
                            <h4 className="card-stats-number">  <Link  to={balance.link}>{balance.count} </Link> </h4>
                            <p className="card-stats-compare">
                            </p>
                        </div>
                        <div className={b_l}>
                            <div  className="center-align row"> 
                              <div className="col s12">{balance.title.toUpperCase()}  </div>
                            </div>
                        </div>
                    </div>
                </div>
            )
        })
        return client_data;
    }
 
    render(){
        let new_client_data = this.ClientsCount();
        if(this.state.is_loading){
            return <Loader />;
        } 
        if(this.state.hasError){
           return <h4 className="header2">Something went wrong</h4>
        }   else{
            return(
                <div className="dashboard">
                    <div className="card-panel">
                        <div className="row">
                            
                            <form className="col s12" >
                                <div id="leave_htmlForm"> 
                                    <div className="row">                                                
                                        <div className="col s12 l10 center-align">
                                            <h4 className="header2">  Overview </h4>
                                        </div>
                                        <div className="col s12 l2 input-field">
                                            <select
                                                value={this.state.period}
                                                onChange={this.handleFieldChange}
                                                name="period"
                                                id="period"
                                                className="browser-default"
                                            >
                                                <option value='1'> Today</option>
                                                <option key= '7' value='7' >Last 7 Days</option>
                                                <option key= '15' value='15' >Last 15 Days</option>
                                                <option key= '30' value='30' >Last 30 Days</option>
                                            </select>
                                        </div>
                                    </div>
                                    <div id="card-stats">
                                        <div className="row">
                                            {
                                                new_client_data
                                            }
                                        </div>
                                        <div className="row">
                                            <div className="col s12">
                                                <ReactFC {...this.state.chartConfigs} />
                                            </div>
                                        </div>
                                        <div className="row">
                                            <div className="col s12">
                                                <ReactFC {...this.state.chartConfigs1} />
                                            </div>
                                        </div>                                        
                                    </div>                                     
                                </div>
                            </form>                            
                        </div>
                    </div>
                </div>
            );
        }
    }
}


