/**
 * Created by Sandeep Maurya on 21/07/2019.
 * @ 11:55 PM
 */

import React ,{ Fragment }from 'react'; 
import axios from 'axios';
import {toast} from 'react-toastify';
import Loader from './../admin-settings/usable_components/Loader';
import LoadingButton from './../admin-settings/usable_components/LoadingButton';
import {authHeader,logout} from  "./../../helpers/responseAuthozisation";

export default class PasswordUpdate extends React.Component{
	constructor(props){
		super(props); 
		this.state = {
            req_user_id : 'eyJpdiI6ImJCVHRRRkNwUXRNeGZNYXdxZHFVN0E9PSIsInZhbHVlIjoiZmdBc0d2UldqUDlJVCtwXC84QlFHbUE9PSIsIm1hYyI6IjljMjBlODBmYWNmODFlYzNhZDc5NjVkN2VhZWFiYTc4YmNlNDhlZjFmMjJlZjQ3YWRiYTNlZDBiZWM1NDU4MjkifQ',
            password : '',
            confirm : '',
            is_validating : false,
            policy : [
                'Must have atleast six character(s)',
                'Must contain atleast one special character(s) ',
                'Must contain atleast one number',
                'Must contain atleast one capital letter'
            ]
		}
	}

    handleFieldChange(event){
        event.preventDefault();
        this.setState({
            [event.target.name] : event.target.value
        });
    }

    handlePasswordChange(event){
        event.preventDefault();
        let {password , confirm}  = this.state;
        let flag =  true ;
        let message = '';
        password = password.trim();
        confirm = confirm.trim();
        this.setState({
            is_validating : true
        });
        var pattern =  /^(?=.*\d)(?=.*[a-z])(?=.*[A-Z])(?=.*[^a-zA-Z0-9])(?!.*\s).{6,200}$/;
        if( ! password.match(pattern)){
            flag = false;
            message = "Please carefully read password policy  "; 
        }
        if(password.length < 6){
            flag = false;
            message = "Password must be atleast six character(s) ";
        }
        if(confirm != password){
            flag = false;
            message = "Please confirm password ";
        }
        if(password == undefined || confirm == undefined || password == '' || confirm == '' ){
            flag = false;
            message = "Please enter password/confirm password ";
        }

        if(flag){
            let self = this ;
            let url = '/api/password';
            let {history} = this.props;
            axios.post(url,{
                req_user_id : self.state.req_user_id,
                password : self.state.password,
                confirm : self.state.confirm
            },{
                headers : authHeader()
            }).then(response=>{
                if(response.status == 200){
                    if(response.data.result){
                        toast(({ closeToast }) => <div>{response.data.message}</div>);
                        self.setState({
                            is_validating : false
                        });
                        history.push('/welcome') ; 
                    } else{
                        toast(({ closeToast }) => <div>{response.data.error} </div>);
                    }
                }else{
                    toast(({ closeToast }) => <div>Something went wrong , please try after some time  </div>);
                    history.push('/') ;   
                }
            }) .catch(error=> {
                switch(error.response.status){
                    case 500 :
                        // logout();
                        toast(({ closeToast }) => <div> {error.response.data.message} </div>);
                        history.push('/') ;
                        break ;
                    case 401 :
                        toast(({ closeToast }) => <div> {error.response.data.error} </div>);
                        logout();
                        break
                    default :
                        toast(({ closeToast }) => <div> could not fetch leave details </div>);
                        history.push('/') ;
                        break;
                }
            }) ;
        }else{
            this.setState({
                is_validating : false
            });
            toast(({ closeToast }) => <div>{message}</div>)
        }
    }

    policy(){
        return(
            this.state.policy.map((val,index)=>{
                return(
                    <div key={index+'_'} className="card light-blue lighten-5">
                        <div className="card-content light-blue-text">
                            <p>{val}</p>
                        </div>
                    </div>
                )
            })
        )
    }

	render(){
        const info = this.state ;
        let policy = this.policy();
		return(
            <div className="dashboard">
                <div className="card-panel">
                    <form onSubmit= {this.handlePasswordChange.bind(this)} >
                        <div className="row">
                            <div className ="col s12 m3"> 
                                <div className="row">
                                    <div className="input-field col s12">
                                        <i className="mdi-action-lock-outline prefix"></i>
                                        <input 
                                            id="password" 
                                            name="password" 
                                            type="password" 
                                            value = {this.state.password}
                                            onChange = {this.handleFieldChange.bind(this)} 
                                        />
                                        <label htmlFor="password" className="center-align">New Password</label>
                                    </div>
                                </div>
                                <div className="row">
                                    <div className="input-field col s12 ">
                                        <i className="mdi-action-lock-outline prefix"></i>
                                        <input 
                                            id="confirm" 
                                            name="confirm" 
                                            type="password" 
                                            value = {this.state.confirm}
                                            onChange = {this.handleFieldChange.bind(this)} 
                                        />
                                        <label htmlFor="confirm" className="center-align">Confirm Password</label>
                                    </div>
                                </div>
                                
                                <div className="row">
                                    <div className="input-field col s12">
                                        {
                                            (this.state.is_validating) ? (
                                                <LoadingButton />
                                            ) : (
                                                <button className= "btn waves-effect cyan waves-light col s12"  type="submit">
                                                Update 
                                                </button>
                                            )
                                        }
                                    </div>
                                </div>
                            </div>
                            
                            <div className ="col s12 m5"> 
                                {
                                    policy
                                }
                            </div>
                        </div>
                    </form>
                </div>
            </div>
		)
	}
}