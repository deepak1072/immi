/**
 * Created by Sandeep Maurya on 23rd  dec 2018
 */

import React from "react";
import axios from 'axios';
import {Link} from  'react-router-dom';
import Loader from './../admin-settings/usable_components/Loader';
import LoadingButton from './../admin-settings/usable_components/LoadingButton';
import Sandesh from './../admin-settings/usable_components/Sandesh';
class Login extends React.Component {
    constructor(props){
        super(props);
        this.state = {
            user_code : '',
            password : '' ,
            hasError : false,
            message : '',  
            background : '',
            flag : true,
            is_loading : true,
            is_validating : false
        };
    }

    componentDidMount(){
        let url = '/api/company-login-info';
        let self = this;
        axios.get(url,{
            params:{}
        }).then(function(response){
            if(response.status == 200){
                let info = response.data;
                self.setState({
                    is_loading : false,
                    banner : info.login_banner,
                    logo:info.logo,
                    id : info.id,
                    code : info.code,
                    theme :info.theme,
                    url : info.url
                });
            }
        }).catch(function(error){
            console.log(error);
        }); 
    }

     /**
     * form validation
     * @returns {boolean|*}
     */
    validate(){
        this.state.flag = true;
        if(this.state.password.trim() =='' || this.state.password.trim() == 'undefined'){
            this.state.message = 'Please enter password ';
            this.state.flag = false;
        }
        if( (this.state.user_code.trim() =='' || this.state.user_code.trim() == 'undefined')  ) {
            this.state.message = 'Please enter access code ';
            this.state.flag = false;
        }
        return this.state.flag;
    }

    handleFieldChange(event){
        this.setState({
            [event.target.name] : event.target.value
        });
    }

    handleLogin(event){
        event.preventDefault();
        const {history} = this.props;
        const login = this.state;
        this.setState({
            is_validating : true
        });
        let self = this;
        if(this.validate()){
            axios.post('/api/login', login)
                .then(response => {
                    self.setState({
                        is_validating : false
                    });
                    if(response.data.result === true){
                        Sandesh(response.data.message,'success');
                        localStorage.setItem('data',JSON.stringify(response.data.data));
                        if(response.data.data.active_role == 2 ){
                            window.location.href = '/#/dashboard';
                        }else{
                            window.location.href = '/#/welcome';
                        }
                        
                    } else{ 
                        Sandesh(response.data.error,'warn');
                    }
                })
                .catch(error => {
                    Sandesh('Something went wrong ! please try after some  time','error');
                    history.push('/login') ;
                })
        }  else{
            this.setState({
                is_validating : false
            });
            Sandesh(this.state.message,'warn');
        }
    }
    static getDerivedStateFromError(error) {
        // Update state so the next render will show the fallback UI.
        return { hasError: true };
    }

    render(){
        if(this.state.is_loading){
            return <Loader />
        }
        let banner = {
            backgroundImage :'url(' + this.state.banner + ')'
        }
        let back = {
            background : 'transparent'
        }
        if(this.state.hasError){
           return <h4 className="header2">Something went wrong</h4>
        } else{
            return(
                    <div className="container-fluid login-container"  style={ banner } >
                        <div id="login-page" className="row login-page"  >
                            <div className="col s12 z-depth-4 back-login " >
                                <form className="login-form" id="login_form" onSubmit = {this.handleLogin.bind(this)}>
                                    <div className="row">
                                        <div className="input-field col s12 center">
                                            <img className="login-logo" src = {this.state.logo} />
                                            <p className="center login-form-text"> Login Portal</p>
                                        </div>
                                    </div>
                                    <div className="row margin">
                                        <div className="input-field col s12">
                                            <i className="mdi-social-person-outline prefix"></i>
                                            <input 
                                                id="user_code" 
                                                name="user_code" 
                                                type="text" 
                                                value = {this.state.user_code}
                                                onChange = {this.handleFieldChange.bind(this)} 
                                            />
                                            <label htmlFor="user_code" className="center-align">User access code</label>
                                        </div>
                                    </div>
                                    <div className="row margin">
                                        <div className="input-field col s12">
                                            <i className="mdi-action-lock-outline prefix"></i>
                                            <input 
                                                id="password" 
                                                name="password" 
                                                type="password"
                                                value = {this.state.password}
                                                onChange = {this.handleFieldChange.bind(this)}
                                            />
                                            <label htmlFor="password">Password</label>
                                        </div>
                                    </div>
                                    {/* <div className="row">
                                        <div className="input-field col s12 m12 l12  login-text">
                                            <input type="checkbox" id="remember-me" />
                                            <label htmlFor="remember-me">Remember me</label>
                                        </div>
                                    </div> */}
                                    <div className="row">
                                        <div className="input-field col s12">
                                            
                                            {
                                                (this.state.is_validating) ? (
                                                    <LoadingButton />
                                                ) : (
                                                    <button   id="login_submit" className= "btn waves-effect cyan waves-light col s12"  type="submit">
                                                    Login
                                                    </button>
                                                )
                                            }
                                        </div>
                                        
                                    </div>
                                    <div className="row">
                                        {/* <div className="input-field col s6 m6 l6">
                                            <p className="margin right-align medium-small"><Link to="/forgot-password">Forgot password ?</Link></p>
                                        </div> */}
                                    </div>

                                </form>
                            </div>
                        </div>
                    </div>
                );
        }
    }

}

export default Login;

// Login.propTypes = {
//
//     cookies : PropTypes.instanceOf(Cookies)
// };

