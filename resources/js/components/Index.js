import React, { Component } from 'react';

import ReactDOM from 'react-dom';
// import 'bootstrap/dist/css/bootstrap.min.css';
import { BrowserRouter,HashRouter, Route, Switch } from 'react-router-dom';
import { CookiesProvider } from 'react-cookie';
import { instanceOf } from 'prop-types';
import store, { history } from './../redux/store' ;
import { Provider } from 'react-redux' ;

import {getRequest} from  "./../helpers/ApiRequest";

import Header from './layout/Header';
import Footer from './layout/Footer';
import Sidebar from './layout/Sidebar';
import { ToastContainer, toast } from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css';
import 'react-notifications-component/dist/theme.css';

import Dashboard from './Dashboard';
import Home from './Home';
import EmployeeProfile from './profile/EmployeeProfile';
import Calendar from './attendance/Calendar';
import AR from './attendance/AR';
import ArTransactions from './attendance/ArTransactions';
import ArDetails from './attendance/ArDetails';
import OD from './attendance/OD';
import OdTransactions from './attendance/OdTransactions';
import OdDetails from './attendance/OdDetails';

import Reports from './reports/Reports';
import Settings from './settings/Settings';
import Test from './Test';
import Login from './login/Login';
import PasswordUpdate from './login/PasswordUpdate';

import CreateMenu from './admin-settings/menu/CreateMenu';
import MenuList from './admin-settings/menu/MenuList';
import EditMenu from './admin-settings/menu/EditMenu';

import CompanyList from './admin-settings/company/CompanyList';
import CreateCompany from './admin-settings/company/CreateCompany';
import EditCompany from './admin-settings/company/EditCompany';
import RolesAddUpdate from './admin-settings/roles/RolesAddUpdate';
import AssignRolesAddUpdate from './admin-settings/roles/AssignRolesAddUpdate';

import RolesList from './admin-settings/roles/RolesList';

import WofkFlowList from './admin-settings/work_flows/WorkFlowList';
import AddUpdateWorkflow from './admin-settings/work_flows/AddUpdateWorkflow';
import DivisionList from './admin-settings/division/DivisionList';
import AddUpdateDivision from './admin-settings/division/AddUpdateDivision';

import ProcessList from './admin-settings/process/ProcessList';
import AddUpdateProcess from './admin-settings/process/AddUpdateProcess';

import GradeList from './admin-settings/grades/GradeList';
import AddUpdateGrade from './admin-settings/grades/AddUpdateGrade';

import EmployeeTypeList from './admin-settings/employee_type/EmployeeTypeList';
import AddUpdateEmployeeType from './admin-settings/employee_type/AddUpdateEmployeeType';

import RegionsList from './admin-settings/regions/RegionsList';
import AddUpdateRegions from './admin-settings/regions/AddUpdateRegions';

import DesignationList from './admin-settings/designation/DesignationList';
import AddUpdateDesignation from './admin-settings/designation/AddUpdateDesignation';

import CostCenterList from './admin-settings/cost_center/CostCenterList';
import AddUpdateCostCenter from './admin-settings/cost_center/AddUpdateCostCenter';

import QualificationList from './admin-settings/qualification/QualificationList';
import AddUpdateQualification from './admin-settings/qualification/AddUpdateQualification';

import FunctionsList from './admin-settings/functions/FunctionsList';
import AddUpdateFunctions from './admin-settings/functions/AddUpdateFunctions';

import Holidays from './admin-settings/holidays/Holidays';
import AddUpdateHolidays from './admin-settings/holidays/AddUpdateHolidays';

import ShiftList from './admin-settings/attendance/ShiftList';
import AddUpdateShift from './admin-settings/attendance/AddUpdateShift';
import RosterSetup from './admin-settings/attendance/RosterSetup';
import RosterMapping from './admin-settings/attendance/RosterMapping';
import RosterList from './admin-settings/attendance/RosterList';
import Roster from './attendance/Roster';

import Leave from './leave/Leave';
import CreateLeaveRequest from './leave/CreateLeaveRequest';
import LeaveBalance from './leave/LeaveBalance';
import LeaveTransactions from './leave/LeaveTransactions';
import LeaveTransactionDetails from './leave/LeaveTransactionDetails';
import CompOffRequest from './leave/CompOffRequest';
import LeaveList from './admin-settings/leave/LeaveList';
import AddUpdateLeave from './admin-settings/leave/AddUpdateLeave';
import ReactTable from './react-table/Demo';
import ImportExports from './reports/ImportExports';
import OpeningBalance from './leave/OpeningBalance';
import Agents from './agents/Agents';
import AgentsDetails from './agents/AgentsDetails';
import Customer from './customer/customer';
import CustomerDetails from './customer/CustomerDetails';
import CustomerDetails1 from './customer/CustomerDetails1';
import DeadLine from './settings/DeadLine';

export default class Index extends Component {
    constructor(props){
        super(props);
        // const { cookies } = props;
        this.state = {
            token : '',
            user_login_status :  false,
            user_coc_acceptance: false,
            user_assigned_role : [],
            active_role : '',
            active_role_name : '',
            role_masters_id : '',
            user_code : '',
            company_code : '',
            oemail_id : '',
            user_name : '',
            profile_pic : ''
        }
    }
    componentDidMount(){
        this.hydrateStateWithLocalStorage();
        this.refreshToken();
    }

    handleRoleChanges(event,role_name,role_masters_id){

        this.setState({
            active_role : event.target.value ,
            active_role_name : role_name,
            role_masters_id :   role_masters_id
        });

        if(localStorage.hasOwnProperty('data')){
            let data = JSON.parse(localStorage.getItem('data'));
            // const {history} = this.props;
            data.role_masters_id = role_masters_id;
            data.active_role =  event.target.value;
            data.active_role_name   = role_name;
            localStorage.setItem('data',JSON.stringify(data));
            // history.push('/');
        }
        window.location.reload();
    }

    hydrateStateWithLocalStorage() {
        // for all items in state
        if(localStorage.hasOwnProperty('data')){
            let data = JSON.parse(localStorage.getItem('data'));
            for (let key in this.state) {
                // if the key exists in localStorage
                if (data.hasOwnProperty(key)) {
                    let value = '';
                    try {
                        value = data[key];
                        this.setState({ [key]: value });
                    } catch (e) {
                        // handle empty string
                        this.setState({ [key]: value });
                    }
                }
            }
        }
    }

    refreshToken(){
        setInterval(()=>{
            let url = '/api/refresh-token';
            let request = getRequest(url);
            request.then((response) => {
                if(response.data.result){
                    if(localStorage.hasOwnProperty('data')){
                        let data = JSON.parse(localStorage.getItem('data'));
                        if (data.hasOwnProperty('token')) {
                            data.token = response.data.data;
                            localStorage.setItem('data',JSON.stringify(data));
                        }
                    }
                }
            });
        },800000);
    }


    render() {
        const user_login_status = this.state.user_login_status; 
        
        return (
            <div >
                <HashRouter>
                    {user_login_status ? (
                        <div>
                            <Header 
                             active_role = {this.state.active_role}
                            />
                            <div id="main">
                                <div className="wrapper">
                                    <section id="content">
                                        <div className="container-fluid">
                                            <div className="row" >
                                                <div className ="col s12">
                                                    <Sidebar
                                                        roles = {this.state.user_assigned_role}
                                                        active_role = {this.state.active_role}
                                                        active_role_name = {this.state.active_role_name}
                                                        user_code = {this.state.user_code}
                                                        user_name ={this.state.user_name}
                                                        profile_pic ={this.state.profile_pic}
                                                        role_masters_id = {this.state.role_masters_id}
                                                        company_code = {this.state.company_code}
                                                        handleRoleChanges = {this.handleRoleChanges.bind(this)}

                                                    />
                                                    <Switch>  
                                                        
                                                        <Route  exact path='/welcome' component={Home} />
                                                        <Route  exact path='/' component={Home} />
                                                        <Route exact path='/dashboard' component={Dashboard} />
                                                        <Route exact path='/calendar' component={Calendar} />
                                                        <Route exact path='/create-attendance-request' component={AR} />
                                                        <Route exact path='/mark-attendance-transactions' component={ArTransactions} />
                                                        <Route exact path='/ar-request-approval/:mark/:trx' component={ArDetails} />
                                                        <Route exact path='/create-od-request' component={OD} />
                                                        <Route exact path='/od-transactions' component={OdTransactions} />
                                                        <Route exact path='/od-request-approval/:od/:trx' component={OdDetails} />

                                                        <Route exact path='/reports' component={Reports} />
                                                        <Route exact path='/settings' component={Settings} />
                                                        <Route exact path='/test' component={Test} />
                                                        <Route exact path='/create-menu' component={CreateMenu} />
                                                        <Route exact path='/menu-list' component={MenuList} />
                                                        <Route exact  path='/edit-menu/:id' component={EditMenu} />
                                                        <Route exact path='/react-table' component={ReactTable} />
                                                        <Route exact path='/company-list' component={CompanyList} />
                                                        <Route exact path='/create-company' component={CreateCompany} />
                                                        <Route exact path='/edit-company/:id' component={EditCompany} />

                                                        <Route exact path='/role-setup' component={RolesList} />
                                                        <Route exact path='/add-update-roles' component={RolesAddUpdate} />
                                                        <Route exact path='/add-update-roles/:id' component={RolesAddUpdate} />
                                                        <Route exact path='/assigned-roles-list' component={AssignRolesAddUpdate} />

                                                        <Route exact path='/work-flows' component={WofkFlowList} />
                                                        <Route exact path='/add-update-work-flow' component={AddUpdateWorkflow} />
                                                        <Route exact path='/add-update-work-flow/:id' component={AddUpdateWorkflow} />

                                                        <Route exact path='/division-setup' component={DivisionList} />
                                                        <Route exact path='/add-update-division' component={AddUpdateDivision} />
                                                        <Route exact path='/add-update-division/:id' component={AddUpdateDivision} />

                                                        <Route exact path='/process-setup' component={ProcessList} />
                                                        <Route exact path='/add-update-process' component={AddUpdateProcess} />
                                                        <Route exact path='/add-update-process/:id' component={AddUpdateProcess} />

                                                        <Route exact path='/grade-setup' component={GradeList} />
                                                        <Route exact path='/add-update-grade' component={AddUpdateGrade} />
                                                        <Route exact path='/add-update-grade/:id' component={AddUpdateGrade} />

                                                        <Route exact path='/employee-type-setup' component={EmployeeTypeList} />
                                                        <Route exact path='/add-update-employee-type' component={AddUpdateEmployeeType} />
                                                        <Route exact path='/add-update-employee-type/:id' component={AddUpdateEmployeeType} />

                                                        <Route exact path='/regions-setup' component={RegionsList} />
                                                        <Route exact path='/add-update-regions' component={AddUpdateRegions} />
                                                        <Route exact path='/add-update-regions/:id' component={AddUpdateRegions} />

                                                        <Route exact path='/designation-setup' component={DesignationList} />
                                                        <Route exact path='/add-update-designation' component={AddUpdateDesignation} />
                                                        <Route exact path='/add-update-designation/:id' component={AddUpdateDesignation} />

                                                        <Route exact path='/cost-center-setup' component={CostCenterList} />
                                                        <Route exact path='/add-update-cost-center' component={AddUpdateCostCenter} ></Route>
                                                        <Route exact path='/add-update-cost-center/:id' component={AddUpdateCostCenter} ></Route>

                                                        <Route exact path='/qualification-setup' component={QualificationList} />
                                                        <Route exact path='/add-update-qualification' component={AddUpdateQualification} />
                                                        <Route exact path='/add-update-qualification/:id' component={AddUpdateQualification} />


                                                        <Route exact path='/function-setup' component={FunctionsList} />
                                                        <Route exact path='/add-update-function' component={AddUpdateFunctions} />
                                                        <Route exact path='/add-update-function/:id' component={AddUpdateFunctions} />

                                                        <Route exact path='/leave-setup' component={LeaveList} />
                                                        <Route exact path='/add-update-leave' component={AddUpdateLeave} />
                                                        <Route exact path='/add-update-leave/:id' component={AddUpdateLeave} />
                                                        <Route exact path='/leave-request' component={CreateLeaveRequest} />
                                                        <Route exact path='/leave-balance' component={LeaveBalance} />
                                                        <Route exact path='/leave-transactions' component={LeaveTransactions} />
                                                        <Route exact path='/leave-request-approval/:leave/:trx' component={LeaveTransactionDetails} />
                                                        <Route exact path='/comp-off-request' component={CompOffRequest} />
                                                        <Route exact path='/leave-master-uploads' component={OpeningBalance} />
                                                        
                                                        
                                                        
                                                        
                                                        <Route exact path='/holidays-setup' component={Holidays} />
                                                        <Route exact path='/add-update-holidays' component={AddUpdateHolidays} />
                                                        <Route exact path='/add-update-holidays/:id' component={AddUpdateHolidays} />
                                                        <Route exact path='/shift-setup' component={ShiftList} />
                                                        <Route exact path='/add-update-shift' component={AddUpdateShift} />
                                                        <Route exact path='/add-update-shift/:id' component={AddUpdateShift} />
                                                        <Route exact path='/roster-setup' component={RosterSetup} />
                                                        <Route exact path='/roster-setup/:id' component={RosterSetup} />
                                                        <Route exact path='/roster-list' component={RosterList} /> 
                                                        <Route exact path='/roster-map/:id' component={RosterMapping} /> 
                                                        <Route exact path='/view-roster' component={Roster} /> 
                                                        
                                                        <Route exact path='/my-profile' component={EmployeeProfile} />
                                                        <Route exact path='/my-profile/:id' component={EmployeeProfile} />
                                                        <Route exact path='/change-password' component={PasswordUpdate} />
                                                        <Route exact path='/import-export' component={ImportExports} />
                                                        <Route exact path='/customer' component={Customer} />
                                                        <Route exact path='/customer-details' component={CustomerDetails} />
                                                        <Route exact path='/customer-new' component={CustomerDetails1} />

                                                        <Route exact path='/customer-details/:id' component={CustomerDetails} />

                                                        <Route exact path='/agents' component={Agents} />
                                                        <Route exact path='/agents-details' component={AgentsDetails} />
                                                        <Route exact path='/agents-details/:id' component={AgentsDetails} />
                                                        <Route exact path='/deadline' component={DeadLine} />
                                                        
                                                        
                                                        <Route   path='*' component={Home} />
                                                    </Switch>
                                                </div>
                                            </div>
                                        </div>
                                    </section>
                                </div>
                            </div>
                            <Footer />
                        </div>
                    ) : (
                        <Switch>
                            <Route   path='/' component={Login}   >
                                <Route exact path='/login' component={Login} />
                            </Route>
                        </Switch>

                    )}

                </HashRouter>
                <div id="card_alert"></div>
                <ToastContainer />
                {/* <ReactNotification /> */}
            </div>
        );
    }
}


if (document.getElementById('main-app')) {
    ReactDOM.render( <Provider store={store}> <Index /> </Provider>, document.getElementById('main-app'));
}
