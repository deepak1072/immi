/**
 * Created by Sandeep Maurya on 15th dec 2018.
 */

import React from 'react';
import axios from 'axios';
import { toast } from 'react-toastify';
import {Link} from 'react-router-dom';
import {authHeader,logout,ITEM_PER_PAGE} from  "./../../../helpers/responseAuthozisation";
 


export default class AddUpdateGrade extends React.Component{
    constructor(props){
        super(props);

        this.state = {
            grade_name : '',
            grade_code : '', 
            grade_status:0,
            create_at : '',
            created_by : '',
            min_salary_scale : '',
            max_salary_scale : '',
            min_salary_tolerance: '',
            max_salary_tolerance: '',
            message : '',
            flag : true ,
            hasError:false,
            action: 'Create', 
             
        }


        this.handleAddUpdateGrade = this.handleAddUpdateGrade.bind(this);
        this.handleFieldChange   = this.handleFieldChange.bind(this);

    }


    componentDidMount(){

        let self = this;
        const { id } = this.props.match.params;
        console.log('fine');
        console.log(id);
        if(id != undefined){ 
            axios.get('/api/grade-details',{
                params:{
                    id : id
                },
                headers : authHeader()
            }).then(function(response){
                if(response.status == 200){
                    const data = response.data[0];
                    self.setState({
                        grade_name : data['grade_name'],
                        grade_id : data['grade_id'], 
                        grade_code: data['grade_code'] , 
                        min_salary_scale: data['min_salary_scale'] , 
                        max_salary_scale: data['max_salary_scale'] , 
                        min_salary_tolerance: data['min_salary_tolerance'] , 
                        max_salary_tolerance: data['max_salary_tolerance'] , 
                        action : 'Update' 
                        
                    });
                    
                }else{

                    toast(({ closeToast }) => <div>Something went wrong , please try after some time  </div>);
                    history.push('/grade-setup') ;   
                }
                

            }).catch(function(error){
                switch(error.response.status){
                    case 500 :
                        // logout();
                        toast(({ closeToast }) => <div> {error.response.data.message} </div>); 
                        history.push('/process-setup') ;   
                        break ;
                    case 401 :
                        toast(({ closeToast }) => <div> {error.response.data.error} </div>);
                        logout();
                        break;
                    default :
                        toast(({ closeToast }) => <div> could not fetch process list </div>); 
                        history.push('/process-setup') ;   
                        break;
                } 
            });
        }

        
    }

    static getDerivedStateFromError(error) {
        // Update state so the next render will show the fallback UI.
        return { hasError: true };
    }

    /**
     * form validation
     * @returns {boolean|*}
     */
    validate(){
    	 this.state.flag = true;
          
        if(this.state.grade_name =='' || this.state.grade_name == 'undefined'){
            this.state.message = 'Please enter grade name';
            this.state.flag = false;
        }
        
        if(this.state.grade_code =='' || this.state.grade_code == 'undefined'){
            this.state.message = 'Please enter grade code';
            this.state.flag = false;
        }
       
        return this.state.flag;
    }



    handleAddUpdateGrade(event){
        event.preventDefault();
        const {history} = this.props;
        const grades = this.state;

        
        if(this.validate()){
            axios.post('/api/add-update-grade', grades,{headers : authHeader()})
                .then(response => {
                    if(response.data.result){
                        toast(({ closeToast }) => <div>{response.data.message}</div>);
                        history.push('/grade-setup') ;
                    } else{
                        response.data.error.map((errors)=>{
                            toast(({ closeToast }) => <div>{errors} </div>);
                        });
                    }
                })
                .catch(error => { 
                    
                    switch(error.response.status){
                        case 500 :
                            // logout();
                            toast(({ closeToast }) => <div> {error.response.data.message} </div>); 
                            history.push('/grade-setup') ;  
                            break ;
                        case 401 :
                            toast(({ closeToast }) => <div> {error.response.data.error} </div>);
                            logout();
                            break;
                        default :
                            toast(({ closeToast }) => <div> something went wrong , please try after some time  </div>); 
                            history.push('/grade-setup') ; 
                            break;
                    } 
                })
        }  else{
            toast(({ closeToast }) => <div>{this.state.message}</div>)
        }

    }
    handleFieldChange(event){
        
        this.setState({
            [event.target.name] : event.target.value
        });
    }
 
    render(){

        if(this.state.hasError){
           return <h4 className="header2">Something went wrong</h4>
        }   else{
            return(
                <div className="dashboard">
                    <div className="card-panel">
                        <div className="row">
                            <form className="col s12" onSubmit={this.handleAddUpdateGrade}>
                                <div id="leave_htmlForm">
                                    <div className="row">                                                
                                        
                                        <div className="col s12 l12 center-align">
                                            <h4 className="header2">{this.state.action} Grade </h4>
                                        </div>
                                    </div>
                                    <div className="row"> 
                                            
                                            
                                        <div className="input-field col s2">
                                            <input
                                                id="grade_code"
                                                type="text"
                                                name="grade_code"
                                                value={this.state.grade_code}
                                                onChange={this.handleFieldChange}
                                            /> 
                                            <label htmlFor="grade_code" className="active">Grade Code * 
                                            </label>
                                        </div>

                                        <div className="input-field col s2">
                                            <input
                                                id="grade_name"
                                                type="text"
                                                name="grade_name"
                                                value={this.state.grade_name}
                                                onChange={this.handleFieldChange}
                                            /> 
                                            <label htmlFor="grade_name" className="active">Grade Name * 
                                            </label>
                                        </div>

                                        <div className="input-field col s2">
                                            <input
                                                id="min_salary_scale"
                                                type="text"
                                                name="min_salary_scale"
                                                value={this.state.min_salary_scale}
                                                onChange={this.handleFieldChange}
                                            /> 
                                            <label htmlFor="min_salary_scale" className="active">Min Salary Scale 
                                            </label>
                                        </div>

                                            <div className="input-field col s2">
                                            <input
                                                id="max_salary_scale"
                                                type="text"
                                                name="max_salary_scale"
                                                value={this.state.max_salary_scale}
                                                onChange={this.handleFieldChange}
                                            /> 
                                            <label htmlFor="max_salary_scale" className="active">Max Salary Scale 
                                            </label>
                                        </div>

                                        <div className="input-field col s2">
                                            <input
                                                id="min_salary_tolerance"
                                                type="text"
                                                name="min_salary_tolerance"
                                                value={this.state.min_salary_tolerance}
                                                onChange={this.handleFieldChange}
                                            /> 
                                            <label htmlFor="min_salary_tolerance" className="active">Min Salary Tolerance 
                                            </label>
                                        </div>
                                        <div className="input-field col s2">
                                            <input
                                                id="max_salary_tolerance"
                                                type="text"
                                                name="max_salary_tolerance"
                                                value={this.state.max_salary_tolerance}
                                                onChange={this.handleFieldChange}
                                            /> 
                                            <label htmlFor="max_salary_tolerance" className="active">Max Salary Tolerance 
                                            </label>
                                        </div>

                                        

                            
                                    </div>
                                        
                                            
                                        
                                </div>
                                <div className="row">                                            
                                    <div className="input-field col s12">
                                        <Link to="/grade-setup">
                                            <button className="btn default   waves-light"   name="action">
                                                <i className="mdi-navigation-arrow-back left "></i>
                                                cancel
                                            </button>
                                        </Link>
                                        <button className="btn cyan waves-effect waves-light  " type="submit" name="action">{(this.state.action)}
                                            <i className="mdi-content-send right"></i>
                                        </button>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            );
        }
    }
}