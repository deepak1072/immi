/**
 * Created by Sandeep Maurya on 20th April 2019.
 */

import React from 'react';
import axios from 'axios';
import { toast } from 'react-toastify';
import {Link} from 'react-router-dom';
import SelectSearch from 'react-select-search';
import {authHeader,logout,ITEM_PER_PAGE} from  "./../../../helpers/responseAuthozisation";
import OuApplicability from './../usable_components/OuApplicability';
import DatePicker from "react-datepicker"; 
import "react-datepicker/dist/react-datepicker.css";
 

export default class AddUpdateHolidays extends React.Component{
    constructor(props){
        super(props);

        this.state = {
            h_name : '',
            h_date : new Date(), 
            h_status:0,
            h_type : '1',
            is_additional : '0',
            create_at : '',
            created_by : '',
            message : '',
            flag : true ,
            hasError:false,
            action: 'Create',
            h_applicable:[]
             
        }


        this.handleAddUpdateHoliday = this.handleAddUpdateHoliday.bind(this);
        this.handleFieldChange   = this.handleFieldChange.bind(this);
        this.handleRadioChange = this.handleRadioChange.bind(this);
        this.handleChange = this.handleChange.bind(this);

    }


    componentDidMount(){

        let self = this;
        const { id } = this.props.match.params;
        
        if(id != undefined){
        
            axios.get('/api/holidays-details',{
                params:{
                    id : id
                },
                headers : authHeader()
            }).then(function(response){
                if(response.status == 200){
                    const data = response.data[0];
                    const h_applicable =  JSON.parse(data.h_applicable);
                    self.setState({
                        h_date : new Date(data['h_date']),
                        h_id : data['h_id'], 
                        h_name: data['h_name'] , 
                        h_type: data['h_type'] , 
                        h_status: data['h_status'] , 
                        is_additional: data['is_additional'] , 
                        h_applicable: h_applicable , 
                        action : 'Update' 
                        
                    });
                    
                }else{

                    toast(({ closeToast }) => <div>Something went wrong , please try after some time  </div>);
                    history.push('/function-setup') ;   
                }
                

            }).catch(function(error){
                switch(error.response.status){
                    case 500 :
                        // logout();
                        toast(({ closeToast }) => <div> {error.response.data.message} </div>); 
                        history.push('/function-setup') ;
                        break ;
                    case 401 :
                        toast(({ closeToast }) => <div> {error.response.data.error} </div>);
                        logout();
                        break;
                    default :
                        toast(({ closeToast }) => <div>Something went wrong , please try after some  time  </div>); 
                        history.push('/function-setup') ;  
                        break;
                } 
            });
        }

         
    }

    static getDerivedStateFromError(error) {
        // Update state so the next render will show the fallback UI.
        return { hasError: true };
    }

    /**
     * form validation
     * @returns {boolean|*}
     */
    validate(){
    	 this.state.flag = true;
          
        if(this.state.h_type =='' || this.state.h_type == 'undefined'){
            this.state.message = 'Please select holiday type';
            this.state.flag = false;
        }
        if(this.state.h_date =='' || this.state.h_date == 'undefined'){
            this.state.message = 'Please enter holiday date ';
            this.state.flag = false;
        }
        
        if(this.state.h_name =='' || this.state.h_name == 'undefined'){
            this.state.message = 'Please enter holiday name';
            this.state.flag = false;
        }
        
        return this.state.flag;
    }



    handleAddUpdateHoliday(event){
        event.preventDefault();
        const {history} = this.props;
        const holidays = this.state;

        
        if(this.validate()){
            axios.post('/api/add-update-holidays', holidays,{headers : authHeader()})
                .then(response => {
                    if(response.data.result){
                        toast(({ closeToast }) => <div>{response.data.message}</div>);
                        history.push('/holidays-setup') ;
                    } else{
                        response.data.error.map((errors)=>{
                            toast(({ closeToast }) => <div>{errors} </div>);
                        });
                    }
                })
                .catch(error => {  
                    switch(error.response.status){
                        case 500 :
                            // logout();
                            toast(({ closeToast }) => <div> {error.response.data.message} </div>); 
                            history.push('/holidays-setup') ;
                            break ;
                        case 401 :
                            toast(({ closeToast }) => <div> {error.response.data.error} </div>);
                            logout();
                            break;
                        default :
                            toast(({ closeToast }) => <div>Something went wrong , please try after some  time  </div>); 
                            history.push('/holidays-setup') ;  
                            break;
                    } 
                })
        }  else{
            toast(({ closeToast }) => <div>{this.state.message}</div>)
        }

    }
    handleFieldChange(event){
        
        this.setState({
            [event.target.name] : event.target.value
        });
    }
    
    handleRadioChange(event){
        this.setState({
            [event.target.name] :  event.target.value
        }); 
    }
    handleOuChange(ou,event){
        
        this.setState({
            'h_applicable' : ou
        });  
    }
    handleChange(date) {
        this.setState({
          h_date: date
        });
    }
 
    render(){

        if(this.state.hasError){
           return <h4 className="header2">Something went wrong</h4>
        }   else{
            return(
                <div className="dashboard">
                    <div className="card-panel">
                        <div className="row">
                            <form className="col s12" onSubmit={this.handleAddUpdateHoliday}>
                                <div id="leave_htmlForm">
                                    <div className="row">                                                
                                        
                                        <div className="col s12 l12 center-align">
                                            <h4 className="header2">{this.state.action} Holiday </h4>
                                        </div>
                                    </div>
                                    <div className="row"> 
                                            
                                        <div className="input-field col s3">
                                            
                                            <DatePicker
                                                selected={this.state.h_date}
                                                onChange={this.handleChange}
                                            />
                                            <label htmlFor="h_date" className="active">Holiday Date * 
                                            </label>
                                        </div>
                                        <div className="input-field col s3">
                                            <input
                                                id="h_name"
                                                type="text"
                                                name="h_name"
                                                value={this.state.h_name}
                                                onChange={this.handleFieldChange}
                                            /> 
                                            <label htmlFor="h_name" className="active">Holiday Name * 
                                            </label>
                                        </div> 
                                        <div className="col s12 m4"> 
                                                <label   className="active">
                                                <small> Holiday Type </small>
                                            </label>
                                            <input
                                                id="mandatory"
                                                type="radio"
                                                className="with-gap"
                                                name="h_type"
                                                value='1'
                                                checked = {this.state.h_type == '1'}
                                                onChange={this.handleRadioChange }
                                                    
                                            /> 
                                            <label htmlFor="mandatory">Mandatory</label>
                                            <input
                                                id="optional"
                                                type="radio" 
                                                name="h_type"
                                                className="with-gap"
                                                value= '0'
                                                checked = {this.state.h_type == '0'}
                                                onChange={this.handleRadioChange }
                                            />  
                                            <label htmlFor="optional">Optional</label>
                                        </div> 
                                    </div>
                                    <div className="row">
                                        <OuApplicability 
                                            handleOuChange = {this.handleOuChange.bind(this)}
                                            ou = {this.state.h_applicable}
                                        />
                                        <div className="col s12 m3"> 
                                                <label   className="active">
                                                <small> Is Additional Holiday </small>
                                            </label>
                                            <input
                                                id="yes"
                                                type="radio"
                                                className="with-gap"
                                                name="is_additional"
                                                value='1'
                                                checked = {this.state.is_additional == '1'}
                                                onChange={this.handleRadioChange }
                                                    
                                            /> 
                                            <label htmlFor="yes">Yes</label>
                                            <input
                                                id="no"
                                                type="radio" 
                                                name="is_additional"
                                                className="with-gap"
                                                value= '0'
                                                checked = {this.state.is_additional == '0'}
                                                onChange={this.handleRadioChange }
                                            />  
                                            <label htmlFor="no">No</label>
                                        </div>
                                    </div>
                                        
                                            
                                        
                                </div>
                                <div className="row">                                            
                                    <div className="input-field col s12">
                                        <Link to="/holidays-setup">
                                            <button className="btn default   waves-light"   name="action">
                                                <i className="mdi-navigation-arrow-back left "></i>
                                                cancel
                                            </button>
                                        </Link>
                                        <button className="btn cyan waves-effect waves-light  " type="submit" name="action">{(this.state.action)}
                                            <i className="mdi-content-send right"></i>
                                        </button>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            );
        }
    }
}