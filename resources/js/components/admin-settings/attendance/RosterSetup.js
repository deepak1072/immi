/**
 * Created by Sandeep Maurya on 20th April 2019.
 */
import React from 'react';
import axios from 'axios';
import { toast } from 'react-toastify';
import {Link} from 'react-router-dom';
import  Loader from './../../admin-settings/usable_components/Loader';
import {authHeader,logout,ITEM_PER_PAGE} from  "./../../../helpers/responseAuthozisation";
import FixedRosterPattern from './FixedRosterPattern';
import VariableRosterPattern from './VariableRosterPattern';

export default class RosterSetup extends React.Component{
    constructor(props){
        super(props);
        this.state = {
            p_name : '', 
            is_fixed : '1',  
            flag : true ,
            hasError:false,
            action: 'Create',
            is_loading :true,
            options : [],
            shift:['','','','','','','','']  ,
            shift_v:[
                ['','','','',''],
                ['','','','',''],
                ['','','','',''],
                ['','','','',''],
                ['','','','',''],
                ['','','','',''],
                ['','','','',''],
                ['','','','','']
            ] 
        }


        this.handleAddUpdateRoster = this.handleAddUpdateRoster.bind(this);
        this.handleFieldChange   = this.handleFieldChange.bind(this);
        this.onShiftChange = this.onShiftChange.bind(this);
    }

    componentDidMount(){
        let self = this;
        const { id } = this.props.match.params;
        if(id != undefined){     
            axios.get('/api/roster-details',{
                params:{
                    id : id
                },
                headers : authHeader()
            }).then(function(response){
                if(response.status == 200){
                    let data = response.data;

                    self.setState({
                       ...data
                    });
                }else{
                    toast(({ closeToast }) => <div>Something went wrong , please try after some time  </div>);
                    history.push('/roster-list') ;   
                }
            }).catch(function(error){
                switch(error.response.status){
                    case 500 :
                        // logout();
                        toast(({ closeToast }) => <div> {error.response.data.message} </div>); 
                        history.push('/roster-list') ;
                        break ;
                    case 401 :
                        toast(({ closeToast }) => <div> {error.response.data.error} </div>);
                        logout();
                        break;
                    default :
                        toast(({ closeToast }) => <div>Something went wrong , please try after some  time  </div>); 
                        history.push('/roster-list') ;  
                        break;
                } 
            });
        }
        axios.get('/api/active-shift',{
            params:{},
            headers : authHeader()
        }).then(function(response){
            if(response.status == 200){
                self.setState({
                    options :  response.data,
                    is_loading : false
                }); 
            }else{ 
                toast(({ closeToast }) => <div>Something went wrong , please try after some time  </div>);
                history.push('/shift-setup') ;   
            }
        }).catch(function(error){
            switch(error.response.status){
                case 500 :
                    // logout();
                    toast(({ closeToast }) => <div> {error.response.data.message} </div>); 
                    history.push('/shift-setup') ;
                    break ;
                case 401 :
                    toast(({ closeToast }) => <div> {error.response.data.error} </div>);
                    logout();
                    break;
                default :
                    toast(({ closeToast }) => <div>Something went wrong , please try after some  time  </div>); 
                    history.push('/shift-setup') ;  
                    break;
            } 
        });
    }

    static getDerivedStateFromError(error) {
        // Update state so the next render will show the fallback UI.
        return { hasError: true };
    }

    /**
     * form validation
     * @returns {boolean|*}
     */
    validate(){
    	this.state.flag = true;
        if(this.state.is_fixed =='' || this.state.is_fixed == 'undefined'){
            this.state.message = 'Please select roster type';
            this.state.flag = false;
        }
        if(this.state.p_name =='' || this.state.p_name == 'undefined'){
            this.state.message = 'Please enter roster name';
            this.state.flag = false;
        }
        return this.state.flag;
    }


    onShiftChange(value, state, props){
        if(this.state.action != 'Create'){
            return false;
        }
        let $shift = [];
        if( this.state.is_fixed == 0){
            $shift = this.state.shift_v.map((week,index)=>{
                return week.map((day,i)=>{
                    if(props.week == (i+1)){
                        if(props.days == 0 ){
                            return value.value;
                        }
                        if(props.days == (index)){
                            return value.value;
                        } else {
                            return day;
                        }
                    }else{
                        return day;
                    }
                })
            }); 
            this.setState({
                shift_v : $shift
            });
        }else{
            $shift = this.state.shift.map((shift,index)=>{
                if(props.days == 0 ){
                    return value.value ;
                }else{
                    if(index == props.days){
                        return value.value;
                    }else{
                        return shift;
                    }
                }  
            });
            this.setState({
                shift : $shift
            });
        }
        
        
    }

    handleAddUpdateRoster(event){
        event.preventDefault();
        let {history} = this.props; 
        let pattern = this.state.shift;
        if(this.state.is_fixed != 1){
            pattern = this.state.shift_v;
        }
        let $param = {
            name : this.state.p_name,
            is_fixed : this.state.is_fixed,
            action : this.state.action,
            pattern : pattern
        };
        // $param = JSON.stringify($param);
        if(this.validate()){
            axios.post('/api/roster', $param,{headers : authHeader()})
                .then(response => {
                    if(response.data.result){
                        toast(({ closeToast }) => <div>{response.data.message}</div>);
                        history.push('/roster-list') ;
                    } else{
                        toast(({ closeToast }) => <div>{response.data.message} </div>);
                    }
                })
                .catch(error => {  
                    switch(error.response.status){
                        case 500 :
                            // logout();
                            toast(({ closeToast }) => <div> {error.response.data.message} </div>); 
                            history.push('/roster-list') ;
                            break ;
                        case 401 :
                            toast(({ closeToast }) => <div> {error.response.data.message} </div>);
                            logout();
                            break;
                        default :
                            toast(({ closeToast }) => <div>Something went wrong , please try after some  time  </div>); 
                            history.push('/roster-list') ;  
                            break;
                    } 
                })
        }  else{
            toast(({ closeToast }) => <div>{this.state.message}</div>)
        }

    }
    handleFieldChange(event){
        if(this.state.action != 'Create'){
            return false;
        }
        this.setState({
            [event.target.name] : event.target.value
        });
    }
    
    render(){
        let info = this.state;
        if(info.is_loading){
            return <Loader />;
        } 
        if(this.state.hasError){
           return <h4 className="header2">Something went wrong</h4>
        }   else{
            return(
                <div className="dashboard">
                    <div className="card-panel"> 
                        <div className="row">
                            <form className="col s12" onSubmit={this.handleAddUpdateRoster}>
                                <div id="leave_htmlForm">
                                    <div className="row">   
                                        <div className="col s12 l12 center-align">
                                            <h4 className="header2"></h4>
                                        </div>
                                    </div>
                                    <div className="row"> 
                                        <div className="input-field col s12 m2">
                                            <input
                                                id="p_name"
                                                type="text"
                                                name="p_name"
                                                value={info.p_name}
                                                onChange={this.handleFieldChange}
                                            /> 
                                            <label htmlFor="p_name" className="active">Roster Name * 
                                            </label>
                                        </div> 
                                        
                                        <div className="col s12 m3"> 
                                            <label   className="active">
                                                <small> Is Fixed Pattern </small>
                                            </label>
                                            <input
                                                id="mandatory"
                                                type="radio"
                                                className="with-gap"
                                                name="is_fixed"
                                                value='1'
                                                checked = {info.is_fixed == '1'}
                                                onChange={this.handleFieldChange }
                                                
                                            /> 
                                            <label htmlFor="mandatory">Yes</label>
                                            <input
                                                id="optional"
                                                type="radio" 
                                                name="is_fixed"
                                                className="with-gap"
                                                value= '0'
                                                checked = {info.is_fixed == '0'}
                                                onChange={this.handleFieldChange }
                                            />  
                                            <label htmlFor="optional">No</label>
                                        </div> 
                                    </div> 
                                    <div className="row">
                                        {
                                            (info.is_fixed == 1) ? (
                                                <FixedRosterPattern 
                                                    options = {info.options} 
                                                    shift ={info.shift}
                                                    onShiftChange ={this.onShiftChange}  
                                                />
                                            ) : (
                                                <VariableRosterPattern
                                                    options = {info.options} 
                                                    shift ={info.shift_v}
                                                    onShiftChange ={this.onShiftChange}
                                                />
                                            )
                                        } 
                                    </div>
                                </div>
                                <div className="row">                                            
                                    <div className="input-field col s12">
                                        <Link to="/roster-list">
                                            <button className="btn default   waves-light"   name="action">
                                                <i className="mdi-navigation-arrow-back left "></i>
                                                cancel
                                            </button>
                                        </Link>
                                        {
                                            (info.action == 'Create') ? (
                                                <button className="btn cyan waves-effect waves-light  " type="submit" name="action">{(info.action)}
                                                <i className="mdi-content-send right"></i>
                                            </button>
                                            ) : ('')
                                        }
                                    </div>
                                </div>
                            </form>
                        </div> 
                    </div>
                </div>
            );
        }
    }
}