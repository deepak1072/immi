import React from 'react';
import SelectSearch from 'react-select-search';
 

export default class VariablePatternDays extends React.Component{
    constructor(props){
        super(props);
        this.onShiftChange = this.onShiftChange.bind(this);
    }

    onShiftChange(value,state,props){
        this.props.onShiftChange(value, state, props);
    }
    render(){
        let info = this.props;
        return(
            <div className="row ">  
                 <div className="col s1 ">  
                    {info.day_name} 
                </div>
                <div className="col s2 ">   
                    <SelectSearch  
                        options={info.options} 
                        value={info.shift[0]} 
                        name="shift" 
                        days = {info.days}
                        week = "1"
                        placeholder="Select Shift"
                        onChange={this.onShiftChange} 
                    /> 
                </div>
                <div className="col s2 ">   
                    <SelectSearch  
                        options={info.options} 
                        value={info.shift[1]} 
                        name="shift" 
                        days = {info.days}
                        week = "2"
                        placeholder="Select Shift"
                        onChange={this.onShiftChange} 
                    /> 
                </div>
                <div className="col s2 ">   
                    <SelectSearch  
                        options={info.options} 
                        value={info.shift[2]} 
                        name="shift" 
                        days = {info.days}
                        week = "3"
                        placeholder="Select Shift"
                        onChange={this.onShiftChange} 
                    /> 
                </div>
                <div className="col s2 ">   
                    <SelectSearch  
                        options={info.options} 
                        value={info.shift[3]} 
                        name="shift" 
                        days = {info.days}
                        week = "4"
                        placeholder="Select Shift"
                        onChange={this.onShiftChange} 
                    /> 
                </div>
                <div className="col s2 ">   
                    <SelectSearch  
                        options={info.options} 
                        value={info.shift[4]} 
                        name="shift" 
                        days = {info.days}
                        week = "5"
                        placeholder="Select Shift"
                        onChange={this.onShiftChange} 
                    /> 
                </div>
            </div>
        )
    }
}