import React from 'react';
import SelectSearch from 'react-select-search';
import "react-datepicker/dist/react-datepicker.css";
export default class FixedPatternDays extends React.Component{
    constructor(props){
        super(props);
        this.onShiftChange = this.onShiftChange.bind(this);
    }

    onShiftChange(value,state,props){
        this.props.onShiftChange(value, state, props);
    }
    render(){
        let info = this.props;
        return(
            <div className="row ">  
                <div className="col s2 ">  
                    {info.day_name} 
                </div>
                <div className="col s2 ">   
                    <SelectSearch  
                        options={info.options} 
                        value={info.shift} 
                        name="shift" 
                        days = {info.days}
                        placeholder="Select Shift"
                        onChange={this.onShiftChange} 
                    /> 
                </div>
            </div>
        )
    }
}