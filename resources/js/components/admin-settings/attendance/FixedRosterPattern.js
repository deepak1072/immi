import React,{Fragment} from 'react';
import FixedPatternDays from './FixedPatternDays'; 

export default class FixedPatternRoster extends React.Component{
    constructor(props){
        super(props);
    }

    render(){
        let info = this.props ;
        return(
            <Fragment >
                <FixedPatternDays 
                    day_name= "All" 
                    days = "0"
                    options = {info.options} 
                    shift ={info.shift[0]} 
                    onShiftChange={info.onShiftChange} 
                />
                <FixedPatternDays 
                    day_name= "Mon" 
                    days = "1"
                    options = {info.options} 
                    shift ={info.shift[1]} 
                    onShiftChange={info.onShiftChange} 
                />
                <FixedPatternDays 
                    day_name= "Tue" 
                    days = "2"
                    options = {info.options} 
                    shift ={info.shift[2]} 
                    onShiftChange ={info.onShiftChange}
                />
                <FixedPatternDays 
                    day_name= "Wed" 
                    days = "3"
                    options = {info.options} 
                    shift ={info.shift[3]}
                    onShiftChange ={info.onShiftChange} 
                />
                <FixedPatternDays 
                    day_name= "Thu" 
                    days = "4"
                    options = {info.options} 
                    shift ={info.shift[4]}
                    onShiftChange ={info.onShiftChange} 
                />
                <FixedPatternDays 
                    day_name= "Fri"
                    days = "5" 
                    options = {info.options} 
                    shift ={info.shift[5]}
                    onShiftChange ={info.onShiftChange} 
                />
                <FixedPatternDays 
                    day_name= "Sat" 
                    days = "6"
                    options = {info.options} 
                    shift ={info.shift[6]} 
                    onShiftChange ={info.onShiftChange} 
                />
                <FixedPatternDays 
                    day_name= "Sun" 
                    days = "7"
                    options = {info.options} 
                    shift ={info.shift[7]} 
                    onShiftChange ={info.onShiftChange} 
                />
            </Fragment>
        )
    }
}