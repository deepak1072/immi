/**
 * Created by Sandeep Maurya on 20th April 2019.
 */

import React, { Fragment } from 'react';
import axios from 'axios';
import { toast } from 'react-toastify';
import {Link} from 'react-router-dom';
import {authHeader,logout,ITEM_PER_PAGE} from  "./../../../helpers/responseAuthozisation";
import OuApplicability from './../usable_components/OuApplicability';
import  Loader from './../usable_components/Loader';
import DatePicker from "react-datepicker"; 
let back_url = '/roster-list';
export default class RosterMapping extends React.Component{
    constructor(props){
        super(props); 
        this.state = {
            roster : '', 
            name : '', 
            options : [],
            start_date : new Date(), 
            end_date : new Date(), 
            flag : true ,
            hasError:false,
            action: 'Create',
            is_loading : true,
            applicable_ou:[],
        }
        this.handleAddUpdate = this.handleAddUpdate.bind(this);
    }


    componentDidMount(){
        let self = this;
        const { id } = this.props.match.params;
        if(id != undefined){     
            axios.get('/api/roster-details',{
                params:{
                    id : id
                },
                headers : authHeader()
            }).then(function(response){
                if(response.status == 200){
                    let data = response.data; 
                    self.setState({
                        is_loading : false,
                        name : data.p_name,
                        roster : id
                    });
                }else{
                    toast(({ closeToast }) => <div>Something went wrong , please try after some time  </div>);
                    history.push(back_url) ;   
                }
            }).catch(function(error){
                switch(error.response.status){
                    case 500 :
                        // logout();
                        toast(({ closeToast }) => <div> {error.response.data.message} </div>); 
                        history.push(back_url) ;
                        break ;
                    case 401 :
                        toast(({ closeToast }) => <div> {error.response.data.error} </div>);
                        logout();
                        break;
                    default :
                        toast(({ closeToast }) => <div>Something went wrong , please try after some  time  </div>); 
                        history.push(back_url) ;  
                        break;
                } 
            });
        }
    }

    static getDerivedStateFromError(error) {
        // Update state so the next render will show the fallback UI.
        return { hasError: true };
    }

    /**
     * form validation
     * @returns {boolean|*}
     */
    validate(){
    	this.state.flag = true;
         
        if(this.state.roster =='' || this.state.roster == 'undefined'){
            this.state.message = 'Please select roster type';
            this.state.flag = false;
        } 
        if(this.state.from_date > this.state.end_date){
            this.state.message = 'Start date can not be greater than end date ';
            this.state.flag = false;
        }
        return this.state.flag;
    }

      
    handleAddUpdate(event){
        event.preventDefault();
        const {history} = this.props;
        const roster_data = JSON.stringify(this.state);
        
        if(this.validate()){
            axios.post('/api/roster-assign', roster_data,{headers : authHeader()})
                .then(response => {
                    if(response.data.result){
                        toast(({ closeToast }) => <div>{response.data.message}</div>);
                        history.push(back_url) ;
                    } else{
                        toast(({ closeToast }) => <div>{response.data.message}</div>);
                    }
                })
                .catch(error => {  
                    switch(error.response.status){
                        case 500 :
                            // logout();
                            toast(({ closeToast }) => <div> {error.response.data.message} </div>); 
                            history.push(back_url) ;
                            break ;
                        case 401 :
                            toast(({ closeToast }) => <div> {error.response.data.error} </div>);
                            logout();
                            break;
                        default :
                            toast(({ closeToast }) => <div>Something went wrong , please try after some  time  </div>); 
                            history.push(back_url) ;  
                            break;
                    } 
                })
        }  else{
            toast(({ closeToast }) => <div>{this.state.message}</div>)
        }

    }
    
    handleOuChange(ou,event){
        this.setState({
            'applicable_ou' : ou
        });  
    }
    handleChange(field,date) {
        this.setState({
            [field]: date
        });
    }
   
    render(){ 
        if(this.state.is_loading){
            return <Loader />;
        }       

        if(this.state.hasError){
           return <h4 className="header2">Something went wrong</h4>
        }   else{
            return(
                <div className="dashboard">
                    <div className="card-panel">
                        <div className="row">
                            <form className="col s12" onSubmit={this.handleAddUpdate}>
                                <div id="leave_htmlForm">
                                    <div className="row">    
                                        <div className="col s12 l12 center-align">
                                            <h4 className="header2">  Roster Assign </h4>
                                        </div>
                                    </div>
                                    <div className="row"> 
                                        <div className="col s12 l6"> 
                                            <div className="row"> 
                                                <div className="col s12 m4"> 
                                                    <label  >{this.state.name}</label>
                                                </div>  
                                                <div className="input-field col s12 m4">
                                                    <DatePicker
                                                        selected={this.state.start_date}
                                                        onChange={this.handleChange.bind(this,'start_date')}
                                                        dateFormat ="d-M-yyyy"
                                                    />
                                                    <label htmlFor="start_date" className="active">Start Date 
                                                    </label>
                                                </div>
                                                <div className="input-field col s12 m4">
                                                    <DatePicker
                                                        selected={this.state.end_date}
                                                        onChange={this.handleChange.bind(this,'end_date')}
                                                        dateFormat ="d-M-yyyy"
                                                    />
                                                    <label htmlFor="end_date" className="active">End Date 
                                                    </label>
                                                </div>
                                            </div> 
                                        </div>
                                        
                                        <div className="col s12 l6">
                                            <OuApplicability 
                                                handleOuChange = {this.handleOuChange.bind(this)}
                                                ou = {this.state.applicable_ou}
                                                col=" m12 l12 "
                                            />
                                        </div> 
                                    </div>
                                            
                                </div>
                                <div className="row">                                            
                                    <div className="input-field col s12">
                                        <Link to={back_url}>
                                            <button className="btn default   waves-light"   name="action">
                                                <i className="mdi-navigation-arrow-back left "></i>
                                                Back
                                            </button>
                                        </Link>
                                        <button className="btn cyan waves-effect waves-light  " type="submit" name="action">Submit
                                            <i className="mdi-content-send right"></i>
                                        </button>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            );
        }
    }
}