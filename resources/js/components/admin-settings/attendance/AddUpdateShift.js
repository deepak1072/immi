/**
 * Created by Sandeep Maurya on 20th April 2019.
 */

import React from 'react';
import axios from 'axios';
import { toast } from 'react-toastify';
import {Link} from 'react-router-dom';
import {authHeader,logout,ITEM_PER_PAGE} from  "./../../../helpers/responseAuthozisation";
import  Loader from './../usable_components/Loader';
import DatePicker from "react-datepicker"; 
import "react-datepicker/dist/react-datepicker.css";
import TimeField from 'react-simple-timefield';
 

export default class AddUpdateShift extends React.Component{
    constructor(props){
        super(props);
        var d_date = '00:00'  ;
        this.state = {
            shift_name : '',
            code : '', 
            round_clock : 0,
            mark_out : '0',
            shift_start : '00:00',
            shift_end : d_date,
            mandate_start : d_date,
            mandate_end : d_date,
            full_day_mandate_hours : d_date,
            half_day_mandate_hours : d_date,
            swipe_before_shift_allowed : d_date,
            swipe_after_shift_allowed : d_date,
            break : 0,
            break_start : d_date,
            break_end : d_date,
            late_allowed_minute : d_date,
            late_allowed_frequency : 0,
            late_allowed_grace_period : d_date,
            early_going_minute : d_date,
            early_going_frequency : 0,
            early_going_grace_period : d_date,
            create_at : '',
            created_by : '',
            message : '',
            flag : true ,
            hasError:false,
            action: 'Create' ,
            is_loading:true
             
        }


        this.handleAddUpdateShift = this.handleAddUpdateShift.bind(this);
        this.handleFieldChange   = this.handleFieldChange.bind(this);
        this.handleRadioChange = this.handleRadioChange.bind(this);
        this.handleChange = this.handleChange.bind(this);

    }


    componentDidMount(){

        let self = this;
        const { id } = this.props.match.params;
        
        if(id != undefined){
            
            axios.get('/api/shift-details',{
                params:{
                    id : id
                },
                headers : authHeader()
            }).then(function(response){
                if(response.status == 200){
                    const data = response.data[0];

                    self.setState({
                        shift_name : data['shift_name'] ,
                        code : data['shift_code'],
                        round_clock : data['round_clock_shift'],
                        mark_out : data['mark_out_mandate'],
                        shift_start : data['shift_start'],
                        shift_end : data['shift_end'],
                        mandate_start : data['shift_start_mandatory'],
                        mandate_end : data['shift_end_mandatory'],
                        full_day_mandate_hours : data['mandate_time_full_day'],
                        half_day_mandate_hours : data['mandate_time_half_day'],
                        swipe_before_shift_allowed : data['swipe_allowed_before_shift'],
                        swipe_after_shift_allowed : data['swipe_allowed_after_shift'],
                        break : data['is_break_shift'],
                        break_start : data['break_start'],
                        break_end : data['break_end'],
                        late_allowed_minute : data['late_coming_allowed'],
                        late_allowed_frequency : data['late_coming_frequency'],
                        late_allowed_grace_period : data['late_coming_grace_period'],
                        early_going_minute : data['early_going_allowed'],
                        early_going_frequency : data['early_going_frequency'],
                        early_going_grace_period : data['early_going_grace_period'],
                        shift_id : data['shift_id'],
                        action : 'Update' ,
                        is_loading : false
                        
                    });
                    
                }else{

                    toast(({ closeToast }) => <div>Something went wrong , please try after some time  </div>);
                    history.push('/shift-setup') ;   
                }
                

            }).catch(function(error){
                switch(error.response.status){
                    case 500 :
                        // logout();
                        toast(({ closeToast }) => <div> {error.response.data.message} </div>); 
                        history.push('/shift-setup') ;
                        break ;
                    case 401 :
                        toast(({ closeToast }) => <div> {error.response.data.error} </div>);
                        logout();
                        break;
                    default :
                        toast(({ closeToast }) => <div>Something went wrong , please try after some  time  </div>); 
                        history.push('/shift-setup') ;  
                        break;
                } 
            });
        }else{
            this.setState({
                is_loading : false 
            });
        }
    }

    static getDerivedStateFromError(error) {
        // Update state so the next render will show the fallback UI.
        return { hasError: true };
    }

    /**
     * form validation
     * @returns {boolean|*}
     */
    validate(){
        if(this.state.shift_name =='' || this.state.shift_name == 'undefined'){
            this.state.message = 'Please shift name ';
            this.state.flag = false;
        }
        if(this.state.code =='' || this.state.code == 'undefined'){
            this.state.message = 'Please enter holiday date ';
            this.state.flag = false;
        }
        return this.state.flag;
    }



    handleAddUpdateShift(event){
        event.preventDefault();
        const {history} = this.props;
        const holidays = this.state;

        
        if(this.validate()){
            axios.post('/api/add-update-shift', holidays,{headers : authHeader()})
                .then(response => {
                    if(response.data.result){
                        toast(({ closeToast }) => <div>{response.data.message}</div>);
                        history.push('/shift-setup') ;
                    } else{
                        response.data.error.map((errors)=>{
                            toast(({ closeToast }) => <div>{errors} </div>);
                        });
                    }
                })
                .catch(error => {  
                    switch(error.response.status){
                        case 500 :
                            // logout();
                            toast(({ closeToast }) => <div> {error.response.data.message} </div>); 
                            history.push('/shift-setup') ;
                            break ;
                        case 401 :
                            toast(({ closeToast }) => <div> {error.response.data.error} </div>);
                            logout();
                            break;
                        default :
                            toast(({ closeToast }) => <div>Something went wrong , please try after some  time  </div>); 
                            history.push('/shift-setup') ;  
                            break;
                    } 
                })
        }  else{
            toast(({ closeToast }) => <div>{this.state.message}</div>)
        }

    }
    handleFieldChange(event){
        
        this.setState({
            [event.target.name] : event.target.value
        });
    }
    
    handleRadioChange(event){
        this.setState({
            [event.target.name] :  event.target.value
        }); 
    }
     
    handleChange(name,date) {  
         
        this.setState({
          [name]: date
        });
    }
 
    render(){
        if(this.state.is_loading){
            return <Loader />
        }
        if(this.state.hasError){
           return <h4 className="header2">Something went wrong</h4>
        }   else{
            const is_break_shift = (this.state.break == 1) ? true : false;
            return(
                <div className="dashboard">
                    <div className="card-panel">
                        <div className="row">
                            <form className="col s12" onSubmit={this.handleAddUpdateShift}>
                                <div id="leave_htmlForm">
                                    <div className="row">                                                
                                        
                                        <div className="col s12 l12 center-align">
                                            <h4 className="header2">{this.state.action} Shift </h4>
                                        </div>
                                    </div>
                                    <div className="row"> 
                                            
                                        <div className="input-field col s3">
                                            <input
                                                id="code"
                                                type="text"
                                                name="code"
                                                value={this.state.code}
                                                onChange={this.handleFieldChange}
                                            /> 
                                            <label htmlFor="code" className="active">Shift Code * 
                                            </label> 
                                                
                                        </div>
                                        <div className="input-field col s3">
                                            <input
                                                id="shift_name"
                                                type="text"
                                                name="shift_name"
                                                value={this.state.shift_name}
                                                onChange={this.handleFieldChange}
                                            /> 
                                            <label htmlFor="shift_name" className="active">Shift Shift_name * 
                                            </label>
                                        </div> 
                                            
                                    </div> 
                                    <div className="row"> 
                                        <div className="input-field col s3">
                                            <TimeField
                                                value={this.state.shift_start}
                                                onChange={this.handleChange.bind(this,'shift_start')}
                                                style={{
                                                    width: 200
                                                }}
                                            />
                                            <label htmlFor="shift_start" className="active">Shift Start Time * (24 Hrs Clock)
                                            </label>
                                        </div> 
                                        <div className="input-field col s3">
                                                

                                            <TimeField
                                                value={this.state.shift_end}
                                                onChange={this.handleChange.bind(this,'shift_end')}
                                                style={{
                                                    width: 200
                                                }}
                                            />
                                            <label htmlFor="shift_end" className="active">Shift End Time * (24 Hrs Clock)
                                            </label>
                                        </div>
                                        <div className="input-field col s3">
                                            <TimeField
                                                value={this.state.mandate_start}
                                                onChange={this.handleChange.bind(this,'mandate_start')}
                                                style={{
                                                    width: 200
                                                }}
                                            />
                                            <label htmlFor="mandate_start" className="active">Mandatory Start Time * (24 Hrs Clock)
                                            </label>
                                        </div> 
                                        <div className="input-field col s3">
                                            <TimeField
                                                value={this.state.mandate_end}
                                                onChange={this.handleChange.bind(this,'mandate_end')}
                                                style={{
                                                    width: 200
                                                }}
                                            />
                                            <label htmlFor="mandate_end" className="active">Mandatory End Time * (24 Hrs Clock)
                                            </label>
                                        </div> 
                                    </div> 
                                    <div className="row"> 
                                        <div className="input-field col s3">
                                            <TimeField
                                                value={this.state.full_day_mandate_hours}
                                                onChange={this.handleChange.bind(this,'full_day_mandate_hours')}
                                                style={{
                                                    width: 200
                                                }}
                                            />
                                            <label htmlFor="full_day_mandate_hours" className="active">Mandatory Hours For Full Day (HH:MM)
                                            </label>
                                        </div> 
                                        <div className="input-field col s3">
                                                

                                            <TimeField
                                                value={this.state.half_day_mandate_hours}
                                                onChange={this.handleChange.bind(this,'half_day_mandate_hours')}
                                                style={{
                                                    width: 200
                                                }}
                                            />
                                            <label htmlFor="half_day_mandate_hours" className="active">Mandatory Hours For Half Day (HH:MM)
                                            </label>
                                        </div>
                                        <div className="input-field col s3">
                                            <TimeField
                                                value={this.state.swipe_before_shift_allowed}
                                                onChange={this.handleChange.bind(this,'swipe_before_shift_allowed')}
                                                style={{
                                                    width: 200
                                                }}
                                            />
                                            <label htmlFor="swipe_before_shift_allowed" className="active">allowed to swipe before shift start (MM:SS)
                                            </label>
                                        </div> 
                                        <div className="input-field col s3">
                                            <TimeField
                                                value={this.state.swipe_after_shift_allowed}
                                                onChange={this.handleChange.bind(this,'swipe_after_shift_allowed')}
                                                style={{
                                                    width: 200
                                                }}
                                            />
                                            <label htmlFor="swipe_after_shift_allowed" className="active">allowed to swipe after shift end (MM:SS)
                                            </label>
                                        </div> 
                                    </div> 
                                    <fieldset>
                                        <legend> Other Shift Settings
                                        </legend> 
                                        <div className="row"> 
                                            <div className="col s12 m3"> 
                                                    <label   className="active">
                                                    <small> Round Clock Shift * </small>
                                                </label>
                                                <input
                                                    id="round_clock_yes"
                                                    type="radio"
                                                    className="with-gap"
                                                    name="round_clock"
                                                    value='1'
                                                    checked = {this.state.round_clock == '1'}
                                                    onChange={this.handleRadioChange }
                                                        
                                                /> 
                                                <label htmlFor="round_clock_yes">Yes</label>
                                                <input
                                                    id="round_clock_no"
                                                    type="radio" 
                                                    name="round_clock"
                                                    className="with-gap"
                                                    value= '0'
                                                    checked = {this.state.round_clock == '0'}
                                                    onChange={this.handleRadioChange }
                                                />  
                                                <label htmlFor="round_clock_no">No</label>
                                            </div>  
                                            
                                            <div className="col s12 m3"> 
                                                    <label   className="active">
                                                    <small> Mark Out Mandatory </small>
                                                </label>
                                                <input
                                                    id="yes"
                                                    type="radio"
                                                    className="with-gap"
                                                    name="mark_out"
                                                    value='1'
                                                    checked = {this.state.mark_out == '1'}
                                                    onChange={this.handleRadioChange }
                                                        
                                                /> 
                                                <label htmlFor="yes">Yes</label>
                                                <input
                                                    id="no"
                                                    type="radio" 
                                                    name="mark_out"
                                                    className="with-gap"
                                                    value= '0'
                                                    checked = {this.state.mark_out == '0'}
                                                    onChange={this.handleRadioChange }
                                                />  
                                                <label htmlFor="no">No</label>
                                            </div>

                                            <div className="col s12 m3"> 
                                                    <label   className="active">
                                                    <small> Fixed Break Time  </small>
                                                </label>
                                                <input
                                                    id="break_yes"
                                                    type="radio"
                                                    className="with-gap"
                                                    name="break"
                                                    value='1'
                                                    checked = {this.state.break == '1'}
                                                    onChange={this.handleRadioChange }
                                                        
                                                /> 
                                                <label htmlFor="break_yes">Yes</label>
                                                <input
                                                    id="break_no"
                                                    type="radio" 
                                                    name="break"
                                                    className="with-gap"
                                                    value= '0'
                                                    checked = {this.state.break == '0'}
                                                    onChange={this.handleRadioChange }
                                                />  
                                                <label htmlFor="break_no">No</label>
                                            </div>
                                        </div>
                                        {
                                            (is_break_shift) ? (
                                                <div className="row"> 
                                                    <div className="input-field col s3">
                                                        <TimeField
                                                            value={this.state.break_start}
                                                            onChange={this.handleChange.bind(this,'break_start')}
                                                            style={{
                                                                width: 200
                                                            }}
                                                        />
                                                        <label htmlFor="break_start" className="active">Break Start Time (24 Hrs Clock)
                                                        </label>
                                                    </div> 
                                                    <div className="input-field col s3">
                                                        <TimeField
                                                            value={this.state.break_end}
                                                            onChange={this.handleChange.bind(this,'break_end')}
                                                            style={{
                                                                width: 200
                                                            }}
                                                        />
                                                        <label htmlFor="break_end" className="active">Break End Time (24 Hrs Clock)
                                                        </label>
                                                    </div>
                                                </div>

                                                )  : ('')
                                        }
                                    </fieldset>
                                        
                                    <fieldset>
                                        <legend> Late Coming
                                        </legend> 
                                        <div className="row"> 
                                            <div className="input-field col s3">
                                                <TimeField
                                                    value={this.state.late_allowed_minute}
                                                    onChange={this.handleChange.bind(this,'late_allowed_minute')}
                                                    style={{
                                                        width: 200
                                                    }}
                                                />
                                                <label htmlFor="late_allowed_minute" className="active">Allowed to come upto (MM:SS)
                                                </label>
                                            </div> 
                                            <div className="input-field col s3">
                                                    
                                                <input
                                                    id="late_allowed_frequency"
                                                    type="text"
                                                    name="late_allowed_frequency"
                                                    value={this.state.late_allowed_frequency}
                                                    onChange={this.handleFieldChange}
                                                    className="active"
                                                /> 
                                                <label htmlFor="late_allowed_frequency" className="active">Allowed Frequency 
                                                </label>
                                            </div>
                                            <div className="input-field col s3">

                                                <TimeField
                                                    value={this.state.late_allowed_grace_period}
                                                    onChange={this.handleChange.bind(this,'late_allowed_grace_period')}
                                                    style={{
                                                        width: 200
                                                    }}
                                                />
                                                <label htmlFor="late_allowed_grace_period" className="active">Grace Period (MM:SS)
                                                </label>
                                            </div>
                                        </div> 

                                    </fieldset> 
                                    
                                    <fieldset>
                                        <legend> Early Going
                                        </legend> 
                                        <div className="row"> 
                                            <div className="input-field col s3">

                                                <TimeField
                                                    value={this.state.early_going_minute}
                                                    onChange={this.handleChange.bind(this,'early_going_minute')}
                                                    style={{
                                                        width: 200
                                                    }}
                                                />
                                                <label htmlFor="early_going_minute" className="active">Allowed to leave upto  (MM:SS)
                                                </label>
                                            </div> 
                                            <div className="input-field col s3">
                                                    
                                                <input
                                                    id="early_going_frequency"
                                                    type="text"
                                                    name="early_going_frequency"
                                                    value={this.state.early_going_frequency}
                                                    onChange={this.handleFieldChange}
                                                    className="active"
                                                /> 
                                                <label htmlFor="early_going_frequency" className="active">Allowed Frequency 
                                                </label>
                                            </div>
                                            <div className="input-field col s3">

                                                <TimeField
                                                    value={this.state.early_going_grace_period}
                                                    onChange={this.handleChange.bind(this,'early_going_grace_period')}
                                                    style={{
                                                        width: 200
                                                    }}
                                                />
                                                <label htmlFor="early_going_grace_period" className="active">Grace Period (MM:SS)
                                                </label>
                                            </div>
                                        </div> 

                                    </fieldset> 
                                        
                                </div>
                                <div className="row">                                            
                                    <div className="input-field col s12">
                                        <Link to="/shift-setup">
                                            <button className="btn default   waves-light"   shift_name="action">
                                                <i className="mdi-navigation-arrow-back left "></i>
                                                cancel
                                            </button>
                                        </Link>
                                        <button className="btn cyan waves-effect waves-light  " type="submit" shift_name="action">{(this.state.action)}
                                            <i className="mdi-content-send right"></i>
                                        </button>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            );
        }
    }
}