import React,{Fragment} from 'react';
import VariablePatternDays from './VariablePatternDays';

export default class VariablePatternRoster extends React.Component{
    constructor(props){
        super(props);
    }

    render(){
        let info = this.props ;
        return(
            <Fragment >
                <div className="row ">  
                    <div className="col s1 ">  
                        Weeks
                    </div>
                    <div className="col s2 ">  
                         1st Week
                    </div>
                    <div className="col s2 ">  
                         2nd Week
                    </div>
                    <div className="col s2 ">  
                         3rd Week
                    </div>
                    <div className="col s2 ">  
                         4rth Week
                    </div>
                    <div className="col s2 ">  
                        5th Week
                    </div>
                </div>
                <hr />
                <VariablePatternDays 
                    day_name= "All" 
                    days = "0"
                    options = {info.options} 
                    shift ={info.shift[0]} 
                    onShiftChange={info.onShiftChange} 
                />
                <VariablePatternDays 
                    day_name= "Mon" 
                    days = "1"
                    options = {info.options} 
                    shift ={info.shift[1]} 
                    onShiftChange={info.onShiftChange} 
                />
                <VariablePatternDays 
                    day_name= "Tue" 
                    days = "2"
                    options = {info.options} 
                    shift ={info.shift[2]} 
                    onShiftChange ={info.onShiftChange}
                />
                <VariablePatternDays 
                    day_name= "Wed" 
                    days = "3"
                    options = {info.options} 
                    shift ={info.shift[3]}
                    onShiftChange ={info.onShiftChange} 
                />
                <VariablePatternDays 
                    day_name= "Thu" 
                    days = "4"
                    options = {info.options} 
                    shift ={info.shift[4]}
                    onShiftChange ={info.onShiftChange} 
                />
                <VariablePatternDays 
                    day_name= "Fri"
                    days = "5" 
                    options = {info.options} 
                    shift ={info.shift[5]}
                    onShiftChange ={info.onShiftChange} 
                />
                <VariablePatternDays 
                    day_name= "Sat" 
                    days = "6"
                    options = {info.options} 
                    shift ={info.shift[6]} 
                    onShiftChange ={info.onShiftChange} 
                />
                <VariablePatternDays 
                    day_name= "Sun" 
                    days = "7"
                    options = {info.options} 
                    shift ={info.shift[7]} 
                    onShiftChange ={info.onShiftChange} 
                />
            </Fragment>
        )
    }
}