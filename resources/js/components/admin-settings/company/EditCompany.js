/**
 * Created by Sandeep Maurya on 09/12/2018.
 */

import React from 'react';
import axios from 'axios';
import { toast } from 'react-toastify';
import {Link} from 'react-router-dom';


export default class EditComapny extends React.Component{
    constructor(props){
        super(props);

        this.state = {
            company_name : '',
            company_code : '',
            company_ip_address : '',
            company_url:'',
            company_id : '',
            message : '',
            flag : true ,
            hasError:false
        }

        this.handleUpdateCompany = this.handleUpdateCompany.bind(this);
        this.handleFieldChange   = this.handleFieldChange.bind(this);
    }


    componentDidMount(){
        const { id } = this.props.match.params;
        let self = this;
        axios.get('/api/company-details',{
            params:{
                id : id
            }
        }).then(function(response){
            if(response.status == 200){
                const data = response.data[0];
                self.setState({
                    company_name : data['company_name'],
                    company_ip_address : data['company_ip_address'],
                    company_url : data['company_url'],
                    company_code:data['company_code'],
                    company_id: data['company_id'] 
                });
            }else{

                toast(({ closeToast }) => <div>Something went wrong , please try after some time  </div>);
                history.push('/menu-list') ;   
            }
            

        })

    }

    static getDerivedStateFromError(error) {
        // Update state so the next render will show the fallback UI.
        return { hasError: true };
    }

    /**
     * form validation
     * @returns {boolean|*}
     */
    validate(){
    	 this.state.flag = true;
        if(this.state.company_url =='' || this.state.company_url == 'undefined' ) {
            this.state.message = 'Please enter company url ';
            this.state.flag = false;
        }
        if(this.state.company_ip_address =='' || this.state.company_ip_address == 'undefined'){
            this.state.message = 'Please enter company IP address ';
            this.state.flag = false;
        }
        if(this.state.company_code =='' || this.state.company_code == 'undefined'){
            this.state.message = 'Please enter company code ';
            this.state.flag = false;
        }
        if(this.state.company_name =='' || this.state.company_name == 'undefined'){
            this.state.message = 'Please enter company name ';
            this.state.flag = false;
        }
        return this.state.flag;
    }



    handleUpdateCompany(event){
        event.preventDefault();
        const {history} = this.props;
        const menu = this.state;

        console.log(menu);

        if(this.validate()){
            axios.post('/api/update-company', menu)
                .then(response => {
                    if(response.data.result){
                        toast(({ closeToast }) => <div>{response.data.message}</div>);
                        history.push('/company-list') ;
                    } else{
                        response.data.error.map((errors)=>{
                            toast(({ closeToast }) => <div>{errors} </div>);
                        });
                    }
                })
                .catch(error => {
                    toast(({ closeToast }) => <div>Something went wrong ! please try after some  time</div>);
                    history.push('/company-list') ;
                })
        }  else{
            toast(({ closeToast }) => <div>{this.state.message}</div>)
        }

    }
    handleFieldChange(event){
        this.setState({
            [event.target.name] : event.target.value
        });
    }

    render(){

        if(this.state.hasError){
           return <h4 className="header2">Something went wrong</h4>
        }   else{
            return(
                <div className="dashboard">
                    <div className="card-panel">
                        <div className="row">
                            <form className="col s12" onSubmit={this.handleUpdateCompany}>
                                <div id="leave_htmlForm">
                                    <div className="row">                                                
                                        
                                        <div className="col s12 l12 center-align">
                                            <h4 className="header2">Update Company </h4>
                                        </div>
                                    </div>
                                    <div className="row">                                                 
                                        <div className="input-field col s2">
                                            <input
                                                id="company_name"
                                                type="text"
                                                name="company_name"
                                                value={this.state.company_name}
                                                onChange={this.handleFieldChange}
                                            /> 
                                            <label htmlFor="company_name" className="active">Comany Name *
                                            </label>
                                        </div>
                                        <div className="input-field col s2">
                                            <input
                                                id="company_url"
                                                type="text"
                                                name ="company_url"
                                                value={this.state.company_url}
                                                onChange={this.handleFieldChange}

                                            />
                                            <label htmlFor="company_url" className="active">Company Url
                                            </label>
                                        </div>
                                        <div className="input-field col s2">
                                            <input
                                                id="company_ip_address"
                                                type="text"
                                                name="company_ip_address"
                                                value={this.state.company_ip_address}
                                                onChange={this.handleFieldChange}
                                            />
                                            <label htmlFor="company_ip_address" className="active">Company IP Address
                                            </label>
                                        </div>
                                        <div className="input-field col s2">
                                            <input
                                                id="company_code"
                                                type="text"
                                                name="company_code"
                                                value={this.state.company_code}
                                                onChange={this.handleFieldChange}
                                            />
                                            <label htmlFor="company_code" className="active">Company Code
                                            </label>
                                        </div>
                                    </div>
                                </div>
                                <div className="row">                                            
                                    <div className="input-field col s12">
                                        <Link to="/company-list">
                                            <button className="btn default   waves-light"   name="action">
                                                <i className="mdi-navigation-arrow-back left "></i>
                                                cancel
                                            </button>
                                        </Link>
                                        <button className="btn cyan waves-effect waves-light  " type="submit" name="action">Update
                                            <i className="mdi-content-send right"></i>
                                        </button>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            );
        }
    }
}