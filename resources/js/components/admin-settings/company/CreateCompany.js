/**
 * Created by Sandeep Maurya on 09/12/2018.
 */

import React from 'react';
import axios from 'axios';
import { toast } from 'react-toastify';
import {Link} from 'react-router-dom';


export default class CreateComapny extends React.Component{
    constructor(props){
        super(props);

        this.state = {
            company_name : '',
            company_code : '',
            company_ip_address : '',
            company_url:'',
            message : '',
            flag : true ,
            hasError:false
        }

        this.handleCreateNewCompany = this.handleCreateNewCompany.bind(this);
        this.handleFieldChange   = this.handleFieldChange.bind(this);
    }

    static getDerivedStateFromError(error) {
        // Update state so the next render will show the fallback UI.
        return { hasError: true };
    }

    /**
     * form validation
     * @returns {boolean|*}
     */
    validate(){

        if(this.state.company_url =='' || this.state.company_url == 'undefined' ) {
            this.state.message = 'Please enter company url ';
            this.state.flag = false;
        }
        if(this.state.company_ip_address =='' || this.state.company_ip_address == 'undefined'){
            this.state.message = 'Please enter company IP address ';
            this.state.flag = false;
        }
        if(this.state.company_code =='' || this.state.company_code == 'undefined'){
            this.state.message = 'Please enter company code ';
            this.state.flag = false;
        }
        if(this.state.company_name =='' || this.state.company_name == 'undefined'){
            this.state.message = 'Please enter company name ';
            this.state.flag = false;
        }
        return this.state.flag;
    }



    handleCreateNewCompany(event){
        event.preventDefault();
        const {history} = this.props;
        const menu = this.state;

        if(this.validate()){
            axios.post('/api/create-company', menu)
                .then(response => {
                    if(response.data.result){
                        toast(({ closeToast }) => <div>{response.data.message}</div>);
                        history.push('/company-list') ;
                    } else{
                        response.data.error.map((errors)=>{
                            toast(({ closeToast }) => <div>{errors} </div>);
                        });
                    }
                })
                .catch(error => {
                    toast(({ closeToast }) => <div>Something went wrong ! please try after some  time</div>);
                    history.push('/company-list') ;
                })
        }  else{
            toast(({ closeToast }) => <div>{this.state.message}</div>)
        }

    }
    handleFieldChange(event){
        this.setState({
            [event.target.name] : event.target.value
        });
    }

    render(){

        if(this.state.hasError){
           return <h4 className="header2">Something went wrong</h4>
        }   else{
            return(
                <div className="dashboard">
                    <div className="card-panel">
                        <div className="row">
                            <form className="col s12" onSubmit={this.handleCreateNewCompany}>
                                <div id="leave_htmlForm">
                                    <div className="row">                                                
                                        <div className="col s6 l3">
                                            <Link to="/company-list">
                                                <button className="btn cyan waves-effect waves-light">
                                                    Back
                                                    <i className="mdi-content-reply left"></i>
                                                </button>
                                            </Link>
                                        </div>
                                        <div className="col s6 l4 center-align">
                                            <h4 className="header2">Create Comany </h4>
                                        </div>
                                    </div>
                                    <div className="row">                                                 
                                        <div className="input-field col s2">
                                            <input
                                                id="company_name"
                                                type="text"
                                                name="company_name"
                                                value={this.state.company_name}
                                                onChange={this.handleFieldChange}
                                            /> 
                                            <label htmlFor="company_name">Comany Name *
                                            </label>
                                        </div>
                                        <div className="input-field col s2">
                                            <input
                                                id="company_url"
                                                type="text"
                                                name ="company_url"
                                                value={this.state.company_url}
                                                onChange={this.handleFieldChange}

                                            />
                                            <label htmlFor="company_url">Company Url
                                            </label>
                                        </div>
                                        <div className="input-field col s2">
                                            <input
                                                id="company_ip_address"
                                                type="text"
                                                name="company_ip_address"
                                                value={this.state.company_ip_address}
                                                onChange={this.handleFieldChange}
                                            />
                                            <label htmlFor="company_ip_address">Company IP Address
                                            </label>
                                        </div>
                                        <div className="input-field col s2">
                                            <input
                                                id="company_code"
                                                type="text"
                                                name="company_code"
                                                value={this.state.company_code}
                                                onChange={this.handleFieldChange}
                                            />
                                            <label htmlFor="company_code">Company Code
                                            </label>
                                        </div>
                                    </div>
                                </div>
                                <div className="row">                                            
                                    <div className="input-field col s12">
                                        <button className="btn cyan waves-effect waves-light right" type="submit" name="action">Submit
                                            <i className="mdi-content-send right"></i>
                                        </button>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            );
        }
    }
}