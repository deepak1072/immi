import React,{Fragment} from 'react';  

const ActionButton = (props)=>{
    if(props.actions.length < 1){
        return '' ;
    }
    return props.actions.map((action,index)=>{
        let cl = action.type+" btn waves-light ";
        return (
            <Fragment  key ={index+'_'+action.status}>
                <button type="button" className={cl}    onClick={(e)=>{props.handleSubmit(e,action.status)}} >
                    {action.name}
                </button>   
            </Fragment>
        )
    })
}
export default ActionButton ;