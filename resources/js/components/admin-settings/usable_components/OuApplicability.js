/**
 * Created by Sandeep Maurya on 22/02/2019.
 * @ 11:55 PM
 */

import React,{Fragment} from 'react';
import { Link } from 'react-router-dom';
import {authHeader,logout} from  "./../../../helpers/responseAuthozisation";
import { toast } from 'react-toastify';
import axios from 'axios';
import ChieldOu  from './ChieldOu';
 

 
export default class OuApplicability extends React.Component{
	constructor(props){
		super(props);
		const {history} = this.props; 
		this.state = {
			'ou' : this.props.ou
		}; 
		this.handleParentChange = this.handleParentChange.bind(this); 
		this.handleChildChange = this.handleChildChange.bind(this);
		
	}

	handleParentChange(flag,event){ 
	 	const new_flag = (flag == '0') ? '1' : '0';
	 	const new_ou =  this.state.ou.map((ou,index)=>{ 		
	 		if(ou.name == event.target.name){
	 			ou.flag = new_flag; 
	 			ou.children = ou.children.map((child,i)=>{
	 				child.flag = new_flag;
	 				return child;
	 			});
	 		} 
	 		return ou;
	 	});
	 	this.setState({
 			'ou' : new_ou 			
 		});
 	 
 		this.handleOuChange();
	}
	handleOuChange(){
		this.props.handleOuChange(this.state.ou);
	}
	handleChildChange(flag,name,event){
		const new_flag = (flag == '0') ? '1' : '0';
	 	const new_ou =  this.state.ou.map((ou,index)=>{ 		
 			ou.children = ou.children.map((child,i)=>{
 				if(child.name == name){
 					child.flag = new_flag;
 				} 
 				return child;
 			}); 
	 		return ou;
	 	});
	 	this.setState({
 			'ou' : new_ou 			
 		});
 		this.handleOuChange();
	}

	/**
	 * set checked / unchecked ou details
     */
	setOuDetailsState(ou){

	}
	componentDidMount(){
		const self = this;
		axios.get('/api/ou-details',{
			params:{
			},
			headers : authHeader()
		}).then(response =>{
			if(response.status == 200){
				this.setState({
					'ou' : response.data
				});
			}else{
				toast(({ closeToast }) => <div>Something went wrong , please try after some time  </div>);
				history.push('/') ;
			}
		}) .catch(error =>{
			switch(error.response.status){
				case 500 :
					// logout();
					toast(({ closeToast }) => <div> {error.response.data.message} </div>);
					history.push('/') ;
					break ;
				case 401 :
					toast(({ closeToast }) => <div> {error.response.data.error} </div>);
					logout();
					break
				default :
					toast(({ closeToast }) => <div> could not fetch ou details </div>);
					history.push('/') ;
					break;
			}
		}) ;
	}

	handleFinalOU(){
		const props_ou 	=  	this.props.ou ;
		const  ou_data	=  	this.state.ou;
		const new_ou = ou_data.map((ou,index)=>{
			    props_ou.map((pou,i)=>{
					if(ou.name==pou.name){
						ou.flag = pou.flag
						ou.children = ou.children.map((cou,index1)=>{
							pou.children.map((cpou,i1)=>{
								if(cpou.name == cou.name){
									cou.flag = cpou.flag
								}
							})
							return cou
						})
					}
				})
			    return ou ;
			})
		return new_ou;
	}
	
	handleOU(){
		const ou_data 	= 	this.handleFinalOU();
		const ou =[] ;
		ou_data.map((cou,index)=>{
			ou.push(
                <li className="ou-parent-li" key = {index} >
                  	<div>
                  		<input 
	                  		type="checkbox" 
	                  		id= {cou.name} 
	                  		checked = {cou.flag == '1'}
	                  		value = {cou.flag}
	                  		name = {cou.name}
	                  		onChange = {this.handleParentChange.bind(this,cou.flag)}
	                  	/>
	                  	<label htmlFor={cou.name}>  {cou.name} </label>
	                  	<ul className = "ou-child-ul" key = {index}>
              				<ChieldOu 
                  				key = {index}
                  				ou = { cou.children }
                  				handleChildChange  = {this.handleChildChange.bind(this) }
                  			/>
              			</ul>
                  	</div>
                </li>       
			)
		})
		return ou;
	}


	render(){ 
		const ou = this.handleOU();
		let class_name = (this.props.col) ? 'col s12 '+this.props.col : 'col s12 m6 l4';
		return (
			<div className={class_name}>
	             <label   className="active">Assign Access 
	            </label>
	            <div className="assign_menu_box">
	            	<ul id="task-card" className=" ou-parent-ul ">
	            	{  ou } 
	            	</ul>
	            </div> 
	        </div> 
		)
	}
}