/**
 * Created by Sandeep Maurya on 22/02/2019.
 * @ 11:55 PM
 */

import React ,{ Fragment }from 'react'; 
 


export default class ChieldOu extends React.Component{
	constructor(props){
		super(props); 
	}
	 
	 
 	handleChildOu(){
 		const cou = [];
 		this.props.ou.map((ou,index)=>{
 		 
 			const keyy = ou.name.concat(ou.id);
			cou.push(
				<li key = {ou.id} className="ou-child-li"> 
                	<div>
                		<input 
	                		type="checkbox" 
	                		id= {ou.name}
	                		name = {ou.name}
	                		value = {ou.flag}
	                		checked = {ou.flag == '1'}
	                		onChange = {this.props.handleChildChange.bind(this,ou.flag,ou.name)}
	                	/>
	                  	<label htmlFor={ou.name}>{ou.name}
	                  	</label>
                	</div>
                     
				</li>
			)
		})
		return cou;
 	}

	render(){  
		const childou = this.handleChildOu();
		return (
			<Fragment >
	            {
            		childou
            	} 
            </Fragment>	  
	        
		)
	}
}