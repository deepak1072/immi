import React,{Fragment} from 'react';
import ReactDOM from 'react-dom';
const  Info = (props)=>{
    return(
        <div className="col s12 m4 l4"> 
            <div id="card-alert"  className="card cyan lighten-5">
                <div className="card-content cyan-text darken-1">
                    <span className="card-title cyan-text darken-1">Information</span>
                    <p>{props.message}</p>
                </div>
                <button type="button" className="close cyan-text" data-dismiss="alert" aria-label="Close">
                <span aria-hidden="true">×</span>
                </button>
            </div>
        </div>
    )
}

const Success = (props) =>{
    return(
        <div className="col s12 m4 l4"> 
            <div id="card-alert"  className="card green lighten-5">
                <div className="card-content green-text">
                    <span className="card-title green-text darken-1">
                        Success
                    </span>
                    <p>{props.message}</p>                
                </div>
                <button type="button" className="close green-text" data-dismiss="alert" aria-label="Close">
                <span aria-hidden="true">×</span>
                </button>
            </div>
        </div>
    )
}

const Warning = (props) =>{
    return(
        <div className="col s12 m4 l4"> 
            <div id="card-alert" className="card orange  lighten-5">
                <div className="card-content orange-text">
                    <span className="card-title orange-text darken-1">
                        Warning
                    </span>
                    <p>{props.message}</p>
                </div>
                <button type="button" className="close orange-text" data-dismiss="alert" aria-label="Close">
                <span aria-hidden="true">×</span>
                </button>
            </div>
            
        </div>
    )
}

const Error = (props) =>{
    return(
        <div className="col s12 m4 l4"> 
            <div  id="card-alert" className="card red  lighten-5">
                <div className="card-content red-text">
                    <span className="card-title red-text darken-1">
                        Error
                    </span>
                    <p>{props.message}</p>
                </div>
                <button type="button" className="close red-text" data-dismiss="alert" aria-label="Close">
                <span aria-hidden="true">×</span>
                </button>
            </div>
        </div>
    )
}

export function Sandesh(message='Welcome',type = 'info',time = 6000){
    if(document.querySelector('.card_alert')){
        hideAlert();
    }
    setTimeout(function(){
        hideAlert();
    },time);
    let badge = '';
    switch(type){
        case 'success' :
            badge =  <Success message={message} />
        break;
        case 'error' :
            badge = <Error  message={message} />
        break;
        case 'warn' :
            badge =  <Warning  message={message} />
        break;
        case 'info' :
            badge =  <Info  message={message} />
        break;
        default : 
            badge = <Info  message={message} />
        break;
    }
    var div = document.createElement('div');
    var att = document.createAttribute("class");
    att.value = "card_alert";
    div.setAttributeNode(att); 
    document.getElementById("card_alert").appendChild(div); 
    ReactDOM.render(badge, div );
}

function hideAlert(){
    let alert = document.querySelector('.card_alert');
    if(alert){
        document.querySelector('.card_alert').remove();
    }
}
export default Sandesh ;
