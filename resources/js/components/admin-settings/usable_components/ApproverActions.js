import React,{Fragment} from 'react';  

import ShortName from './ShortName';
const ApproverActions = (props)=>{
    return  props.apprs.map((appr,index)=>{ 
        return(
            <Fragment key={index+'_'+appr.id}>
                <tr>
                    <td>
                        <div className="chip chips-no">
                            <ShortName name = {appr.short_name} />
                            &nbsp;&nbsp;{appr.name}
                        </div> 
                    </td>
                    <td>
                        {appr.status}
                        {
                            (appr.remarks) ? (
                            <span>, &nbsp;&nbsp;({appr.remarks})</span>
                            ):('')
                        }
                    </td>
                </tr>
            </Fragment>
        )
    }); 
}
export default ApproverActions ;