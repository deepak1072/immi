/**
 * Created by Sandeep Maurya on 29/12/2019.
 * @ 11:55 PM
 */

import React ,{ Fragment }from 'react'; 
 


export default class Loader extends React.Component{
	constructor(props){
		super(props); 
	} 
	render(){   
		return (
			<Fragment >
                <div className="dashboard ">
                    <div className="card-panel">
                        <div className="row loader">
                            <div className="lds-ripple"><div></div><div></div></div>
                            <div>Loading...</div>
                                
                        </div>
                    </div>
                </div>
            </Fragment>	 
		)
	}
}