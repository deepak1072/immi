
/**
 * Created by Sandeep Maurya on 29/12/2019.
 * @ 11:55 PM
 */

import React ,{ Fragment } from 'react'; 
const  LoadingButton = (props)=>{
    return(
        <button className= "btn waves-effect cyan waves-light col s12"  type="button">
            <i className ="fa fa-spin fa-spinner"></i>
        </button>
    )
}
export default LoadingButton ;
