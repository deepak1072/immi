import React,{Fragment} from 'react';  
import ShortName from './ShortName';
const  Team = (props)=>{ 
    return (
        <div className="row team">
            <div className="col s4 m2 short ">
                <ShortName name = {props.info.short_name}/>
            </div> 
            <div className="col s8 m10 info ">
                <span className="info-title">{props.info.name}</span>
                <br />
                <label className="info-meta"> <b>from date : </b> {props.info.from_date}</label>
                <label className="info-meta"><b> &nbsp;&nbsp;to date : </b>{props.info.to_date}</label>
            </div> 
        </div>
    )
}
export default Team ;