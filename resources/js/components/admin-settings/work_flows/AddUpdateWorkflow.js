/**
 * Created by Sandeep Maurya on 20th April 2019.
 */

import React, { Fragment } from 'react';
import axios from 'axios';
import { toast } from 'react-toastify';
import {Link} from 'react-router-dom';
import SelectSearch from 'react-select-search';
import {authHeader,logout,ITEM_PER_PAGE} from  "./../../../helpers/responseAuthozisation";
import OuApplicability from './../usable_components/OuApplicability';
import Levels from './Levels';
import  Loader from './../usable_components/Loader';

export default class AddUpdateWorkflow extends React.Component{
    constructor(props){
        super(props); 
        this.state = {
            wf_name : '',  
            wf_type : '1', 
            wf_code : '',
            wf_module :'',
            wf_events :100,
            flag : true ,
            hasError:false,
            action: 'Create',
            applicable_ou:[],
            module_options: [{name : 'Select Module',value : ''}],
            event_options : [{name:'Select Event',value:0}],
            approver_options :[{name:'Select Approver',value:''}] ,
            wf_levels : [{
                _id:0,
                level : 1,
                wf_days : 0,
                approver : '',
                requ_noti : 0,
                approver_noti : 0,
                next_level_noti :0,
                f_cc :[],
                f_bcc : [],
                dynamic_cc:'',
                dynamic_bcc:''
            }],
        }
        this.handleAddUpdate = this.handleAddUpdate.bind(this);
        this.handleFieldChange   = this.handleFieldChange.bind(this);  
        this.handleFieldInputChange   = this.handleFieldInputChange.bind(this); 
        this.handleChange = this.handleChange.bind(this);
        this.onApproverChange = this.onApproverChange.bind(this);
         
        this.onFccChange = this.onFccChange.bind(this);
        this.onFbccChange = this.onFbccChange.bind(this);
       
    }


    componentDidMount(){
        this.loadParentMenu();
        this.loadApprover();
        
        let self = this;
        const { id } = this.props.match.params;
        if(id != undefined){
            
            axios.get('/api/workflow-details',{
                params:{
                    id : id
                },
                headers : authHeader()
            }).then(function(response){ 
                if(response.status == 200){
                    const data = response.data;
                    const applicable_ou =  JSON.parse(data.applicable_ou); 
                     
                        if(data['wf_type'] ==1 ){
                            let levels = data['wf_levels'].map((level)=>{
                                 
                                level.f_cc = JSON.parse(level.f_cc);
                                level.f_bcc = JSON.parse(level.f_bcc); 
                                return level;
                            });
                            self.setState({ 
                                wf_id : data['wf_id'], 
                                wf_name: data['wf_name'] , 
                                wf_code: data['wf_code'] ,  
                                wf_events: data['wf_events'] , 
                                wf_module : data['wf_module'], 
                                wf_type: data['wf_type'] ,    
                                applicable_ou: applicable_ou , 
                                wf_levels:levels,
                                action : 'Update'  
                            }); 
                        }  else{
                            self.setState({ 
                                wf_id : data['wf_id'], 
                                wf_name: data['wf_name'] , 
                                wf_code: data['wf_code'] ,  
                                wf_events: data['wf_events'] , 
                                wf_module : data['wf_module'], 
                                wf_type: data['wf_type'] ,    
                                applicable_ou: applicable_ou , 
                                action : 'Update'  
                            }); 
                        }
                        
                        self.loadEvents(data['wf_module']); 
                }else{
                    toast(({ closeToast }) => <div>Something went wrong , please try after some time  </div>);
                    history.push('/work-flows') ;   
                } 
            }).catch(function(error){
                switch(error.response.status){
                    case 500 :
                        // logout();
                        toast(({ closeToast }) => <div> {error.response.data.message} </div>); 
                        history.push('/work-flows') ;
                        break ;
                    case 401 :
                        toast(({ closeToast }) => <div> {error.response.data.error} </div>);
                        logout();
                        break;
                    default :
                        toast(({ closeToast }) => <div>Something went wrong , please try after some  time  </div>); 
                        history.push('/work-flows') ;  
                        break;
                } 
            });
        }else{
            this.loadEvents();
        }
         
    }

    static getDerivedStateFromError(error) {
        // Update state so the next render will show the fallback UI.
        return { hasError: true };
    }

    /**
     * form validation
     * @returns {boolean|*}
     */
    validate(){
    	this.state.flag = true;
        if(this.state.wf_events =='' || this.state.wf_events == 'undefined'){
            this.state.message = 'Please select events';
            this.state.flag = false;
        }
        if(this.state.wf_module =='' || this.state.wf_module == 'undefined'){
            this.state.message = 'Please select module  ';
            this.state.flag = false;
        }
        if(this.state.wf_type == 1){
            this.state.wf_levels.map((level,index)=>{
                if(level.approver == '' || level.approver == undefined){
                    this.state.message = 'Please select approver in levels  ';
                    this.state.flag = false;
                }
            }); 
        }
        if(this.state.wf_type =='' || this.state.wf_type == 'undefined'){
            this.state.message = 'Please select workflow type';
            this.state.flag = false;
        }
        if(this.state.wf_code =='' || this.state.wf_code == 'undefined'){
            this.state.message = 'Please enter workflow code  ';
            this.state.flag = false;
        } 
        if(this.state.wf_name =='' || this.state.wf_name == 'undefined'){
            this.state.message = 'Please enter workflow name';
            this.state.flag = false;
        } 
        return this.state.flag;
    }

    loadParentMenu(){
        let self = this;
        axios.get('/api/modules',{
            params:{ 
            },
            headers : authHeader()
        }).then(function(response){
            if(response.status == 200){
                  
                self.setState({ 
                    module_options: response.data    
                });
                
            }  
        }).catch(function(error){
            switch(error.response.status){
                case 500 :
                    // logout();
                    toast(({ closeToast }) => <div> {error.response.data.message} </div>); 
                    history.push('/work-flows') ;
                    break ;
                case 401 :
                    toast(({ closeToast }) => <div> {error.response.data.error} </div>);
                    logout();
                    break;
                default :
                    toast(({ closeToast }) => <div>Something went wrong , please try after some  time  </div>); 
                    history.push('/work-flows') ;  
                    break;
            } 
        });
    }

    loadEvents($id = 'LV'){
        let self = this; 
        axios.get('/api/get-events',{
            params:{ 
                id:$id
            },
            headers : authHeader()
        }).then(function(response){
            if(response.status == 200){  
                self.setState({ 
                    event_options: response.data  
                }); 
            }  
        }).catch(function(error){
            switch(error.response.status){
                case 500 :
                    // logout();
                    toast(({ closeToast }) => <div> {error.response.data.message} </div>); 
                    history.push('/work-flows') ;
                    break ;
                case 401 :
                    toast(({ closeToast }) => <div> {error.response.data.error} </div>);
                    logout();
                    break;
                default :
                    toast(({ closeToast }) => <div>Something went wrong , please try after some  time  </div>); 
                    history.push('/work-flows') ;  
                    break;
            } 
        });
    }
    loadApprover(){
        let self = this;
        axios.get('/api/authorities',{
            params:{ },
            headers : authHeader()
        }).then(function(response){
            if(response.status == 200){ 
                self.setState({ 
                    approver_options: response.data    
                }); 
            }  
        }).catch(function(error){
            switch(error.response.status){
                case 500 :
                    // logout();
                    toast(({ closeToast }) => <div> {error.response.data.message} </div>); 
                    history.push('/work-flows') ;
                    break ;
                case 401 :
                    toast(({ closeToast }) => <div> {error.response.data.error} </div>);
                    logout();
                    break;
                default :
                    toast(({ closeToast }) => <div>Something went wrong , please try after some  time  </div>); 
                    history.push('/work-flows') ;  
                    break;
            } 
        });
    }

    handleAddUpdate(event){
        event.preventDefault();
        const {history} = this.props;
        const workflow = JSON.stringify(this.state);
    
        if(this.validate()){
            axios.post('/api/add-update-workflow', workflow,{headers : authHeader()})
                .then(response => {
                    if(response.data.result){
                        toast(({ closeToast }) => <div>{response.data.message}</div>);
                        history.push('/work-flows') ;
                    } else{
                        response.data.error.map((errors)=>{
                            toast(({ closeToast }) => <div>{errors} </div>);
                        });
                    }
                })
                .catch(error => {  
                    switch(error.response.status){
                        case 500 :
                            // logout();
                            toast(({ closeToast }) => <div> {error.response.data.message} </div>); 
                            history.push('/work-flows') ;
                            break ;
                        case 401 :
                            toast(({ closeToast }) => <div> {error.response.data.error} </div>);
                            logout();
                            break;
                        default :
                            toast(({ closeToast }) => <div>Something went wrong , please try after some  time  </div>); 
                            history.push('/work-flows') ;  
                            break;
                    } 
                })
        }  else{
            toast(({ closeToast }) => <div>{this.state.message}</div>)
        }

    }
    handleFieldInputChange(event){ 
        this.setState({
            [event.target.name] : event.target.value
        });
    }
     
    handleOuChange(ou,event){
        
        this.setState({
            'applicable_ou' : ou
        });  
    }
    handleChange(value, state, props) {
        if(props.name == 'wf_module'){
            this.setState({
                [props.name] : value.value,
                wf_events : ''
            }); 
            this.loadEvents(value.value);
        }else{
            this.setState({
                [props.name] : value.value
            }); 
        } 
    }

    onApproverChange(value, state, props){
        let level = this.state.wf_levels.map((level,index)=>{
            if(index == state){
                level.approver = value.value;
                return level;
            }else{
                return level;
            }
        });
        this.setState({
            wf_levels : level
        });
    }

    
    onFccChange(value, state, props){
         
        // let _level = this.state.wf_levels.map((level,index)=>{
        //     if(index == state){
        //         let f_cc = level;
        //         if(level.f_cc){ 
        //             f_cc = level.f_cc; 

        //         } 
        //         console.log(value);
        //         // f_cc.f_cc = value; 
        //         return f_cc;
        //     }else{
        //         return level;
        //     }
        // });  
        // this.setState({
        //     wf_levels : _level
        // });
    }

    handleFieldChange(name,val,level_){
        let level = this.state.wf_levels.map((level,index)=>{
            if(index == level_){
                level[name] = val;
                return level;
            }else{
                return level;
            }
        });
        this.setState({
            wf_levels : level
        });
    }

    onFbccChange(value, state, props){
        // let level = this.state.wf_levels.map((level,index)=>{
        //     if(index == state){
        //         level.f_bcc = value.value;
        //         return level;
        //     }else{
        //         return level;
        //     }
        // });
        // this.setState({
        //     wf_levels : level
        // });
    }
    createNewLevel(e){
        e.preventDefault();
        let l_l = this.state.wf_levels.length + 1;
        let levels = this.state.wf_levels;
        levels.push({
            _id:0,
            level : l_l,
            wf_days : 0,
            approver : '',
            requ_noti : 0,
            approver_noti : 0,
            next_level_noti :0,
            f_cc :[],
            f_bcc : [],
            dynamic_cc:'',
            dynamic_bcc:''
        });

        this.setState({
            wf_levels:levels
        });
    }
    removeLevel(level,event){ 
        let levels = this.state.wf_levels;
        let new_levels = [];
        let count = 1;
        levels.map((lev,index)=>{
            if((level-1 ) != index){
                lev.level = (count++);
                new_levels.push(lev);
            }
        }); 
        this.setState({
            wf_levels:new_levels
        });
    }
    rendorLevels(){
        let levels = []; 
        if(this.state.approver_options.length <=1){
            return <Loader />;
        }

        this.state.wf_levels.map(($level,index)=>{
            levels.push(<Levels 
                key={'_'+index+'__'+$level.level} 
                level={$level} 
                total_level = {this.state.wf_levels.length}
                onApproverChange = {this.onApproverChange}
                onFccChange = {this.onFccChange}
                onFbccChange = {this.onFbccChange}
                handleFieldChange = {this.handleFieldChange.bind(this)}
                handleCheckChange = {this.handleFieldChange.bind(this)}
                createNewLevel ={this.createNewLevel.bind(this)}
                removeLevel = {this.removeLevel.bind(this)}
                appr = {this.state.approver_options} />)
        });
        return levels;
    }
 
    render(){
        let _level = (this.state.wf_type == 1) ? true : false ;
        if(this.state.event_options.length <=1 || this.state.approver_options.length <=1){
            return <Loader />;
        }       

        if(this.state.hasError){
           return <h4 className="header2">Something went wrong</h4>
        }   else{
            return(
                <div className="dashboard">
                    <div className="card-panel">
                        <div className="row">
                            <form className="col s12" onSubmit={this.handleAddUpdate}>
                                <div id="leave_htmlForm">
                                    <div className="row">    
                                        <div className="col s12 l12 center-align">
                                            <h4 className="header2">{this.state.action} Work Flow </h4>
                                        </div>
                                    </div>
                                    <div className="row"> 
                                        <div className="col s12 l6"> 
                                            <div className="row">
                                                <div className="input-field col s12 m7 ">
                                                    <input
                                                        id="wf_name"
                                                        type="text"
                                                        name="wf_name"
                                                        value={this.state.wf_name}
                                                        onChange={this.handleFieldInputChange}
                                                    /> 
                                                    <label htmlFor="wf_name" className="active">Workflow Name * 
                                                    </label>
                                                </div> 
                                                <div className="input-field col s12 m7 ">
                                                    <input
                                                        id="wf_code"
                                                        type="text"
                                                        name="wf_code"
                                                        value={this.state.wf_code}
                                                        onChange={this.handleFieldInputChange}
                                                    /> 
                                                    <label htmlFor="wf_code" className="active">Workflow Code * 
                                                    </label>
                                                </div> 
                                                
                                                <div className="col s12 m7"> 
                                                    <label  >Select Module </label>
                                                    <SelectSearch  
                                                        options={this.state.module_options} 
                                                        value={this.state.wf_module} 
                                                        name="wf_module" 
                                                        placeholder="Choose Module"
                                                        onChange = {this.handleChange} 
                                                    /> 
                                                </div> 
                                                <div className="col s12 m7"> 
                                                    <label  >Select Events </label>
                                                    <SelectSearch  
                                                        options={this.state.event_options} 
                                                        value={this.state.wf_events} 
                                                        name="wf_events" 
                                                        onChange={this.handleChange}
                                                        placeholder="Choose Events" 
                                                    /> 
                                                </div> 
                                                <div className="col s12 m7 "> 
                                                    <label   className="active">
                                                        <small> Workflow Type </small>
                                                    </label>
                                                    <input
                                                        id="manual"
                                                        type="radio"
                                                        className="with-gap"
                                                        name="wf_type"
                                                        value='1'
                                                        checked = {this.state.wf_type == '1'}
                                                        onChange={this.handleFieldInputChange } 
                                                    /> 
                                                    <label htmlFor="manual">Manager </label>
                                                    <input
                                                        id="automatic"
                                                        type="radio" 
                                                        name="wf_type"
                                                        className="with-gap"
                                                        value= '2'
                                                        checked = {this.state.wf_type == '2'}
                                                        onChange={this.handleFieldInputChange }
                                                    />  
                                                    <label htmlFor="automatic">Automatic</label>
                                                </div>
                                            </div> 
                                        </div>
                                        <div className="col s12 l6">
                                            <OuApplicability 
                                                handleOuChange = {this.handleOuChange.bind(this)}
                                                ou = {this.state.applicable_ou}
                                                col=" m12 l12 "
                                            />
                                        </div> 
                                    </div>
                                    <div className="row">
                                        { _level  ? (
                                                <fieldset className="margin-top">
                                                    <legend>Approval Process</legend>
                                                    {this.rendorLevels()}
                                                </fieldset>
                                            ) : ('')
                                        }
                                    </div>    
                                </div>
                                <div className="row">                                            
                                    <div className="input-field col s12">
                                        <Link to="/work-flows">
                                            <button className="btn default   waves-light"   name="action">
                                                <i className="mdi-navigation-arrow-back left "></i>
                                                Back
                                            </button>
                                        </Link>
                                        <button className="btn cyan waves-effect waves-light  " type="submit" name="action">{(this.state.action)}
                                            <i className="mdi-content-send right"></i>
                                        </button>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            );
        }
    }
}