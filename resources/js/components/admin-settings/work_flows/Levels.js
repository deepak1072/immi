/**
 * Created by Sandeep Maurya on 25/12/2019.
 * @ 06:21 PM
 */

import React ,{ Fragment }from 'react'; 
import SelectSearch from 'react-select-search';
import InfoLabel from './../../profile/InfoLabel';

export default class Levels extends React.Component{
	constructor(props){
        super(props); 
        this.handleFieldChange = this.handleFieldChange.bind(this);
        this.handleCheckChange = this.handleCheckChange.bind(this);
        this.onApproverChange = this.onApproverChange.bind(this);
        this.onFbccChange = this.onFbccChange.bind(this);
        this.onFccChange = this.onFccChange.bind(this);
    } 
    handleFieldChange(event){ 
        this.props.handleFieldChange(event.target.name,event.target.value,(this.props.level.level-1));
    }
    handleCheckChange(event){
        let val = (event.target.value == 1) ?   0 : 1;
        this.props.handleCheckChange(event.target.name,val,(this.props.level.level-1));
    }
    removeLevel(event){ 
        event.preventDefault();
        this.props.removeLevel(this.props.level.level);
    }
    onApproverChange(value, state, props){
        this.props.onApproverChange(value, (this.props.level.level-1 ), props);
    }
    onFbccChange(value, state, props){
        this.props.onFbccChange(value, (this.props.level.level-1 ), props);
    }
    onFccChange(value, state, props){
        this.props.onFccChange(value, (this.props.level.level-1 ), props);
    }
	render(){  
        let info = this.props;    
		return (
			<Fragment > 
	            <div className="row row-margin-bottom">
                    <div className="col s12 m6">
                        <div className="row">
                            <InfoLabel label="Level" label_value = {info.level.level} col="col s12 m4" /> 
                            <div className="input-field col s12 m4 ">
                                <input 
                                    type="text"
                                    name="wf_days"
                                    value={info.level.wf_days}
                                    onChange={this.handleFieldChange}
                                /> 
                                <label   className="active">Day(s) 
                                </label>
                            </div>
                            <div className="col s12 m4"> 
                                <label  >Select Approver </label>
                                <SelectSearch  
                                    options={info.appr} 
                                    value={info.level.approver} 
                                    name="approver" 
                                    placeholder="Choose Approver"
                                    onChange={this.onApproverChange} 
                                /> 
                            </div>  
                        </div>
                        <div className="row"> 
                            <div className="  col s12 m4 row">
                                <div className="col s12">
                                    <input
                                        id={'requester_'+info.level.level}
                                        type="checkbox"
                                        className="with-gap  "
                                        name="requ_noti"
                                        value={info.level.requ_noti}
                                        checked = {info.level.requ_noti == '1'}
                                        onChange={this.handleCheckChange }  
                                    />  
                                    <label htmlFor={'requester_'+info.level.level}>Requester</label>
                                </div>
                                <div className="col s12">
                                    <input
                                        id={'next_level_'+info.level.level}
                                        type="checkbox"
                                        className="with-gap "
                                        name="next_level_noti"
                                        value={info.level.next_level_noti}
                                        checked = {info.level.next_level_noti == '1'}
                                        onChange={this.handleCheckChange }  
                                    />  
                                    <label htmlFor={'next_level_'+info.level.level}>Next Level</label>
                                </div>
                                <div className="col s12">
                                    <input
                                        id={'appr_'+info.level.level}
                                        type="checkbox"
                                        className="with-gap  "
                                        name="approver_noti"
                                        value={info.level.approver_noti}
                                        checked = {info.level.approver_noti == '1'}
                                        onChange={this.handleCheckChange }  
                                    />  
                                    <label htmlFor={'appr_'+info.level.level}>Approver</label>
                                </div>                     
                            </div>
                            <div className="input-field col s12 m4">
                                <input
                                    id={'d_cc_'+info.level.level}
                                    type="text"
                                    name="dynamic_cc"
                                    className="with-gap"
                                    value= {info.level.dynamic_cc} 
                                    onChange={this.handleFieldChange } 
                                />  
                                <label htmlFor={'d_cc_'+info.level.level} className="active">Dynamic Cc</label>
                            </div>
                            <div className="input-field col s12 m4">
                                <input
                                    id={'d_bcc_'+info.level.level}
                                    type="text"
                                    name="dynamic_bcc"
                                    className="with-gap"
                                    value= {info.level.dynamic_bcc} 
                                    onChange={this.handleFieldChange } 
                                />  
                                <label htmlFor={'d_bcc_'+info.level.level} className="active">Dynamic Bcc</label>
                            </div>
                        </div> 
                    </div>
                    <div className="col s12 m4">
                        <div className="col s12 m6"> 
                            <label  >Cc </label>
                            <SelectSearch  
                                key={"_cc_"+info.level.level+'_'}
                                options={info.appr} 
                                value={info.level.f_cc} 
                                name="f_cc" 
                                placeholder="Cc" 
                                onChange={this.onFccChange}  
                                multiple={true}   
                            /> 
                        </div>
                        <div className="col s12 m6"> 
                            <label  >Bcc </label>
                            <SelectSearch 
                                key={"_bcc_"+info.level.level+'_'}
                                options={info.appr} 
                                value={info.level.f_bcc} 
                                name="f_bcc" 
                                placeholder="Bcc" 
                                onChange={this.onFbccChange}
                                multiple={true}  
                            /> 
                        </div>
                    </div> 
                    <div className="col s12 m2"> 
                        {
                            (info.total_level == info.level.level) ? (
                                <button type="btn" onClick={info.createNewLevel} className="btn cyan waves-effect waves-light action_button">
                                    <i className="mdi-content-add"></i>
                                </button>
                            ) : ('')
                        }
                        {
                            (info.total_level !=1) ? (
                                <button type="btn" onClick={this.removeLevel.bind(this)}   className="btn default  waves-effect waves-light action_button">
                                    <i className="mdi-content-clear"></i>
                                </button>
                            ) : ('')
                        }
                        
                    </div>
                </div>
            </Fragment>	  
		)
	}
}