/**
 * Created by Sandeep Maurya on 25th dec 2018.
 */

import React from 'react';
import axios from 'axios';
import { toast } from 'react-toastify';
import {Link} from 'react-router-dom';
import {authHeader,logout} from  "./../../../helpers/responseAuthozisation";
 


export default class LeaveMaster extends React.Component{
    constructor(props){
        super(props);

        this.state = {
            masters : [] ,
            hasError : false  
        }

        this.handleFieldChange   = this.handleFieldChange.bind(this);

    }


    componentDidMount(){
        let self = this;
        axios.get('/api/leave-masters',{headers:authHeader()})
            .then(function(response){
                if(response.status == 200){
                    const data = response.data;
                    self.setState({
                        masters : data
                    });
                }else{
                    toast(({ closeToast }) => <div>Something went wrong , please try after some time  </div>);
                    history.push('/leave-setup') ;
                }
            }).catch(function(error){

                switch(error.response.status){
                    case 500 :
                        // logout();
                        toast(({ closeToast }) => <div> {error.response.data.message} </div>);
                        break ;
                    case 401 :
                        toast(({ closeToast }) => <div> {error.response.data.error} </div>);
                        logout();
                    default :
                        toast(({ closeToast }) => <div> could not fetch leave list </div>);
                }
            })
    }


    static getDerivedStateFromError(error) {
        // Update state so the next render will show the fallback UI.
        return { hasError: true };
    }
 


 
    handleFieldChange(event){
        this.props.onMasterChangeHandle(event.target.value);
    }
 
    render(){

        if(this.state.hasError){
           return <h4 className="header2">Something went wrong</h4>
        }   else{
            return(
                <div className="col s12 m3 input-field">
                    <select  
                        value={this.props.selected_masters} 
                        onChange={this.handleFieldChange} 
                        name="leave_masters" 
                        id="leave_masters" 
                        className="browser-default"
                        disabled = {this.props.is_disabled}
                        >
                        <option value=''>Select Leave Master </option>
                         
                        {
                            this.state.masters.map((master,index)=>{
                                 return(
                                    <option key={index} value={master.leave_master_id}>{master.leave_master_name}</option>
                                )
                            })
                        }
                    </select>
                    <label htmlFor="leave_masters" className="active">Select Leave Master
                    </label>
                </div>
            );
        }
    }
}