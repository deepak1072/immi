/**
 * Created by Sandeep Maurya on 29th dec 2018.
 */

import React from 'react';
 
 
export default class Attachments extends React.Component{
    constructor(props){
        super(props);
        this.state = {
            hasError : false
        }
    }

    static getDerivedStateFromError(error) {
        // Update state so the next render will show the fallback UI.
        return { hasError: true };
    }
 

 
    render(){
        const attachments = this.props.attachments_required == '0' ? false : true ;
        if(this.state.hasError){
           return <h4 className="header2">Something went wrong</h4>
        }   else{
            return(
                <div className= {"input-field col  "+this.props.col}>                              
                    <label   className="active">
                        <small>Attachments in a leave request </small>
                    </label>
                    <div className="col s12 m6">
                        <input
                            id="attachments_required"
                            type="radio"
                            className="with-gap"
                            name="attachments_required"
                            value='0'
                            checked = {this.props.attachments_required == '0'}
                            onChange={this.props.handleRadioChange }
                             
                        /> 
                        <label htmlFor="attachments_required">Not Required</label>

                        <input
                            id="attachments_required1"
                            type="radio"
                            name="attachments_required"
                            className="with-gap"
                            value= '1'
                            checked = {this.props.attachments_required == '1'}
                            onChange={this.props.handleRadioChange }
                             
                        />  
                        <label htmlFor="attachments_required1">Required but Optional</label>
                        <input
                            id="attachments_required2"
                            type="radio"
                            name="attachments_required"
                            className="with-gap"
                            value= '2'
                            checked = {this.props.attachments_required == '2'}
                            onChange={this.props.handleRadioChange }
                             
                        />  
                        <label htmlFor="attachments_required2">Required and Mandatory</label>
                    </div>
                    { attachments ? (
                        <div className="input-field col s12 m4">
                            <input
                                id="attachments_label"
                                type="text"
                                name="attachments_label"
                                className="with-gap"
                                value= {this.props.attachments_label}
                                 
                                onChange={this.props.handleFieldChange }
                                
                                 
                            />  
                            <label htmlFor="attachments_label" className="active">Attachments Label</label>
                        </div>
                        ) : ('') 
                    }
                    
                    
                </div>
            );
        }
    }
}