/**
 * Created by Sandeep Maurya on 8th Jan 2019.
 * @11:02PM
 */

import React from 'react';
 
 
export default class CompOffRequestRules extends React.Component{
    constructor(props){
        super(props);
        this.state = {
            hasError : false
        }
        this.handleRadioChange   = this.handleRadioChange.bind(this);
        this.handleInputChange   = this.handleInputChange.bind(this);


    }

    componentDidMount(){
       
    }

    static getDerivedStateFromError(error) {
        // Update state so the next render will show the fallback UI.
        return { hasError: true };
    }
 


 
    handleRadioChange(event){
        this.props.handleCompOffRequestRules(event.target.value);
    }
    handleInputChange(event){
        this.props.handleCompOffRequestDays(event.target.value);
    }
 
    render(){
        const request_days = this.props.compoff_request_rules == '1' ? true : false ;
        if(this.state.hasError){
           return <h4 className="header2">Something went wrong</h4>
        }   else{
            return(
                <div className= {"input-field col  "+this.props.col}>                              
                    <label   className="active">
                        <small>   Maximum period to create comp off request </small>
                    </label>
                    <div className="col s12 m4">
                        <input
                            id="compoff_request_rules"
                            type="radio"
                            className="with-gap"
                            name="compoff_request_rules"
                            value='0'
                            checked = {this.props.compoff_request_rules == '0'}
                            onChange={this.handleRadioChange }
                             
                        /> 
                        <label htmlFor="compoff_request_rules">Any time</label>

                        <input
                            id="compoff_request_rules1"
                            type="radio"
                            name="compoff_request_rules"
                            className="with-gap"
                            value= '1'
                            checked = {this.props.compoff_request_rules == '1'}
                            onChange={this.handleRadioChange }
                             
                        />  
                        <label htmlFor="compoff_request_rules1">Must be </label>
                    </div>
                    { request_days ? (
                        <div className="input-field col s12 m3">
                            <input
                                id="compoff_request_days"
                                type="text"
                                name="compoff_request_days"
                                className="with-gap"
                                value= {this.props.compoff_request_days}
                                 
                                onChange={this.handleInputChange }
                                
                                 
                            />  
                            <label htmlFor="compoff_request_days" className="active"><small>Maximum days allowed </small></label>
                        </div>
                        ) : ('') 
                    }
                    
                    
                </div>
            );
        }
    }
}