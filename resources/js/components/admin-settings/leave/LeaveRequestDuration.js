/**
 * Created by Sandeep Maurya on 29th dec 2018.
 */

import React from 'react';
 
 
export default class LeaveRequestDuration extends React.Component{
    constructor(props){
        super(props);
        this.state = {
            hasError : false
        }
        this.handleHalfDaysAllowedCheck   = this.handleHalfDaysAllowedCheck.bind(this);
        this.handleMustBeAtleastDaysCheck   = this.handleMustBeAtleastDaysCheck.bind(this);
        this.handleMustBeAtleastDaysLimitsCheck   = this.handleMustBeAtleastDaysLimitsCheck.bind(this);
        this.handleNotMoreThanDaysCheck   = this.handleNotMoreThanDaysCheck.bind(this);
        this.handleNotMoreThanDaysLimitsCheck   = this.handleNotMoreThanDaysLimitsCheck.bind(this);
        this.handleNotMoreThanMonthsCheck   = this.handleNotMoreThanMonthsCheck.bind(this);
        this.handleNotMoreThanMonthsLimitsCheck   = this.handleNotMoreThanMonthsLimitsCheck.bind(this);
    }

    componentDidMount(){
       
    }

    static getDerivedStateFromError(error) {
        // Update state so the next render will show the fallback UI.
        return { hasError: true };
    }
 


 
    handleHalfDaysAllowedCheck(event){
        this.props.handleHalfDaysAllowed(event.target.value);
    }
    handleMustBeAtleastDaysCheck(event){
        this.props.handleMustBeAtleastDays(event.target.value);
    }
    handleMustBeAtleastDaysLimitsCheck(event){
        this.props.handleMustBeAtleastDaysLimits(event.target.value);
    }
    handleNotMoreThanDaysCheck(event){
        this.props.handleNotMoreThanDays(event.target.value);
    }
    handleNotMoreThanDaysLimitsCheck(event){
        this.props.handleNotMoreThanDaysLimits(event.target.value);
    } 
    handleNotMoreThanMonthsCheck(event){
        this.props.handleNotMoreThanMonths(event.target.value);
    }
 
    handleNotMoreThanMonthsLimitsCheck(event){
        this.props.handleNotMoreThanMonthsLimits(event.target.value);
    }
    render(){
       
        if(this.state.hasError){
           return <h4 className="header2">Something went wrong</h4>
        }   else{
            return(
                <div className= {"input-field col  "+this.props.col}>                              
                    <label   className="active">
                        <small>  Leave request duration </small>
                    </label>
                    <div className="row">
                        
                    </div>
                    <div className="row ">
                        <div className="  col s12 m6">
                            <input
                                id="half_day_allowed"
                                type="checkbox"
                                className="with-gap"
                                name="half_day_allowed"
                                value={this.props.half_day_allowed}
                                checked = {this.props.half_day_allowed == '1'}
                                onChange={this.handleHalfDaysAllowedCheck }
                                 
                            /> 
                            <label htmlFor="half_day_allowed">Can be multiple of half days (e.g. - 0.5 day, 1 day, 1.5 days)</label>
                        </div>
                        <div className="col s12 m3">
                            <input
                                id="must_be_atleast_days"
                                type="checkbox"
                                name="must_be_atleast_days"
                                className="with-gap"
                                value= {this.props.must_be_atleast_days}
                                checked = {this.props.must_be_atleast_days == '1'}
                                onChange={this.handleMustBeAtleastDaysCheck}
                                 
                            />  
                            <label htmlFor="must_be_atleast_days">Must be at least</label>
                        </div>
                        <div className="col s12 m3 input-field">
                            <input
                                id="must_be_atleast_days_limits"
                                type="text"
                                name="must_be_atleast_days_limits"
                                value= {this.props.must_be_atleast_days_limits}
                                onChange={this.handleMustBeAtleastDaysLimitsCheck}
                            />  
                            <label htmlFor="must_be_atleast_days_limits" className="active">Days</label>
                        </div>
                    </div>
                    <div className="row ">
                        <div className="col s12 m3">
                            <input
                                id="not_morethan_days"
                                type="checkbox"
                                name="not_morethan_days"
                                className="with-gap"
                                value= {this.props.not_morethan_days}
                                checked = {this.props.not_morethan_days == '1'}
                                onChange={this.handleNotMoreThanDaysCheck}
                                 
                            />  
                            <label htmlFor="not_morethan_days">Cannot be more than</label>
                        </div>
                        <div className="col s12 m3 input-field">
                            <input
                                id="not_morethan_days_limits"
                                type="text"
                                name="not_morethan_days_limits" 
                                value= {this.props.not_morethan_days_limits} 
                                onChange={this.handleNotMoreThanDaysLimitsCheck}
                                 
                            />  
                            <label htmlFor="not_morethan_days_limits" className="active">Days</label>
                        </div>
                     
                        <div className="col s12 m3">
                            <input
                                id="not_morethan_months"
                                type="checkbox"
                                name="not_morethan_months"
                                className="with-gap"
                                value= {this.props.not_morethan_months}
                                checked = {this.props.not_morethan_months == '1'}
                                onChange={this.handleNotMoreThanMonthsCheck}
                                 
                            />  
                            <label htmlFor="not_morethan_months">Cannot be more than</label>
                 
                        </div>
                        <div className="col s12 m3 input-field">
                            <input
                                id="not_morethan_months_limits"
                                type="text"
                                name="not_morethan_months_limits" 
                                value= {this.props.not_morethan_months_limits} 
                                onChange={this.handleNotMoreThanMonthsLimitsCheck}
                                 
                            />  
                            <label htmlFor="not_morethan_months_limits" className="active">Days</label>
                        </div>
                        
                    </div>
                    
                </div>
            );
        }
    }
}