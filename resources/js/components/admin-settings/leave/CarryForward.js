/**
 * Created by Sandeep Maurya on 4th Jan 2019.
 * 
 */

import React from 'react';
 
 
export default class CarryForward extends React.Component{
    constructor(props){
        super(props);
        this.state = {
            hasError : false
        }
 
    }
 

    static getDerivedStateFromError(error) {
        // Update state so the next render will show the fallback UI.
        return { hasError: true };
    }
 
 
 
    render(){

        if(this.state.hasError){
           return <h4 className="header2">Something went wrong</h4>
        }   else{
            return(
                <div className= {"input-field col  "+this.props.col}>                              
                    <label   className="active">
                        <small> Carry forward end of year leave balances to next year ?   </small>
                    </label>
                    <input
                        id="carry_forward"
                        type="radio"
                        className="with-gap"
                        name="carry_forward"
                        value='1'
                        checked = {this.props.carry_forward == '1'}
                        onChange={this.props.handleRadioChange }
                         
                    /> 
                    <label htmlFor="carry_forward">Yes</label>

                    <input
                        id="carry_forward1"
                        type="radio"
                        name="carry_forward"
                        className="with-gap"
                        value= '0'
                        checked = {this.props.carry_forward == '0'}
                        onChange={this.props.handleRadioChange }
                         
                    />  
                    <label htmlFor="carry_forward1">No</label>
                    
                </div>
            );
        }
    }
}