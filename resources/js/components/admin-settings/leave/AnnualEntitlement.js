/**
 * Created by Sandeep Maurya on 28th dec 2018.
 */

import React from 'react';
 
 
export default class AnnualEntitlement extends React.Component{
    constructor(props){
        super(props);
        this.state = {
            hasError : false
        }
    }
 

    static getDerivedStateFromError(error) {
        // Update state so the next render will show the fallback UI.
        return { hasError: true };
    }
 
    render(){
        // const approval_days = this.props.prior_approval_required == '1' ? true : false ;
        if(this.state.hasError){
           return <h4 className="header2">Something went wrong</h4>
        }   else{
            return(
                <div className= {"input-field col  "+this.props.col}>                              
                    <label   className="active">
                        <small> Entitlement Base </small>
                    </label>
                    <div className="row  row-margin-bottom"> 
                        <div className="col s12 m4">
                            <input
                                id="annual_ent_base"
                                type="radio"
                                className="with-gap"
                                name="annual_ent_base"
                                value='0'
                                checked = {this.props.annual_ent_base == '0'}
                                onChange={this.props.handleRadioChange }
                                 
                            /> 
                            <label htmlFor="annual_ent_base"> Annual </label>

                            <input
                                id="annual_ent_base1"
                                type="radio"
                                name="annual_ent_base"
                                className="with-gap"
                                value= '1'
                                checked = {this.props.annual_ent_base == '1'}
                                onChange={this.props.handleRadioChange }
                                 
                            />  
                            <label htmlFor="annual_ent_base1">Tenure</label>
                            <input
                                id="annual_ent_base2"
                                type="radio"
                                name="annual_ent_base"
                                className="with-gap"
                                value= '2'
                                checked = {this.props.annual_ent_base == '2'}
                                onChange={this.props.handleRadioChange }
                                 
                            />  
                            <label htmlFor="annual_ent_base2">Age</label>
                        </div>
                    </div>  
                    <div className="row  row-margin-bottom"> 
                        <div  className=" input-field col s12 m3" > 
                            <input
                                id="annual_entitlement"
                                name="annual_entitlement"
                                type="text"
                                value={this.props.annual_entitlement}
                                onChange={this.props.handleFieldChange}
                            />
                            <label htmlFor="annual_entitlement" className="active">
                                <small>Annual Entitlement</small>
                            </label>
                             
                        </div>
                    </div>
                     
                    
                    
                </div>
            );
        }
    }
}