/**
 * Created by Sandeep Maurya on 28th dec 2018.
 */

import React from 'react';
 
 
export default class GenderApplicability extends React.Component{
    constructor(props){
        super(props);
        this.state = {
            hasError : false
        }
        this.handleRadioChange   = this.handleRadioChange.bind(this);
    }

    componentDidMount(){
       
    }

    static getDerivedStateFromError(error) {
        // Update state so the next render will show the fallback UI.
        return { hasError: true };
    }
 


 
    handleRadioChange(event){
        this.props.handleGenderApplicability(event.target.value);
    }
     
 
    render(){ 
        if(this.state.hasError){
           return <h4 className="header2">Something went wrong</h4>
        }   else{
            return(
                <div className= {"input-field col  "+this.props.col}>                              
                    <label   className="active">
                        <small> Applicable Gender  </small>
                    </label>
                    <div className="col s12 m12">
                        <input
                            id="gender_applicable"
                            type="radio"
                            className="with-gap"
                            name="gender_applicable"
                            value='0'
                            checked = {this.props.gender_applicable == '0'}
                            onChange={this.handleRadioChange }
                             
                        /> 
                        <label htmlFor="gender_applicable">All</label>

                        <input
                            id="gender_applicable1"
                            type="radio"
                            name="gender_applicable"
                            className="with-gap"
                            value= '1'
                            checked = {this.props.gender_applicable == '1'}
                            onChange={this.handleRadioChange }
                             
                        />  
                        <label htmlFor="gender_applicable1">Male only</label>
                        <input
                            id="gender_applicable2"
                            type="radio"
                            name="gender_applicable"
                            className="with-gap"
                            value= '2'
                            checked = {this.props.gender_applicable == '2'}
                            onChange={this.handleRadioChange }
                             
                        />  
                        <label htmlFor="gender_applicable2">Female only</label>
                        <input
                            id="gender_applicable3"
                            type="radio"
                            name="gender_applicable"
                            className="with-gap"
                            value= '3'
                            checked = {this.props.gender_applicable == '3'}
                            onChange={this.handleRadioChange }
                             
                        />  
                        <label htmlFor="gender_applicable3">Male & Female only</label>
                        <input
                            id="gender_applicable4"
                            type="radio"
                            name="gender_applicable"
                            className="with-gap"
                            value= '4'
                            checked = {this.props.gender_applicable == '4'}
                            onChange={this.handleRadioChange }
                             
                        />  
                        <label htmlFor="gender_applicable4">Transgender only</label>
                    </div>
                     
                    
                </div>
            );
        }
    }
}