/**
 * Created by Sandeep Maurya on 15th dec 2018.
 */

import React from 'react';
import axios from 'axios';
import { toast } from 'react-toastify';
import {Link} from 'react-router-dom';
import LeaveMaster from './LeaveMaster';
import LeaveTracking from './LeaveTracking'; 
import ManagerApply from './ManagerApply';
import Delegation from './Delegation';
import ConfirmedEmployee from './ConfirmedEmployee';
import ApplyPreYrLeave from './ApplyPreYrLeave';
import ApprovePreYrLeave from './ApprovePreYrLeave';
import PriorApproval from './PriorApproval';
import LeaveRequestFrequency from  './LeaveRequestFrequency';
import Declaration from './Declaration';
import Attachments from './Attachments';
import LeaveDurationNotExceeds from './LeaveDurationNotExceeds';
import RoundOff from './RoundOff';
import CreditLeave from './CreditLeave';
import FirstPeriodCreditDoe from './FirstPeriodCreditDoe';
import SpecialAccrual from './SpecialAccrual';
import DependsOnDoe from './DependsOnDoe';
import WeeklyOffHolidays from './WeeklyOffHolidays';
import MaximumBalance from './MaximumBalance';
import CarryForward from './CarryForward';
import CarryForwardLimits from './CarryForwardLimits';
import LeaveRequestDuration from './LeaveRequestDuration';
import AnnualEntitlement from './AnnualEntitlement';
import GenderApplicability from './GenderApplicability';
import AutoCreditCompOff from './AutoCreditCompOff';
import CompOffRequestRules from './CompOffRequestRules';
import CompOffCreditLapsingRules from './CompOffCreditLapsingRules';
import CompOffBasisDays from './CompOffBasisDays';
import {authHeader,logout} from  "./../../../helpers/responseAuthozisation";
import OuApplicability from './../usable_components/OuApplicability';
import Loader from '../usable_components/Loader';

export default class AddUpdateLeave extends React.Component{
    constructor(props){
        super(props);

        this.state = {
            leave_name : '',
            leave_master_id : '1',
            leave_desc : '',
            leave_short_name : '', 
            leave_status:'0',
            create_at : '',
            created_by : 'maurya',
            created_by_name : 'Sandeep Kumar Maurya',
            gender_applicable : '0',
            applicable_ou :[],
            leave_credit_period : '1',
            leave_credit_at : '0',
            annual_ent_base : '0',
            annual_entitlement : '0',

            maximum_balance_allowed_anytime : '0',
            maximum_balance_allowed_anytime_limits : '0',
            carry_forward : '0',
            carry_forward_limits : '0',
            carry_forward_limits_days : '0',

            half_day_allowed : '0',
            must_be_atleast_days : '0',
            must_be_atleast_days_limits : '0',
            not_morethan_days : '0',
            not_morethan_days_limits: '0',
            not_morethan_months : '0',
            not_morethan_months_limits : '0',

            leave_commencement_days : '60',
            eligible_leave_months : '0',
            only_confirmed_emp_eligible : '0',
            leave_track : '0',
            manager_can_apply : '0',
            delegation_required : '0',
            apply_pre_yr_leave : '0',
            approve_pre_yr_leave : '0',
            prior_approval_required : '0',
            prior_approval_days : '0',
            leave_request_frequency : '0',
            leave_request_frequency_count:'0',
            declaration_required: '0',
            declaration_label : 'Declaration',
            attachments_required : '0',
            attachments_label : 'Attachments',
            leave_duration_not_exceeds : '1',
            round_off_rule : '1',
            round_off_value : '1',
            credit_leave_rule : '1',
            credit_leave_after: '1',
            first_period_credit_rule : '1',
            special_accrual_base : '1',
            special_accrual_working_days : '0',
            special_accrual_leave_days : '0',
            special_accrual_weeklyoff : '0',
            special_accrual_holidays : '0',
            credit_for_first_half_months : '0',
            credit_for_second_half_months : '0',
            deduction_for_first_half_months: '0',
            deduction_for_second_half_months : '0',
            weekly_off_countable : '0',
            holidays_countable:'0',
            prefixed_sufixed:'1',
            leave_hours:'0',
            auto_credit_compoff : '0',
            compoff_request_rules : '0',
            compoff_request_days : '0',
            compoff_credit_lapse_rules : '0',
            compoff_credit_lapse_days:'0',
            compoff_credit_lapse_basis : '0',
            compoff_workday_basis : '0',
            compoff_holidays_basis : '0',
            compoff_weeklyoff_basis : '0',
            min_req_full_workday_credit: '0',
            min_req_half_workday_credit: '0',
            half_credit_workday_basis : '0', 
            min_req_full_holiday_credit: '0',
            min_req_half_holiday_credit: '0',
            half_credit_holiday_basis : '0',
            min_req_full_weekoff_credit: '0',
            min_req_half_weekoff_credit: '0',
            half_credit_weekoff_basis : '0',
            visibility_flag : '1',
 
            message : '',
            flag : true ,
            hasError:false,
            action: 'Create',
            is_master_disabled : false, 
            is_loading: false
        }


        this.handleAddUpdateLeave = this.handleAddUpdateLeave.bind(this);
        this.MasterChangeHandle   = this.MasterChangeHandle.bind(this);
        this.handleCreditLeaveRule = this.handleCreditLeaveRule.bind(this);
        this.handleCreditLeaveAfter = this.handleCreditLeaveAfter.bind(this);
        this.handleFirstCreditLeaveRule = this.handleFirstCreditLeaveRule.bind(this);

        this.handleSpecialAccrualBase = this.handleSpecialAccrualBase.bind(this);
        this.handleSpecialWorking = this.handleSpecialWorking.bind(this);
        this.handleSpecialLeave = this.handleSpecialLeave.bind(this);
        this.handleSpecialHolidays= this.handleSpecialHolidays.bind(this);
        this.handleSpecialWeekOff = this.handleSpecialWeekOff.bind(this);
        this.handleFirstHalfCredit = this.handleFirstHalfCredit.bind(this);
        this.handleSecondHalfCredit = this.handleSecondHalfCredit.bind(this);
        this.handleFirstHalfDeduction = this.handleFirstHalfDeduction.bind(this);
        this.handleSecondHalfDeduction = this.handleSecondHalfDeduction.bind(this);
        this.handleWeeklyOffCountable = this.handleWeeklyOffCountable.bind(this);
        
        this.handleHolidaysCountable = this.handleHolidaysCountable.bind(this);
        this.handlePrefixedSufixed = this.handlePrefixedSufixed.bind(this);

        this.handleHalfDaysAllowed = this.handleHalfDaysAllowed.bind(this);
        this.handleMustBeAtleastDays = this.handleMustBeAtleastDays.bind(this);
        this.handleMustBeAtleastDaysLimits = this.handleMustBeAtleastDaysLimits.bind(this);
        this.handleNotMoreThanDays = this.handleNotMoreThanDays.bind(this);
        this.handleNotMoreThanDaysLimits = this.handleNotMoreThanDaysLimits.bind(this);
        this.handleNotMoreThanMonthsLimits = this.handleNotMoreThanMonthsLimits.bind(this);
        this.handleNotMoreThanMonths = this.handleNotMoreThanMonths.bind(this);
         
        this.handleGenderApplicability = this.handleGenderApplicability.bind(this);
        this.handleAutoCreditCompOff = this.handleAutoCreditCompOff.bind(this);
        this.handleCompOffRequestRules = this.handleCompOffRequestRules.bind(this);
        this.handleCompOffRequestDays = this.handleCompOffRequestDays.bind(this);
        this.handleCompOffCreditLapseRules = this.handleCompOffCreditLapseRules.bind(this);
        this.handleCompOffCreditLapseDays = this.handleCompOffCreditLapseDays.bind(this);
        this.handleCompOffCreditLapseBasis = this.handleCompOffCreditLapseBasis.bind(this); 
    }


    componentDidMount(){

        let self = this;
        const { id } = this.props.match.params;
        
        if(id != undefined){
            self.setState({
                is_loading:true
            });
            axios.get('/api/leave-details',{
                params:{
                    id : id
                },
                headers : authHeader()
            }).then(function(response){
                if(response.status == 200){
                    const data = response.data[0];
                    const settings =  JSON.parse(data.leave_settings);
                    
                    self.setState({
                        is_loading:false,
                        leave_name : data['leave_name'],
                        leave_id : data['leave_id'], 
                        leave_short_name: data['leave_short_name'] , 
                        leave_master_id: data['leave_master_id'] , 
                        leave_desc: data['leave_desc'] , 
                        leave_credit_period : settings.leave_credit_period,
                        leave_credit_at : settings.leave_credit_at,
                        gender_applicable : settings.gender_applicable,
                        applicable_ou : settings.applicable_ou,
                        annual_ent_base : settings.annual_ent_base,
                        annual_entitlement : settings.annual_entitlement,
                        maximum_balance_allowed_anytime : settings.maximum_balance_allowed_anytime,
                        maximum_balance_allowed_anytime_limits : settings.maximum_balance_allowed_anytime_limits,
                        carry_forward : settings.carry_forward,
                        carry_forward_limits : settings.carry_forward_limits,
                        carry_forward_limits_days : settings.carry_forward_limits_days, 

                        half_day_allowed : settings.half_day_allowed,
                        must_be_atleast_days : settings.must_be_atleast_days,
                        must_be_atleast_days_limits : settings.must_be_atleast_days_limits,
                        not_morethan_days : settings.not_morethan_days,
                        not_morethan_days_limits: settings.not_morethan_days_limits,
                        not_morethan_months : settings.not_morethan_months,
                        not_morethan_months_limits : settings.not_morethan_months_limits,

                        only_confirmed_emp_eligible : settings.only_confirmed_emp_eligible,
                        leave_track : settings.leave_track,
                        manager_can_apply : settings.manager_can_apply,
                        delegation_required : settings.delegation_required,
                        apply_pre_yr_leave : settings.apply_pre_yr_leave,
                        approve_pre_yr_leave : settings.approve_pre_yr_leave,
                        leave_commencement_days : settings.leave_commencement_days,
                        eligible_leave_months : settings.eligible_leave_months,
                        prior_approval_required : settings.prior_approval_required,
                        prior_approval_days : settings.prior_approval_days,
                        leave_request_frequency : settings.leave_request_frequency,
                        leave_request_frequency_count : settings.leave_request_frequency_count,
                        declaration_required : settings.declaration_required,
                        declaration_label : settings.declaration_label,
                        attachments_required : settings.attachments_required,
                        attachments_label : settings.attachments_label,
                        leave_duration_not_exceeds : settings.leave_duration_not_exceeds,
                        round_off_rule : settings.round_off_rule,
                        round_off_value : settings.round_off_value,
                        credit_leave_rule : settings.credit_leave_rule,
                        credit_leave_after : settings.credit_leave_after,
                        first_period_credit_rule : settings.first_period_credit_rule,
                        special_accrual_base : settings.special_accrual_base,
                        special_accrual_working_days : settings.special_accrual_working_days,
                        special_accrual_leave_days : settings.special_accrual_leave_days,
                        special_accrual_weeklyoff : settings.special_accrual_weeklyoff,
                        special_accrual_holidays : settings.special_accrual_holidays,
                        credit_for_first_half_months : settings.credit_for_first_half_months,
                        credit_for_second_half_months : settings.credit_for_second_half_months,
                        deduction_for_first_half_months : settings.deduction_for_first_half_months,
                        deduction_for_second_half_months : settings.deduction_for_second_half_months,
                        weekly_off_countable : settings.weekly_off_countable,
                        holidays_countable : settings.holidays_countable,
                        prefixed_sufixed : settings.prefixed_sufixed,
                        leave_hours : settings.leave_hours,
                        auto_credit_compoff : settings.auto_credit_compoff,
                        compoff_request_rules : settings.compoff_request_rules,
                        compoff_request_days : settings.compoff_request_days,
                        compoff_credit_lapse_rules : settings.compoff_credit_lapse_rules,
                        compoff_credit_lapse_days : settings.compoff_credit_lapse_days,
                        compoff_credit_lapse_basis : settings.compoff_credit_lapse_basis,
                        compoff_workday_basis : settings.compoff_workday_basis,
                        compoff_weeklyoff_basis : settings.compoff_weeklyoff_basis,
                        compoff_holidays_basis : settings.compoff_holidays_basis,
                        min_req_full_workday_credit : settings.min_req_full_workday_credit,
                        min_req_half_workday_credit : settings.min_req_half_workday_credit,
                        half_credit_workday_basis  : settings.half_credit_workday_basis,
                         
                        min_req_full_holiday_credit : settings.min_req_full_holiday_credit,
                        min_req_half_holiday_credit : settings.min_req_half_holiday_credit,
                        half_credit_holiday_basis  : settings.half_credit_holiday_basis,
                         
                        min_req_full_weekoff_credit : settings.min_req_full_weekoff_credit,
                        min_req_half_weekoff_credit : settings.min_req_half_weekoff_credit,
                        half_credit_weekoff_basis  : settings.half_credit_weekoff_basis, 
                        action : 'Update' ,
                        is_master_disabled : true,
                        visibility_flag : settings.visibility_flag,
                        
                    });

                     
                }else{
                    toast(({ closeToast }) => <div>Something went wrong , please try after some time  </div>);
                    history.push('/leave-setup') ;   
                }
            }) .catch(function(error){

                switch(error.response.status){
                    case 500 :
                        // logout();
                        toast(({ closeToast }) => <div> {error.response.data.message} </div>);
                        history.push('/leave-setup') ;
                        break ;
                    case 401 :
                        toast(({ closeToast }) => <div> {error.response.data.error} </div>);
                        logout();
                        break
                    default :
                        toast(({ closeToast }) => <div> could not fetch leave details </div>);
                        history.push('/leave-setup') ;
                        break;
                }
            }) ;
        }
    }

    static getDerivedStateFromError(error) {
        // Update state so the next render will show the fallback UI.
        return { hasError: true };
    }

    handleOuChange(ou,event){
        
        this.setState({
            'applicable_ou' : ou
        });  
    }

    /**
     * form validation
     * @returns {boolean|*}
     */
    validate(){
    	 this.state.flag = true;
          
        
        
        if(this.state.leave_short_name =='' || this.state.leave_short_name == 'undefined'){
            this.state.message = 'Please enter leave short name ';
            this.state.flag = false;
        }
        if(this.state.leave_name =='' || this.state.leave_name == 'undefined'){
            this.state.message = 'Please enter leave name';
            this.state.flag = false;
        }
        if(this.state.leave_master_id =='' || this.state.leave_master_id == 'undefined'){
            this.state.message = 'Please select leave master  ';
            this.state.flag = false;
        }
        return this.state.flag;
    }



    handleAddUpdateLeave(event){
        event.preventDefault();
        const {history} = this.props;
        const leave = this.state;


        
        if(this.validate()){
            axios.post('/api/add-update-leave', leave,{headers  : authHeader() })
                .then(response => {
                    if(response.data.result){
                        toast(({ closeToast }) => <div>{response.data.message}</div>);
                        history.push('/leave-setup') ;
                    } else{
                        response.data.error.map((errors)=>{
                            toast(({ closeToast }) => <div>{errors} </div>);
                        });
                    }
                })
                .catch(function(error){

                    switch(error.response.status){
                        case 500 :
                            // logout();
                            toast(({ closeToast }) => <div> {error.response.data.message} </div>);
                            history.push('/leave-setup') ;
                            break ;
                        case 401 :
                            toast(({ closeToast }) => <div> {error.response.data.error} </div>);
                            logout();
                            break;
                        default :
                            toast(({ closeToast }) => <div> Something went wrong ! please try after some  time </div>);
                            history.push('/leave-setup') ;
                            break;
                    }
                })
        }  else{
            toast(({ closeToast }) => <div>{this.state.message}</div>)
        }

    }
    handleFieldChange(event){
        
        this.setState({
            [event.target.name] : event.target.value
        });
        
    }
    handleRadioChange(event){
        this.setState({
            [event.target.name] :  event.target.value
        }); 
    }
    

    MasterChangeHandle(master){
        this.setState({
            leave_master_id : master
        });
    }
 
    
    
    
    handleCreditLeaveRule(rule){
        this.setState({
            credit_leave_rule : rule
        });
    }
    handleCreditLeaveAfter(value){
        this.setState({
            credit_leave_after : value
        });
    }
    handleFirstCreditLeaveRule(value){
        this.setState({
            first_period_credit_rule : value
        });
    }

    handleSpecialAccrualBase(base){
        this.setState({
            special_accrual_base : base
        });
    }
 
    handleSpecialWorking(base){
        base = base == '1'?'0':'1';
        this.setState({
            special_accrual_working_days : base
        });
    }
    handleSpecialLeave(base){
        base = base == '1'?'0':'1';
       this.setState({
            special_accrual_leave_days : base
        });
    }
    handleSpecialHolidays(base){
        base = base == '1'?'0':'1';
        this.setState({
            special_accrual_holidays : base
        });
    }
    handleSpecialWeekOff(base){
        base = base == '1'?'0':'1';
        this.setState({
            special_accrual_weeklyoff : base
        });
    }
    handleFirstHalfCredit(base){
        this.setState({
            credit_for_first_half_months : base
        });
    }
    handleSecondHalfCredit(base){
        this.setState({
            credit_for_second_half_months : base
        });
    }
    handleFirstHalfDeduction(base){
        this.setState({
            deduction_for_first_half_months : base
        });
    }
    handleSecondHalfDeduction(base){
        this.setState({
            deduction_for_second_half_months : base
        });
    }
    handleWeeklyOffCountable(base){
        this.setState({
            weekly_off_countable : base
        });
    }
    handleHolidaysCountable(base){
        this.setState({
            holidays_countable : base
        });
    }
    handlePrefixedSufixed(base){
        this.setState({
            prefixed_sufixed : base
        });
    }
   
    
    handleHalfDaysAllowed(value){
        value = value == '1' ? '0': '1';
        this.setState({
            half_day_allowed : value
        }); 
    }
    handleMustBeAtleastDays(value){
        value = value=='0'?'1':'0';
        this.setState({
            must_be_atleast_days : value
        }); 
    }
    handleMustBeAtleastDaysLimits(value){
        this.setState({
            must_be_atleast_days_limits : value
        }); 
    }
    handleNotMoreThanDays(value){
        value = value == '0' ? '1' : '0';
        this.setState({
            not_morethan_days : value
        }); 
    }
    handleNotMoreThanDaysLimits(value){
        this.setState({
            not_morethan_days_limits : value
        }); 
    }
    handleNotMoreThanMonths(value){
        value = value == '0' ? '1' : '0';
        this.setState({
            not_morethan_months : value
        }); 
    }
    handleNotMoreThanMonthsLimits(value){
        this.setState({
            not_morethan_months_limits : value
        }); 
    }
    
    handleGenderApplicability(value)
    {
        this.setState({
            gender_applicable : value
        }); 
    }
    handleAutoCreditCompOff(value)
    {
        this.setState({
            auto_credit_compoff : value
        }); 
    }
    handleCompOffRequestRules(value)
    {
        this.setState({
            compoff_request_rules : value
        }); 
    }
    handleCompOffRequestDays(value)
    {
        this.setState({
            compoff_request_days : value
        }); 
    }

     handleCompOffCreditLapseRules(value)
    {
        this.setState({
            compoff_credit_lapse_rules : value
        }); 
    }
    
     handleCompOffCreditLapseDays(value)
    {
        this.setState({
            compoff_credit_lapse_days : value
        }); 
    }
    handleCompOffCreditLapseBasis(value){
        this.setState({
            compoff_credit_lapse_basis : value
        }); 
    }
    handleCompOffBasisDays(event){
        const value = event.target.value == '0'?'1':'0';
        this.setState({
            [event.target.name] : value
        }); 
    }
    handleCompOffBasisDaysInput(event){

        this.setState({
            [event.target.name] : value
        }); 
    }
    handleInputChange(event){
        this.setState({
            [event.target.name] : event.target.value
        }); 
    }
    render(){
        if(this.state.is_loading){
            return <Loader />
        }
        const credit_leave_4  = this.state.first_period_credit_rule == '4'  ? true : false ;
        const credit_leave_5 =  this.state.first_period_credit_rule == '5' ? true : false;
        const carry_forward  = this.state.carry_forward == '1' ? true : false;
        const short_leave  = this.state.leave_master_id == '7' ? true : false;
        const comp_off  = this.state.leave_master_id == '3' ? true : false;
        if(this.state.hasError){
           return <h4 className="header2">Something went wrong</h4>
        }   else{
            return(
                <div className="dashboard">
                    <div className="card-panel">
                        <div className="row">
                            <form className="col s12" onSubmit={this.handleAddUpdateLeave}>
                                <div id="leave_htmlForm">
                                    <div className="row">                                                
                        
                                        <div className="col s12 l12 center-align">
                                            <h4 className="header2">{this.state.action} Leave </h4>
                                        </div>
                                    </div>
                                    <div className="row"> 
                                        <LeaveMaster 
                                            onMasterChangeHandle={this.MasterChangeHandle} 
                                            selected_masters= {this.state.leave_master_id}  
                                            is_disabled = {this.state.is_master_disabled}
                                        />   
                                            
                                        <div className="input-field col s12 m3">
                                            <input
                                                id="leave_name"
                                                type="text"
                                                name="leave_name"
                                                value={this.state.leave_name}
                                                onChange={this.handleFieldChange.bind(this)}
                                            /> 
                                            <label htmlFor="leave_name" className="active">
                                                <small>Leave Name *</small> 
                                            </label>
                                        </div>

                                        <div className="input-field col s12 m3">
                                            <input
                                                id="leave_desc"
                                                type="text"
                                                name="leave_desc"
                                                value={this.state.leave_desc}
                                                onChange={this.handleFieldChange.bind(this)}
                                            /> 
                                            <label htmlFor="leave_desc" className="active">
                                                <small>Leave Descriptions</small> 
                                            </label>
                                        </div>

                                        <div className="input-field col s12 m3">
                                            <input
                                                id="leave_short_name"
                                                type="text"
                                                name="leave_short_name"
                                                value={this.state.leave_short_name}
                                                onChange={this.handleFieldChange.bind(this)}
                                                disabled ={this.state.is_master_disabled}
                                            /> 
                                            <label htmlFor="leave_short_name" className="active">
                                                <small>Short Name</small>
                                            </label>
                                        </div>
                                    </div>      
                                </div>
                                    
                                
                                <fieldset>
                                    <legend> Leave Credit Rules
                                    </legend>     
                                    <div className="row">
                                        <div className="col s12 m3 input-field">
                                            <select  
                                                value={this.state.leave_credit_period} 
                                                onChange={this.handleFieldChange.bind(this)} 
                                                name="leave_credit_period" 
                                                id="leave_credit_period" 
                                                className="browser-default"
                                                    
                                                >
                                                <option value='1'>Monthly </option>
                                                <option value='2'>Bi-Monthly </option>
                                                <option value='3'>Quarterly </option>
                                                <option value='6'>Half-Yearly </option>
                                                <option value='12'>Yearly </option>
                                                    
                                                
                                            </select>
                                            <label htmlFor="leave_credit_period" className="active"> Leave Credit Period
                                            </label>
                                        </div>
                                        <div className="col s12 m3 input-field">
                                            <select  
                                                value={this.state.leave_credit_at} 
                                                onChange={this.handleFieldChange.bind(this)} 
                                                name="leave_credit_at" 
                                                id="leave_credit_at" 
                                                className="browser-default"
                                                    
                                                >
                                                <option value='0'>Start </option>
                                                <option value='1'>End </option>
                                            </select>
                                            <label htmlFor="leave_credit_at" className="active">Leave Credit At
                                            </label>
                                        </div>
                                    </div>
                                </fieldset> 
                                <fieldset>
                                    <legend> 
                                        Global Applicabililty 
                                    </legend>
                                    <div className="row row-margin-bottom">

                                        <GenderApplicability
                                            gender_applicable = {this.state.gender_applicable}
                                            col = "s12 m12"
                                            handleGenderApplicability = {this.handleGenderApplicability}

                                        />
                                        <OuApplicability 
                                            handleOuChange = {this.handleOuChange.bind(this)}
                                            ou = {this.state.applicable_ou}
                                        />
                                    </div>
                                </fieldset>
                                <fieldset>
                                    <legend>
                                        Annual Entitlement
                                    </legend>
                                    <div className="row">
                                        <AnnualEntitlement
                                            col = "s12 m12"
                                            handleRadioChange = {this.handleRadioChange.bind(this)}
                                            annual_ent_base = {this.state.annual_ent_base}
                                            annual_entitlement = {this.state.annual_entitlement}
                                            handleFieldChange = {this.handleFieldChange.bind(this)}
                                        />
                                    </div>
                                </fieldset>

                                <fieldset>
                                    <legend>
                                        Accumulation Rules
                                    </legend>
                                        <div className="row row-margin-bottom">
                                        <MaximumBalance
                                            maximum_balance_allowed_anytime = {this.state.maximum_balance_allowed_anytime}
                                            handleRadioChange = {this.handleRadioChange.bind(this)}
                                            handleFieldChange = {this.handleFieldChange.bind(this)}
                                            maximum_balance_allowed_anytime_limits = {this.state.maximum_balance_allowed_anytime_limits}
                                            col = 's12 m12'
                                        />
                                    </div>
                                    <div className="row row-margin-bottom">
                                        <CarryForward
                                            carry_forward = {this.state.carry_forward}
                                            col = "s12 m12"
                                            handleRadioChange = {this.handleRadioChange.bind(this)}
                                        /> 
                                    </div>
                                    { carry_forward ? (
                                            <div className="row row-margin-bottom">
                                                <CarryForwardLimits
                                                    carry_forward_limits_days = {this.state.carry_forward_limits_days}
                                                    carry_forward_limits = {this.state.carry_forward_limits}
                                                    col = "s12 m12 "
                                                    handleRadioChange = {this.handleRadioChange.bind(this)}
                                                    handleFieldChange = {this.handleFieldChange.bind(this)}
                                                />
                                            </div>
                                        ) : ('')
                                    }
                                </fieldset>
                                {   comp_off ? (

                                        <fieldset>
                                            <legend>
                                                Comp - Off Rules
                                            </legend>
                                            <div className="row row-margin-bottom">
                                                <AutoCreditCompOff
                                                    col= "s12 m12"
                                                    auto_credit_compoff = {this.state.auto_credit_compoff}
                                                    handleAutoCreditCompOff = {this.handleAutoCreditCompOff}

                                                />
                                            </div>
                                            <div className="row row-margin-bottom">
                                                <CompOffRequestRules
                                                    col= "s12 m12"
                                                    compoff_request_rules = {this.state.compoff_request_rules}
                                                    compoff_request_days = {this.state.compoff_request_days}
                                                    handleCompOffRequestRules = {this.handleCompOffRequestRules}
                                                    handleCompOffRequestDays = {this.handleCompOffRequestDays}

                                                />
                                            </div>
                                            <div className="row row-margin-bottom">
                                                <CompOffCreditLapsingRules
                                                    col= "s12 m12"
                                                    compoff_credit_lapse_rules = {this.state.compoff_credit_lapse_rules}
                                                    compoff_credit_lapse_days = {this.state.compoff_credit_lapse_days}
                                                    compoff_credit_lapse_basis = {this.state.compoff_credit_lapse_basis}
                                                    handleCompOffCreditLapseRules = {this.handleCompOffCreditLapseRules}
                                                    handleCompOffCreditLapseDays = {this.handleCompOffCreditLapseDays}
                                                    handleCompOffCreditLapseBasis = {this.handleCompOffCreditLapseBasis}
                                                />
                                            </div>
                                            <div className="row row-margin-bottom">
                                                <CompOffBasisDays
                                                    col= "s12 m12"
                                                    compoff_workday_basis = {this.state.compoff_workday_basis}
                                                    compoff_holidays_basis = {this.state.compoff_holidays_basis}
                                                    compoff_weeklyoff_basis = {this.state.compoff_weeklyoff_basis}
                                                    min_req_full_workday_credit = {this.state.min_req_full_workday_credit}
                                                    min_req_half_workday_credit = {this.state.min_req_half_workday_credit}
                                                    half_credit_workday_basis = {this.state.half_credit_workday_basis}
                                                    full_credit_workday_basis = {this.state.full_credit_workday_basis}

                                                    min_req_full_holiday_credit = {this.state.min_req_full_holiday_credit}
                                                    min_req_half_holiday_credit = {this.state.min_req_half_holiday_credit}
                                                    half_credit_holiday_basis = {this.state.half_credit_holiday_basis}
                                                    full_credit_holiday_basis = {this.state.full_credit_holiday_basis}

                                                    min_req_full_weekoff_credit = {this.state.min_req_full_weekoff_credit}
                                                    min_req_half_weekoff_credit = {this.state.min_req_half_weekoff_credit}
                                                    half_credit_weekoff_basis = {this.state.half_credit_weekoff_basis}
                                                    full_credit_weekoff_basis = {this.state.full_credit_weekoff_basis}
                                                    
                                                    handleCompOffBasisDays = {this.handleCompOffBasisDays.bind(this)}
                                                    handleCompOffBasisDaysInput = {this.handleCompOffBasisDaysInput.bind(this)}
                                                    handleRadioChange = {this.handleRadioChange.bind(this) }
                                                    
                                                    handleInputChange = {this.handleInputChange.bind(this)}
                                                    

                                                />
                                            </div>

                                        </fieldset>

                                    ) : ('')
                                }
                                

                                <fieldset>
                                    <legend>
                                        Leave Request Rules
                                    </legend>
                                    <div className="row row-margin-bottom">
                                        <ConfirmedEmployee
                                            handleRadioChange = {this.handleRadioChange.bind(this)}
                                            col='s12 m3'
                                            only_confirmed_emp_eligible = {this.state.only_confirmed_emp_eligible}
                                        />
                                        <LeaveTracking 
                                            handleRadioChange = {this.handleRadioChange.bind(this)}
                                            col='s12 m3'
                                            leave_track = {this.state.leave_track}
                                        />
                                        <ManagerApply
                                            manager_can_apply = {this.state.manager_can_apply}
                                            handleRadioChange = {this.handleRadioChange.bind(this)}
                                            col = 's12 m3'
                                        />
                                        <Delegation
                                            delegation_required = {this.state.delegation_required}
                                            handleRadioChange = {this.handleRadioChange.bind(this)}
                                            col = 's12 m3'
                                        />
                                    </div>
                                    <div className="row row-margin-bottom">
                                        <ApplyPreYrLeave
                                            apply_pre_yr_leave = {this.state.apply_pre_yr_leave}
                                            handleRadioChange = {this.handleRadioChange.bind(this)}
                                            col = 's12 m3'
                                        />
                                        <ApprovePreYrLeave
                                            approve_pre_yr_leave = {this.state.approve_pre_yr_leave}
                                            handleRadioChange = {this.handleRadioChange.bind(this)}
                                            col = 's12 m3'
                                        />

                                        

                                        <div className="input-field col s12 m3">
                                            <input
                                                id="leave_commencement_days"
                                                type="text"
                                                name="leave_commencement_days"
                                                value={this.state.leave_commencement_days}
                                                onChange={this.handleFieldChange.bind(this)}
                                            /> 
                                            <label htmlFor="leave_commencement_days" className="active">
                                                <small>Leave Request Commencement Days</small>
                                            </label>
                                        </div>

                                        <div className="input-field col s12 m3">
                                            <input
                                                id="eligible_leave_months"
                                                type="text"
                                                name="eligible_leave_months"
                                                value={this.state.eligible_leave_months}
                                                onChange={this.handleFieldChange.bind(this)}
                                            /> 
                                            <label htmlFor="eligible_leave_months" className="active">
                                            <small> Eligible for leave after DOJ (in months) </small>
                                            </label>
                                        </div>
                                        { short_leave ? (
                                                <div className="input-field col s12 m3">
                                                    <input
                                                        id="leave_hours"
                                                        type="text"
                                                        name="leave_hours"
                                                        value={this.state.leave_hours}
                                                        onChange={this.handleFieldChange.bind(this)}
                                                    /> 
                                                    <label htmlFor="leave_hours" className="active">
                                                    <small> Leave Hours </small>
                                                    </label>
                                                </div>

                                            ) : ('')

                                        }

                                    </div>
                                    <div className="row row-margin-bottom">
                                        <LeaveRequestDuration
                                            half_day_allowed = {this.state.half_day_allowed}
                                            must_be_atleast_days = {this.state.must_be_atleast_days}
                                            must_be_atleast_days_limits ={this.state.must_be_atleast_days_limits}
                                            not_morethan_days = {this.state.not_morethan_days}
                                            not_morethan_days_limits = {this.state.not_morethan_days_limits}
                                            not_morethan_months = {this.state.not_morethan_months}
                                            not_morethan_months_limits = {this.state.not_morethan_months_limits}
                                            col="s12 m12"
                                            handleHalfDaysAllowed = {this.handleHalfDaysAllowed}
                                            handleMustBeAtleastDays = {this.handleMustBeAtleastDays}
                                            handleMustBeAtleastDaysLimits = {this.handleMustBeAtleastDaysLimits}
                                            handleNotMoreThanDays = {this.handleNotMoreThanDays}
                                            handleNotMoreThanDaysLimits = {this.handleNotMoreThanDaysLimits}
                                            handleNotMoreThanMonths = {this.handleNotMoreThanMonths}
                                            handleNotMoreThanMonthsLimits = {this.handleNotMoreThanMonthsLimits}


                                        />
                                    </div>

                                    <div className="row row-margin-bottom">
                                        <WeeklyOffHolidays
                                            col = "s12 m3"
                                            weekly_off_countable = {this.state.weekly_off_countable}
                                            holidays_countable = {this.state.holidays_countable}
                                            prefixed_sufixed= {this.state.prefixed_sufixed}
                                            handleWeeklyOffCountable = {this.handleWeeklyOffCountable}
                                            handleHolidaysCountable = {this.handleHolidaysCountable}
                                            handlePrefixedSufixed={this.handlePrefixedSufixed}
                                        />
                                    </div>
                                    <div className="row row-margin-bottom">
                                        <PriorApproval
                                            prior_approval_required = {this.state.prior_approval_required}
                                            handleRadioChange = {this.handleRadioChange.bind(this)}
                                            handleFieldChange = {this.handleFieldChange.bind(this)}
                                            prior_approval_days = {this.state.prior_approval_days}
                                            col = 's12 m12'
                                        />

                                    </div>
                                    <div className="row row-margin-bottom">
                                        <LeaveRequestFrequency
                                            leave_request_frequency = {this.state.leave_request_frequency}
                                            handleRadioChange = {this.handleRadioChange.bind(this)}
                                            handleFieldChange = {this.handleFieldChange.bind(this)}
                                            leave_request_frequency_count = {this.state.leave_request_frequency_count}
                                            col = 's12 m12'
                                        />
                                    </div>
                                    <div className="row row-margin-bottom">
                                        <LeaveDurationNotExceeds
                                            leave_duration_not_exceeds = {this.state.leave_duration_not_exceeds}
                                            handleRadioChange = {this.handleRadioChange.bind(this)}
                                            col = 's12 m12'
                                        />
                                    </div>
                                    <div className="row row-margin-bottom">
                                        <Declaration
                                            declaration_required = {this.state.declaration_required}
                                            handleFieldChange = {this.handleFieldChange.bind(this)}
                                            handleRadioChange = {this.handleRadioChange.bind(this)}
                                            declaration_label = {this.state.declaration_label}
                                            col = 's12 m12'
                                        />
                                    </div>
                                    <div className="row row-margin-bottom">
                                        <Attachments
                                            attachments_required = {this.state.attachments_required}
                                            handleFieldChange = {this.handleFieldChange.bind(this)}
                                            handleRadioChange = {this.handleRadioChange.bind(this)}
                                            attachments_label = {this.state.attachments_label}
                                            col = 's12 m12'
                                        />
                                    </div>
                                    <div className="row row-margin-bottom">
                                        <RoundOff
                                            round_off_rule = {this.state.round_off_rule}
                                            handleRadioChange = {this.handleRadioChange.bind(this)}
                                            handleFieldChange = {this.handleFieldChange.bind(this)}
                                            round_off_value = {this.state.round_off_value}
                                            col = 's12 m12'
                                        />
                                    </div>
                                    <div className="row row-margin-bottom">
                                        <CreditLeave
                                            credit_leave_rule = {this.state.credit_leave_rule}
                                            handleCreditLeaveRule = {this.handleCreditLeaveRule}
                                            handleCreditLeaveAfter = {this.handleCreditLeaveAfter}
                                            credit_leave_after = {this.state.credit_leave_after}
                                            col = 's12 m12'
                                        />
                                    </div>
                                    <div className="row row-margin-bottom">
                                        <FirstPeriodCreditDoe
                                            first_period_credit_rule = {this.state.first_period_credit_rule}
                                            handleFirstCreditLeaveRule = {this.handleFirstCreditLeaveRule}
                                            
                                            col = 's12 m12'
                                        />
                                        { credit_leave_5 ? (
                                                <SpecialAccrual
                                                    col="s12 m11"
                                                    special_accrual_base = {this.state.special_accrual_base}
                                                    special_accrual_working_days = {this.state.special_accrual_working_days}
                                                    special_accrual_leave_days = {this.state.special_accrual_leave_days}
                                                    special_accrual_weeklyoff = {this.state.special_accrual_weeklyoff}
                                                    special_accrual_holidays = {this.state.special_accrual_holidays}
                                                    handleSpecialAccrualBase = {this.handleSpecialAccrualBase}
                                                    handleSpecialLeave = {this.handleSpecialLeave}
                                                    handleSpecialWorking = {this.handleSpecialWorking}
                                                    handleSpecialWeekOff = {this.handleSpecialWeekOff}
                                                    handleSpecialHolidays = {this.handleSpecialHolidays}
                                                />
                                            ) : ('') 
                                        }
                                        { credit_leave_4 ? (
                                                <DependsOnDoe
                                                    col= "s12 m11"
                                                    credit_for_first_half_months = {this.state.credit_for_first_half_months}
                                                    credit_for_second_half_months = {this.state.credit_for_second_half_months}
                                                    deduction_for_first_half_months = {this.state.deduction_for_first_half_months}
                                                    deduction_for_second_half_months = {this.state.deduction_for_second_half_months}
                                                    handleFirstHalfCredit = {this.handleFirstHalfCredit}
                                                    handleSecondHalfCredit= {this.handleSecondHalfCredit}
                                                    handleFirstHalfDeduction = {this.handleFirstHalfDeduction}
                                                    handleSecondHalfDeduction = {this.handleSecondHalfDeduction}
                                                /> 
                                            ) : ('') 
                                        }

                                    </div>
                                </fieldset>
                                <div className="row">                                            
                                    <div className="input-field col s12">
                                        <Link to="/leave-setup">
                                            <button className="btn default   waves-light"   name="action">
                                                <i className="mdi-navigation-arrow-back left "></i>
                                                cancel
                                            </button>
                                        </Link>
                                        <button className="btn cyan waves-effect waves-light  " type="submit" name="action">{(this.state.action)}
                                            <i className="mdi-content-send right"></i>
                                        </button>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            );
        }
    }
}