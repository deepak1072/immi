/**
 * Created by Sandeep Maurya on 29th dec 2018.
 */

import React from 'react';
export default class DependsOnDoe extends React.Component{
    constructor(props){
        super(props);
        this.state = {
            hasError : false
        }
        this.handleRadioChange   = this.handleRadioChange.bind(this);
        this.handleFirstRadioChange   = this.handleFirstRadioChange.bind(this);
        this.handleThirdRadioChange = this.handleThirdRadioChange.bind(this);
        this.handleFourthRadioChange = this.handleFourthRadioChange.bind(this);
    }

    static getDerivedStateFromError(error) {
        // Update state so the next render will show the fallback UI.
        return { hasError: true };
    }
  
    handleFirstRadioChange(event){
        this.props.handleFirstHalfCredit(event.target.value);
    }
     
    handleRadioChange(event){
        this.props.handleSecondHalfCredit(event.target.value);
    }
    handleThirdRadioChange(event){
        this.props.handleFirstHalfDeduction(event.target.value);
    }
    handleFourthRadioChange(event){
        this.props.handleSecondHalfDeduction(event.target.value);
    }
    render(){
        const credit_leave_4  = this.props.first_period_credit_rule == '4'  ? true : false ;
        const credit_leave_5 =  this.props.first_period_credit_rule == '5' ? true : false;
        if(this.state.hasError){
           return <h4 className="header2">Something went wrong</h4>
        }   else{
            return(
                <div className="">
                    <div className= {"row margin-top row-margin-bottom col offset-s1 offset-m1  "+this.props.col}> 
                        <label   className="active">
                            <small>Credit of leave when employee joins between 1st and 15th of the month </small>
                        </label>
                        <div className=" input-field col s12 m12">
                            <input
                                id="credit_for_first_half_months"
                                type="radio"
                                className="with-gap"
                                name="credit_for_first_half_months"
                                value='0'
                                checked = {this.props.credit_for_first_half_months == '0'}
                                onChange={this.handleFirstRadioChange }
                                 
                            /> 
                            <label htmlFor="credit_for_first_half_months">No credit</label>
                            <input
                                id="credit_for_first_half_months1"
                                type="radio"
                                className="with-gap"
                                name="credit_for_first_half_months"
                                value='1'
                                checked = {this.props.credit_for_first_half_months == '1'}
                                onChange={this.handleFirstRadioChange }
                                 
                            /> 
                            <label htmlFor="credit_for_first_half_months1">Half month credit</label>
                            <input
                                id="credit_for_first_half_months2"
                                type="radio"
                                className="with-gap"
                                name="credit_for_first_half_months"
                                value='2'
                                checked = {this.props.credit_for_first_half_months == '2'}
                                onChange={this.handleFirstRadioChange }
                                 
                            /> 
                            <label htmlFor="credit_for_first_half_months2">Full month credit</label>
                        </div>
                    </div>
                    <div className= {"row row-margin-bottom col offset-s1 offset-m1  "+this.props.col}> 
                        <label   className="active">
                            <small>Credit of leave when employee joins between 16th and end of the month </small>
                        </label>
                        <div className="input-field col s12 m12">
                            <input
                                id="credit_for_second_half_months"
                                type="radio"
                                className="with-gap"
                                name="credit_for_second_half_months"
                                value='0'
                                checked = {this.props.credit_for_second_half_months == '0'}
                                onChange={this.handleRadioChange }
                                 
                            /> 
                            <label htmlFor="credit_for_second_half_months">No credit</label>
                            <input
                                id="credit_for_second_half_months1"
                                type="radio"
                                className="with-gap"
                                name="credit_for_second_half_months"
                                value='1'
                                checked = {this.props.credit_for_second_half_months == '1'}
                                onChange={this.handleRadioChange }
                                 
                            /> 
                            <label htmlFor="credit_for_second_half_months1">Half month credit</label>
                            <input
                                id="credit_for_second_half_months2"
                                type="radio"
                                className="with-gap"
                                name="credit_for_second_half_months"
                                value='2'
                                checked = {this.props.credit_for_second_half_months == '2'}
                                onChange={this.handleRadioChange }
                                 
                            /> 
                            <label htmlFor="credit_for_second_half_months2">Full month credit</label>
                        </div> 
                    </div> 
                    <div className= {"row row-margin-bottom col offset-s1 offset-m1  "+this.props.col}> 
                        <label   className="active">
                            <small>Deduction of leave when employee separates between 1st and 15th of the month  </small>
                        </label>
                        <div className=" input-field col s12 m12">
                            <input
                                id="deduction_for_first_half_months"
                                type="radio"
                                className="with-gap"
                                name="deduction_for_first_half_months"
                                value='0'
                                checked = {this.props.deduction_for_first_half_months == '0'}
                                onChange={this.handleThirdRadioChange }
                                 
                            /> 
                            <label htmlFor="deduction_for_first_half_months">No deduction</label>
                            <input
                                id="deduction_for_first_half_months1"
                                type="radio"
                                className="with-gap"
                                name="deduction_for_first_half_months"
                                value='1'
                                checked = {this.props.deduction_for_first_half_months == '1'}
                                onChange={this.handleThirdRadioChange }
                                 
                            /> 
                            <label htmlFor="deduction_for_first_half_months1">Half month deduction</label>
                            <input
                                id="deduction_for_first_half_months2"
                                type="radio"
                                className="with-gap"
                                name="deduction_for_first_half_months"
                                value='2'
                                checked = {this.props.deduction_for_first_half_months == '2'}
                                onChange={this.handleThirdRadioChange }
                                 
                            /> 
                            <label htmlFor="deduction_for_first_half_months2">Full month deduction</label>
                        </div> 
                    </div>
                    <div className= {"row row-margin-bottom col offset-s1 offset-m1  "+this.props.col}> 
                        <label   className="active">
                            <small>Deduction of leave when employee separates between 16th and end of the month  </small>
                        </label>
                        <div className="input-field col s12 m12">
                            <input
                                id="deduction_for_second_half_months"
                                type="radio"
                                className="with-gap"
                                name="deduction_for_second_half_months"
                                value='0'
                                checked = {this.props.deduction_for_second_half_months == '0'}
                                onChange={this.handleFourthRadioChange }
                                 
                            /> 
                            <label htmlFor="deduction_for_second_half_months">No deduction</label>
                            <input
                                id="deduction_for_second_half_months1"
                                type="radio"
                                className="with-gap"
                                name="deduction_for_second_half_months"
                                value='1'
                                checked = {this.props.deduction_for_second_half_months == '1'}
                                onChange={this.handleFourthRadioChange }
                                 
                            /> 
                            <label htmlFor="deduction_for_second_half_months1">Half month deduction</label>
                            <input
                                id="deduction_for_second_half_months2"
                                type="radio"
                                className="with-gap"
                                name="deduction_for_second_half_months"
                                value='2'
                                checked = {this.props.deduction_for_second_half_months == '2'}
                                onChange={this.handleFourthRadioChange }
                                 
                            /> 
                            <label htmlFor="deduction_for_second_half_months2">Full month deduction</label>
                        </div> 
                    </div>
                </div>
            );
        }
    }
}