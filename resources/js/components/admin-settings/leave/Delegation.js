/**
 * Created by Sandeep Maurya on 25th dec 2018.
 */

import React from 'react';
 
 
export default class Delegation extends React.Component{
    constructor(props){
        super(props);
        this.state = {
            hasError : false
        }
         

    }

    componentDidMount(){
       
    }

    static getDerivedStateFromError(error) {
        // Update state so the next render will show the fallback UI.
        return { hasError: true };
    }
 
 
 
    render(){

        if(this.state.hasError){
           return <h4 className="header2">Something went wrong</h4>
        }   else{
            return(
                <div className= {"input-field col  "+this.props.col}>                              
                    <label   className="active">
                        <small> Delegation Required   </small>
                    </label>
                    <input
                        id="delegation_required"
                        type="radio"
                        className="with-gap"
                        name="delegation_required"
                        value='1'
                        checked = {this.props.delegation_required == '1'}
                        onChange={this.props.handleRadioChange }
                         
                    /> 
                    <label htmlFor="delegation_required">Yes</label>

                    <input
                        id="delegation_required1"
                        type="radio"
                        name="delegation_required"
                        className="with-gap"
                        value= '0'
                        checked = {this.props.delegation_required == '0'}
                        onChange={this.props.handleRadioChange }
                         
                    />  
                    <label htmlFor="delegation_required1">No</label>
                    
                </div>
            );
        }
    }
}