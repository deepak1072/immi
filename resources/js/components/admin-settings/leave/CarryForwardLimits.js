/**
 * Created by Sandeep Maurya on 4th Jan 2019.
 * @11:49 PM
 */

import React from 'react';
 
 
export default class CarryForwardLimits extends React.Component{
    constructor(props){
        super(props);
        this.state = {
            hasError : false
        }
  
    }

 

    static getDerivedStateFromError(error) {
        // Update state so the next render will show the fallback UI.
        return { hasError: true };
    }
 

 
 
    render(){
        const carry_forward_limits = this.props.carry_forward_limits == '1' ? true : false ;
        if(this.state.hasError){
           return <h4 className="header2">Something went wrong</h4>
        }   else{
            return(
                <div className= {"input-field col  "+this.props.col}>                              
                    <label   className="active">
                        <small>From the total balance at the end of the year, employees can carry forward </small>
                    </label>
                    <div className="col s12 m6">
                        <input
                            id="carry_forward_limits"
                            type="radio"
                            className="with-gap"
                            name="carry_forward_limits"
                            value='0'
                            checked = {this.props.carry_forward_limits == '0'}
                            onChange={this.props.handleRadioChange }
                             
                        /> 
                        <label htmlFor="carry_forward_limits">Unlimited</label>

                        <input
                            id="carry_forward_limits1"
                            type="radio"
                            name="carry_forward_limits"
                            className="with-gap"
                            value= '1'
                            checked = {this.props.carry_forward_limits == '1'}
                            onChange={this.props.handleRadioChange }
                             
                        />  
                        <label htmlFor="carry_forward_limits1">Not more than </label>
                    </div>
                    { carry_forward_limits ? (
                        <div className="input-field col s12 m2">
                            <input
                                id="carry_forward_limits_days"
                                type="text"
                                name="carry_forward_limits_days"
                                className="with-gap"
                                value= {this.props.carry_forward_limits_days}
                                 
                                onChange={this.props.handleFieldChange }
                                
                                 
                            />  
                            <label htmlFor="carry_forward_limits_days" className="active">Days</label>
                        </div>
                        ) : ('') 
                    }
                    
                    
                </div>
            );
        }
    }
}