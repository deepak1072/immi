/**
 * Created by Sandeep Maurya on 2nd Jan 2019.
 * @3:17 am
 */

import React from 'react';

 
 
export default class WeeklyOffHolidays extends React.Component{
    constructor(props){
        super(props);
        this.state = {
            hasError : false
        }
        this.handleRadioChange   = this.handleRadioChange.bind(this);
        this.handleInputChange   = this.handleInputChange.bind(this);
        this.handlePrefixedSufixedChange   = this.handlePrefixedSufixedChange.bind(this);
    }

    static getDerivedStateFromError(error) {
        // Update state so the next render will show the fallback UI.
        return { hasError: true };
    }
  
    handleRadioChange(event){
        this.props.handleWeeklyOffCountable(event.target.value);
    }
    handleInputChange(event){
        this.props.handleHolidaysCountable(event.target.value);
    
    }
 
    handlePrefixedSufixedChange(event){
        this.props.handlePrefixedSufixed(event.target.value);
    }
    render(){
         
        if(this.state.hasError){
           return <h4 className="header2">Something went wrong</h4>
        }   else{
            return(
                <div>
                    <div className= {"input-field col  "+this.props.col}>                              
                        <label   className="active">
                            <small>Weekly off counted as day of leave  </small>
                        </label>
                        <input
                                id="weekly_off_countable"
                                type="radio"
                                className="with-gap"
                                name="weekly_off_countable"
                                value='1'
                                checked = {this.props.weekly_off_countable == '1'}
                                onChange={this.handleRadioChange }
                                 
                            /> 
                        <label htmlFor="weekly_off_countable">Counted</label>
                        <input
                                id="weekly_off_countable2"
                                type="radio"
                                name="weekly_off_countable"
                                className="with-gap"
                                value= '0'
                                checked = {this.props.weekly_off_countable == '0'}
                                onChange={this.handleRadioChange }
                                 
                            />  
                        <label htmlFor="weekly_off_countable2">Not Counted</label>
                    </div>
                    <div className= {"input-field col  "+this.props.col}>  
                        <label   className="active">
                            <small>Holiday counted as day of leave </small>
                        </label>
                        <input
                                id="holidays_countable"
                                type="radio"
                                name="holidays_countable"
                                className="with-gap"
                                value= '1'
                                checked = {this.props.holidays_countable == '1'}
                                onChange={this.handleInputChange }
                                 
                            />  
                        <label htmlFor="holidays_countable">Counted</label>
                        <input
                                id="holidays_countable4"
                                type="radio"
                                name="holidays_countable"
                                className="with-gap"
                                value= '0'
                                checked = {this.props.holidays_countable == '0'}
                                onChange={this.handleInputChange }
                                 
                            />  
                        <label htmlFor="holidays_countable4">Not Counted</label> 
                    </div>
                    <div className= "input-field col s12 m6 ">  
                        <label   className="active">
                            <small>Weekly-offs/Holidays should be counted as leave days if prefixed/suffixed with leave period</small>
                        </label>
                        <input
                                id="prefixed_sufixed"
                                type="radio"
                                name="prefixed_sufixed"
                                className="with-gap"
                                value= '1'
                                checked = {this.props.prefixed_sufixed == '1'}
                                onChange={this.handlePrefixedSufixedChange }
                                 
                            />  
                        <label htmlFor="prefixed_sufixed">Count if prefixed</label>
                        <input
                                id="prefixed_sufixed2"
                                type="radio"
                                name="prefixed_sufixed"
                                className="with-gap"
                                value= '2'
                                checked = {this.props.prefixed_sufixed == '2'}
                                onChange={this.handlePrefixedSufixedChange }
                                 
                            />  
                        <label htmlFor="prefixed_sufixed2">Count if suffixed</label> 
                        <input
                                id="prefixed_sufixed3"
                                type="radio"
                                name="prefixed_sufixed"
                                className="with-gap"
                                value= '3'
                                checked = {this.props.prefixed_sufixed == '3'}
                                onChange={this.handlePrefixedSufixedChange }
                                 
                            />  
                        <label htmlFor="prefixed_sufixed3">Count both if prefixed and suffixed both</label> 
                    </div>

                </div>
            );
        }
    }
}