/**
 * Created by Sandeep Maurya on 25th dec 2018.
 */

import React from 'react';
 
 
export default class ManagerApply extends React.Component{
    constructor(props){
        super(props);
        this.state = {
            hasError : false
        }
    }

    componentDidMount(){
       
    }

    static getDerivedStateFromError(error) {
        // Update state so the next render will show the fallback UI.
        return { hasError: true };
    }
 
 
    render(){

        if(this.state.hasError){
           return <h4 className="header2">Something went wrong</h4>
        }   else{
            return(
                <div className= {"input-field col  "+this.props.col}>                              
                    <label   className="active">
                        <small> Can manager apply for leave  </small>
                    </label>
                    <input
                        id="manager_can_apply"
                        type="radio"
                        className="with-gap"
                        name="manager_can_apply"
                        value='1'
                        checked = {this.props.manager_can_apply == '1'}
                        onChange={this.props.handleRadioChange }
                         
                    /> 
                    <label htmlFor="manager_can_apply">Yes</label>

                    <input
                        id="manager_can_apply1"
                        type="radio"
                        name="manager_can_apply"
                        className="with-gap"
                        value= '0'
                        checked = {this.props.manager_can_apply == '0'}
                        onChange={this.props.handleRadioChange }
                         
                    />  
                    <label htmlFor="manager_can_apply1">No</label>
                    
                </div>
            );
        }
    }
}