/**
 * Created by Sandeep Maurya on 8th Jan 2019.
 * @11:02PM
 */

import React ,{Fragment} from 'react';
 
 
export default class CompOffCreditLapsingRules extends React.Component{
    constructor(props){
        super(props);
        this.state = {
            hasError : false
        }
        this.handleRadioChange   = this.handleRadioChange.bind(this);
        this.handleRadio1Change   = this.handleRadio1Change.bind(this);
        this.handleInputChange   = this.handleInputChange.bind(this);


    }

    componentDidMount(){
       
    }

    static getDerivedStateFromError(error) {
        // Update state so the next render will show the fallback UI.
        return { hasError: true };
    }
 


 
    handleRadioChange(event){
        this.props.handleCompOffCreditLapseRules(event.target.value);
    }
    handleRadio1Change(event){
        this.props.handleCompOffCreditLapseBasis(event.target.value);
    }
    handleInputChange(event){
        this.props.handleCompOffCreditLapseDays(event.target.value);
    }
 
    render(){
        const request_days = this.props.compoff_credit_lapse_rules == '1' ? true : false ;
        if(this.state.hasError){
           return <h4 className="header2">Something went wrong</h4>
        }   else{
            return(
                <div className= {"input-field col  "+this.props.col}>                              
                    <label   className="active">
                        <small>Maximum period to credit & lapse Comp off as Leave balance </small>
                    </label>
                    <div className="col s12 m4">
                        <input
                            id="compoff_credit_lapse_rules"
                            type="radio"
                            className="with-gap"
                            name="compoff_credit_lapse_rules"
                            value='0'
                            checked = {this.props.compoff_credit_lapse_rules == '0'}
                            onChange={this.handleRadioChange }
                             
                        /> 
                        <label htmlFor="compoff_credit_lapse_rules">Any time</label>

                        <input
                            id="compoff_credit_lapse_rules1"
                            type="radio"
                            name="compoff_credit_lapse_rules"
                            className="with-gap"
                            value= '1'
                            checked = {this.props.compoff_credit_lapse_rules == '1'}
                            onChange={this.handleRadioChange }
                             
                        />  
                        <label htmlFor="compoff_credit_lapse_rules1">Must be </label>
                    </div>
                    { request_days ? (
                        <Fragment>
                            <div className="input-field col s12 m3">
                                <input
                                    id="compoff_credit_lapse_days"
                                    type="text"
                                    name="compoff_credit_lapse_days"
                                    className="with-gap"
                                    value= {this.props.compoff_credit_lapse_days}
                                     
                                    onChange={this.handleInputChange }
                                    
                                     
                                />  
                                <label htmlFor="compoff_credit_lapse_days" className="active"><small>Maximum days allowed </small></label>
                            </div>
                            <div className="col s12 m4">
                                <input
                                    id="compoff_credit_lapse_basis"
                                    type="radio"
                                    className="with-gap"
                                    name="compoff_credit_lapse_basis"
                                    value='0'
                                    checked = {this.props.compoff_credit_lapse_basis == '0'}
                                    onChange={this.handleRadio1Change }
                                     
                                /> 
                                <label htmlFor="compoff_credit_lapse_basis">Work done date</label>

                                <input
                                    id="compoff_credit_lapse_basis1"
                                    type="radio"
                                    name="compoff_credit_lapse_basis"
                                    className="with-gap"
                                    value= '1'
                                    checked = {this.props.compoff_credit_lapse_basis == '1'}
                                    onChange={this.handleRadio1Change }
                                     
                                />  
                                <label htmlFor="compoff_credit_lapse_basis1">Approval date </label>
                            </div>
                        </Fragment>


                        ) : ('') 
                    }
                    
                    
                </div>
            );
        }
    }
}