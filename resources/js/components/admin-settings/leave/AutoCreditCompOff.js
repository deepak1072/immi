/**
 * Created by Sandeep Maurya on 25th dec 2018.
 */

import React from 'react';
 
 
export default class AutoCreditCompOff extends React.Component{
    constructor(props){
        super(props);
        this.state = {
            hasError : false
        }
        this.handleRadioChange   = this.handleRadioChange.bind(this);

    }

    componentDidMount(){
       
    }

    static getDerivedStateFromError(error) {
        // Update state so the next render will show the fallback UI.
        return { hasError: true };
    }
 


 
    handleRadioChange(event){
        this.props.handleAutoCreditCompOff(event.target.value);
    }
 
    render(){

        if(this.state.hasError){
           return <h4 className="header2">Something went wrong</h4>
        }   else{
            return(
                <div className= {"input-field col  "+this.props.col}>                              
                    <label   className="active">
                        <small> Comp-Off credit automatically </small>
                    </label>
                    <input
                        id="auto_credit_compoff"
                        type="radio"
                        className="with-gap"
                        name="auto_credit_compoff"
                        value='1'
                        checked = {this.props.auto_credit_compoff == '1'}
                        onChange={this.handleRadioChange }
                         
                    /> 
                    <label htmlFor="auto_credit_compoff">Yes</label>

                    <input
                        id="auto_credit_compoff1"
                        type="radio"
                        name="auto_credit_compoff"
                        className="with-gap"
                        value= '0'
                        checked = {this.props.auto_credit_compoff == '0'}
                        onChange={this.handleRadioChange }
                         
                    />  
                    <label htmlFor="auto_credit_compoff1">No</label>
                    
                </div>
            );
        }
    }
}