/**
 * Created by Sandeep Maurya on 4th Jan 2019.
 * @11:23 PM
 */

import React from 'react';
 
 
export default class MaximumBalance extends React.Component{
    constructor(props){
        super(props);
        this.state = {
            hasError : false
        }
 

    }


    static getDerivedStateFromError(error) {
        // Update state so the next render will show the fallback UI.
        return { hasError: true };
    }
 
 
    render(){
        const maximum_balance = this.props.maximum_balance_allowed_anytime == '1' ? true : false ;
        if(this.state.hasError){
           return <h4 className="header2">Something went wrong</h4>
        }   else{
            return(
                <div className= {"input-field col  "+this.props.col}>                              
                    <label   className="active">
                        <small>  Maximum leave balance allowed at any time</small>
                    </label>
                    <div className="col s12 m6">
                        <input
                            id="maximum_balance_allowed_anytime"
                            type="radio"
                            className="with-gap"
                            name="maximum_balance_allowed_anytime"
                            value='0'
                            checked = {this.props.maximum_balance_allowed_anytime == '0'}
                            onChange={this.props.handleRadioChange }
                             
                        /> 
                        <label htmlFor="maximum_balance_allowed_anytime">Unlimited</label>

                        <input
                            id="maximum_balance_allowed_anytime1"
                            type="radio"
                            name="maximum_balance_allowed_anytime"
                            className="with-gap"
                            value= '1'
                            checked = {this.props.maximum_balance_allowed_anytime == '1'}
                            onChange={this.props.handleRadioChange }
                             
                        />  
                        <label htmlFor="maximum_balance_allowed_anytime1">Not more than </label>
                    </div>
                    { maximum_balance ? (
                        <div className="input-field col s12 m2">
                            <input
                                id="maximum_balance_allowed_anytime_limits"
                                type="text"
                                name="maximum_balance_allowed_anytime_limits"
                                className="with-gap"
                                value= {this.props.maximum_balance_allowed_anytime_limits}
                                 
                                onChange={this.props.handleFieldChange }
                                
                                 
                            />  
                            <label htmlFor="maximum_balance_allowed_anytime_limits" className="active">Days</label>
                        </div>
                        ) : ('') 
                    }
                    
                    
                </div>
            );
        }
    }
}