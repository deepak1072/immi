/**
 * Created by Sandeep Maurya on 29th dec 2018.
 */

import React from 'react';
 
 
export default class CreditLeave extends React.Component{
    constructor(props){
        super(props);
        this.state = {
            hasError : false
        }
        this.handleRadioChange   = this.handleRadioChange.bind(this);
        this.handleInputChange   = this.handleInputChange.bind(this);
    }

    static getDerivedStateFromError(error) {
        // Update state so the next render will show the fallback UI.
        return { hasError: true };
    }
  
    handleRadioChange(event){
        this.props.handleCreditLeaveRule(event.target.value);
    }
    handleInputChange(event){
        this.props.handleCreditLeaveAfter(event.target.value);
    }
 
    render(){
        const credit_leave  = this.props.credit_leave_rule == '3' || this.props.credit_leave_rule == '4'  ? true : false ;
         
        if(this.state.hasError){
           return <h4 className="header2">Something went wrong</h4>
        }   else{
            return(
                <div className= {"input-field col  "+this.props.col}>                              
                    <label   className="active">
                        <small>Credit leave to new employee </small>
                    </label>
                    <div className="col s12 m6">
                        <input
                            id="credit_leave_rule"
                            type="radio"
                            className="with-gap"
                            name="credit_leave_rule"
                            value='1'
                            checked = {this.props.credit_leave_rule == '1'}
                            onChange={this.handleRadioChange }
                             
                        /> 
                        <label htmlFor="credit_leave_rule">On Joining</label>

                        <input
                            id="credit_leave_rule2"
                            type="radio"
                            name="credit_leave_rule"
                            className="with-gap"
                            value= '2'
                            checked = {this.props.credit_leave_rule == '2'}
                            onChange={this.handleRadioChange }
                             
                        />  
                        <label htmlFor="credit_leave_rule2">On confirmation</label>
                        <input
                            id="credit_leave_rule3"
                            type="radio"
                            name="credit_leave_rule"
                            className="with-gap"
                            value= '3'
                            checked = {this.props.credit_leave_rule == '3'}
                            onChange={this.handleRadioChange }
                             
                        />  
                        <label htmlFor="credit_leave_rule3">After Months</label>
                        <input
                            id="credit_leave_rule4"
                            type="radio"
                            name="credit_leave_rule"
                            className="with-gap"
                            value= '4'
                            checked = {this.props.credit_leave_rule == '4'}
                            onChange={this.handleRadioChange }
                             
                        />  
                        <label htmlFor="credit_leave_rule4">After Days </label>
                    </div>
                    { credit_leave ? (
                            <div className="input-field col s12 m2">
                                <input
                                    id="credit_leave_after"
                                    type="text"
                                    name="credit_leave_after"
                                     
                                    value= {this.props.credit_leave_after}
                                    onChange={this.handleInputChange }  
                                /> 
                                <label htmlFor="credit_leave_after" className="active"> of Joining</label>
                            </div> 
                        ) : ('') 
                    }
                    
                    
                </div>
            );
        }
    }
}