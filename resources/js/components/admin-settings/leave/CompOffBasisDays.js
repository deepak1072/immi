/**
 * Created by Sandeep Maurya on 29th dec 2018.
 */

import React,{Fragment} from 'react';

export default class CompOffBasisDays extends React.Component{
    constructor(props){
        super(props);
        this.state = {
            hasError : false
        }         
    }

    static getDerivedStateFromError(error) {
        // Update state so the next render will show the fallback UI.
        return { hasError: true };
    }
     
    render(){
        const compoff_workday_basis = (this.props.compoff_workday_basis ==1 ) ? true : false;
        const half_days_work_basis = (this.props.half_credit_workday_basis == 1) ? true : false;
        const half_days_holiday_basis = (this.props.half_credit_holiday_basis == 1) ? true : false;
        const compoff_holiday_basis = (this.props.compoff_holidays_basis == 1) ? true : false;
        const compoff_weeklyoff_basis = (this.props.compoff_weeklyoff_basis == 1) ? true : false;
        const half_days_weeklyoff_basis = (this.props.half_credit_weekoff_basis == 1) ? true : false;
        if(this.state.hasError){
           return <h4 className="header2">Something went wrong</h4>
        }   else{
            return(
                <div className= {"input-field col  "+this.props.col}>                              
                    <label   className="active">
                        <small>  Comp off earn basis </small>
                    </label>
                    <div className="row  ">
                        <div className="  col offset-m1 s12 m11">
                            <input
                                id="compoff_workday_basis"
                                type="checkbox"
                                className="with-gap"
                                name="compoff_workday_basis"
                                value={this.props.compoff_workday_basis}
                                checked = {this.props.compoff_workday_basis == '1'}
                                onChange={this.props.handleCompOffBasisDays  }
                            /> 
                            <label htmlFor="compoff_workday_basis">Working extra hours on working day</label>
                        </div>
                    </div>
                    <div className="row margin-top">
                        { (compoff_workday_basis) ? (
                            <Fragment>
                                <div className="input-field col offset-m2 s12 m3">
                                    <input
                                        id="min_req_full_workday_credit"
                                        type="text"
                                        name="min_req_full_workday_credit"
                                        className="with-gap"
                                        value= {this.props.min_req_full_workday_credit} 
                                        onChange={this.props.handleInputChange  }
                                    />  
                                    <label htmlFor="min_req_full_workday_credit" className="active">
                                        <small>Min hours required to get full day credit</small>
                                    </label>
                                </div>
                                <div className= {"input-field col s12  m7 "}>                              
                                    <label   className="active">
                                        <small>  Allow half day credit </small>
                                    </label>
                                    <div className="col s12 m5">
                                        <input 
                                            id="half_credit_workday_basis"
                                            type="radio"
                                            className="with-gap"
                                            name="half_credit_workday_basis"
                                            value='0'
                                            checked = {this.props.half_credit_workday_basis == '0'}
                                            onChange={this.props.handleRadioChange  }
                                             
                                        /> 
                                        <label htmlFor="half_credit_workday_basis">No</label>

                                        <input
                                            id="half_credit_workday_basis1"
                                            type="radio"
                                            name="half_credit_workday_basis"
                                            className="with-gap"
                                            value= '1'
                                            checked = {this.props.half_credit_workday_basis == '1'}
                                            onChange={this.props.handleRadioChange  }
                                             
                                        />  
                                        <label htmlFor="half_credit_workday_basis1">Yes</label>
                                    </div>
                                    { half_days_work_basis ? (
                                        <div className="input-field col s12 m5">
                                            <input
                                                id="min_req_half_workday_credit"
                                                type="text"
                                                name="min_req_half_workday_credit"
                                                className="with-gap"
                                                value= {this.props.min_req_half_workday_credit}
                                                 
                                                onChange={this.props.handleInputChange  }
                                                 
                                            />  
                                            <label htmlFor="min_req_half_workday_credit" className="active">
                                                <small>Min hours required to get half day credit</small>
                                            </label>
                                        </div>
                                        ) : ('') 
                                    }
                                     
                                </div>
                                   
                            </Fragment>
                                    
                            ) : ('')
                        }
                    </div>
                    <div className="row">   
                        <div className="  col offset-m1 s12 m11">
                            <input
                                id="compoff_holidays_basis"
                                type="checkbox"
                                className="with-gap"
                                name="compoff_holidays_basis"
                                value={this.props.compoff_holidays_basis}
                                checked = {this.props.compoff_holidays_basis == '1'}
                                onChange={this.props.handleCompOffBasisDays }
                                 
                            /> 
                            <label htmlFor="compoff_holidays_basis">Working on company holiday</label>
                        </div>
                    </div>
                    <div className="row margin-top">
                        { (compoff_holiday_basis) ? (
                            <Fragment>
                                <div className="input-field col offset-m2 s12 m3">
                                    <input
                                        id="min_req_full_holiday_credit"
                                        type="text"
                                        name="min_req_full_holiday_credit"
                                        className="with-gap"
                                        value= {this.props.min_req_full_holiday_credit} 
                                        onChange={this.props.handleInputChange  }
                                    />  
                                    <label htmlFor="min_req_full_holiday_credit" className="active">
                                        <small>Min hours required to get full day credit</small>
                                    </label>
                                </div>
                                <div className= {"input-field col s12  m7 "}>                              
                                    <label   className="active">
                                        <small>  Allow half day credit </small>
                                    </label>
                                    <div className="col s12 m5">
                                        <input 
                                            id="half_credit_holiday_basis"
                                            type="radio"
                                            className="with-gap"
                                            name="half_credit_holiday_basis"
                                            value='0'
                                            checked = {this.props.half_credit_holiday_basis == '0'}
                                            onChange={this.props.handleRadioChange  }
                                             
                                        /> 
                                        <label htmlFor="half_credit_holiday_basis">No</label>

                                        <input
                                            id="half_credit_holiday_basis1"
                                            type="radio"
                                            name="half_credit_holiday_basis"
                                            className="with-gap"
                                            value= '1'
                                            checked = {this.props.half_credit_holiday_basis == '1'}
                                            onChange={this.props.handleRadioChange  }
                                             
                                        />  
                                        <label htmlFor="half_credit_holiday_basis1">Yes</label>
                                    </div>
                                    { half_days_holiday_basis ? (
                                        <div className="input-field col s12 m5">
                                            <input
                                                id="min_req_half_holiday_credit"
                                                type="text"
                                                name="min_req_half_holiday_credit"
                                                className="with-gap"
                                                value= {this.props.min_req_half_holiday_credit} 
                                                onChange={this.props.handleInputChange  }   
                                            />  
                                            <label htmlFor="min_req_half_holiday_credit" className="active">
                                                <small>Min hours required to get half day credit</small>
                                            </label>
                                        </div>
                                        ) : ('') 
                                    } 
                                </div>   
                            </Fragment>   
                            ) : ('')
                        }
                    </div>
                    <div className="row">
                        <div className="  col offset-m1 s12 m11 ">
                            <input
                                id="compoff_weeklyoff_basis"
                                type="checkbox"
                                className="with-gap"
                                name="compoff_weeklyoff_basis"
                                value={this.props.compoff_weeklyoff_basis}
                                checked = {this.props.compoff_weeklyoff_basis == '1'}
                                onChange={this.props.handleCompOffBasisDays  }   
                            /> 
                            <label htmlFor="compoff_weeklyoff_basis">Working on weekly-off</label>
                        </div> 
                    </div>
                    <div className="row margin-top">
                        { (compoff_weeklyoff_basis) ? (
                            <Fragment>
                                <div className="input-field col offset-m2 s12 m3">
                                    <input
                                        id="min_req_full_weekoff_credit"
                                        type="text"
                                        name="min_req_full_weekoff_credit"
                                        className="with-gap"
                                        value= {this.props.min_req_full_weekoff_credit} 
                                        onChange={this.props.handleInputChange  }
                                    />  
                                    <label htmlFor="min_req_full_weekoff_credit" className="active">
                                        <small>Min hours required to get full day credit</small>
                                    </label>
                                </div>
                                <div className= {"input-field col s12  m7 "}>                              
                                    <label   className="active">
                                        <small>  Allow half day credit </small>
                                    </label>
                                    <div className="col s12 m5">
                                        <input 
                                            id="half_credit_weekoff_basis"
                                            type="radio"
                                            className="with-gap"
                                            name="half_credit_weekoff_basis"
                                            value='0'
                                            checked = {this.props.half_credit_weekoff_basis == '0'}
                                            onChange={this.props.handleRadioChange  }    
                                        /> 
                                        <label htmlFor="half_credit_weekoff_basis">No</label>
                                        <input
                                            id="half_credit_weekoff_basis1"
                                            type="radio"
                                            name="half_credit_weekoff_basis"
                                            className="with-gap"
                                            value= '1'
                                            checked = {this.props.half_credit_weekoff_basis == '1'}
                                            onChange={this.props.handleRadioChange  }
                                             
                                        />  
                                        <label htmlFor="half_credit_weekoff_basis1">Yes</label>
                                    </div>
                                    { half_days_weeklyoff_basis ? (
                                        <div className="input-field col s12 m5">
                                            <input
                                                id="min_req_half_weekoff_credit"
                                                type="text"
                                                name="min_req_half_weekoff_credit"
                                                className="with-gap"
                                                value= {this.props.min_req_half_weekoff_credit}  
                                                onChange={this.props.handleInputChange  }
                                                 
                                            />  
                                            <label htmlFor="min_req_half_weekoff_credit" className="active">
                                                <small>Min hours required to get half day credit</small>
                                            </label>
                                        </div>
                                        ) : ('') 
                                    }  
                                </div> 
                            </Fragment>
                                
                            ) : ('')
                        }
                    </div>    
                </div>
            );
        }
    }
}