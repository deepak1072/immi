/**
 * Created by Sandeep Maurya on 29th dec 2018.
 */

import React from 'react';
export default class LeaveDurationNotExceeds extends React.Component{
    constructor(props){
        super(props);
        this.state = {
            hasError : false
        }
         
    }
    static getDerivedStateFromError(error) {
        // Update state so the next render will show the fallback UI.
        return { hasError: true };
    }

   
    
 
    render(){
         
        if(this.state.hasError){
           return <h4 className="header2">Something went wrong</h4>
        }   else{
            return(
                <div className= {"input-field col  "+this.props.col}>                              
                    <label   className="active">
                        <small>  Leave duration should not exceed  </small>
                    </label>
                    <div className="col s12 m12">
                        <input
                            id="leave_duration_not_exceeds"
                            type="radio"
                            className="with-gap"
                            name="leave_duration_not_exceeds"
                            value='1'
                            checked = {this.props.leave_duration_not_exceeds == '1'}
                            onChange={this.props.handleRadioChange }
                             
                        /> 
                        <label htmlFor="leave_duration_not_exceeds"> Balance at time of requesting</label>

                        <input
                            id="leave_duration_not_exceeds1"
                            type="radio"
                            name="leave_duration_not_exceeds"
                            className="with-gap"
                            value= '2'
                            checked = {this.props.leave_duration_not_exceeds == '2'}
                            onChange={this.props.handleRadioChange }
                             
                        />  
                        <label htmlFor="leave_duration_not_exceeds1">Balance at time of requesting plus what would be credited in the remaining leave year</label>
                    </div> 
                </div>
            );
        }
    }
}