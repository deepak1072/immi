/**
 * Created by Sandeep Maurya on 25th dec 2018.
 */

import React from 'react';
 
 
export default class LeaveTracking extends React.Component{
    constructor(props){
        super(props);
        this.state = {
            hasError : false
        }
         

    }

    componentDidMount(){
       
    }

    static getDerivedStateFromError(error) {
        // Update state so the next render will show the fallback UI.
        return { hasError: true };
    }
 


 
    
 
    render(){

        if(this.state.hasError){
           return <h4 className="header2">Something went wrong</h4>
        }   else{
            return(
                <div className= {"input-field col  "+this.props.col}>                              
                    <label   className="active">
                        <small> Track planned/unplanned leaves  </small>
                    </label>
                    <input
                        id="leave_track"
                        type="radio"
                        className="with-gap"
                        name="leave_track"
                        value='1'
                        checked = {this.props.leave_track == '1'}
                        onChange={this.props.handleRadioChange }
                         
                    /> 
                    <label htmlFor="leave_track">Yes</label>

                    <input
                        id="leave_track1"
                        type="radio"
                        name="leave_track"
                        className="with-gap"
                        value= '0'
                        checked = {this.props.leave_track == '0'}
                        onChange={this.props.handleRadioChange }
                         
                    />  
                    <label htmlFor="leave_track1">No</label>
                    
                </div>
            );
        }
    }
}