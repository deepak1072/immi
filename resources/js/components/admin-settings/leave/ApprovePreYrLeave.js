/**
 * Created by Sandeep Maurya on 28th dec 2018.
 */

import React from 'react';
 
 
export default class ApprovePreYrLeave extends React.Component{
    constructor(props){
        super(props);
        this.state = {
            hasError : false
        }
    }

    componentDidMount(){
       
    }

    static getDerivedStateFromError(error) {
        // Update state so the next render will show the fallback UI.
        return { hasError: true };
    }
 
 
    render(){

        if(this.state.hasError){
           return <h4 className="header2">Something went wrong</h4>
        }   else{
            return(
                <div className= {"input-field col  "+this.props.col}>                              
                    <label   className="active">
                        <small> Can approve leave of previous leave year  </small>
                    </label>
                    <input
                        id="approve_pre_yr_leave"
                        type="radio"
                        className="with-gap"
                        name="approve_pre_yr_leave"
                        value='1'
                        checked = {this.props.approve_pre_yr_leave == '1'}
                        onChange={this.props.handleRadioChange }
                         
                    /> 
                    <label htmlFor="approve_pre_yr_leave">Yes</label>

                    <input
                        id="approve_pre_yr_leave1"
                        type="radio"
                        name="approve_pre_yr_leave"
                        className="with-gap"
                        value= '0'
                        checked = {this.props.approve_pre_yr_leave == '0'}
                        onChange={this.props.handleRadioChange }
                         
                    />  
                    <label htmlFor="approve_pre_yr_leave1">No</label>
                    
                </div>
            );
        }
    }
}