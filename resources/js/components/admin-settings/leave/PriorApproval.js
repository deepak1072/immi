/**
 * Created by Sandeep Maurya on 28th dec 2018.
 */

import React from 'react';
 
 
export default class PriorApproval extends React.Component{
    constructor(props){
        super(props);
        this.state = {
            hasError : false
        }
         
    }

    static getDerivedStateFromError(error) {
        // Update state so the next render will show the fallback UI.
        return { hasError: true };
    }
 
    render(){
        const approval_days = this.props.prior_approval_required == '1' ? true : false ;
        if(this.state.hasError){
           return <h4 className="header2">Something went wrong</h4>
        }   else{
            return(
                <div className= {"input-field col  "+this.props.col}>                              
                    <label   className="active">
                        <small>  Leave request time (Prior Approval) </small>
                    </label>
                    <div className="col s12 m4">
                        <input
                            id="prior_approval_required"
                            type="radio"
                            className="with-gap"
                            name="prior_approval_required"
                            value='0'
                            checked = {this.props.prior_approval_required == '0'}
                            onChange={this.props.handleRadioChange }
                             
                        /> 
                        <label htmlFor="prior_approval_required">Any Time</label>

                        <input
                            id="prior_approval_required1"
                            type="radio"
                            name="prior_approval_required"
                            className="with-gap"
                            value= '1'
                            checked = {this.props.prior_approval_required == '1'}
                            onChange={this.props.handleRadioChange }
                             
                        />  
                        <label htmlFor="prior_approval_required1">Must be made</label>
                    </div>
                    { approval_days ? (
                        <div className="input-field col s12 m2">
                            <input
                                id="prior_approval_days"
                                type="text"
                                name="prior_approval_days"
                                className="with-gap"
                                value= {this.props.prior_approval_days} 
                                onChange={this.props.handleFieldChange }
                            />  
                            <label htmlFor="prior_approval_days" className="active">
                                <small>Days before leave start</small>
                            </label>
                        </div>
                        ) : ('') 
                    }
                    
                    
                </div>
            );
        }
    }
}