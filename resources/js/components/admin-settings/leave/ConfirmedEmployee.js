/**
 * Created by Sandeep Maurya on 25th dec 2018.
 */

import React from 'react';
 
 
export default class ConfirmedEmployee extends React.Component{
    constructor(props){
        super(props);
        this.state = {
            hasError : false
        }
       

    }

    componentDidMount(){
       
    }

    static getDerivedStateFromError(error) {
        // Update state so the next render will show the fallback UI.
        return { hasError: true };
    }
 


  
 
    render(){

        if(this.state.hasError){
           return <h4 className="header2">Something went wrong</h4>
        }   else{
            return(
                <div className= {"input-field col  "+this.props.col}>                              
                    <label   className="active">
                        <small> Only confirmed employee can apply for leave   </small>
                    </label>
                    <input
                        id="only_confirmed_emp_eligible"
                        type="radio"
                        className="with-gap"
                        name="only_confirmed_emp_eligible"
                        value='1'
                        checked = {this.props.only_confirmed_emp_eligible == '1'}
                        onChange={this.props.handleRadioChange }
                         
                    /> 
                    <label htmlFor="only_confirmed_emp_eligible">Yes</label>

                    <input
                        id="only_confirmed_emp_eligible1"
                        type="radio"
                        name="only_confirmed_emp_eligible"
                        className="with-gap"
                        value= '0'
                        checked = {this.props.only_confirmed_emp_eligible == '0'}
                        onChange={this.props.handleRadioChange }
                         
                    />  
                    <label htmlFor="only_confirmed_emp_eligible1">No</label>
                    
                </div>
            );
        }
    }
}