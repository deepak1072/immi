/**
 * Created by Sandeep Maurya on 29th dec 2018.
 */

import React from 'react';

 
 
export default class FirstPeriodCreditDoe extends React.Component{
    constructor(props){
        super(props);
        this.state = {
            hasError : false
        }
        this.handleRadioChange   = this.handleRadioChange.bind(this);
        this.handleInputChange   = this.handleInputChange.bind(this);
    }

    static getDerivedStateFromError(error) {
        // Update state so the next render will show the fallback UI.
        return { hasError: true };
    }
  
    handleRadioChange(event){
        this.props.handleFirstCreditLeaveRule(event.target.value);
    }
    handleInputChange(event){
        this.props.handleCreditLeaveAfter(event.target.value);
    }
 
    render(){
        const credit_leave_4  = this.props.first_period_credit_rule == '4'  ? true : false ;
        const credit_leave_5 =  this.props.first_period_credit_rule == '5' ? true : false;
        if(this.state.hasError){
           return <h4 className="header2">Something went wrong</h4>
        }   else{
            return(
                <div className= {"input-field col  "+this.props.col}>                              
                    <label   className="active">
                        <small>Leave credit on the first period of the date of entitlement (DoE)  </small>
                    </label>
                    <div className="col s12 m12">
                        <input
                            id="first_period_credit_rule"
                            type="radio"
                            className="with-gap"
                            name="first_period_credit_rule"
                            value='1'
                            checked = {this.props.first_period_credit_rule == '1'}
                            onChange={this.handleRadioChange }
                             
                        /> 
                        <label htmlFor="first_period_credit_rule">If DoE is after 1st of month, do not credit that month's leave required</label>
                    </div>
                    <div className="col s12 m12">
                        <input
                            id="first_period_credit_rule2"
                            type="radio"
                            name="first_period_credit_rule"
                            className="with-gap"
                            value= '2'
                            checked = {this.props.first_period_credit_rule == '2'}
                            onChange={this.handleRadioChange }
                             
                        />  
                        <label htmlFor="first_period_credit_rule2">Credit complete period's leave irrespective of DoE</label>
                    </div>
                    <div className="col s12 m12">
                        <input
                            id="first_period_credit_rule3"
                            type="radio"
                            name="first_period_credit_rule"
                            className="with-gap"
                            value= '3'
                            checked = {this.props.first_period_credit_rule == '3'}
                            onChange={this.handleRadioChange }
                             
                        />  
                        <label htmlFor="first_period_credit_rule3">Treat DoE as it falls and compute first period leave on a prorated days basis</label>
                    </div>
                    <div className="col s12 m12">
                        <input
                            id="first_period_credit_rule4"
                            type="radio"
                            name="first_period_credit_rule"
                            className="with-gap"
                            value= '4'
                            checked = {this.props.first_period_credit_rule == '4'}
                            onChange={this.handleRadioChange }
                             
                        />  
                        <label htmlFor="first_period_credit_rule4">Depends on when the DoE falls</label>
                    </div>
                    <div className="col s12 m12">
                        <input
                            id="first_period_credit_rule5"
                            type="radio"
                            name="first_period_credit_rule"
                            className="with-gap"
                            value= '5'
                            checked = {this.props.first_period_credit_rule == '5'}
                            onChange={this.handleRadioChange }
                             
                        />  
                        <label htmlFor="first_period_credit_rule5">Special Accrual based on working days</label>
                    </div>
                     
                </div>
            );
        }
    }
}