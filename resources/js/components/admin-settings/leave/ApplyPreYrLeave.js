/**
 * Created by Sandeep Maurya on 25th dec 2018.
 */

import React from 'react';
 
 
export default class ApplyPreYrLeave extends React.Component{
    constructor(props){
        super(props);
        this.state = {
            hasError : false
        }
   
    }

 

    static getDerivedStateFromError(error) {
        // Update state so the next render will show the fallback UI.
        return { hasError: true };
    }
 
 
 
    render(){

        if(this.state.hasError){
           return <h4 className="header2">Something went wrong</h4>
        }   else{
            return(
                <div className= {"input-field col  "+this.props.col}>                              
                    <label   className="active">
                        <small> Can apply for leave in previous leave year </small>
                    </label>
                    <input
                        id="apply_pre_yr_leave"
                        type="radio"
                        className="with-gap"
                        name="apply_pre_yr_leave"
                        value='1'
                        checked = {this.props.apply_pre_yr_leave == '1'}
                        onChange={this.props.handleRadioChange }
                         
                    /> 
                    <label htmlFor="apply_pre_yr_leave">Yes</label>

                    <input
                        id="apply_pre_yr_leave1"
                        type="radio"
                        name="apply_pre_yr_leave"
                        className="with-gap"
                        value= '0'
                        checked = {this.props.apply_pre_yr_leave == '0'}
                        onChange={this.props.handleRadioChange }
                         
                    />  
                    <label htmlFor="apply_pre_yr_leave1">No</label>
                    
                </div>
            );
        }
    }
}