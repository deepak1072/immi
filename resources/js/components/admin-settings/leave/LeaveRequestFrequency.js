/**
 * Created by Sandeep Maurya on 28th dec 2018.
 */

import React from 'react';
 
 
export default class LeaveRequestFrequency extends React.Component{
    constructor(props){
        super(props);
        this.state = {
            hasError : false
        }
 
    }

    componentDidMount(){
       
    }

    static getDerivedStateFromError(error) {
        // Update state so the next render will show the fallback UI.
        return { hasError: true };
    }
  
 
    render(){
        const frequency = this.props.leave_request_frequency == '1' ? true : false ;
        if(this.state.hasError){
           return <h4 className="header2">Something went wrong</h4>
        }   else{
            return(
                <div className= {"input-field col  "+this.props.col}>                              
                    <label   className="active">
                        <small>  Leave Request Frequency </small>
                    </label>
                    <div className="col s12 m6">
                        <input
                            id="leave_request_frequency"
                            type="radio"
                            className="with-gap"
                            name="leave_request_frequency"
                            value='0'
                            checked = {this.props.leave_request_frequency == '0'}
                            onChange={this.props.handleRadioChange }
                             
                        /> 
                        <label htmlFor="leave_request_frequency">Any number of times</label>

                        <input
                            id="leave_request_frequency1"
                            type="radio"
                            name="leave_request_frequency"
                            className="with-gap"
                            value= '1'
                            checked = {this.props.leave_request_frequency == '1'}
                            onChange={this.props.handleRadioChange }
                             
                        />  
                        <label htmlFor="leave_request_frequency1">Limited number of times</label>
                    </div>
                    { frequency ? (
                        <div className="input-field col s12 m2">
                            <input
                                id="leave_request_frequency_count"
                                type="text"
                                name="leave_request_frequency_count"
                                className="with-gap"
                                value= {this.props.leave_request_frequency_count}
                                 
                                onChange={this.props.handleFieldChange }
                                
                                 
                            />  
                            <label htmlFor="leave_request_frequency_count" className="active">
                                <small>max times in a leave year</small>
                            </label>
                        </div>
                        ) : ('') 
                    }
                    
                    
                </div>
            );
        }
    }
}