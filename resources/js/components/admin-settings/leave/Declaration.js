/**
 * Created by Sandeep Maurya on 29th dec 2018.
 */

import React from 'react';
 
 
export default class Declaration extends React.Component{
    constructor(props){
        super(props);
        this.state = {
            hasError : false
        }
    }
 

    static getDerivedStateFromError(error) {
        // Update state so the next render will show the fallback UI.
        return { hasError: true };
    }
  
 
    render(){
        const declaration = this.props.declaration_required == '0' ? false : true ;
        if(this.state.hasError){
           return <h4 className="header2">Something went wrong</h4>
        }   else{
            return(
                <div className= {"input-field col  "+this.props.col}>                              
                    <label   className="active">
                        <small>  Declaration on leave request </small>
                    </label>
                    <div className="col s12 m6">
                        <input
                            id="declaration_required"
                            type="radio"
                            className="with-gap"
                            name="declaration_required"
                            value='0'
                            checked = {this.props.declaration_required == '0'}
                            onChange={this.props.handleRadioChange }
                             
                        /> 
                        <label htmlFor="declaration_required">Not Required</label>

                        <input
                            id="declaration_required1"
                            type="radio"
                            name="declaration_required"
                            className="with-gap"
                            value= '1'
                            checked = {this.props.declaration_required == '1'}
                            onChange={this.props.handleRadioChange }
                             
                        />  
                        <label htmlFor="declaration_required1">Required but Optional</label>
                        <input
                            id="declaration_required2"
                            type="radio"
                            name="declaration_required"
                            className="with-gap"
                            value= '2'
                            checked = {this.props.declaration_required == '2'}
                            onChange={this.props.handleRadioChange }
                             
                        />  
                        <label htmlFor="declaration_required2">Required and Mandatory</label>
                    </div>
                    { declaration ? (
                        <div className="input-field col s12 m4">
                            <input
                                id="declaration_label"
                                type="text"
                                name="declaration_label"
                                className="with-gap"
                                value= {this.props.declaration_label}
                                 
                                onChange={this.props.handleFieldChange }
                                
                                 
                            />  
                            <label htmlFor="declaration_label" className="active">Declaration Label</label>
                        </div>
                        ) : ('') 
                    }
                    
                    
                </div>
            );
        }
    }
}