/**
 * Created by Sandeep Maurya on 29th dec 2018.
 */

import React from 'react';
export default class SpecialAccrual extends React.Component{
    constructor(props){
        super(props);
        this.state = {
            hasError : false
        }
        this.handleRadioChange   = this.handleRadioChange.bind(this);
        this.handleSpecialWorking   = this.handleSpecialWorking.bind(this);
        this.handleSpecialLeave   = this.handleSpecialLeave.bind(this);
        this.handleSpecialWeekOff   = this.handleSpecialWeekOff.bind(this);
        this.handleSpecialHolidays   = this.handleSpecialHolidays.bind(this);
    }

    static getDerivedStateFromError(error) {
        // Update state so the next render will show the fallback UI.
        return { hasError: true };
    }
  
    handleRadioChange(event){
        this.props.handleSpecialAccrualBase(event.target.value);
    }
    handleSpecialWorking(event){
        this.props.handleSpecialWorking(event.target.value);
    }
    handleSpecialLeave(event){
        this.props.handleSpecialLeave(event.target.value);
    }
    handleSpecialHolidays(event){
        this.props.handleSpecialHolidays(event.target.value);
    }
    handleSpecialWeekOff(event){
        this.props.handleSpecialWeekOff(event.target.value);
    }
 
    render(){
       
        if(this.state.hasError){
           return <h4 className="header2">Something went wrong</h4>
        }   else{
            return(
                <div className= {"input-field col offset-m1 offset-s1 "+this.props.col}>                              
                     
                    <div className= " col s10 m10">
                        <input
                            id="special_accrual_working_days"
                            type="checkbox"
                            className=""
                            name="special_accrual_working_days"
                            value= {this.props.special_accrual_working_days}
                            checked = {this.props.special_accrual_working_days == '1'}
                            onChange={this.handleSpecialWorking }
                             
                        /> 
                        <label htmlFor="special_accrual_working_days">Include Working Days / Present Days</label>
                    </div>
                    <div className=" col s10 m10">
                        <input
                            id="special_accrual_leave_days"
                            type="checkbox"
                            name="special_accrual_leave_days"
                            className="with-gap"
                            value= {this.props.special_accrual_leave_days}
                            checked = {this.props.special_accrual_leave_days == '1'}
                            onChange={this.handleSpecialLeave }
                             
                        />  
                        <label htmlFor="special_accrual_leave_days">Include Leave Days</label>
                    </div>
                    <div className="  col s10 m10">
                        <input
                            id="special_accrual_weeklyoff"
                            type="checkbox"
                            name="special_accrual_weeklyoff"
                            className="with-gap"
                            value= {this.props.special_accrual_weeklyoff}
                            checked = {this.props.special_accrual_weeklyoff == '1'}
                            onChange={this.handleSpecialWeekOff }
                             
                        />  
                        <label htmlFor="special_accrual_weeklyoff">Include Weekly Off</label>
                    </div>
                    <div className=" col s10 m10">
                        <input
                            id="special_accrual_holidays"
                            type="checkbox"
                            name="special_accrual_holidays"
                            className="with-gap"
                            value= {this.props.special_accrual_holidays}
                            checked = {this.props.special_accrual_holidays == '1'}
                            onChange={this.handleSpecialHolidays }
                             
                        />  
                        <label htmlFor="special_accrual_holidays">Include Holidays</label>
                    </div>
                     
                     <div className="  col s10 m10">
                        <input
                            id="calendar_days"
                            type="radio"
                            name="special_accrual_base"
                            className="with-gap"
                            value= '1'
                            checked = {this.props.special_accrual_base == '1'}
                            onChange={this.handleRadioChange }
                             
                        />  
                        <label htmlFor="calendar_days">Calendar Days</label>
                        <input
                            id="working_days"
                            type="radio"
                            name="special_accrual_base"
                            className="with-gap"
                            value= '2'
                            checked = {this.props.special_accrual_base == '2'}
                            onChange={this.handleRadioChange }
                             
                        />  
                        <label htmlFor="working_days">Working Days</label>
                    </div>
                </div>
            );
        }
    }
}