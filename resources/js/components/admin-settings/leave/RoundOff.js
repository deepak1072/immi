/**
 * Created by Sandeep Maurya on 29th dec 2018.
 */

import React from 'react';
 
 
export default class RoundOff extends React.Component{
    constructor(props){
        super(props);
        this.state = {
            hasError : false
        }
       
    }

    static getDerivedStateFromError(error) {
        // Update state so the next render will show the fallback UI.
        return { hasError: true };
    }
  
    
 
    render(){
        const round_off  = this.props.round_off_rule == '1' ? true : false ;
        if(this.state.hasError){
           return <h4 className="header2">Something went wrong</h4>
        }   else{
            return(
                <div className= {"input-field col  "+this.props.col}>                              
                    <label   className="active">
                        <small>While crediting leaves </small>
                    </label>
                    <div className="col s12 m8">
                        <input
                            id="round_off_rule"
                            type="radio"
                            className="with-gap"
                            name="round_off_rule"
                            value='1'
                            checked = {this.props.round_off_rule == '1'}
                            onChange={this.props.handleRadioChange }
                             
                        /> 
                        <label htmlFor="round_off_rule">Round down to</label>

                        <input
                            id="round_off_rule2"
                            type="radio"
                            name="round_off_rule"
                            className="with-gap"
                            value= '2'
                            checked = {this.props.round_off_rule == '2'}
                            onChange={this.props.handleRadioChange }
                             
                        />  
                        <label htmlFor="round_off_rule2">Round off to the lowest</label>
                        <input
                            id="round_off_rule3"
                            type="radio"
                            name="round_off_rule"
                            className="with-gap"
                            value= '3'
                            checked = {this.props.round_off_rule == '3'}
                            onChange={this.props.handleRadioChange }
                             
                        />  
                        <label htmlFor="round_off_rule3">Round off to the highest</label>
                        <input
                            id="round_off_rule4"
                            type="radio"
                            name="round_off_rule"
                            className="with-gap"
                            value= '4'
                            checked = {this.props.round_off_rule == '4'}
                            onChange={this.props.handleRadioChange }
                             
                        />  
                        <label htmlFor="round_off_rule4">Round off to the nearest</label>
                        
                    </div>
                    { round_off ? (
                            <div className="input-field col s12 m2">
                                <select
                                    id="round_off_value"
                                    type="text"
                                    name="round_off_value"
                                    className="browser-default"
                                    value= {this.props.round_off_value}
                                     
                                    onChange={this.props.handleFieldChange } > 
                                    <option value='1' >One</option> 
                                    <option value='2' >Two</option> 
                                     
                                    </select>
                                <label htmlFor="round_off_value" className="active">Decimal Places</label>
                            </div> 
                        ) : (
                           <div className="input-field col s12 m2">
                                <select
                                    id="round_off_value"
                                    type="text"
                                    name="round_off_value"
                                    className="browser-default"
                                    value= {this.props.round_off_value}
                                     
                                    onChange={this.props.handleFieldChange } > 
                                    <option value='1' >Full</option> 
                                    <option value='2' >Half</option> 
                                    <option value='3' >Quarter</option> 
                                    </select>
                                <label htmlFor="round_off_value" className="active">Day</label>
                            </div> 
                        ) 
                    }
                    
                    
                </div>
            );
        }
    }
}