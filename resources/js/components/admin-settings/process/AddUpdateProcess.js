/**
 * Created by Sandeep Maurya on 15th dec 2018.
 */

import React from 'react';
import axios from 'axios';
import { toast } from 'react-toastify';
import {Link} from 'react-router-dom';
import {authHeader,logout,ITEM_PER_PAGE} from  "./../../../helpers/responseAuthozisation";
 


export default class AddUpdateProcess extends React.Component{
    constructor(props){
        super(props);

        this.state = {
            process_name : '',
            process_code : '', 
            process_status:0,
            create_at : '',
            created_by : '',
            message : '',
            flag : true ,
            hasError:false,
            action: 'Create',
             
            master_company:[],
             
        }


        this.handleAddUpdateProcess = this.handleAddUpdateProcess.bind(this);
        this.handleFieldChange   = this.handleFieldChange.bind(this);

    }


    componentDidMount(){

        let self = this;
        const { id } = this.props.match.params;
        
        if(id != undefined){
        
            axios.get('/api/process-details',{
                params:{
                    id : id
                },
                headers : authHeader()
            }).then(function(response){
                if(response.status == 200){
                    const data = response.data[0];
                    self.setState({
                        process_name : data['process_name'],
                        process_id : data['process_id'], 
                        process_code: data['process_code'] , 
                        action : 'Update' 
                        
                    });
                    
                }else{

                    toast(({ closeToast }) => <div>Something went wrong , please try after some time  </div>);
                    history.push('/process-setup') ;   
                }
                

            }).catch(function(error){
                 switch(error.response.status){
                    case 500 :
                        // logout();
                        toast(({ closeToast }) => <div> {error.response.data.message} </div>); 
                        history.push('/process-setup') ;   
                        break ;
                    case 401 :
                        toast(({ closeToast }) => <div> {error.response.data.error} </div>);
                        logout();
                        break;
                    default :
                        toast(({ closeToast }) => <div> could not fetch process list </div>); 
                        history.push('/process-setup') ;   
                        break;
                } 
            });
        }
 
    }

    static getDerivedStateFromError(error) {
        // Update state so the next render will show the fallback UI.
        return { hasError: true };
    }

    /**
     * form validation
     * @returns {boolean|*}
     */
    validate(){
    	 this.state.flag = true;
          
        if(this.state.process_name =='' || this.state.process_name == 'undefined'){
            this.state.message = 'Please enter process name';
            this.state.flag = false;
        }
        
        if(this.state.process_code =='' || this.state.process_code == 'undefined'){
            this.state.message = 'Please enter process code';
            this.state.flag = false;
        }
      
        return this.state.flag;
    }



    handleAddUpdateProcess(event){
        event.preventDefault();
        const {history} = this.props;
        const processs = this.state;

        
        if(this.validate()){
            axios.post('/api/add-update-process', processs,{headers : authHeader()})
                .then(response => {
                    if(response.data.result){
                        toast(({ closeToast }) => <div>{response.data.message}</div>);
                        history.push('/process-setup') ;
                    } else{
                        response.data.error.map((errors)=>{
                            toast(({ closeToast }) => <div>{errors} </div>);
                        });
                    }
                })
                .catch(error => {
                     
                    switch(error.response.status){
                        case 500 :
                            // logout();
                            toast(({ closeToast }) => <div> {error.response.data.message} </div>); 
                            history.push('/process-setup') ;   
                            break ;
                        case 401 :
                            toast(({ closeToast }) => <div> {error.response.data.error} </div>);
                            logout();
                            break;
                        default :
                            toast(({ closeToast }) => <div> could not update  </div>); 
                            history.push('/process-setup') ;   
                            break;
                    } 
                })
        }  else{
            toast(({ closeToast }) => <div>{this.state.message}</div>)
        }

    }
    handleFieldChange(event){
        
        this.setState({
            [event.target.name] : event.target.value
        });
    }
 
    render(){

        if(this.state.hasError){
           return <h4 className="header2">Something went wrong</h4>
        }   else{
            return(
                <div className="dashboard">
                    <div className="card-panel">
                        <div className="row">
                            <form className="col s12" onSubmit={this.handleAddUpdateProcess}>
                                <div id="leave_htmlForm">
                                    <div className="row">                                                
                                        
                                        <div className="col s12 l12 center-align">
                                            <h4 className="header2">{this.state.action} Process </h4>
                                        </div>
                                    </div>
                                    <div className="row"> 
                                            
                                        <div className="input-field col s3">
                                            <input
                                                id="process_code"
                                                type="text"
                                                name="process_code"
                                                value={this.state.process_code}
                                                onChange={this.handleFieldChange}
                                            /> 
                                            <label htmlFor="process_code" className="active">Process Code * 
                                            </label>
                                        </div>

                                        <div className="input-field col s3">
                                            <input
                                                id="process_name"
                                                type="text"
                                                name="process_name"
                                                value={this.state.process_name}
                                                onChange={this.handleFieldChange}
                                            /> 
                                            <label htmlFor="process_name" className="active">Process Name * 
                                            </label>
                                        </div>

                                        

                            
                                    </div>
                                        
                                            
                                        
                                </div>
                                <div className="row">                                            
                                    <div className="input-field col s12">
                                        <Link to="/process-setup">
                                            <button className="btn default   waves-light"   name="action">
                                                <i className="mdi-navigation-arrow-back left "></i>
                                                cancel
                                            </button>
                                        </Link>
                                        <button className="btn cyan waves-effect waves-light  " type="submit" name="action">{(this.state.action)}
                                            <i className="mdi-content-send right"></i>
                                        </button>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            );
        }
    }
}