/**
 * Created by Sandeep Maurya on 14th dec 2018
 */

import React from "react";
import _ from "lodash";
import axios from 'axios';
import { toast } from 'react-toastify';
 // Import React Table
import ReactTable from "react-table";
import "react-table/react-table.css";
import {Link} from  'react-router-dom';

import {authHeader,logout,ITEM_PER_PAGE} from  "./../../../helpers/responseAuthozisation";



export  default class ProcessList extends React.Component {
    constructor(props) {
        super(props);
        this.state = {

            data: [],
            pages: null,
            loading: true
        };
        this.fetchData = this.fetchData.bind(this);
        this.handleStatus = this.handleStatus.bind(this);
    }

    fetchData(state, instance) {
        // Whenever the table model changes, or the user sorts or changes pages, this method gets called and passed the current table model.
        // You can set the `loading` prop of the table to true to use the built-in one or show you're own loading bar if you want.
        this.setState({ loading: true });
    
        // Request the data however you want.  Here, we'll use our mocked service we created earlier
        this.requestData(
            state.pageSize,
            state.page,
            state.sorted,
            state.filtered
        ).then((res) => {
            // Now just get the rows of data to your React Table (and update anything else like total pages or loading)

            this.setState({
                data: res.rows,
                pages: res.pages,
                loading: false
            });
        });
    }

     requestData(pageSize, page, sorted, filtered) {

        return new Promise((resolve, reject) => {
            // You can retrieve your data however you want, in this case, we will just use some local data.
             
            axios.get('/api/process-list',{
                params : {
                    filter : filtered,
                    pageSize : pageSize,
                    page : page,
                    sort : sorted
                },
                headers : authHeader()
            })
            .then(function(response) {
                let filteredData = response.data;

                const res = {
                    rows: filteredData.data ,
                    pages: Math.ceil(filteredData.count / pageSize)
                };

                // Here we'll simulate a server response with 500ms of delay.
                setTimeout(() => resolve(res), 100);

            })
            .catch(function(error){
                switch(error.response.status){
                    case 500 :
                        // logout();
                        toast(({ closeToast }) => <div> {error.response.data.message} </div>);
                        break ;
                    case 401 :
                        toast(({ closeToast }) => <div> {error.response.data.error} </div>);
                        logout();
                        break;
                    default :
                        toast(({ closeToast }) => <div> could not fetch process list </div>);
                        break;
                } 
            })
        });
    }

    handleStatus(event){
        event.preventDefault();
        const id = event.target.id;
        const status = event.target.value;
        self = this;
        axios({
            method: 'put',
            url: '/api/process-list',
            data: {
                id: id,
                status: status
            },
            async : true,
            headers : authHeader()
        })
        .then(function(response){
            if(response.data){
                let new_state = self.state.data.map(function(data){
                    if(data.process_id == id){
                        data.process_status = (status == 0) ? 1  : 0;
                    } 
                    return data ;
                });
                self.setState({
                    data : new_state
                });
                 toast(({ closeToast }) => <div >  Process {(status == 0) ?'activated ' : 'deactivated '}successfully </div>);
            }else{
                toast(({ closeToast }) => <div > could not fetch process list </div>);
            }
        })
        .catch(function(error){
             
            switch(error.response.status){
                case 500 :
                    // logout();
                    toast(({ closeToast }) => <div> {error.response.data.message} </div>);
                    break ;
                case 401 :
                    toast(({ closeToast }) => <div> {error.response.data.error} </div>);
                    logout();
                    break;
                default :
                    toast(({ closeToast }) => <div> could not update process status  </div>);
                    break;
            } 
        });
            
    }


    render() {
        const { data, pages, loading } = this.state;
        return (
                <div className="dashboard">
                    <div className="card-panel">
                        <div className="row">
                            <div className=" col s6 m6 l3">
                                <Link to="/add-update-process">
                                    <button className="btn cyan waves-effect waves-light  ">
                                        <i className="mdi-content-create left"></i>
                                        Create Process
                                    </button>
                                </Link>
                            </div>
                            <div className="col s6 m6 l4 center-align">
                                <h4 className="header2">
                                    Process List
                                    
                                </h4>
                            </div>
                        </div>
                        
                        <ReactTable
                            columns={[
                                {
                                    Header: "Process Name",
                                    accessor: "process_name"
                                },
                                {
                                    Header: "Process Code",
                                    accessor: "process_code"
                                },
                                    
                                {
                                    Header: "Created By",
                                    accessor: "created_by"
                                },
                                {
                                    Header: "Created On",
                                    accessor: "created_at"
                                },
                                {
                                    Header: "Status",
                                    accessor: "process_status",
                                    Cell:(row)=>(
                                            <div className="switch">
                                                <label>
                                                    Off
                                                    <input
                                                        type="checkbox"
                                                        checked={(row.original.process_status == 1) ? 'checked':'' }
                                                        onChange={this.handleStatus}
                                                        name="process_status"
                                                        row = {JSON.stringify(row)}
                                                        id={row.original.process_id} 
                                                        value={(row.original.process_status == 1) ? '1':'0' }
                                                    />

                                                    <span className="lever"></span> On
                                                </label>
                                            </div>
                                        )

                                },
                                {
                                    Header: "Action",
                                    accessor: "process_id",
                                    Cell:(row)=>(
                                            <div className="switch">
                                                <Link to={'/add-update-process/'+ row.original.process_id } className="input-field ">
                                                    <button className="btn cyan waves-effect waves-light">Edit</button>
                                                </Link>
                                            </div>
                                        )

                                }
                            ]}
                            manual // Forces table not to paginate or sort automatically, so we can handle it server-side
                            data={data}
                            pages={pages} // Display the total number of pages
                            loading={loading} // Display the loading overlay when we need it
                            onFetchData={this.fetchData} // Request new data when things change
                            filterable
                            defaultPageSize={ITEM_PER_PAGE}
                            className="-highlight" //-striped
                        />
                        <br />
                    </div>
                </div>
        );
    }
}


