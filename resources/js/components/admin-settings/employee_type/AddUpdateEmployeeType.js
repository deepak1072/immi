/**
 * Created by Sandeep Maurya on 15th dec 2018.
 */

import React from 'react';
import axios from 'axios';
import { toast } from 'react-toastify';
import {Link} from 'react-router-dom';
import {authHeader,logout,ITEM_PER_PAGE} from  "./../../../helpers/responseAuthozisation";
 


export default class AddUpdateEmployeeType extends React.Component{
    constructor(props){
        super(props);

        this.state = {
            employee_type_name : '',
            employee_type_code : '', 
            employee_type_status:0,
            create_at : '',
            created_by : '',
            message : '',
            flag : true ,
            hasError:false,
            action: 'Create', 
             
        }


        this.handleAddUpdateEmployeeType = this.handleAddUpdateEmployeeType.bind(this);
        this.handleFieldChange   = this.handleFieldChange.bind(this);

    }


    componentDidMount(){

        let self = this;
        const { id } = this.props.match.params;
        
        if(id != undefined){
            
            axios.get('/api/employee-type-details',{
                params:{
                    id : id
                },
                headers : authHeader()
            }).then(function(response){
                if(response.status == 200){
                    const data = response.data[0];
                    self.setState({
                        employee_type_name : data['employee_type_name'],
                        employee_type_id : data['employee_type_id'], 
                        employee_type_code: data['employee_type_code'] , 
                        action : 'Update' 
                        
                    });
                    
                }else{

                    toast(({ closeToast }) => <div>Something went wrong , please try after some time  </div>);
                    history.push('/employee-type-setup') ;   
                }
                

            }).catch(function(error){
                switch(error.response.status){
                    case 500 :
                        // logout();
                        toast(({ closeToast }) => <div> {error.response.data.message} </div>);
                        history.push('/employee-type-setup') ;  
                        break ;
                    case 401 :
                        toast(({ closeToast }) => <div> {error.response.data.error} </div>);
                        logout();
                        break;
                    default :
                        toast(({ closeToast }) => <div> we could not fetch employee type details , please try after sometime  </div>);
                        history.push('/employee-type-setup') ;  
                        break;
                }
            });
        }
    }

    static getDerivedStateFromError(error) {
        // Update state so the next render will show the fallback UI.
        return { hasError: true };
    }

    /**
     * form validation
     * @returns {boolean|*}
     */
    validate(){
    	 this.state.flag = true;
          
        if(this.state.employee_type_name =='' || this.state.employee_type_name == 'undefined'){
            this.state.message = 'Please enter employee type name';
            this.state.flag = false;
        }
        
        if(this.state.employee_type_code =='' || this.state.employee_type_code == 'undefined'){
            this.state.message = 'Please enter employee type code';
            this.state.flag = false;
        }
     
        return this.state.flag;
    }



    handleAddUpdateEmployeeType(event){
        event.preventDefault();
        const {history} = this.props;
        const employee_type = this.state;

        
        if(this.validate()){
            axios.post('/api/add-update-employee-type', employee_type,{headers : authHeader()})
                .then(response => {
                    if(response.data.result){
                        toast(({ closeToast }) => <div>{response.data.message}</div>);
                        history.push('/employee-type-setup') ;
                    } else{
                        response.data.error.map((errors)=>{
                            toast(({ closeToast }) => <div>{errors} </div>);
                        });
                    }
                })
                .catch(error => {
                     
                    switch(error.response.status){
                        case 500 :
                            // logout();
                            toast(({ closeToast }) => <div> {error.response.data.message} </div>);
                            history.push('/employee-type-setup') ;  
                            break ;
                        case 401 :
                            toast(({ closeToast }) => <div> {error.response.data.error} </div>);
                            logout();
                            break;
                        default :
                            toast(({ closeToast }) => <div> Something went wrong , please try after sometime  </div>);
                            history.push('/employee-type-setup') ;  
                            break;
                    }
                })
        }  else{
            toast(({ closeToast }) => <div>{this.state.message}</div>)
        }

    }
    handleFieldChange(event){
        
        this.setState({
            [event.target.name] : event.target.value
        });
    }
 
    render(){

        if(this.state.hasError){
           return <h4 className="header2">Something went wrong</h4>
        }   else{
            return(
                <div className="dashboard">
                    <div className="card-panel">
                        <div className="row">
                            <form className="col s12" onSubmit={this.handleAddUpdateEmployeeType}>
                                <div id="leave_htmlForm">
                                    <div className="row">
                                        <div className="col s12 l12 center-align">
                                            <h4 className="header2">{this.state.action} Employee Type </h4>
                                        </div>
                                    </div>
                                    <div className="row">
                                        <div className="input-field col s3">
                                            <input
                                                id="employee_type_code"
                                                type="text"
                                                name="employee_type_code"
                                                value={this.state.employee_type_code}
                                                onChange={this.handleFieldChange}
                                            /> 
                                            <label htmlFor="employee_type_code" className="active">Employee Type Code * 
                                            </label>
                                        </div>

                                        <div className="input-field col s3">
                                            <input
                                                id="employee_type_name"
                                                type="text"
                                                name="employee_type_name"
                                                value={this.state.employee_type_name}
                                                onChange={this.handleFieldChange}
                                            /> 
                                            <label htmlFor="employee_type_name" className="active">Employee Type Name * 
                                            </label>
                                        </div>
                                    </div>
                                </div>
                                <div className="row">                                            
                                    <div className="input-field col s12">
                                        <Link to="/employee-type-setup">
                                            <button className="btn default   waves-light"   name="action">
                                                <i className="mdi-navigation-arrow-back left "></i>
                                                cancel 
                                            </button>
                                        </Link>
                                        <button className="btn cyan waves-effect waves-light  " type="submit" name="action">{(this.state.action)}
                                            <i className="mdi-content-send right"></i>
                                        </button>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            );
        }
    }
}