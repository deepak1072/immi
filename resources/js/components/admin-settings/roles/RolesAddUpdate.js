/**
 * Created by Sandeep Maurya on 09/12/2018.
 */

import React from 'react';
import axios from 'axios';
import { toast } from 'react-toastify';
import {Link} from 'react-router-dom';
import 'react-checkbox-tree/lib/react-checkbox-tree.css';
import CheckboxTree from 'react-checkbox-tree';
import {authHeader,logout} from  "./../../../helpers/responseAuthozisation";


export default class RolesAddUpdate extends React.Component{
    constructor(props){
        super(props);

        this.state = {
            role_name : '',
            super_role_id : '', 
            role_status:0,
            role_menu_access : '',
            role_data_access: '',
            create_at : '',
            created_by : '',
            message : '',
            flag : true ,
            hasError:false,
            action: 'Create',
            role_master_list:[
                {'id' : '1','name':'Employee'},
                {'id' : '2','name':'Manager'},
                {'id' : '3','name': 'ERO'},
                {'id' : '4','name': 'HR'},
                {'id' : '5','name': 'Admin'},
                {'id' : '5','name': 'Central Admin'},
                {'id' : '7','name' : 'Reporting Manager'},
                {'id' : '8','name' : 'Functional Manager'},
                {'id' : '9','name' : 'Shift Planner'},
                {'id' : '10','name' : 'Custom Role'}
            ],
            master_company:[],
            checked: [],
            expanded: [],
            menu_nodes:[],
            AccessChecked:[],
            AccessExpanded : [],
            access_node : []
        }


        this.handleAddUpdateRoles = this.handleAddUpdateRoles.bind(this);
        this.handleFieldChange   = this.handleFieldChange.bind(this);

        //get menu ou 
        let self = this;
        axios.get('/api/menu-masters',{headers:authHeader()})
        .then(function(response){
            if(response.status == 200){
                const data = response.data;

                self.state.menu_nodes = data;
                self.state.access_node = data;
            }else{

                toast(({ closeToast }) => <div>Something went wrong , please try after some time  </div>);
                history.push('/role-setup') ;   
            }
        }).catch(function(error){

            switch(error.response.status){
                case 500 :
                    // logout();
                    toast(({ closeToast }) => <div> {error.response.data.message} </div>);
                    history.push('/role-setup') ;
                    break ;
                case 401 :
                    toast(({ closeToast }) => <div> {error.response.data.error} </div>);
                    logout();
                    break;
                default :
                    toast(({ closeToast }) => <div> could not fetch details </div>);
                    break;
            }
        });

    }


    componentDidMount(){

        let self = this;
        const { id } = this.props.match.params;
        if(id != undefined){
            
            axios.get('/api/role-details',{
                params:{
                    id : id
                } ,
                headers:authHeader()
            }).then(function(response){
                if(response.status == 200){
                    const data = response.data[0];
                    let role_menu_access = JSON.parse(data['role_menu_access']) ;
                    let checked = role_menu_access.checked;
                    let expanded = role_menu_access.expanded;
                    
                    self.setState({
                        role_name : data['role_name'],
                        super_role_id : data['super_role_id'],  
                        role_id: data['role_id'] ,
                        action : 'Update',
                        checked : checked,
                        expanded : expanded
                        
                    });
                    
                }else{

                    toast(({ closeToast }) => <div>Something went wrong , please try after some time  </div>);
                    history.push('/role-setup') ;   
                }
                

            }).catch(function(error){

                switch(error.response.status){
                    case 500 :
                        // logout();
                        toast(({ closeToast }) => <div> {error.response.data.message} </div>);
                        history.push('/role-setup') ;
                        break ;
                    case 401 :
                        toast(({ closeToast }) => <div> {error.response.data.error} </div>);
                        logout();
                        break;
                    default :
                        toast(({ closeToast }) => <div> could not fetch details </div>);
                        break;
                }
            });
        }

        // master company 
        axios.get('/api/master-company',{headers:authHeader()})
        .then(function(response){
            if(response.status == 200){
                const data = response.data;
                self.setState({
                    master_company : data
                });
            }else{

                toast(({ closeToast }) => <div>Something went wrong , please try after some time  </div>);
                history.push('/role-setup') ;   
            }
        }).catch(function(error){

            switch(error.response.status){
                case 500 :
                    // logout();
                    toast(({ closeToast }) => <div> {error.response.data.message} </div>);
                    history.push('/role-setup') ;
                    break ;
                case 401 :
                    toast(({ closeToast }) => <div> {error.response.data.error} </div>);
                    logout();
                    break;
                default :
                    toast(({ closeToast }) => <div> could not fetch details </div>);
                    break;
            }
        });
    }

    static getDerivedStateFromError(error) {
        // Update state so the next render will show the fallback UI.
        return { hasError: true };
    }

    /**
     * form validation
     * @returns {boolean|*}
     */
    validate(){
    	 this.state.flag = true;
        if(this.state.checked.length< 1){
            this.state.message = 'Please select menu for this role ';
            this.state.flag = false; 
        } 
        if(this.state.role_name =='' || this.state.role_name == 'undefined'){
            this.state.message = 'Please enter role name';
            this.state.flag = false;
        }
       
        if(this.state.super_role_id =='' || this.state.super_role_id == 'undefined' ) {
            this.state.message = 'Please select Role Type';
            this.state.flag = false;
        }
        return this.state.flag;
    }



    handleAddUpdateRoles(event){
        event.preventDefault();
        const {history} = this.props;
        const roles = this.state;

        
        if(this.validate()){
            axios.post('/api/add-update-roles', roles,{headers : authHeader()})
                .then(response => {
                    if(response.data.result){
                        toast(({ closeToast }) => <div>{response.data.message}</div>);
                        history.push('/role-setup') ;
                    } else{
                        response.data.error.map((errors)=>{
                            toast(({ closeToast }) => <div>{errors} </div>);
                        });
                    }
                })
                .catch(error => {
                    switch(error.response.status){
                        case 500 :
                            // logout();
                            toast(({ closeToast }) => <div> {error.response.data.message} </div>);
                            history.push('/role-setup') ;
                            break ;
                        case 401 :
                            toast(({ closeToast }) => <div> {error.response.data.error} </div>);
                            logout();
                            break;
                        default :
                            toast(({ closeToast }) => <div>Something went wrong ! please try after some  time </div>);
                            history.push('/role-setup') ;
                            break;
                    }


                })
        }  else{
            toast(({ closeToast }) => <div>{this.state.message}</div>)
        }

    }
    handleFieldChange(event){
        
        this.setState({
            [event.target.name] : event.target.value
        });
    }

    onCheck(checked){
        this.setState({ checked : checked});   
    }
 

    onExpand(expanded){
        this.setState({ expanded : expanded });
    }

     
    render(){

        if(this.state.hasError){
           return <h4 className="header2">Something went wrong</h4>
        }   else{
            return(
                <div className="dashboard">
                    <div className="card-panel">
                        <div className="row">
                            <form className="col s12" onSubmit={this.handleAddUpdateRoles}>
                                <div id="leave_htmlForm">
                                    <div className="row">                                                
                                        
                                        <div className="col s12 l12 center-align">
                                            <h4 className="header2">{this.state.action} Role </h4>
                                        </div>
                                    </div>
                                    <div className="row"> 
                                        <div className="col s2 input-field">
                                            <select  
                                                value={this.state.super_role_id} 
                                                onChange={this.handleFieldChange} 
                                                name="super_role_id" 
                                                id="super_role_id" 
                                                className="browser-default"
                                                >
                                                <option value=''> Select Role</option>
                                                    
                                                {
                                                    this.state.role_master_list.map((role,index)=>{
                                                        return(
                                                            <option key={index} value={role.id}>{role.name}</option>
                                                        )
                                                    })
                                                }
                                            </select>
                                            <label htmlFor="super_role_id" className="active">Select Role Type
                                            </label>
                                        </div>  
                                            

                                        <div className="input-field col s2">
                                            <input
                                                id="role_name"
                                                type="text"
                                                name="role_name"
                                                value={this.state.role_name}
                                                onChange={this.handleFieldChange}
                                            /> 
                                            <label htmlFor="role_name" className="active">Role Name * 
                                            </label>
                                        </div>

                            
                                    </div>
                                    <div className="row">
                                        <div className="col s6 l4">
                                                <label   className="active">Assign Menu
                                            </label>
                                            <div className="assign_menu_box">
                                                <CheckboxTree
                                                    nodes={this.state.menu_nodes}
                                                    checked={this.state.checked}
                                                    expanded={this.state.expanded}
                                                    onCheck={this.onCheck.bind(this)}
                                                    onExpand={this.onExpand.bind(this)}

                                                    
                                                    showNodeIcon = {false}
                                                        icons={{
                                                        check: <span className="mdi-toggle-check-box" />,
                                                        uncheck: <span className="mdi-toggle-check-box-outline-blank" />,
                                                        halfCheck: <span className="mdi-toggle-check-box" />,
                                                        expandClose: <span className="mdi-navigation-chevron-right" />,
                                                        expandOpen: <span className="mdi-navigation-expand-more" />,
                                                        expandAll: <span className="rct-icon rct-icon-expand-all" />,
                                                        collapseAll: <span className="rct-icon rct-icon-collapse-all" />,
                                                        parentClose: <span className="rct-icon rct-icon-parent-close" />,
                                                        parentOpen: <span className="rct-icon rct-icon-parent-open" />,
                                                        leaf: <span className="rct-icon rct-icon-leaf" />,
                                                    }}
                                                />
                                            </div>
                                        </div>
                                        <div className="col s6 l4">
                                                <label   className="active">Assign Access (only for custom role)
                                            </label>
                                            <div className="assign_menu_box">
                                                    
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                    

                                    
                                <div className="row">                                            
                                    <div className="input-field col s12">
                                        <Link to="/role-setup">
                                            <button className="btn default   waves-light"   name="action">
                                                <i className="mdi-navigation-arrow-back left "></i>
                                                cancel
                                            </button>
                                        </Link>
                                        <button className="btn cyan waves-effect waves-light  " type="submit" name="action">{(this.state.action)}
                                            <i className="mdi-content-send right"></i>
                                        </button>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            );
        }
    }
}