/**
 * Created by Sandeep Maurya on 11/24/2018.
 */

import React from "react";
import _ from "lodash";
import axios from 'axios';
import { toast } from 'react-toastify';
 // Import React Table
import ReactTable from "react-table";
import "react-table/react-table.css";
import {Link} from  'react-router-dom';
import {authHeader,logout} from  "./../../../helpers/responseAuthozisation";


export  default class AssignRolesAddUpdate extends  React.Component {
    constructor(props) {
        super(props);
        this.state = {
            users : [],
            roles : [],
            role : '',
            user : '',
            data: [],
            pages: null,
            loading: true
        };
        this.fetchData = this.fetchData.bind(this);
        this.handleStatus = this.handleStatus.bind(this);
        this.getUsersRolesList();
    }

    fetchData(state, instance) {
        // Whenever the table model changes, or the user sorts or changes pages, this method gets called and passed the current table model.
        // You can set the `loading` prop of the table to true to use the built-in one or show you're own loading bar if you want.
        this.setState({ loading: true });
    
        // Request the data however you want.  Here, we'll use our mocked service we created earlier
        this.requestData(
            state.pageSize,
            state.page,
            state.sorted,
            state.filtered
        ).then((res) => {
            // Now just get the rows of data to your React Table (and update anything else like total pages or loading)
            
            this.setState({
                data: res.rows,
                pages: res.pages,
                loading: false
            });
        });
    }

     requestData(pageSize, page, sorted, filtered) {

        return new Promise((resolve, reject) => {
            // You can retrieve your data however you want, in this case, we will just use some local data.
             
            axios.get('/api/assigned-roles-list',{
                params : {
                    filter : filtered,
                    pageSize : pageSize,
                    page : page,
                    sort : sorted
                } ,
                headers : authHeader()
            })
            .then(function(response) {
                let filteredData = response.data;
                 
                const res = {
                    rows: filteredData  ,
                    pages: Math.ceil(filteredData.count / pageSize)
                };

                // Here we'll simulate a server response with 500ms of delay.
                setTimeout(() => resolve(res), 100);

            }).catch(function(error){

                switch(error.response.status){
                    case 500 :
                        // logout();
                        toast(({ closeToast }) => <div> {error.response.data.message} </div>);
                        // history.push('/role-setup') ;
                        break ;
                    case 401 :
                        toast(({ closeToast }) => <div> {error.response.data.error} </div>);
                        logout();
                    default :
                        toast(({ closeToast }) => <div> could not fetch assigned role list </div>);
                }
            })
        });
    }

    getUsersRolesList(){
         //get menu ou 
        let self = this;
        axios.get('/api/users-roles-list',{headers:authHeader()})
        .then(function(response){
            if(response.status == 200){
                const data = response.data;

                self.state.menu_nodes = data;
                self.state.access_node = data;
            }else{

                toast(({ closeToast }) => <div>Something went wrong , please try after some time  </div>);
                history.push('/role-setup') ;   
            }
        }).catch(function(error){

            switch(error.response.status){
                case 500 :
                    // logout();
                    toast(({ closeToast }) => <div> {error.response.data.message} </div>);
                    history.push('/role-setup') ;
                    break ;
                case 401 :
                    toast(({ closeToast }) => <div> {error.response.data.error} </div>);
                    logout();
                    break;
                default :
                    toast(({ closeToast }) => <div> could not fetch details </div>);
                    break;
            }
        });
    }

    handleStatus(event){
        event.preventDefault();
        const id = event.target.id;
        const status = event.target.value;
        self = this;
        axios({
            method: 'put',
            url: '/api/assigned-roles-list',
            data: {
                id: id,
                status: status
            },
            async : true ,
            headers:authHeader()
        })
        .then(function(response){
            if(response.data){
                let new_state = self.state.data.map(function(data){
                    if(data.user_role_id == id){
                        data.is_active = (status == 0) ? 1  : 0;
                    } 
                    return data ;
                });
                self.setState({
                    data : new_state
                });
                 toast(({ closeToast }) => <div >  Role {(status == 0) ?'activated ' : 'deactivated '}successfully </div>);
            }else{
                toast(({ closeToast }) => <div > could not fetch roles list </div>);
            }
        }) .catch(function(error){

            switch(error.response.status){
                case 500 :
                    // logout();
                    toast(({ closeToast }) => <div> {error.response.data.message} </div>);
                    // history.push('/role-setup') ;
                    break ;
                case 401 :
                    toast(({ closeToast }) => <div> {error.response.data.error} </div>);
                    logout();
                default :
                    toast(({ closeToast }) => <div> could not update role status </div>);
            }
        });
            
    }

    handleFieldChange(event){
        this.setState({
            [event.target.name] : event.target.value
        });
    }




    render() {
        const { data, pages, loading } = this.state;
        return (
                <div className="dashboard">
                    <div className="card-panel">
                        <div className = "row">
                            <div className=" col s6 m4 l3">
                                <select
                                    name = "user"
                                    id = "user"
                                    value = {this.state.user}
                                    onChange={this.handleFieldChange.bind(this)} 
                            
                                    className="browser-default"
                                >
                                    <option> select user  
                                    </option>

                                </select>
                            </div>
                            <div className=" col s6 m4 l3">
                                <select
                                    name = "role"
                                    id  = "role"
                                    value={this.state.role} 
                                    onChange={this.handleFieldChange.bind(this)} 
                            
                                    className="browser-default"
                                >                                    
                                <option> select role 
                                </option>


                                </select>
                            </div>
                            <div className=" col s6 m4 l3">
                                <button className="btn cyan waves-effect waves-light  " type="submit" name="action"> 
                                    Assign Role 
                                    <i className="mdi-content-send right"></i>
                                </button>
                            </div>
                        </div>
                        <div className="row">
                            
                            <div className="col s12 m12 l12 center-align">
                                <h4 className="header2">
                                    Role Assignment
                                    
                                </h4>
                            </div>
                        </div>
                        
                        <ReactTable
                            columns={[
                                {
                                    Header: "Role Name",
                                    accessor: "role_name"
                                },
                                {
                                    Header : "User Name",
                                    accessor : "user_code"
                                },
                                    
                                
                                {
                                    Header: "Created On",
                                    accessor: "created_at"
                                },
                                {
                                    Header: "Status",
                                    accessor: "is_active",
                                    Cell:(row)=>(
                                            <div className="switch">
                                                <label>
                                                    Off
                                                    <input
                                                        type="checkbox"
                                                        checked={(row.original.is_active == 1) ? 'checked':'' }
                                                        onChange={this.handleStatus}
                                                        name="is_active"
                                                        row = {JSON.stringify(row)}
                                                        id={row.original.user_role_id} 
                                                        value={(row.original.is_active == 1) ? '1':'0' }
                                                    />

                                                    <span className="lever"></span> On
                                                </label>
                                            </div>
                                        )

                                } 
                            ]}
                            manual // Forces table not to paginate or sort automatically, so we can handle it server-side
                            data={data}
                            pages={pages} // Display the total number of pages
                            loading={loading} // Display the loading overlay when we need it
                            onFetchData={this.fetchData} // Request new data when things change
                            filterable
                            defaultPageSize={5}
                            className="-highlight" //-striped
                        />
                        <br />
                    </div>
                </div>
        );
    }
}


