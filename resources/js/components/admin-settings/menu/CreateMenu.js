/**
 * Created by Sandeep Maurya on 11/18/2018.
 */

import React from 'react';
import axios from 'axios';
import { toast } from 'react-toastify';
import {Link} from 'react-router-dom';
import {authHeader,logout} from  "./../../../helpers/responseAuthozisation";


export default class CreateMenu extends React.Component{
    constructor(props){
        super(props);

        this.state = {
            parent_menu : 0,
            menu_name : '',
            menu_url : '',
            menu_icons:'',
            menu_attribute: '_self',
            menu_id:'',
            menu_class:'',
            is_menu_hidden:0,
            errors:[]  ,
            message : '',
            flag : true ,
            action: 0 ,
            parent_menu_list:[] ,
            hasError:false
        }

        this.handleCreateNewMenu = this.handleCreateNewMenu.bind(this);
        this.handleFieldChange   = this.handleFieldChange.bind(this);
        axios.get('/api/parent-menu',{headers:authHeader()})
            .then(response => {

                this.setState({
                    parent_menu_list: response.data
                })
            })
            .catch(error => {
                switch(error.response.status){
                    case 500 :
                        // logout();
                        toast(({ closeToast }) => <div> {error.response.data.message} </div>);
                        break ;
                    case 401 :
                        toast(({ closeToast }) => <div> {error.response.data.error} </div>);
                        logout();
                        break;
                    default :
                        toast(({ closeToast }) => <div> Something went wrong ! please try after some  time</div>);
                        history.push('/menu-list') ;
                        break;
                }

            })
    }

    static getDerivedStateFromError(error) {
        // Update state so the next render will show the fallback UI.
        return { hasError: true };
    }

    /**
     * form validation
     * @returns {boolean|*}
     */
    validate(){
         this.state.flag = true;
        if( (this.state.menu_url =='' || this.state.menu_url == 'undefined') && this.state.parent_menu != 0 ) {
            this.state.message = 'Please enter Menu Url ';
            this.state.flag = false;
        }
        if(this.state.menu_name =='' || this.state.menu_name == 'undefined'){
            this.state.message = 'Please enter Menu Name ';
            this.state.flag = false;
        }
        return this.state.flag;
    }



    handleCreateNewMenu(event){
        event.preventDefault();
        const {history} = this.props;
        const menu = this.state;

        if(this.validate()){
            axios.post('/api/create-menu', menu,{headers : authHeader()})
                .then(response => {
                    if(response.data.result){
                        toast(({ closeToast }) => <div>{response.data.message}</div>);
                        history.push('/menu-list') ;
                    } else{
                        response.data.error.map((errors)=>{
                            toast(({ closeToast }) => <div>{errors} </div>);
                        });
                    }
                })
                .catch(error => {
                    switch(error.response.status){
                        case 500 :
                            // logout();
                            toast(({ closeToast }) => <div> {error.response.data.message} </div>);
                            break ;
                        case 401 :
                            toast(({ closeToast }) => <div> {error.response.data.error} </div>);
                            logout();
                            break;
                        default :
                            toast(({ closeToast }) => <div> Something went wrong ! please try after some  time</div>);
                            history.push('/menu-list') ;
                            break;
                    }


                })
        }  else{
            toast(({ closeToast }) => <div>{this.state.message}</div>)

        }

    }
    handleFieldChange(event){
        this.setState({
            [event.target.name] : event.target.value
        });
    }

    render(){

        if(this.state.hasError){
           return <h4 className="header2">Something went wrong</h4>
        }   else{
            return(
                <div className="dashboard">
                    <div className="card-panel">
                        <div className="row">
                            <form className="col s12" onSubmit={this.handleCreateNewMenu}>
                                <div id="leave_htmlForm">
                                    <div className="row">
                                        
                                        <div className="col s6 l3">
                                            <Link to="/menu-list">
                                                <button className="btn cyan waves-effect waves-light">
                                                    Back
                                                    <i className="mdi-content-reply left"></i>
                                                </button>
                                            </Link>
                                        </div>
                                        <div className="col s6 l4 center-align">
                                            <h4 className="header2">Create Menu </h4>
                                        </div>
                                    </div>
                                    <div className="row">
                                        <div className="input-field col s2">

                                            
                                            <select  
                                                value={this.state.parent_menu} 
                                                onChange={this.handleFieldChange} 
                                                name="parent_menu" 
                                                id="parent_menu" 
                                                className="browser-default"
                                                >
                                                <option value='0'>Parent Menu</option>
                                                {
                                                    this.state.parent_menu_list.map((menu,index)=>{
                                                        return(
                                                            <option key={index} value={menu.menu_id}>{menu.menu_name}</option>
                                                        )
                                                    })
                                                }
                                            </select>
                                            <label htmlFor="parent_menu" className="active">Select Parent Menu
                                            </label>

                                        </div>
                                        <div className="input-field col s2">

                                            <input
                                                id="menu_name"
                                                type="text"
                                                name="menu_name"
                                                value={this.state.menu_name}
                                                onChange={this.handleFieldChange}
                                            /> 
                                            <label htmlFor="menu_name">Menu Name *
                                            </label>

                                        </div>
                                        <div className="input-field col s2">
                                            <input
                                                id="menu_url"
                                                type="text"
                                                name ="menu_url"
                                                value={this.state.menu_url}
                                                onChange={this.handleFieldChange}

                                            />
                                            <label htmlFor="menu_url">Menu Url

                                            </label>
                                        </div>
                                        <div className="input-field col s2">

                                            <input
                                                id="menu_icons"
                                                type="text"
                                                name="menu_icons"
                                                value={this.state.menu_icons}
                                                onChange={this.handleFieldChange}
                                            />
                                            <label htmlFor="menu_icons">Menu Icon
                                            </label>
                                        </div>
                                        <div className="input-field col s2">

                                            <input
                                                id="menu_class"
                                                type="text"
                                                name="menu_class"
                                                value={this.state.menu_class}
                                                onChange={this.handleFieldChange}
                                            />
                                            <label htmlFor="menu_class">Menu Class Attributes
                                            </label>
                                        </div>
                                        <div className="input-field col s2">
                                            <input
                                                id="menu_id"
                                                type="text"
                                                name="menu_id"
                                                value={this.state.menu_id}
                                                onChange={this.handleFieldChange}
                                            />
                                            <label htmlFor="menu_id">Menu ID Attributes

                                            </label>
                                        </div>

                                    </div>
                                </div>
                                <div className="row">
                                    <div className="input-field col s2">
                                        
                                        <select   
                                            value={this.state.menu_attribute} 
                                            onChange={this.handleFieldChange} 
                                            name="menu_attribute" 
                                            id="menu_attribute"
                                            className="browser-default"
                                            >
                                            <option value='_self'>_self</option>
                                            <option value='_blank'>_blank</option>
                                            <option value='_parent'>_parent</option>
                                        </select>
                                        <label htmlFor="menu_attribute" className="active">Select Target Attribute
                                            </label>

                                    </div>



                                    <div className="input-field col s12">
                                        <button className="btn cyan waves-effect waves-light right" type="submit" name="action">Submit
                                            <i className="mdi-content-send right"></i>
                                        </button>
                                    </div>
                                </div>



                            </form>
                        </div>
                    </div>
                </div>
            );
        }
    }
}