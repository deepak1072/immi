/**
 * Created by Sandeep Maurya on 15th dec 2018.
 */

import React from 'react';
import axios from 'axios';
import { toast } from 'react-toastify';
import {Link} from 'react-router-dom';
import {authHeader,logout,ITEM_PER_PAGE} from  "./../../../helpers/responseAuthozisation";
 


export default class AddUpdateRegions extends React.Component{
    constructor(props){
        super(props);

        this.state = {
            regions_name : '',
            regions_code : '', 
            regions_status:0,
            create_at : '',
            created_by : '',
            message : '',
            flag : true ,
            hasError:false,
            action: 'Create', 
             
        }


        this.handleAddUpdateRegions = this.handleAddUpdateRegions.bind(this);
        this.handleFieldChange   = this.handleFieldChange.bind(this);

    }


    componentDidMount(){

        let self = this;
        const { id } = this.props.match.params;
        
        if(id != undefined){
        
            axios.get('/api/regions-details',{
                params:{
                    id : id
                },
                headers : authHeader()

            }).then(function(response){
                if(response.status == 200){
                    const data = response.data[0];
                    self.setState({
                        regions_name : data['regions_name'],
                        regions_id : data['regions_id'],  
                        regions_code: data['regions_code'] , 
                        action : 'Update'  
                    });
                }else{
                    toast(({ closeToast }) => <div>Something went wrong , please try after some time  </div>);
                    history.push('/regions-setup') ;   
                }
            }).catch(function(error){
                switch(error.response.status){
                    case 500 :
                        // logout();
                        toast(({ closeToast }) => <div> {error.response.data.message} </div>);
                        history.push('/regions-setup') ;  
                        break ;
                    case 401 :
                        toast(({ closeToast }) => <div> {error.response.data.error} </div>);
                        logout();
                        break;
                    default :
                        toast(({ closeToast }) => <div> we could not fetch region details , please try after sometime  </div>);
                        history.push('/regions-setup') ;  
                        break;
                }
            });
        }
    }

    static getDerivedStateFromError(error) {
        // Update state so the next render will show the fallback UI.
        return { hasError: true };
    }

    /**
     * form validation
     * @returns {boolean|*}
     */
    validate(){
    	 this.state.flag = true;
          
        if(this.state.regions_name =='' || this.state.regions_name == 'undefined'){
            this.state.message = 'Please enter regions name';
            this.state.flag = false;
        }
        
        if(this.state.regions_code =='' || this.state.regions_code == 'undefined'){
            this.state.message = 'Please enter regions code';
            this.state.flag = false;
        }
      
        return this.state.flag;
    }



    handleAddUpdateRegions(event){
        event.preventDefault();
        const {history} = this.props;
        const regionss = this.state;

        
        if(this.validate()){
            axios.post('/api/add-update-regions', regionss,{headers : authHeader()})
                .then(response => {
                    if(response.data.result){
                        toast(({ closeToast }) => <div>{response.data.message}</div>);
                        history.push('/regions-setup') ;
                    } else{
                        response.data.error.map((errors)=>{
                            toast(({ closeToast }) => <div>{errors} </div>);
                        });
                    }
                })
                .catch(error => { 
                    switch(error.response.status){
                        case 500 :
                            // logout();
                            toast(({ closeToast }) => <div> {error.response.data.message} </div>);
                            history.push('/regions-setup') ;  
                            break ;
                        case 401 :
                            toast(({ closeToast }) => <div> {error.response.data.error} </div>);
                            logout();
                            break;
                        default :
                            toast(({ closeToast }) => <div> sometime went wrong , please try after sometime  </div>);
                            history.push('/regions-setup') ;  
                            break;
                    }
                })
        }  else{
            toast(({ closeToast }) => <div>{this.state.message}</div>)
        }

    }
    handleFieldChange(event){
        
        this.setState({
            [event.target.name] : event.target.value
        });
    }
 
    render(){

        if(this.state.hasError){
           return <h4 className="header2">Something went wrong</h4>
        }   else{
            return(
                <div className="dashboard">
                    <div className="card-panel">
                        <div className="row">
                            <form className="col s12" onSubmit={this.handleAddUpdateRegions}>
                                <div id="leave_htmlForm">
                                    <div className="row">
                                        <div className="col s12 l12 center-align">
                                            <h4 className="header2">{this.state.action} Regions </h4>
                                        </div>
                                    </div>
                                    <div className="row"> 
                                        <div className="input-field col s3">
                                            <input
                                                id="regions_code"
                                                type="text"
                                                name="regions_code"
                                                value={this.state.regions_code}
                                                onChange={this.handleFieldChange}
                                            /> 
                                            <label htmlFor="regions_code" className="active">Regions Code * 
                                            </label>
                                        </div>

                                        <div className="input-field col s3">
                                            <input
                                                id="regions_name"
                                                type="text"
                                                name="regions_name"
                                                value={this.state.regions_name}
                                                onChange={this.handleFieldChange}
                                            /> 
                                            <label htmlFor="regions_name" className="active">Regions Name * 
                                            </label>
                                        </div>
                                    </div>   
                                </div>
                                <div className="row">                                            
                                    <div className="input-field col s12">
                                        <Link to="/regions-setup">
                                            <button className="btn default   waves-light"   name="action">
                                                <i className="mdi-navigation-arrow-back left "></i>
                                                cancel
                                            </button>
                                        </Link>
                                        <button className="btn cyan waves-effect waves-light  " type="submit" name="action">{(this.state.action)}
                                            <i className="mdi-content-send right"></i>
                                        </button>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            );
        }
    }
}