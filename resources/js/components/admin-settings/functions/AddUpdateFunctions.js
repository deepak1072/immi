/**
 * Created by Sandeep Maurya on 15th dec 2018.
 */

import React from 'react';
import axios from 'axios';
import { toast } from 'react-toastify';
import {Link} from 'react-router-dom';
import SelectSearch from 'react-select-search';
import {authHeader,logout,ITEM_PER_PAGE} from  "./../../../helpers/responseAuthozisation";
 
const options = [
    {name: 'Swedish', value: 'sv'},
    {name: 'English', value: 'en'},
    {
        type: 'group',
        name: 'Group name',
        items: [
            {name: 'Spanish', value: 'es'},
        ]
    },
];

export default class AddUpdateFunctions extends React.Component{
    constructor(props){
        super(props);

        this.state = {
            function_name : '',
            function_code : '', 
            function_status:0,
            function_head : '',
            create_at : '',
            created_by : '',
            message : '',
            flag : true ,
            hasError:false,
            action: 'Create',
            head_list:[]
             
        }


        this.handleAddUpdateFunction = this.handleAddUpdateFunction.bind(this);
        this.handleFieldChange   = this.handleFieldChange.bind(this);

    }


    componentDidMount(){

        let self = this;
        const { id } = this.props.match.params;
        
        if(id != undefined){
        
            axios.get('/api/function-details',{
                params:{
                    id : id
                },
                headers : authHeader()
            }).then(function(response){
                if(response.status == 200){
                    const data = response.data[0];
                    self.setState({
                        function_name : data['function_name'],
                        function_id : data['function_id'], 
                        function_code: data['function_code'] , 
                        function_head: data['function_head'] , 
                        action : 'Update' 
                        
                    });
                    
                }else{

                    toast(({ closeToast }) => <div>Something went wrong , please try after some time  </div>);
                    history.push('/function-setup') ;   
                }
                

            }).catch(function(error){
                switch(error.response.status){
                    case 500 :
                        // logout();
                        toast(({ closeToast }) => <div> {error.response.data.message} </div>); 
                        history.push('/function-setup') ;
                        break ;
                    case 401 :
                        toast(({ closeToast }) => <div> {error.response.data.error} </div>);
                        logout();
                        break;
                    default :
                        toast(({ closeToast }) => <div>Something went wrong , please try after some  time  </div>); 
                        history.push('/function-setup') ;  
                        break;
                } 
            });
        }

         
    }

    static getDerivedStateFromError(error) {
        // Update state so the next render will show the fallback UI.
        return { hasError: true };
    }

    /**
     * form validation
     * @returns {boolean|*}
     */
    validate(){
    	 this.state.flag = true;
          
        if(this.state.function_head =='' || this.state.function_head == 'undefined'){
            this.state.message = 'Please select function head';
            this.state.flag = false;
        }
        if(this.state.function_name =='' || this.state.function_name == 'undefined'){
            this.state.message = 'Please enter function name';
            this.state.flag = false;
        }
        
        if(this.state.function_code =='' || this.state.function_code == 'undefined'){
            this.state.message = 'Please enter function code';
            this.state.flag = false;
        }
        
        return this.state.flag;
    }



    handleAddUpdateFunction(event){
        event.preventDefault();
        const {history} = this.props;
        const functions = this.state;

        
        if(this.validate()){
            axios.post('/api/add-update-function', functions,{headers : authHeader()})
                .then(response => {
                    if(response.data.result){
                        toast(({ closeToast }) => <div>{response.data.message}</div>);
                        history.push('/function-setup') ;
                    } else{
                        response.data.error.map((errors)=>{
                            toast(({ closeToast }) => <div>{errors} </div>);
                        });
                    }
                })
                .catch(error => {  
                    switch(error.response.status){
                        case 500 :
                            // logout();
                            toast(({ closeToast }) => <div> {error.response.data.message} </div>); 
                            history.push('/function-setup') ;
                            break ;
                        case 401 :
                            toast(({ closeToast }) => <div> {error.response.data.error} </div>);
                            logout();
                            break;
                        default :
                            toast(({ closeToast }) => <div>Something went wrong , please try after some  time  </div>); 
                            history.push('/function-setup') ;  
                            break;
                    } 
                })
        }  else{
            toast(({ closeToast }) => <div>{this.state.message}</div>)
        }

    }
    handleFieldChange(event){
        
        this.setState({
            [event.target.name] : event.target.value
        });
    }
    handleHeadChange(events){
    
             
        this.setState({
            function_head :events.value
        });
    }
    
 
    render(){

        if(this.state.hasError){
           return <h4 className="header2">Something went wrong</h4>
        }   else{
            return(
                <div className="dashboard">
                    <div className="card-panel">
                        <div className="row">
                            <form className="col s12" onSubmit={this.handleAddUpdateFunction}>
                                <div id="leave_htmlForm">
                                    <div className="row">                                                
                                        
                                        <div className="col s12 l12 center-align">
                                            <h4 className="header2">{this.state.action} Function </h4>
                                        </div>
                                    </div>
                                    <div className="row"> 
                                            
                                        
                                        <div className="input-field col s3">
                                            <input
                                                id="function_code"
                                                type="text"
                                                name="function_code"
                                                value={this.state.function_code}
                                                onChange={this.handleFieldChange}
                                            /> 
                                            <label htmlFor="function_code" className="active">Function Code * 
                                            </label>
                                        </div>

                                        <div className="input-field col s3">
                                            <input
                                                id="function_name"
                                                type="text"
                                                name="function_name"
                                                value={this.state.function_name}
                                                onChange={this.handleFieldChange}
                                            /> 
                                            <label htmlFor="function_name" className="active">Function Name * 
                                            </label>
                                        </div>
                                        <div className=" col s2">
                                            <SelectSearch 
                                                options={options} 
                                                value={this.state.function_head} 
                                                name="function_head" 
                                                placeholder="Select Function Head"
                                                onChange={this.handleHeadChange.bind(this)}
                                                // renderValue =(label, valueObj, state, props) => label,
                                            />
                                        </div>

                            
                                    </div>
                                        
                                            
                                        
                                </div>
                                <div className="row">                                            
                                    <div className="input-field col s12">
                                        <Link to="/function-setup">
                                            <button className="btn default   waves-light"   name="action">
                                                <i className="mdi-navigation-arrow-back left "></i>
                                                cancel
                                            </button>
                                        </Link>
                                        <button className="btn cyan waves-effect waves-light  " type="submit" name="action">{(this.state.action)}
                                            <i className="mdi-content-send right"></i>
                                        </button>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            );
        }
    }
}