/**
 * Created by Sandeep Maurya on 15th dec 2018.
 */

import React from 'react';
import axios from 'axios';
import { toast } from 'react-toastify';
import {Link} from 'react-router-dom';
 
import {authHeader,logout,ITEM_PER_PAGE} from  "./../../../helpers/responseAuthozisation";


export default class AddUpdateQualification extends React.Component{
    constructor(props){
        super(props);

        this.state = {
            qualification_name : '',
            qualification_code : '', 
            qualification_status:0,
            create_at : '',
            created_by : '',
            message : '',
            flag : true ,
            hasError:false,
            action: 'Create', 
        }


        this.handleAddUpdateQualification = this.handleAddUpdateQualification.bind(this);
        this.handleFieldChange   = this.handleFieldChange.bind(this);

    }


    componentDidMount(){

        let self = this;
        const { id } = this.props.match.params;
        
        if(id != undefined){
        
            axios.get('/api/qualification-details',{
                params:{
                    id : id
                },
                headers : authHeader()
            }).then(function(response){
                if(response.status == 200){
                    const data = response.data[0];
                    self.setState({
                        qualification_name : data['qualification_name'],
                        qualification_id : data['qualification_id'], 
                        qualification_code: data['qualification_code'] , 
                        action : 'Update' 
                        
                    });
                    
                }else{

                    toast(({ closeToast }) => <div>Something went wrong , please try after some time  </div>);
                    history.push('/qualification-setup') ;   
                }
                

            }).catch(function(error){
                switch(error.response.status){
                    case 500 :
                        // logout();
                        toast(({ closeToast }) => <div> {error.response.data.message} </div>);
                        history.push('/qualification-setup') ;  
                        break ;
                    case 401 :
                        toast(({ closeToast }) => <div> {error.response.data.error} </div>);
                        logout();
                        break;
                    default :
                        toast(({ closeToast }) => <div> we could not fetch qualification details , please try after sometime  </div>);
                        history.push('/qualification-setup') ;  
                        break;
                }
            });
        } 
    }

    static getDerivedStateFromError(error) {
        // Update state so the next render will show the fallback UI.
        return { hasError: true };
    }

    /**
     * form validation
     * @returns {boolean|*}
     */
    validate(){
    	 this.state.flag = true;
          
        if(this.state.qualification_name =='' || this.state.qualification_name == 'undefined'){
            this.state.message = 'Please enter qualification name';
            this.state.flag = false;
        }
        
        if(this.state.qualification_code =='' || this.state.qualification_code == 'undefined'){
            this.state.message = 'Please enter qualification code';
            this.state.flag = false;
        }
       
        return this.state.flag;
    }



    handleAddUpdateQualification(event){
        event.preventDefault();
        const {history} = this.props;
        const qualifications = this.state;

        
        if(this.validate()){
            axios.post('/api/add-update-qualification', qualifications,{headers : authHeader()})
                .then(response => {
                    console.log(response);
                    if(response.data.result){
                        toast(({ closeToast }) => <div>{response.data.message}</div>);
                        history.push('/qualification-setup') ;
                    } else{
                         
                        response.data.error.map((errors)=>{
                            toast(({ closeToast }) => <div>{errors} </div>);
                        });
                    
                    }
                })
                .catch(error => {
                    switch(error.response.status){
                        case 500 :
                            // logout();
                            toast(({ closeToast }) => <div> {error.response.data.message} </div>);
                            history.push('/qualification-setup') ;  
                            break ;
                        case 401 :
                            toast(({ closeToast }) => <div> {error.response.data.error} </div>);
                            logout();
                            break;
                        default :
                            toast(({ closeToast }) => <div> Something went wrong , please try after sometime  </div>);
                            history.push('/qualification-setup') ;  
                            break;
                    }
                })
        }  else{
            toast(({ closeToast }) => <div>{this.state.message}</div>)
        }

    }
    handleFieldChange(event){
        
        this.setState({
            [event.target.name] : event.target.value
        });
    }
 
    render(){

        if(this.state.hasError){
           return <h4 className="header2">Something went wrong</h4>
        }   else{
            return(
                <div className="dashboard">
                    <div className="card-panel">
                        <div className="row">
                            <form className="col s12" onSubmit={this.handleAddUpdateQualification}>
                                <div id="leave_htmlForm">
                                    <div className="row">                                                
                                        
                                        <div className="col s12 l12 center-align">
                                            <h4 className="header2">{this.state.action} Qualification </h4>
                                        </div>
                                    </div>
                                    <div className="row"> 
                                            
                                            
                                        <div className="input-field col s3">
                                            <input
                                                id="qualification_code"
                                                type="text"
                                                name="qualification_code"
                                                value={this.state.qualification_code}
                                                onChange={this.handleFieldChange}
                                            /> 
                                            <label htmlFor="qualification_code" className="active">Qualification Code * 
                                            </label>
                                        </div>

                                        <div className="input-field col s3">
                                            <input
                                                id="qualification_name"
                                                type="text"
                                                name="qualification_name"
                                                value={this.state.qualification_name}
                                                onChange={this.handleFieldChange}
                                            /> 
                                            <label htmlFor="qualification_name" className="active">Qualification Name * 
                                            </label>
                                        </div>

                                        

                            
                                    </div>
                                        
                                            
                                        
                                </div>
                                <div className="row">                                            
                                    <div className="input-field col s12">
                                        <Link to="/qualification-setup">
                                            <button className="btn default   waves-light"   name="action">
                                                <i className="mdi-navigation-arrow-back left "></i>
                                                cancel
                                            </button>
                                        </Link>
                                        <button className="btn cyan waves-effect waves-light  " type="submit" name="action">{(this.state.action)}
                                            <i className="mdi-content-send right"></i>
                                        </button>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            );
        }
    }
}