/**
 * Created by Sandeep Maurya on 15th dec 2018.
 */

import React from 'react';
import axios from 'axios';
import { toast } from 'react-toastify';
import {Link} from 'react-router-dom';
import {authHeader,logout,ITEM_PER_PAGE} from  "./../../../helpers/responseAuthozisation";
 


export default class AddUpdateDesignation extends React.Component{
    constructor(props){
        super(props);

        this.state = {
            designation_name : '',
            designation_code : '' ,
            designation_status:0,
            create_at : '',
            created_by : '',
            message : '',
            flag : true ,
            hasError:false,
            action: 'Create', 
             
        }


        this.handleAddUpdateDesignation = this.handleAddUpdateDesignation.bind(this);
        this.handleFieldChange   = this.handleFieldChange.bind(this);

    }


    componentDidMount(){

        let self = this;
        const { id } = this.props.match.params;
        
        if(id != undefined){
        
            axios.get('/api/designation-details',{
                params:{
                    id : id
                },
                headers : authHeader()
            }).then(function(response){
                if(response.status == 200){
                    const data = response.data[0];
                    self.setState({
                        designation_name : data['designation_name'],
                        designation_id : data['designation_id'], 
                        designation_code: data['designation_code'] , 
                        action : 'Update'  
                    });
                }else{
                    toast(({ closeToast }) => <div>Something went wrong , please try after some time  </div>);
                    history.push('/designation-setup') ;   
                }
            }).catch(function(error){
                switch(error.response.status){
                    case 500 :
                        // logout();
                        toast(({ closeToast }) => <div> {error.response.data.message} </div>);
                        history.push('/designation-setup') ;  
                        break ;
                    case 401 :
                        toast(({ closeToast }) => <div> {error.response.data.error} </div>);
                        logout();
                        break;
                    default :
                        toast(({ closeToast }) => <div> we could not fetch designation details , please try after sometime  </div>);
                        history.push('/designation-setup') ;  
                        break;
                }
            });
        }

         
    }

    static getDerivedStateFromError(error) {
        // Update state so the next render will show the fallback UI.
        return { hasError: true };
    }

    /**
     * form validation
     * @returns {boolean|*}
     */
    validate(){
    	 this.state.flag = true;
          
        if(this.state.designation_name =='' || this.state.designation_name == 'undefined'){
            this.state.message = 'Please enter designation name';
            this.state.flag = false;
        }
        
        if(this.state.designation_code =='' || this.state.designation_code == 'undefined'){
            this.state.message = 'Please enter designation code';
            this.state.flag = false;
        }
      
        return this.state.flag;
    }



    handleAddUpdateDesignation(event){
        event.preventDefault();
        const {history} = this.props;
        const designations = this.state;

        
        if(this.validate()){
            axios.post('/api/add-update-designation', designations,{headers : authHeader()})
                .then(response => {
                    if(response.data.result){
                        toast(({ closeToast }) => <div>{response.data.message}</div>);
                        history.push('/designation-setup') ;
                    } else{
                        response.data.error.map((errors)=>{
                            toast(({ closeToast }) => <div>{errors} </div>);
                        });
                    }
                })
                .catch(error => {
                    switch(error.response.status){
                        case 500 :
                            // logout();
                            toast(({ closeToast }) => <div> {error.response.data.message} </div>);
                            history.push('/designation-setup') ;  
                            break ;
                        case 401 :
                            toast(({ closeToast }) => <div> {error.response.data.error} </div>);
                            logout();
                            break;
                        default :
                            toast(({ closeToast }) => <div> something went wrong, please try after sometime  </div>);
                            history.push('/designation-setup') ;  
                            break;
                    }
                })
        }  else{
            toast(({ closeToast }) => <div>{this.state.message}</div>)
        }

    }
    handleFieldChange(event){
        
        this.setState({
            [event.target.name] : event.target.value
        });
    }
 
    render(){

        if(this.state.hasError){
           return <h4 className="header2">Something went wrong</h4>
        }   else{
            return(
                <div className="dashboard">
                    <div className="card-panel">
                        <div className="row">
                            <form className="col s12" onSubmit={this.handleAddUpdateDesignation}>
                                <div id="leave_htmlForm">
                                    <div className="row">                                                
                                        
                                        <div className="col s12 l12 center-align">
                                            <h4 className="header2">{this.state.action} Designation </h4>
                                        </div>
                                    </div>
                                    <div className="row"> 
                                        
                                        <div className="input-field col s3">
                                            <input
                                                id="designation_code"
                                                type="text"
                                                name="designation_code"
                                                value={this.state.designation_code}
                                                onChange={this.handleFieldChange}
                                            /> 
                                            <label htmlFor="designation_code" className="active">Designation Code * 
                                            </label>
                                        </div>

                                        <div className="input-field col s3">
                                            <input
                                                id="designation_name"
                                                type="text"
                                                name="designation_name"
                                                value={this.state.designation_name}
                                                onChange={this.handleFieldChange}
                                            /> 
                                            <label htmlFor="designation_name" className="active">Designation Name * 
                                            </label>
                                        </div>

                                        

                            
                                    </div>
                                        
                                            
                                        
                                </div>
                                <div className="row">                                            
                                    <div className="input-field col s12">
                                        <Link to="/designation-setup">
                                            <button className="btn default   waves-light"   name="action">
                                                <i className="mdi-navigation-arrow-back left "></i>
                                                cancel
                                            </button>
                                        </Link>
                                        <button className="btn cyan waves-effect waves-light  " type="submit" name="action">{(this.state.action)}
                                            <i className="mdi-content-send right"></i>
                                        </button>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            );
        }
    }
}