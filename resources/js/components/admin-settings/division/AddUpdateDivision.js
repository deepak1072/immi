/**
 * Created by Sandeep Maurya on 09/12/2018.
 */

import React from 'react';
import axios from 'axios';
import { toast } from 'react-toastify';
import {Link} from 'react-router-dom';

import {authHeader,logout,ITEM_PER_PAGE} from  "./../../../helpers/responseAuthozisation";
 


export default class AddUpdateDivision extends React.Component{
    constructor(props){
        super(props);

        this.state = {
            division_name : '',
            division_code : '',
            division_descriptions : '', 
            division_status:0,
            create_at : '',
            created_by : '',
            message : '',
            flag : true ,
            hasError:false,
            action: 'Create',
             
            master_company:[],
             
        }


        this.handleAddUpdateDivision = this.handleAddUpdateDivision.bind(this);
        this.handleFieldChange   = this.handleFieldChange.bind(this);

    }


    componentDidMount(){

        let self = this;
        const { id } = this.props.match.params;
        if(id != undefined){
            
            axios.get('/api/division-details',{
                params:{
                    id : id
                },
                headers : authHeader()

            }).then(function(response){
                if(response.status == 200){
                    const data = response.data[0];
                     
                    
                    self.setState({
                        division_name : data['division_name'],
                        division_id : data['division_id'], 
                        division_code: data['division_code'] ,
                        division_descriptions: data['division_descriptions'] ,
                        action : 'Update' 
                        
                    });
                    
                }else{

                    toast(({ closeToast }) => <div>Something went wrong , please try after some time  </div>);
                    history.push('/division-setup') ;   
                }
                

            })
        }

       
    }

    static getDerivedStateFromError(error) {
        // Update state so the next render will show the fallback UI.
        return { hasError: true };
    }

    /**
     * form validation
     * @returns {boolean|*}
     */
    validate(){
    	 this.state.flag = true;
          
        if(this.state.division_name =='' || this.state.division_name == 'undefined'){
            this.state.message = 'Please enter division name';
            this.state.flag = false;
        }
        
        if(this.state.division_code =='' || this.state.division_code == 'undefined'){
            this.state.message = 'Please enter division code';
            this.state.flag = false;
        }
       
        return this.state.flag;
    }



    handleAddUpdateDivision(event){
        event.preventDefault();
        const {history} = this.props;
        const divisions = this.state;

        
        if(this.validate()){
            axios.post('/api/add-update-division', divisions,{headers : authHeader()})
                .then(response => {
                    if(response.data.result){
                        toast(({ closeToast }) => <div>{response.data.message}</div>);
                        history.push('/division-setup') ;
                    } else{
                        response.data.error.map((errors)=>{
                            toast(({ closeToast }) => <div>{errors} </div>);
                        });
                    }
                })
                .catch(error => {
                    switch(error.response.status){
                        case 500 :
                            // logout();
                            toast(({ closeToast }) => <div> {error.response.data.message} </div>); 
                            history.push('/division-setup') ;
                            break ;
                        case 401 :
                            toast(({ closeToast }) => <div> {error.response.data.error} </div>);
                            logout();
                            break;
                        default :
                            toast(({ closeToast }) => <div>Something went wrong ! please try after some  time  </div>); 
                            history.push('/division-setup') ;  
                            break;
                    }                     
                })
        }  else{
            toast(({ closeToast }) => <div>{this.state.message}</div>)
        }

    }
    handleFieldChange(event){
        
        this.setState({
            [event.target.name] : event.target.value
        });
    }
 
    render(){

        if(this.state.hasError){
           return <h4 className="header2">Something went wrong</h4>
        }   else{
            return(
                <div className="dashboard">
                    <div className="card-panel">
                        <div className="row">
                            <form className="col s12" onSubmit={this.handleAddUpdateDivision}>
                                <div id="leave_htmlForm">
                                    <div className="row">                                                
                                        
                                        <div className="col s12 l12 center-align">
                                            <h4 className="header2">{this.state.action} Division </h4>
                                        </div>
                                    </div>
                                    <div className="row"> 
                                            
                                            
                                        <div className="input-field col s3">
                                            <input
                                                id="division_code"
                                                type="text"
                                                name="division_code"
                                                value={this.state.division_code}
                                                onChange={this.handleFieldChange}
                                            /> 
                                            <label htmlFor="division_code" className="active">Division Code * 
                                            </label>
                                        </div>

                                        <div className="input-field col s3">
                                            <input
                                                id="division_name"
                                                type="text"
                                                name="division_name"
                                                value={this.state.division_name}
                                                onChange={this.handleFieldChange}
                                            /> 
                                            <label htmlFor="division_name" className="active">Division Name * 
                                            </label>
                                        </div>

                                        <div className="input-field col s3">
                                                
                                            <textarea
                                                id="division_descriptions"
                                                type="textarea"
                                                name="division_descriptions"
                                                className="materialize-textarea"
                                                value={this.state.division_descriptions}
                                                onChange={this.handleFieldChange}
                                            > 
                                            </textarea>
                                            <label htmlFor="division_descriptions" className="active">Division Descriptions 
                                            </label>
                                        </div>

                            
                                    </div>
                                        
                                            
                                        
                                </div>
                                <div className="row">                                            
                                    <div className="input-field col s12">
                                        <Link to="/division-setup">
                                            <button className="btn default   waves-light"   name="action">
                                                <i className="mdi-navigation-arrow-back left "></i>
                                                cancel
                                            </button>
                                        </Link>
                                        <button className="btn cyan waves-effect waves-light  " type="submit" name="action">{(this.state.action)}
                                            <i className="mdi-content-send right"></i>
                                        </button>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            );
        }
    }
}