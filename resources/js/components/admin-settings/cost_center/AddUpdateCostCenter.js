/**
 * Created by Sandeep Maurya on 15th dec 2018.
 */

import React from 'react';
import axios from 'axios';
import { toast } from 'react-toastify';
import {Link} from 'react-router-dom';
import {authHeader,logout,ITEM_PER_PAGE} from  "./../../../helpers/responseAuthozisation";
 


export default class AddUpdateCostcenter extends React.Component{
    constructor(props){
        super(props);

        this.state = {
            cost_center_name : '',
            cost_center_code : '', 
            cost_center_status:0,
            create_at : '',
            created_by : '',
            message : '',
            flag : true ,
            hasError:false,
            action: 'Create',
             
            master_company:[],
             
        }


        this.handleAddUpdateCostcenter = this.handleAddUpdateCostcenter.bind(this);
        this.handleFieldChange   = this.handleFieldChange.bind(this);

    }


    componentDidMount(){

        let self = this;
        const { id } = this.props.match.params;
        
        if(id != undefined){
        
            axios.get('/api/cost-center-details',{
                params:{
                    id : id
                },
                headers: authHeader()
            }).then(function(response){
                if(response.status == 200){
                    const data = response.data[0];
                    self.setState({
                        cost_center_name : data['cost_center_name'],
                        cost_center_id : data['cost_center_id'], 
                        cost_center_code: data['cost_center_code'] , 
                        action : 'Update' 
                        
                    });
                    
                }else{

                    toast(({ closeToast }) => <div>Something went wrong , please try after some time  </div>);
                    history.push('/cost-center-setup') ;   
                }
                

            })
        }
 
    }

    static getDerivedStateFromError(error) {
        // Update state so the next render will show the fallback UI.
        return { hasError: true };
    }

    /**
     * form validation
     * @returns {boolean|*}
     */
    validate(){
    	 this.state.flag = true;
          
        if(this.state.cost_center_name =='' || this.state.cost_center_name == 'undefined'){
            this.state.message = 'Please enter cost center name';
            this.state.flag = false;
        }
        
        if(this.state.cost_center_code =='' || this.state.cost_center_code == 'undefined'){
            this.state.message = 'Please enter cost center code';
            this.state.flag = false;
        }
        
        return this.state.flag;
    }



    handleAddUpdateCostcenter(event){
        event.preventDefault();
        const {history} = this.props;
        const cost_centers = this.state;

        
        if(this.validate()){
            axios.post('/api/add-update-cost-center', cost_centers,{headers  : authHeader() })
                .then(response => {
                    if(response.data.result){
                        toast(({ closeToast }) => <div>{response.data.message}</div>);
                        history.push('/cost-center-setup') ;
                    } else{
                        response.data.error.map((errors)=>{
                            toast(({ closeToast }) => <div>{errors} </div>);
                        });
                    }
                })
                .catch(error => {
                    
                    switch(error.response.status){
                        case 500 :
                            // logout();
                            toast(({ closeToast }) => <div> {error.response.data.message} </div>);
                            history.push('/cost-center-setup') ;
                            break ;
                        case 401 :
                            toast(({ closeToast }) => <div> {error.response.data.error} </div>);
                            logout();
                            break;
                        default :
                            toast(({ closeToast }) => <div> Something went wrong ! please try after some  time </div>);
                            history.push('/cost-center-setup') ;
                            break;
                    }
                    
                })
        }  else{
            toast(({ closeToast }) => <div>{this.state.message}</div>)

        }

    }
    handleFieldChange(event){
        
        this.setState({
            [event.target.name] : event.target.value
        });
    }
 
    render(){

        if(this.state.hasError){
           return <h4 className="header2">Something went wrong</h4>
        }   else{
            return(
                <div className="dashboard">
                    <div className="card-panel">
                        <div className="row">
                            <form className="col s12" onSubmit={this.handleAddUpdateCostcenter}>
                                <div id="leave_htmlForm">
                                    <div className="row">                                                
                                        
                                        <div className="col s12 l12 center-align">
                                            <h4 className="header2">{this.state.action} Cost Center </h4>
                                        </div>
                                    </div>
                                    <div className="row"> 
                                            
                                            
                                        <div className="input-field col s3">
                                            <input
                                                id="cost_center_code"
                                                type="text"
                                                name="cost_center_code"
                                                value={this.state.cost_center_code}
                                                onChange={this.handleFieldChange}
                                            /> 
                                            <label htmlFor="cost_center_code" className="active">Cost Center Code * 
                                            </label>
                                        </div>

                                        <div className="input-field col s3">
                                            <input
                                                id="cost_center_name"
                                                type="text"
                                                name="cost_center_name"
                                                value={this.state.cost_center_name}
                                                onChange={this.handleFieldChange}
                                            /> 
                                            <label htmlFor="cost_center_name" className="active">Cost Center Name * 
                                            </label>
                                        </div>

                                        

                            
                                    </div>
                                        
                                            
                                        
                                </div>
                                <div className="row">                                            
                                    <div className="input-field col s12">
                                        <Link to="/cost-center-setup">
                                            <button className="btn default   waves-light"   name="action">
                                                <i className="mdi-navigation-arrow-back left "></i>
                                                cancel
                                            </button>
                                        </Link>
                                        <button className="btn cyan waves-effect waves-light  " type="submit" name="action">{(this.state.action)}
                                            <i className="mdi-content-send right"></i>
                                        </button>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            );
        }
    }
}