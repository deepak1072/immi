/**
 * Created by Sandeep Maurya on 21st march 2020.
 */

import React,{Fragment} from 'react';
import {Link} from 'react-router-dom';
import {getRequest,postRequest} from  "./../../helpers/ApiRequest";
import ApproversBox from './../layout/ApproversBox';
import Sandesh from './../admin-settings/usable_components/Sandesh';
import Loader from './../admin-settings/usable_components/Loader';
import LoadingButton from './../admin-settings/usable_components/LoadingButton';
import DatePicker from "react-datepicker";
import "react-datepicker/dist/react-datepicker.css";
import InfoLabel from './../profile/InfoLabel';

export default class CompOffRequest extends React.Component{
    constructor(props){
        super(props);
        this.state = {
            hasError : false,
            action : 'Create ',
            for_date:new Date(),
            days : 0,
            commencement : 60,
            reason:'',
            day_type : 'Weekly-Off',
            punch_in : '10:00 AM',
            punch_out : '05:00 PM',
            working_hrs : 7,
            _appr : {},
            type : 'Create',
            is_loading : true,
            is_validating : false
        }
        this.handleRequest = this.handleRequest.bind(this);
        this.handleFieldChange = this.handleFieldChange.bind(this);
    }

    componentDidMount(){
        this.loadApprovers();
        this.loadRule();
    }

    handleRequest(event){
        event.preventDefault();
        let {history} = this.props;
        let info = this.state;
        let comoff = {
            for_date : info.for_date,
            days : info.days,
            reason : info.reason,
            day_type : 'Weekly-Off',
            punch_in : '10:00 AM',
            punch_out : '05:00 PM',
            working_hrs : 7,
            action : info.action,
            type : info.type
        };
        this.setState({
            is_validating : true
        });
        if(this.validateRule()){
            let url ='/api/submit-compoff-request';
            let request = postRequest(url,comoff);
            request.then(response=>{
                let data = response.data;
                Sandesh(data.message,data.type);
                if(data.result){
                    // Sandesh(data.message,data.type);
                    setTimeout(()=>{history.push('/')},3000);
                }
            })
        }
        this.setState({
            is_validating : false
        });
    }

    static getDerivedStateFromError(error) {
        // Update state so the next render will show the fallback UI.
        return { hasError: true };
    }

    validateRule(){
        let state = this.state;
        let reason = state.reason.trim();
        
        if(reason == '' || reason == undefined){
            Sandesh('Please enter reason','warn');
            return false;
        }
        return true ;
    }
    handleFieldChange(event){
        this.setState({
            [event.target.name] : event.target.value
        });
    }

    loadApprovers(){
        let self = this;
        let url = '/api/comp-off-approver';
        let request = getRequest(url,{events : 6});
        request.then((response)=>{
            const data = response.data.approvers;
            self.setState({
                '_appr' : data
            });
        });
    }

    handleChange(field,date) {
        this.setState({
            [field]: date
        });
        this.checkCredit(date);
    }

    checkCredit(date){
        let self = this;
        let url = '/api/compoff-credit?day='+date;
        let request = getRequest(url);
        request.then((response)=>{
            const data = response.data.data;
            self.setState({
                'commencement' : data['commencement'],
                'is_loading' : false
            });
        }); 
    }

    loadRule(){
        let self = this;
        let url = '/api/compoff-rule';
        let request = getRequest(url);
        request.then((response)=>{
            const data = response.data.data;
            self.setState({
                'commencement' : data['commencement'],
                'is_loading' : false
            });
        }); 
    }

    render(){
        let info = this.state;
        if(info.is_loading){
            return <Loader />
        }
        if(info.hasError){
           return <h4 className="header2">Something went wrong</h4>
        }   else{
            return(
                <div className="dashboard">
                    <div className="card-panel">
                        <div className="row">
                            <form className="col s12 l8" onSubmit={this.handleRequest}>
                                <div id="leave_htmlForm">
                                    <div className="row">
                                        <div className="col s12 l6 ">
                                            <h4 className="header2">{info.action} Comp-Off Request</h4>
                                        </div>
                                    </div>
                                    <div className="row">
                                        <div className="input-field col s12 m4">
                                            <DatePicker
                                            selected={info.for_date}
                                            onChange={this.handleChange.bind(this,'for_date')}
                                            dateFormat="MMM d, yyyy"
                                            />
                                            <label htmlFor="for_date" className="active"> Date 
                                            </label>
                                        </div>
                                        <div className="col s12 l4 ">
                                            <div className="chip teal white-text"> 
                                                    Credit Day(s) : {info.days}
                                                    <i className="material-icons "></i>
                                                 
                                            </div> 
                                        </div> 
                                    </div>
                                     
                                    <div className="row">
                                        
                                        <div className="col s12 m8">
                                            <label htmlFor="reason">Reason</label>
                                            <textarea id="reason" name="reason" 
                                            onChange={this.handleFieldChange}
                                            value={info.reason}
                                            className="materialize-textarea">
                                            </textarea>
                                        </div>
                                    </div>
                                    <div className="row" style={{"marginBottom":"10px"}}>
                                        <div className="col s12 m8">
                                            <InfoLabel  label ="Day Type" label_value= {info.day_type}></InfoLabel>
                                            <InfoLabel  label ="InTime" label_value= {info.punch_in}></InfoLabel>
                                            <InfoLabel  label ="OutTime" label_value= {info.punch_out}></InfoLabel>
                                            <InfoLabel  label ="Hour(s)" label_value= {info.working_hrs}></InfoLabel>
                                            
                                        </div>
                                    </div>
                                    <div className="row">
                                        <div className="col s12 m5">
                                            <ApproversBox 
                                                approvers = {info._appr}
                                            />
                                        </div>
                                    </div>
                                    <div className="row">                                            
                                        <div className="input-field col s12">
                                            {
                                                (info.is_validating) ? (
                                                    <div className="row col m4" > <LoadingButton /> </div>
                                                ) : (
                                                    <Fragment>
                                                        <Link to="/welcome">
                                                            <button className="btn default   waves-light"   name="action">
                                                                <i className="mdi-navigation-arrow-back left "></i>
                                                                cancel
                                                            </button>
                                                        </Link>
                                                        <button className="btn cyan waves-effect waves-light  " type="submit" name="action">{info.action}
                                                            <i className="mdi-content-send right"></i>
                                                        </button>
                                                    </Fragment>
                                                )
                                            }
                                        </div>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            );
        }
    }
}