/**
 * Created by Sandeep Maurya on 29th dec 2018.
 */

import React from 'react';
import  Loader from './../admin-settings/usable_components/Loader'; 
import {CARD} from  "./../../helpers/responseAuthozisation";
import {getRequest} from  "./../../helpers/ApiRequest";
export default class LeaveBalance extends React.Component{
    constructor(props){
        super(props);
        this.state = {
            hasError : false ,
            is_loading : true,
            leave_balance :[]
        }
    }

    componentDidMount(){
        let self = this;
        var url = "/api/leave-balance";
        let request = getRequest(url);
        request.then((response)=>{
            const data = response.data.leave_balance;
            self.setState({
                'leave_balance' : data,
                is_loading:false
            });
        });
    }

    static getDerivedStateFromError(error) {
        // Update state so the next render will show the fallback UI.
        return { hasError: true };
    }

    LeaveBalance(){
        const leave_balance = [];

        this.state.leave_balance.map((balance,index)=>{
            let color = CARD[parseInt(Math.random()*10)];
            let c_l = "card-content white-text " +color ;
            let b_l = "card-action "+ color+" darken-2 white-text";
            leave_balance.push(
                <div key={index} className="col s12 m4 l3">
                    <div className="card">
                        <div className={c_l}>
                            <p className="card-stats-title center-align"> {balance.lv_name} </p>
                            <h4 className="card-stats-number">{balance.balance}</h4>
                            <p className="card-stats-compare">
                            </p>
                        </div>
                        <div className={b_l}>
                            <div  className="center-align row">
                                <div className="col s6">Credit : {balance.credit}</div>
                                <div className="col s6">Debit : {balance.debit}</div>
                            </div>
                        </div>
                    </div>
                </div>
            )
        })
        return leave_balance;
    }
 
    render(){
        let new_leave_balance = this.LeaveBalance();
        if(this.state.is_loading){
            return <Loader />;
        } 
        if(this.state.hasError){
           return <h4 className="header2">Something went wrong</h4>
        }   else{
            return(
                <div className="dashboard">
                    <div className="card-panel">
                        <div className="row">
                            <form className="col s12" onSubmit={this.handleAddUpdateLeave}>
                                <div id="leave_htmlForm">
                                    <div className="row">                                                
                                        <div className="col s12 l12 center-align">
                                            <h4 className="header2">  Leave Balance </h4>
                                        </div>
                                    </div>
                                    <div id="card-stats">
                                        <div className="row">
                                            {
                                                new_leave_balance
                                            }
                                        </div>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            );
        }
    }
}