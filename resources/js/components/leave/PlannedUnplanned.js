import React from 'react';

const PlannedUnplanned =(props)=>{
    return (
        <div className="col s12 m6">  
            <input
                id="planned"
                type="radio"
                className="with-gap"
                name="p_u_p"
                value='1'
                checked = {props.p_u_p == '1'}
                onChange={props.handleRadioChange }
                    
            /> 
            <label htmlFor="planned">Planned</label>
            <input
                id="unplanned"
                type="radio" 
                name="p_u_p"
                className="with-gap"
                value= '0'
                checked = {props.p_u_p == '0'}
                onChange={props.handleRadioChange }
            />  
            <label htmlFor="unplanned">Unplanned</label>
        </div> 
    )
}

export default PlannedUnplanned;