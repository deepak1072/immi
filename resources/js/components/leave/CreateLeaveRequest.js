/**
 * Created by Sandeep Maurya on 29th dec 2018.
 */

import React from 'react';
import {Link} from 'react-router-dom';
import {authHeader,logout} from  "./../../helpers/responseAuthozisation";
import ApproversBox from './../layout/ApproversBox';
import {getRequest,postRequest,postRequestMultipart} from  "./../../helpers/ApiRequest";
import Sandesh from './../admin-settings/usable_components/Sandesh';
import Loader from './../admin-settings/usable_components/Loader';
import LoadingButton from './../admin-settings/usable_components/LoadingButton';
import DatePicker from "react-datepicker"; 
import "react-datepicker/dist/react-datepicker.css";
import PlannedUnplanned from './PlannedUnplanned';
export default class CreateLeaveRequest extends React.Component{
    constructor(props){
        super(props);
        this.state = {
            hasError : false,
            action : 'Create ',
            leave_balance : [] ,
            emp_code : 'sandeep' ,
            leave_type : 100,
            p_u_p :0, 
            p_u_p_reason : 0,
            master_id : '',
            from_date:new Date(),
            to_date:new Date(),
            from_type:'2FD',
            to_type:'2FD',
            days : 15,
            declaration : 0,
            attachment : 1,
            reason:'',
            rules : [],
            leave_appr : {},
            files:'',
            type : 'Create',
            is_loading : true,
            is_validating : false
        }
        this.handleLeaveRequest = this.handleLeaveRequest.bind(this);
        this.handleLeaveChange = this.handleLeaveChange.bind(this);
        this.handleFieldChange = this.handleFieldChange.bind(this);
        this.handleFileUploads = this.handleFileUploads.bind(this);

    }

    componentDidMount(){
        this.loadLeaveBalance();
        this.loadApprovers();
    }
    
    handleFileUploads(event){
        this.setState({
            files: event.target.files[0]
        });
    }

    loadLeaveBalance(){
        let self = this;
        let url = '/api/leave-balance';
        let request = getRequest(url);
        request.then((response)=>{
            const data = response.data.leave_balance;
            self.setState({
                'leave_balance' : data
            });
        });
    }
    handleLeaveRequest(event){
        event.preventDefault();
        let {history} = this.props;
        let info = this.state;
        let leave = new FormData();
        leave.append('action',info.action);
        leave.append('leave_type',info.leave_type);
        leave.append('p_u_p',info.p_u_p);
        leave.append('p_u_p_reason',info.p_u_p_reason);
        leave.append('master_id',info.master_id);
        leave.append('from_date',info.from_date);
        leave.append('to_date',info.to_date);
        leave.append('from_type',info.from_type);
        leave.append('to_type',info.to_type);
        leave.append('reason',info.reason);
        leave.append('declaration',info.declaration);
        leave.append('days',info.days);
        leave.append('files',info.files);
        leave.append('type',info.type);
        
        let url ='/api/submit-leave-request';
        this.setState({
            is_validating : true
        });
        if(this.validateRule()){
            let request = postRequestMultipart(url,leave);
            request.then(response=>{
                let data = response.data;
                Sandesh(data.message,data.type);
                if(data.result){
                    setTimeout(()=>{history.push('/')},3000);
                }
            })
        }
        this.setState({
            is_validating : false
        });
    }

    static getDerivedStateFromError(error) {
        // Update state so the next render will show the fallback UI.
        return { hasError: true };
    }

    validateRule(){
        let state = this.state;
        if(state.leave_type == 100 || state.leave_type == ''){
            Sandesh('Please select leave','warn');
            return false; 
        }
        let reason = state.reason.trim();
        if(reason == undefined || reason == ''){
            Sandesh('Please enter reason','warn');
            return false; 
        }
        if(state.from_date > state.to_date){
            Sandesh('From Date can not be greater than To Date','warn');
            return false;
        }
        return true ;
    }
    handleFieldChange(event){
        this.setState({
            [event.target.name] : event.target.value
        });
    }

    handleLeaveChange(event){
        const data = [];
        this.setState({
            [event.target.name] : event.target.value ,
            rules : data
        });
        this.loadLeaveRule(event.target.value);
        this.loadApprovers();
 
    }

    loadApprovers(){
        
        let self = this;
        let url = '/api/leave-approver';
        let days = this.state.days ;
        let type = this.state.leave_type;
        let request = getRequest(url,{leave_type : type,days : days});
        request.then((response)=>{
            const data = response.data.leave_approvers;
            self.setState({
                'leave_appr' : data,
                'is_loading' : false
            });
        });
    }

    handleChange(field,date) {
         
        if(field == 'from_date'){
            this.setState({
                [field]: date,
                to_date:date
            });
        }else{
            this.setState({
                [field]: date
            });
        } 
    }
    loadLeaveRule(id){
        if(id != 100 || id != ''){
            let self = this;
            let url = '/api/leave-rule';
            let request = getRequest(url,{id : id});
            request.then((response)=>{
                const data = response.data.rule;
                self.setState({
                    'rules' : data
                });
            });
        } 
    }
  
 
    render(){ 
        let info = this.state;
        if(info.is_loading){
            return <Loader />
        }
        if(info.hasError){
           return <h4 className="header2">Something went wrong</h4>
        }   else{
            return(
                <div className="dashboard">
                    <div className="card-panel">
                        <div className="row">
                            <form type="multipart/formdata" className="col s12 m8" onSubmit={this.handleLeaveRequest}>
                                <div id="leave_htmlForm">
                                    <div className="row">                                                
                        
                                        <div className="col s12 l4 ">
                                            <h4 className="header2">{info.action} Leave Request </h4>
                                        </div>
                                        <div className="col s12 l4 ">
                                        <div className="chip teal white-text"> 
                                                Leave Day(s) : {info.days}
                                                <i className="material-icons "></i>
                                            </div>
                                        </div> 
                                    </div>
                                    <div className="row">
                                        <div className="col s12 m4 input-field">
                                            <select
                                                value={info.leave_type}
                                                onChange={this.handleLeaveChange}
                                                name="leave_type"
                                                id="leave_type"
                                                className="browser-default"
                                                disabled = {info.is_disabled}
                                            >
                                                <option value=''>Select Leave Type </option>

                                                {
                                                    info.leave_balance.map((master,index)=>{
                                                        return(
                                                            <option key={index} value={master.lv_id}>{master.lv_name}</option>
                                                        )
                                                    })
                                                }
                                            </select>
                                            <label htmlFor="leave_type" className="active">Select Leave Type
                                            </label>
                                        </div>
                                        {
                                            (true) ? (<PlannedUnplanned 
                                                handleRadioChange = {this.handleFieldChange}
                                                p_u_p = {info.p_u_p}
                                            />) : ('')
                                        }
                                    </div>
                                    <div className="row">
                                        <div className="input-field col s12 m4">
                                            
                                            <DatePicker
                                                selected={info.from_date}
                                                onChange={this.handleChange.bind(this,'from_date')}
                                                dateFormat ="d-M-yyyy"
                                            />
                                            <label htmlFor="from_date" className="active">From Date 
                                            </label>
                                        </div>
                                        <div className="col s12 m4 input-field">
                                            <select
                                                value={info.from_type}
                                                onChange={this.handleFieldChange.bind(this)}
                                                name="from_type"
                                                id="from_type"
                                                className="browser-default" 
                                            > 
                                            <option value="2FD">Full Day</option>  
                                            <option value="1FH">First Half</option>
                                            <option value="2FH">Second Half</option>
                                            </select>
                                            <label htmlFor="from_type" className="active">From Period
                                            </label>
                                        </div>
                                    </div>
                                    <div className="row">
                                        <div className="input-field col s12 m4">
                                            <DatePicker
                                                selected={info.to_date}
                                                onChange={this.handleChange.bind(this,'to_date')}
                                                dateFormat ="d-M-yyyy"
                                            />
                                            <label htmlFor="to_date" className="active">To Date 
                                            </label>
                                        </div>
                                        <div className="col s12 m4 input-field">
                                            <select
                                                value={info.to_type}
                                                onChange={this.handleFieldChange.bind(this)}
                                                name="to_type"
                                                id="to_type"
                                                className="browser-default" 
                                            > 
                                            <option value="2FD">Full Day</option>  
                                            <option value="1FH">First Half</option>
                                            <option value="2FH">Second Half</option>
                                            </select>
                                            <label htmlFor="to_type" className="active">To Period
                                            </label>
                                        </div>
                                    </div>
                                    <div className="row">
                                        <div className="col s12 m8">
                                            
                                            <label htmlFor="reason">Reason</label>
                                            <textarea id="reason" name="reason" 
                                            onChange={this.handleFieldChange}
                                            value={info.reason}
                                            className="materialize-textarea">

                                            </textarea>
                                            
                                        </div>
                                    </div>
                                    {
                                        (info.attachment ==1 ) ? (
                                            <div className="row">
                                                <div className="col s12 m8 l9">
                                                    <input name = 'file' type="file"  onChange={this.handleFileUploads} />
                                                </div>
                                            </div>
                                        ) : ('')
                                    }
                                    
                                    <div className="row">
                                        <div className="col s12 m5">
                                            <ApproversBox 
                                                approvers = {info.leave_appr}
                                            />
                                        </div>
                                    </div>
                                    
                                    <div className="row">                                            
                                        <div className="input-field col s12">
                                            <Link to="/welcome">
                                                <button className="btn default   waves-light"   name="action">
                                                    <i className="mdi-navigation-arrow-back left "></i>
                                                    cancel
                                                </button>
                                            </Link>
                                            <button className="btn cyan waves-effect waves-light  " type="submit" name="action">{(info.action)}
                                                <i className="mdi-content-send right"></i>
                                            </button>
                                        </div>
                                    </div>
                                </div>
                            </form>
                            <div className="col s12 m4"></div>
                        </div>
                    </div>
                        
                </div>
            );
        }
    }
}