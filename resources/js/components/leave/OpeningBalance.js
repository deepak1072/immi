/**
 * Created by Sandeep Maurya on 2nd Jan 2020
 */

import React from "react";
import _ from "lodash";
import {Link} from 'react-router-dom';
import ReactTable from "react-table";
import "react-table/react-table.css";
import {ITEM_PER_PAGE,__DOCUMENT__} from  "../../helpers/responseAuthozisation";
import {getRequest,postRequestMultipart} from  "../../helpers/ApiRequest";
import Sandesh from './../admin-settings/usable_components/Sandesh';
import LoadingButton from './../admin-settings/usable_components/LoadingButton';

export  default class OpeningBalance extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            data: [],
            pages: null,
            loading: true,
            is_loading : false,
            files : ''
        };
        this.fetchData = this.fetchData.bind(this);
        this.handleFileChange = this.handleFileChange.bind(this);
        this.uploadFile = this.uploadFile.bind(this);
    }

    fetchData(state, instance) {
        this.setState({ loading: true });
        let url = '/api/opening-balance';
        let request = getRequest(url,{
            filter : state.filtered,
            pageSize : state.pageSize,
            page : state.page,
            sort : state.sorted
        });
        request.then((response) => {
            let filteredData = response.data;
            this.setState({
                data: filteredData.data,
                pages: Math.ceil(filteredData.count / state.pageSize),
                loading: false
            });
        });
    }

    handleFileChange(event){
        this.setState({
            files : event.target.files[0]
        });
    }

    uploadFile(event){
        event.preventDefault();
        let self = this;
        let files = this.state.files;
        if(files == '' || files == undefined){
            Sandesh('Please select file for upload','warn');
            return false;
        }
        this.setState({
            is_validating : true
        });
        let formdata = new FormData();
        formdata.append('uploaded_file',files);
        let url ='/api/opening-balance';
        let request = postRequestMultipart(url,formdata);
        request.then(response=>{
            let data = response.data;
            Sandesh(data.message,data.type);
            if(data.result){
                setTimeout(()=>{
                    window.location.reload();
                },3000);
            }
        })
    }

    render() {
        const { data, pages, loading,is_validating } = this.state;
        return (
            <div>
                <div className="dashboard">
                    <div className="card-panel">
                    <div className="row">
                            
                            <div className="col s12 m6 l4 center-align">
                                <h4 className="header2">
                                    Opening Balance
                                </h4>
                            </div>
                            <div className=" col s3 m2">
                                <Link to={"docs_format/Opening_Balance.csv"} target="_blank">
                                    <button className="btn cyan waves-effect waves-light"  >
                                        <i className="mdi-file-file-download left"></i>
                                        Sheet
                                    </button>
                                </Link>
                            </div>
                            <div className=" col s3 m2">
                                <input type = 'file' onChange={this.handleFileChange}/>
                            </div>
                            <div className=" col s3 m2">
                                {
                                    (is_validating)?(
                                        <LoadingButton/>
                                    ):(
                                        <button type = "btn" onClick={this.uploadFile} className="btn cyan waves-effect waves-light  ">
                                        Upload
                                        <i className="mdi-file-file-upload left"></i>
                                    </button>
                                    )
                                }
                                
                            </div>
                        </div>
                        
                        <ReactTable
                            columns={[
                                {
                                    Header: "Requester",
                                    accessor: "emp_name"
                                },
                                {
                                    Header: "Leave Type",
                                    accessor: "leave_name"
                                },
                                {
                                    Header: "Credit ",
                                    accessor: "balance"
                                },
                                {
                                    Header: "Credit On",
                                    accessor: "created_at"
                                },
                                {
                                    Header : "Effective From" ,
                                    accessor : "effective_date"
                                },
                                {
                                    Header: "Remarks",
                                    accessor: "remarks"
                                } ,
                                {
                                    Header: "Credited By",
                                    accessor: "created_by"
                                }
                            ]}
                            manual 
                            data={data}
                            pages={pages} 
                            loading={loading}  
                            onFetchData={this.fetchData}  
                            filterable
                            defaultPageSize={ITEM_PER_PAGE}
                            className="-highlight"  
                        />
                        <br />
                    </div>
                </div>

            </div>
        );
    }
}


