import React from 'react';
import Team from './../admin-settings/usable_components/Team';
import {authHeader} from  "./../../helpers/responseAuthozisation";
export default class TeamOnLeave extends React.Component{
    constructor(props){
        super(props);
        this.state = {
            req_user_id : 'eyJpdiI6ImJCVHRRRkNwUXRNeGZNYXdxZHFVN0E9PSIsInZhbHVlIjoiZmdBc0d2UldqUDlJVCtwXC84QlFHbUE9PSIsIm1hYyI6IjljMjBlODBmYWNmODFlYzNhZDc5NjVkN2VhZWFiYTc4YmNlNDhlZjFmMjJlZjQ3YWRiYTNlZDBiZWM1NDU4MjkifQ',
            from_date : '',
            to_date : '',
            hasError : false,  
            team : [] 
        } 
    }

    componentDidMount(){
        let info = this.props; 
        let self = this;
        if( info.from_date != '' || info.from_date != undefined){
            axios.get('/api/team-on-leave',{
                params:{
                    req_user_id : this.state.req_user_id,
                    from_date : info.from_date,
                    to_date : info.to_date ,
                    role : 1 
                },
                headers : authHeader()
            }).then(response=>{
                if(response.data.status == 200){
                    let  data = response.data;  
                    if(data.result){ 
                        self.setState(data.data);
                    }  
                } 
            }) .catch(error=> {
                
            }) ;
        } 
    }

    
    renderTeam(){ 
        return this.state.team.map((team,index)=>{
            return <Team 
                key = {index+'_'+ team.user_id}
                info = {team}
            />
        })
    }
    render(){
        if(this.state.team.length < 1){
            return '';
        }
        return(
            <ul id="issues-collection" className="collection">
                <li className="collection-item avatar">
                    <i className="mdi-social-group circle"></i>
                    <h6 className="header2">Team On Vacation</h6>
                </li> 
                {
                    this.renderTeam()
                } 
            </ul>
        )
    }
}

