/**
 * Created by Sandeep Maurya on 29th dec 2018.
 */

import React from 'react';
import  Loader from './admin-settings/usable_components/Loader'; 
import {CARD} from  "./../helpers/responseAuthozisation";
import {getRequest} from  "./../helpers/ApiRequest";

 
  
export default class Home extends React.Component{
    constructor(props){
        super(props);
        this.state = {
            hasError : false ,
            is_loading : true, 
        }
        this.handleFieldChange = this.handleFieldChange.bind(this);
    }

    componentDidMount(){
        this.loadClientsCount(1);
    }

    handleFieldChange(event){
      this.setState({
          [event.target.name] : event.target.value
      });
      this.loadClientsCount(event.target.value);
  }

    static getDerivedStateFromError(error) {
        // Update state so the next render will show the fallback UI.
        return { hasError: true };
    }

    loadClientsCount( period = 1 ){
      let self = this;
        var url = "/api/dashboard";
        let request = getRequest(url,{period:period});
        request.then((response)=>{
            const data = response.data.data;
            self.setState({ 
                is_loading:false
            });
        });
    }

    ClientsCount(){
        const client_data = [];

        
        return client_data;
    }
 
    render(){
        let new_client_data = this.ClientsCount();
        if(this.state.is_loading){
            return <Loader />;
        } 
        if(this.state.hasError){
           return <h4 className="header2">Something went wrong</h4>
        }   else{
            return(
                <div className="dashboard">
                    <div className="card-panel">
                        <div className="row">
                            <div id="card-stats">
                                <form className="col s12" >
                                    <div id="leave_htmlForm" className="center"> 
                                        <h3 className="merienda ">Welcome To The Portal Of Immigration</h3>
                                        <p className="merienda center">You can add / update relevant data here...</p>
                                        
                                    </div>
                                </form> 
                                
                            </div>
                        </div>
                    </div>
                </div>
            );
        }
    }
}


