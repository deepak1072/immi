/**
 * Created by Sandeep Maurya on 21/07/2019.
 * @ 11:55 PM
 */

import React ,{ Fragment }from 'react'; 
import axios from 'axios';
 
import SubTabTitle from './SubTabTitle';
import InfoLabel from './InfoLabel';

import {authHeader,logout} from  "./../../helpers/responseAuthozisation";

export default class PersonalDetails extends React.Component{
	constructor(props){
		super(props); 
		this.state = {
			name : '',
			gender : '',
			dob : '',
			blood_group : '',
			m_status : '',
			guardian_name :'',
            nationality : '',
            religion : '',
		}
	}

	 componentDidMount(){

        let self = this;
        const  id  = 'eyJpdiI6ImJCVHRRRkNwUXRNeGZNYXdxZHFVN0E9PSIsInZhbHVlIjoiZmdBc0d2UldqUDlJVCtwXC84QlFHbUE9PSIsIm1hYyI6IjljMjBlODBmYWNmODFlYzNhZDc5NjVkN2VhZWFiYTc4YmNlNDhlZjFmMjJlZjQ3YWRiYTNlZDBiZWM1NDU4MjkifQ';
        const {history}  = this.props;
      
        if(id != undefined){
           
            axios.get('/api/personal-details',{
                params:{
                    req_user_id : id
                },
                headers : authHeader()
            }).then(function(response){
            
                if(response.status === 200){
                    const data = response.data;
                    const bank =  data.data;
                     
                    self.setState({
                    	name : bank.salary_bank,
						gender : bank.s_ifsc,
						dob : bank.salary_account,
						blood_group : bank.reim_bank,
						m_status : bank.reim_ifsc,
                        guardian_name:bank.reim_account,
                        nationality:bank.reim_account,
						religion:bank.reim_account
                    });

                     
                }else{
                    toast(({ closeToast }) => <div>Something went wrong , please try after some time  </div>);
                    history.push('/leave-setup') ;   
                }
            }) .catch(function(error){

                switch(error.response.status){
                    case 500 :
                        // logout();
                        toast(({ closeToast }) => <div> {error.response.data.message} </div>);
                        history.push('/leave-setup') ;
                        break ;
                    case 401 :
                        toast(({ closeToast }) => <div> {error.response.data.error} </div>);
                        logout();
                        break
                    default :
                        toast(({ closeToast }) => <div> could not fetch leave details </div>);
                        history.push('/leave-setup') ;
                        break;
                }
            }) ;
        }
    }

	render(){
		const info = this.state ;
		return(
			<Fragment>
            	<SubTabTitle 
                    title = {this.props.sub_tab_name}
                />
                <div className="row separator"> 
	                <InfoLabel label="Name" label_value = {info.name}col="col s3" />
	                <InfoLabel label="Gender" label_value = {info.gender} col="col s3" />
	                <InfoLabel label="DOB" label_value = {info.dob} col="col s3" /> 
                    <InfoLabel label="Blood Group" label_value = {info.blood_group} col="col s3" />
                </div>
                <div className="row separator"> 
	                
	                <InfoLabel label="Marital Status" label_value = {info.m_status} col="col s3" />
                    <InfoLabel label="Guardian Name" label_value = {info.guardian_name} col="col s3" /> 
                    <InfoLabel label="Nationality" label_value = {info.nationality} col="col s3" /> 
	                <InfoLabel label="Religion" label_value = {info.religion} col="col s3" /> 
                </div>
                
	          
            </Fragment>
		)
	}
}