import React,{Fragment} from 'react';
import { BrowserRouter, Route, Switch } from 'react-router-dom';

import AddUpdateOfficialDetails from './AddUpdateOfficialDetails';
import OfficialDetails from './OfficialDetails';
import AddUpdateBankDetails from './AddUpdateBankDetails';
import BankDetails from './BankDetails';
import PersonalDetails from  './PersonalDetails';
import ContactDetails from  './ContactDetails';

 
export default class ProfileCombine extends React.Component{
    constructor(props){
        super(props);
        this.state = {
        	action_mode : 1 // 1->view ,2->edit/update , 3->add
        }
         
    }

    change_mode(id){
    	this.setState({
    		action_mode : id
    	});
    }

    showSelectPage(){

    	const active_sub_tab = this.props.active_sub_tab;
    	const action_mode = this.state.action_mode;
    	switch(active_sub_tab){
    		case 1 : 

    				if(action_mode === 1){
    					return(
							<OfficialDetails
								sub_tab= {active_sub_tab}
								sub_tab_name = {this.props.active_sub_tab_name}
								action_mode = {action_mode}
								change_mode = {this.change_mode.bind(this)}
							 />
						)
    				}
    			 
    				return(
    					<AddUpdateOfficialDetails
    						sub_tab= {active_sub_tab}
    						sub_tab_name = {this.props.active_sub_tab_name}
    						action_mode = {action_mode}
    						change_mode = {this.change_mode.bind(this)}
    					 />
    				)
    		 
    			break;
    		case 2 : 

    			if(action_mode === 1){
    				return(
    					<BankDetails
    						sub_tab= {active_sub_tab}
							sub_tab_name = {this.props.active_sub_tab_name}
							action_mode = {action_mode}
							change_mode = {this.change_mode.bind(this)}
    					/>
    				)
    			}
    			return(
    				<AddUpdateBankDetails 
    					sub_tab= {active_sub_tab}
    					sub_tab_name = {this.props.active_sub_tab_name}
    					action_mode = {action_mode}
    					change_mode = {this.change_mode.bind(this)}
    				/>
    			)
    		case 9 : 
    			if(action_mode === 1){
    				return(
    					<PersonalDetails
    						sub_tab= {active_sub_tab}
							sub_tab_name = {this.props.active_sub_tab_name}
							action_mode = {action_mode}
							change_mode = {this.change_mode.bind(this)}
    					/>
    				)
    			}
    			return(
    				<AddUpdateBankDetails 
    					sub_tab= {active_sub_tab}
    					sub_tab_name = {this.props.active_sub_tab_name}
    					action_mode = {action_mode}
    					change_mode = {this.change_mode.bind(this)}
    				/>
    			)
    		case 10 : 
    			if(action_mode === 1){
    				return(
    					<ContactDetails
    						sub_tab= {active_sub_tab}
							sub_tab_name = {this.props.active_sub_tab_name}
							action_mode = {action_mode}
							change_mode = {this.change_mode.bind(this)}
    					/>
    				)
    			}
    			return(
    				<AddUpdateBankDetails 
    					sub_tab= {active_sub_tab}
    					sub_tab_name = {this.props.active_sub_tab_name}
    					action_mode = {action_mode}
    					change_mode = {this.change_mode.bind(this)}
    				/>
    			)
    		
    		default : 
    		    
    		   	return(
    		   		<AddUpdateOfficialDetails 
    		   			sub_tab= {active_sub_tab}
    					sub_tab_name = {this.props.active_sub_tab_name}
    					action_mode = {action_mode}
    					change_mode = {this.change_mode.bind(this)}
    		   		/>
    		   	)
    		  
    	}
    }
     
    render(){
        return(
            <Fragment>
 				{
 					this.showSelectPage()
 				}
            </Fragment>
        )
    }
}

