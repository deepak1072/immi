/**
 * Created by Sandeep Maurya on 21/07/2019.
 * @ 11:55 PM
 */

import React ,{ Fragment }from 'react'; 
import axios from 'axios';
 
import SubTabTitle from './SubTabTitle';
import InfoLabel from './InfoLabel';

import {authHeader,logout} from  "./../../helpers/responseAuthozisation";

export default class BankDetails extends React.Component{
	constructor(props){
		super(props); 
		this.state = {
			s_bank_name : '',
			s_bank_ifsc : '',
			s_bank_account : '',
			r_bank_name : '',
			r_bank_ifsc : '',
			r_bank_account:''
		}
	}

	 componentDidMount(){

        let self = this;
        const  id  = 'eyJpdiI6ImJCVHRRRkNwUXRNeGZNYXdxZHFVN0E9PSIsInZhbHVlIjoiZmdBc0d2UldqUDlJVCtwXC84QlFHbUE9PSIsIm1hYyI6IjljMjBlODBmYWNmODFlYzNhZDc5NjVkN2VhZWFiYTc4YmNlNDhlZjFmMjJlZjQ3YWRiYTNlZDBiZWM1NDU4MjkifQ';
        const {history}  = this.props;
      
        if(id != undefined){
           
            axios.get('/api/bank-details',{
                params:{
                    req_user_id : id
                },
                headers : authHeader()
            }).then(function(response){
            
                if(response.status === 200){
                    const data = response.data;
                    const bank =  data.data;
                     
                    self.setState({
                    	s_bank_name : bank.salary_bank,
						s_bank_ifsc : bank.s_ifsc,
						s_bank_account : bank.salary_account,
						r_bank_name : bank.reim_bank,
						r_bank_ifsc : bank.reim_ifsc,
						r_bank_account:bank.reim_account
                    });

                     
                }else{
                    toast(({ closeToast }) => <div>Something went wrong , please try after some time  </div>);
                    history.push('/leave-setup') ;   
                }
            }) .catch(function(error){

                switch(error.response.status){
                    case 500 :
                        // logout();
                        toast(({ closeToast }) => <div> {error.response.data.message} </div>);
                        history.push('/leave-setup') ;
                        break ;
                    case 401 :
                        toast(({ closeToast }) => <div> {error.response.data.error} </div>);
                        logout();
                        break
                    default :
                        toast(({ closeToast }) => <div> could not fetch leave details </div>);
                        history.push('/leave-setup') ;
                        break;
                }
            }) ;
        }
    }

	render(){
		const info = this.state ;
		return(
			<Fragment>
            	<SubTabTitle 
                    title = {this.props.sub_tab_name}
                />
                <div className="row separator"> 
	                <InfoLabel label="Salary Bank Name" label_value = {info.s_bank_name}col="col s3" />
	                <InfoLabel label="IFSC CODE" label_value = {info.s_bank_ifsc} col="col s3" />
	                <InfoLabel label="Salary Account No" label_value = {info.s_bank_account} col="col s3" /> 
                </div>
                <div className="row separator"> 
	                <InfoLabel label="Reimbursement Bank Name" label_value = {info.r_bank_name} col="col s3" />
	                <InfoLabel label="IFSC CODE" label_value = {info.r_bank_ifsc} col="col s3" />
	                <InfoLabel label="Reimbursement Account No" label_value = {info.r_bank_account} col="col s3" /> 
                </div>
                
	          
            </Fragment>
		)
	}
}