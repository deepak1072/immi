
import React,{Fragment} from 'react';

export default class SubTabTitle extends React.Component{
    constructor(props){
        super(props);
        this.state = {
        	active_sub_tab : this.props.active_sub_tab
        }
    }

    render(){
    	return(
			<div className="row sub_title">
		        <div className="col s3"><p className="">{this.props.title}</p></div>
                <a className="btn-floating activator   waves-effect waves-light darken-2 right">
                    <i className="mdi-editor-mode-edit"></i>
                </a>
		    </div>
		)
    }

}
 
 