/**
 * Created by Sandeep Maurya on 11/18/2018.
 */
import React from 'react';
import './profile.css';
import ProfileTop from './ProfileTop';
import MainTabAccess from './MainTabAccess';
import SubTabAccess from './SubTabAccess';
import ProfileCombine from './ProfileCombine';
export default class EmployeeProfile extends React.Component{
    constructor(props){
        super(props);
        this.state = {
            name : 'Sandeep Maurya',
            code : 'SANDEEP0001',
            designation : 'Project Manager',
            img_url : '',
            reporting_manager : 'Saurabh Maurya',
            funct_manager : 'Rahul Maurya ',
            mobile_number : '8050820549',
            email_id : 'sandeepmaurya@gmail.com',
            department : 'Product Engineering ',
            location : 'Gurugram ',
            active_main_tab : 1 ,
            active_sub_tab : 1 ,
            active_sub_tab_name : 'Official Details',
            statsu_name : 'Active',
            main_tab : [
                {
                    name : 'Official ',
                    id : 1 ,
                    child_tab : [
                            {
                                name : 'Official Details',
                                child_id : 1 , 
                                url :'/profile/official-details'
                            },
                            {
                                name : 'Bank Details',
                                child_id : 2 , 
                                url : '/profile/bank-details'
                            },
                            {
                                name : 'Identity Details',
                                child_id : 3 , 
                                url : '/profile/identity-details'
                            },
                            {
                                name : 'Separation Details',
                                child_id : 4 , 
                                url : '/profile/separation-details'
                            },
                            {
                                name : 'Payroll Details',
                                child_id : 5 , 
                                url : '/profile/payroll-details'
                            },
                            {
                                name : 'Official Letters',
                                child_id : 6 ,
                                url : '/profile/official-letter' 
                            },
                            {
                                name : 'Assets',
                                child_id : 7 , 
                                url : '/profile/assets'
                            },
                            {
                                name : 'Document Repository',
                                child_id : 8 , 
                                url : '/profile/document-repository'
                            }
                        ]
                },
                {
                    name : 'Personal ',
                    id : 2 ,
                    child_tab : [
                        {
                            name : 'Personal Details',
                            child_id : 9 , 
                            url : '/profile/persiofnal-details'
                        },
                        {
                            name : 'Contact Details',
                            child_id : 10 , 
                            url : '/profile/contact-details'
                        },
                        {
                            name : 'Emergency Contact Details',
                            child_id : 11 , 
                            url : '/profile/emergency-contact-details'
                        },
                        {
                            name : 'Family Details',
                            child_id : 12 , 
                            url : '/profile/family-details'
                        },
                        {
                            name : 'Qualification',
                            child_id : 13 , 
                            url : '/profile/qualification'
                        },
                        {
                            name : 'Nominee',
                            child_id : 14 , 
                            url : '/profile/nominee'
                        },
                        {
                            name : 'Language',
                            child_id : 15 , 
                            url:'/profile/language'
                        },
                        {
                            name : 'Past Experience',
                            child_id : 16, 
                            url : '/profile/past-experiences'
                        } 

                    ]
                },
                {
                    name : 'Compensation ',
                    id : 3  ,
                    child_tab : [
                        {
                            name : 'Compensation',
                            child_id : 17 , 
                            url : '/profile/compensation'
                        },
                        {
                            name : 'Compensation History',
                            child_id : 18 , 
                            url : '/profile/compensation-history'
                        }
                    ]
                },
                {
                    name : ' History',
                    id : 4 ,
                    child_tab : [
                        {
                            name : 'Official History',
                            child_id : 19 , 
                            url : '/profile/official-change-history'
                        },
                        {
                            name : 'Personal History',
                            child_id : 20 ,
                            url : '/profile/personal-change-history'
                        }
                    ]
                }
            ] 
        }
    }
    mainTabChange(id){  
        const sub_tab = this.state.main_tab.find((main,i)=>{
            if(main.id === id){
                return main.child_tab[0];
            } 
        }) 
        this.setState({
            active_main_tab : id,
            active_sub_tab : sub_tab.child_tab[0].child_id,
            active_sub_tab_name : sub_tab.child_tab[0].name
        });

    }
    changeSubTab(subtab){ 
        this.setState({ 
            active_sub_tab : subtab.child_id,
            active_sub_tab_name : subtab.name
        });
    }
    render(){
        const user = this.state;
        return(
            <div className="dashboard">
                <div className="card-panel">
                    <div className="row"> 
                            <div id="profile-page" className=" ">
                            <div id="profile-page-header" className="card">
                                <ProfileTop 
                                    name = {user.name}
                                    mobile = {user.mobile_number}
                                    email = {user.email_id}
                                    location = {user.location}
                                    code = {user.code}
                                    department = {user.department}
                                    reporting = {user.reporting_manager}
                                    status= {user.statsu_name}
                                    designation ={user.designation}
                                /> 
                            </div> 
                            <div id="profile-page-content" className="row"> 
                                <div id="profile-page-sidebar" className="col s12 m3 ">
                                    <SubTabAccess 
                                        tab = {user.main_tab}
                                        active_sub_tab = {user.active_sub_tab}
                                        active_main_tab = {user.active_main_tab}
                                        changeSubTab = {this.changeSubTab.bind(this)}
                                    /> 
                                </div>
                                
                                <div id="profile-page-wall" className="col s12 m9"> 
                                    <div id="profile-page-wall-share" className="row">
                                        <div className="col s12">
                                            <MainTabAccess 
                                                tab = {user.main_tab}
                                                active_main_tab = {user.active_main_tab}
                                                mainTabChange = {this.mainTabChange.bind(this)}

                                            />
                                    
                                            <div id="UpdateStatus" className="tab-content col s12  ">
                                                <ProfileCombine 
                                                    active_sub_tab = {user.active_sub_tab}
                                                    active_sub_tab_name = {user.active_sub_tab_name}
                                                /> 
                                            </div> 
                                        </div>
                                    </div> 
                                </div> 
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        );
    }
}