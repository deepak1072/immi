
import React from 'react';
export default class InfoLabel extends React.Component{
	constructor(props){
		super(props);
	}
	render(){
		const info = this.props;
		const col = (info.col !== undefined) ? info.col : 'col s12 m3';
		return(
			<div className={col}>            
                <p className="medium-small text_grey uppercase"> {info.label} </p>                        
                <p className="medium-small grey-text truncate">{info.label_value}</p>                        
            </div>
		)
	}
}