/**
 * Created by Sandeep Maurya on 21/07/2019.
 * @ 11:55 PM
 */

import React ,{ Fragment }from 'react'; 
import axios from 'axios';
 
import SubTabTitle from './SubTabTitle';
import InfoLabel from './InfoLabel';

import {authHeader,logout} from  "./../../helpers/responseAuthozisation";

export default class OfficialDetails extends React.Component{
	constructor(props){
		super(props); 
		this.state = {
			access_code: '',
			busi_code: '',
			buss_name: '',
			comp_code: '',
			cost_code: '',
			designation_code: '',
			designation_name: '',
			division_code: '',
			doj: '',
			dol: '',
			domicile_code: '',
			effective_date: '',
			emp_code: '',
			empid: '',
			ero: '',
			function_code: '',
			function_name: '',
			grade_code: '',
			loc_code: '',
			mngr_code: '',
			mngr_code2: '',
			oemail_id: '',
			process_code: '',
			region_code: '',
			role_code: '',
			status_code: '',
			sub_buss_name: '',
			sub_function_name: '',
			subbusi_code: '',
			subfunction_code: '',
			type_code: '',
			user_id: '',
			wloc_code: '',
			work_phone: ''
		}
	}

	 componentDidMount(){

        let self = this;
        const  id  = 'eyJpdiI6ImJCVHRRRkNwUXRNeGZNYXdxZHFVN0E9PSIsInZhbHVlIjoiZmdBc0d2UldqUDlJVCtwXC84QlFHbUE9PSIsIm1hYyI6IjljMjBlODBmYWNmODFlYzNhZDc5NjVkN2VhZWFiYTc4YmNlNDhlZjFmMjJlZjQ3YWRiYTNlZDBiZWM1NDU4MjkifQ';
        const {history}  = this.props;
      
        if(id != undefined){
           
            axios.get('/api/official-details',{
                params:{
                    req_user_id : id
                },
                headers : authHeader()
            }).then(function(response){
            
                if(response.status === 200){
                    const data = response.data;
                    const off_data =  data.data;
                     
                    self.setState(off_data);

                     
                }else{
                    toast(({ closeToast }) => <div>Something went wrong , please try after some time  </div>);
                    history.push('/leave-setup') ;   
                }
            }) .catch(function(error){

                switch(error.response.status){
                    case 500 :
                        // logout();
                        toast(({ closeToast }) => <div> {error.response.data.message} </div>);
                        history.push('/leave-setup') ;
                        break ;
                    case 401 :
                        toast(({ closeToast }) => <div> {error.response.data.error} </div>);
                        logout();
                        break
                    default :
                        toast(({ closeToast }) => <div> could not fetch leave details </div>);
                        history.push('/leave-setup') ;
                        break;
                }
            }) ;
        }
    }

	render(){
		const info = this.state ;
		return(
			<Fragment>
            	<SubTabTitle 
                    title = {this.props.sub_tab_name}
                />
                <div className="row separator"> 
	                <InfoLabel label="Employee Name" label_value = "Sandeep " col="col s3" />
	                <InfoLabel label="Gender" label_value = "Sandeep " col="col s3" />
	                <InfoLabel label="Employee Status" label_value = "Sandeep " col="col s3" />
	                <InfoLabel label="Employee Code" label_value = {info.emp_code} col="col s3" />
                </div>
                <div className="row separator"> 
	                <InfoLabel label="Access Code" label_value = {info.access_code} col="col s3" />
	                <InfoLabel label="Portal Code" label_value = {info.user_id} col="col s3" />
	                <InfoLabel label="Official EMail" label_value = {info.oemail_id} col="col s3" />
	                <InfoLabel label="DOJ" label_value = {info.doj}col="col s3" />
                </div>
                <div className="row separator"> 
	                <InfoLabel label="Company" label_value = "Sandeep " col="col s3" />
	                <InfoLabel label="Business Unit" label_value = {info.buss_name} col="col s3" />
	                <InfoLabel label="Sub Business Unit" label_value ={info.sub_buss_name} col="col s3" />
	                <InfoLabel label="Location" label_value = "Sandeep " col="col s3" />
	            </div>
	            <div className="row separator"> 	
	                <InfoLabel label="Work Location" label_value = "Sandeep " col="col s3" />
	                <InfoLabel label="Function" label_value = {info.function_name}col="col s3" />
	                <InfoLabel label="Function" label_value = "Sandeep " col="col s3" />
	                <InfoLabel label="Sub Function" label_value = {info.sub_function_name} col="col s3" />
	            </div>
	            <div className="row separator"> 
	                <InfoLabel label="Cost Center" label_value = "Sandeep " col="col s3" />
	                <InfoLabel label="Process" label_value = "Sandeep " col="col s3" />
	                <InfoLabel label="Designation" label_value = {info.designation_name}col="col s3" />
	                <InfoLabel label="Role" label_value = {info.role_name}col="col s3" />
	            </div>
	            <div className="row separator"> 
	                <InfoLabel label="Employee Type" label_value = {info.type_name} col="col s3" />
	                <InfoLabel label="Band" label_value = {info.band_name} col="col s3" />
	                <InfoLabel label="Grade" label_value = {info.grade_name} col="col s3" />
	                <InfoLabel label="Division" label_value = {info.division_name} col="col s3" />
	            </div>
	            <div className="row separator"> 
	                <InfoLabel label="Region Master" label_value = {info.regions_name} col="col s3" />
	                <InfoLabel label="Reporting To" label_value = "Sandeep " col="col s3" />
	                <InfoLabel label="Reporting Manager" label_value = "Sandeep " col="col s3" />
	                <InfoLabel label="Reporting Manager" label_value = "Sandeep " col="col s3" />
	            </div>
	            <div className="row separator"> 
	                <InfoLabel label="Functional Supervisor" label_value = "Sandeep " col="col s3" />
	                <InfoLabel label="ERO / HR" label_value = "Sandeep " col="col s3" />
	                <InfoLabel label="DOB" label_value = {info.dob}col="col s3" />
	                <InfoLabel label="Domicile" label_value = "Sandeep " col="col s3" />
	            </div>
	            <div className="row separator"> 
	                <InfoLabel label="Effective Date" label_value ={info.effective_date}col="col s3" />
	                <InfoLabel label="Confirmation Required" label_value = {(info.confirm_req) ? 'Yes' : 'No'} col="col s3" />
	                <InfoLabel label="Probation State Date" label_value = {info.confirm_start_date} col="col s3" />
	                <InfoLabel label="Confirmation Due Date" label_value = {info.confirm_due_date} col="col s3" />
	            </div>
	            <div className="row separator"> 
	                <InfoLabel label="Confirmation Date" label_value = {info.confirmation_date} col="col s3" />
	                <InfoLabel label="Confirmation Status" label_value = {info.cstatus_name} col="col s3" />
	                
                </div>
            </Fragment>
		)
	}
}