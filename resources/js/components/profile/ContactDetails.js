/**
 * Created by Sandeep Maurya on 21/07/2019.
 * @ 11:55 PM
 */

import React ,{ Fragment }from 'react'; 
import axios from 'axios';
 
import SubTabTitle from './SubTabTitle';
import InfoLabel from './InfoLabel';

import {authHeader,logout} from  "./../../helpers/responseAuthozisation";

export default class ContactDetails extends React.Component{
	constructor(props){
		super(props); 
		this.state = {
			mobile : '',
            phone : '',
            email : '',
            current_address : '',
            permanemt_address : ''
		}
	}

	 componentDidMount(){

        let self = this;
        const  id  = 'eyJpdiI6ImJCVHRRRkNwUXRNeGZNYXdxZHFVN0E9PSIsInZhbHVlIjoiZmdBc0d2UldqUDlJVCtwXC84QlFHbUE9PSIsIm1hYyI6IjljMjBlODBmYWNmODFlYzNhZDc5NjVkN2VhZWFiYTc4YmNlNDhlZjFmMjJlZjQ3YWRiYTNlZDBiZWM1NDU4MjkifQ';
        const {history}  = this.props;
      
        if(id != undefined){
           
            axios.get('/api/contact-details',{
                params:{
                    req_user_id : id
                },
                headers : authHeader()
            }).then(function(response){
            
                if(response.status === 200){
                    const data = response.data;
                    const contact =  data.data;
                    let c_address = contact.c_address_1+' ' + contact.c_address_2+' ' + contact.c_address_3 +' ,'+contact.c_city_name+' '+contact.c_state_name+' '+contact.c_nation_name+', '+contact.c_pin_code; 
                    let p_address = contact.p_address_1 +' '+ contact.p_address_2+' ' + contact.p_address_3 +', '+contact.p_pity_name+' '+contact.p_state_name+' '+contact.p_nation_name+', '+contact.p_pin_code; 
                        c_address = self.format_str(c_address);
                        p_address = self.format_str(p_address);
                    self.setState({
                    	mobile : contact.mobile_num,
						phone : contact.phone_num,
						email : contact.email_id,
						current_address : c_address,
						permanemt_address : p_address 
                    });

                     
                }else{
                    toast(({ closeToast }) => <div>Something went wrong , please try after some time  </div>);
                    history.push('/leave-setup') ;   
                }
            }) .catch(function(error){

                switch(error.response.status){
                    case 500 :
                        // logout();
                        toast(({ closeToast }) => <div> {error.response.data.message} </div>);
                        history.push('/leave-setup') ;
                        break ;
                    case 401 :
                        toast(({ closeToast }) => <div> {error.response.data.error} </div>);
                        logout();
                        break
                    default :
                        toast(({ closeToast }) => <div> could not fetch leave details </div>);
                        history.push('/leave-setup') ;
                        break;
                }
            }) ;
        }
    }

    format_str($str){
        $str.trim();
        // $str.replace('null', '');
        // $str.replace(undefined, '');
        // $str.ltrim(',');

        return $str;
    }

	render(){
		const info = this.state ;
		return(
			<Fragment>
            	<SubTabTitle 
                    title = {this.props.sub_tab_name}
                />
                <div className="row separator"> 
	                <InfoLabel label="Mobile No" label_value = {info.mobile}col="col s3" />
	                <InfoLabel label="Phone Number" label_value = {info.phone} col="col s3" />
	                <InfoLabel label="Email Id" label_value = {info.email} col="col s3" /> 
                </div>
                <div className="row separator"> 
	                <InfoLabel label="Current Address " label_value = {info.current_address} col="col s6" />
	                <InfoLabel label="Permanent Address" label_value = {info.permanemt_address} col="col s6" /> 
                </div>
                
	          
            </Fragment>
		)
	}
}