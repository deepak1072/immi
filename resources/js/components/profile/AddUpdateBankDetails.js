
import React,{Fragment} from 'react';
import SubTabTitle from './SubTabTitle';
 
export default class AddUpdateBankDetails extends React.Component{
    constructor(props){
        super(props);
    }
     
    render(){
        return(
            <Fragment>
                <SubTabTitle 
                    title = {this.props.sub_tab_name}
                />
                
                <div className="col s12 m6 right-align"> 
	                <a className='btn cyan waves-effect waves-light '  data-activates='profliePost2'><i className="mdi-navigation-arrow-back"></i> Cancel</a>
	                <a className="btn cyan waves-effect waves-light right"><i className="mdi-content-send  right"></i>Save</a>
	            </div>
                
            </Fragment>
            )
    }
}

