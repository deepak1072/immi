import React,{Fragment} from 'react';
import { Link , withRouter} from 'react-router-dom'; 
 
export default class SubTabAccess extends React.Component{
    constructor(props){
        super(props);
    }

    renderSubTab(){
        const tab = this.props.tab;
        const active_main = this.props.active_main_tab;
        const active_sub = this.props.active_sub_tab;
        return tab.map((tabb,index)=>{
            if(tabb.id === active_main ){
                return tabb.child_tab.map((ttb,i)=>{
                    let sub_class = (ttb.child_id === active_sub) ? 'collection-item active' : 'collection-item'
                    return(
                       
                        <a key={i}   onClick={() => { this.props.changeSubTab(ttb) }} className={sub_class}>{ttb.name}</a>
                         
                    ) 
                }) 
            } 
        })
    }
    render(){
        return(
            <Fragment>
                 <div className="card"> 
                    <div className="card-content"> 
                        <div className="collection ">   
                            {
                                this.renderSubTab()
                            }  
                        </div>
                    </div>
                </div> 
                
            </Fragment>
            )
    }
}

