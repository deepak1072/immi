import React,{Fragment} from 'react';
 
export default class MainTabAccess extends React.Component{
    constructor(props){
        super(props);
    }
    renderMainTab(){
        const tab = this.props.tab;
        const active_main_tab = this.props.active_main_tab;

        return tab.map((tabb,index)=>{
            let main_class = (tabb.id === active_main_tab) ? ' white-text waves-effect waves-light active'  : 'white-text waves-effect waves-light '
            return(
                <li key={index} className="tab col s3 " onClick={(e)=>this.props.mainTabChange(tabb.id)}>
                    <a className= {main_class}  >  {tabb.name}</a>
                </li> 
            )
        })
    }
    render(){
        return(
            <Fragment>
                <ul className="tabs tab-profile z-depth-1 light-blue">
                    {
                        this.renderMainTab()
                    }                        
                </ul>
            </Fragment>
            )
    }
}

