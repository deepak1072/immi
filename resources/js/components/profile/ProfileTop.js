import React,{Fragment} from 'react';
import InfoLabel from './InfoLabel';
 
export default class ProfileTop extends React.Component{
    constructor(props){
        super(props);
    }
    render(){
        const info = this.props;
        const img = 'images/avatar.jpg';
        return(
            <Fragment>
                <div className=" row waves-effect waves-block waves-light">
                    <div className="col s3">
                        <figure className="card-profile-image">
                            <img src={img} alt="profile image" className="circle z-depth-2 responsive-img activator" />
                        </figure>
                    </div> 
                    <div className="col s9 offset-s3">
                        <div className="card-content">
                            <div className="row separator"> 
                                <h4 className="card-title grey-text text-darken-4">{info.name}</h4>
                                 <div className="medium-small grey-text display_inline"><i className="mdi-communication-location-on cyan-text text-darken-2"></i>{info.location}</div>  
                                 <div className="medium-small grey-text display_inline"><i className="mdi-communication-email cyan-text text-darken-2"></i>{info.email} </div> 
                                 <div className="medium-small grey-text display_inline"><i className="mdi-communication-call  cyan-text text-darken-2"></i>{info.mobile}</div> 
                            </div>
                            <div className="row">   
                                <InfoLabel label="Emp Code" label_value = {info.code} col="col s2" />
                                <InfoLabel label="DESIGNATION" label_value = {info.designation} col="col s2" />
                                <InfoLabel label="FUNCTION" label_value = {info.department} col="col s2" />
                                <InfoLabel label="REPORTING TO" label_value = {info.reporting} col="col s2" />
                                <InfoLabel label="STATUS" label_value = {info.status} col="col s2" /> 
                            </div>
                        </div>
                    </div> 
                 </div>                 
            </Fragment>
        )
    }
}

