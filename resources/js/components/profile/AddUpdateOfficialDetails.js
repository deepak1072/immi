
import React,{Fragment} from 'react';
import SelectSearch from 'react-select-search';

import SubTabTitle from './SubTabTitle';
 
const options = [
    {name: 'Sandeep Kumar Maurya', value: 'sv'},
    {name: 'Yogesh Khumbhat', value: 'en'},
    {name: 'Kuldeep Patel', value: 'kk'} ,
    {name: 'Anurag Maurya', value: 'an'}
]; 
export default class AddUpdateOfficialDetails extends React.Component{
    constructor(props){
        super(props);
    }
     
    render(){
        return(
            <Fragment>
            	<SubTabTitle 
                    title = {this.props.sub_tab_name}
                />
                <div className="row"> 
	                <div className="col s4 "> 
	                	 <SelectSearch options={options}   value="sv" name="language" placeholder="Choose your language" />


	                </div>
	                <div className="col s4"> 
	                	 <SelectSearch options={options} value="sv" name="language" placeholder="Choose your language" />


	                </div>
	                <div className="col s4"> 
	                	 <SelectSearch options={options} value="sv" name="language" placeholder="Choose your language" />


	                </div>
	                <div className="col s4"> 
	                	 <SelectSearch options={options} value="sv" name="language" placeholder="Choose your language" />


	                </div>
	                <div className="col s4"> 
	                	 <SelectSearch options={options} value="sv" name="language" placeholder="Choose your language" />


	                </div>
                </div>
                 
                <div className="row">
                	<div className="col s12 m6 right-align"> 
		                <a className='btn cyan waves-effect waves-light '  data-activates='profliePost2'><i className="mdi-navigation-arrow-back"></i> Cancel</a>
		                <a className="btn cyan waves-effect waves-light right"><i className="mdi-content-send  right"></i>Save</a>
		            </div>
                </div>
                
                
            </Fragment>
            )
    }
}

