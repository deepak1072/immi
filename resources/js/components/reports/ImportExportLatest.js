/**
 * Created by Sandeep Maurya on 21st march 2020.
 */

import React,{Fragment} from 'react';
import {Link} from 'react-router-dom';
import {getRequest} from  "./../../helpers/ApiRequest";

export default class ImportExportLatest extends React.Component{
    constructor(props){
        super(props);
        this.state = {
            hasError : false,
            data : []
        }
        this.loadLatest = this.loadLatest.bind(this);
    }

    componentDidMount(){
        
    }

    static getDerivedStateFromError(error) {
        // Update state so the next render will show the fallback UI.
        return { hasError: true };
    }

    loadLatest(){
        let self = this;
        let url = '/api/import-exports-latest';
        let request = getRequest(url);
        request.then((response)=>{
            const data = response.data.data;
            self.setState({
                'data' : data
            });
        });
    }

    renderLatest(){
        let data = [];
        let info = this.props.data;
        if(info){
            info.map((latest,index)=>{
                let class_name = (latest.type == 'Export') ? 'mdi-file-file-download' : '';
                let link = latest.file_link;
                data.push(
                    <li  key = {index}>
                        <small>{latest.type}</small>
                        {
                            (latest.type == 'Export') ? (
                                
                                <a href={link} target="_blank"  >  <i className={class_name}></i>{latest.name} </a>
                            ) : (
                                <a  href="#"  > {latest.name} <i className={class_name}></i> </a>
                            )
                        } 
                        <time className="media-meta" >{latest.created_at}</time>
                    </li>
                );
            });
        }
        return data;
    }
 
    render(){
        let latest = this.renderLatest();
        if(this.state.hasError){
           return <h4 className="header2">Something went wrong</h4>
        }   else{
            return(
                <Fragment>
                    {
                        latest
                    }
                </Fragment>
            );
        }
    }
}