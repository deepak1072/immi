/**
 * Created by Sandeep Maurya on 29th dec 2018.
 */

import React from 'react';
import  Loader from './../admin-settings/usable_components/Loader';
import Sandesh from './../admin-settings/usable_components/Sandesh'; 
import {postRequest} from  "./../../helpers/ApiRequest";
export default class Reports extends React.Component{
    constructor(props){
        super(props);
        this.state = {
            hasError : false ,
            is_loading : true,
            action : 'Generate Report',
            period : 1,
        }
        this.handleFieldChange = this.handleFieldChange.bind(this);
        this.handleClient = this.handleClient.bind(this);
    }

    componentDidMount(){
        setTimeout(()=>{
            this.setState({
                is_loading:false
            });
        },2000);
    }

    handleFieldChange(event){
        this.setState({
            [event.target.name] : event.target.value
        }); 
    }

    static getDerivedStateFromError(error) {
        // Update state so the next render will show the fallback UI.
        return { hasError: true };
    }

    handleClient(event){
        event.preventDefault();
        let {history} = this.props;
        let info = this.state;
        
        this.setState({
            is_loading : true
        });
        let url ='/api/reports';
        let request = postRequest(url,{period:info.period});
        request.then(response=>{
            let data = response.data;
            Sandesh(data.message,data.type);
            if(data.result){
                Sandesh(data.message,data.type);
                setTimeout(()=>{history.push('/import-export')},3000);
            }
        })

        this.setState({
            is_loading : false
        });

    }
 
    render(){
        if(this.state.is_loading){
            return <Loader />;
        } 
        if(this.state.hasError){
           return <h4 className="header2">Something went wrong</h4>
        }   else{
            return(
                <div className="dashboard">
                    <div className="card-panel">
                        <div className="row">
                            <form className="col s12" onSubmit={this.handleClient} >
                                <div id="leave_htmlForm">
                                    <div className="row">                                                
                                        <div className="col s12 l10 center-align">
                                            <h4 className="header2">  Reports </h4>
                                        </div>
                                        
                                    </div>
                                    <div id="card-stats">
                                        <div className="row">
                                            <div className="col s12 l2 input-field">
                                                <select
                                                    value={this.state.period}
                                                    onChange={this.handleFieldChange}
                                                    name="period"
                                                    id="period"
                                                    className="browser-default"
                                                >
                                                    <option value='1'> Today</option>
                                                    <option key= '7' value='7' >Last 7 Days</option>
                                                    <option key= '15' value='15' >Last 15 Days</option>
                                                    <option key= '30' value='30' >Last 1 Month</option> 
                                                    <option key= '180' value='180' >Last 6 Month</option>
                                                </select>
                                            </div>
                                            <div className="input-field col s12 l6 ">
                                                {/* <Link to="/dashboard">
                                                    <button className="btn default   waves-light"   shift_name="action">
                                                        <i className="mdi-navigation-arrow-back left "></i>
                                                        cancel
                                                    </button>
                                                </Link> */}
                                                <button className="btn cyan waves-effect waves-light  " type="submit" shift_name="action">{(this.state.action)}
                                                    <i className="mdi-content-send right"></i>
                                                </button>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            );
        }
    }
}