/**
 * Created by Sandeep Maurya on 2nd Jan 2020
 */

import React from "react";
import _ from "lodash";
 // Import React Table
import ReactTable from "react-table";
import "react-table/react-table.css";
import {Link} from  'react-router-dom';
import {ITEM_PER_PAGE} from  "./../../helpers/responseAuthozisation";
import {getRequest,putRequest} from  "./../../helpers/ApiRequest";
import Sandesh from './../admin-settings/usable_components/Sandesh'; 

export  default class Agents extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            data: [],
            pages: null,
            loading: true
        };
        this.fetchData = this.fetchData.bind(this);
        this.handleStatus = this.handleStatus.bind(this);
    }

    fetchData(state, instance) {
        this.setState({ loading: true });
        let url = '/api/agents';
        let request = getRequest(url,{
            filter : state.filtered,
            pageSize : state.pageSize,
            page : state.page,
            sort : state.sorted
        });
        request.then((response) => {
            let filteredData = response.data;
            this.setState({
                data: filteredData.data,
                pages: Math.ceil(filteredData.count / state.pageSize),
                loading: false
            });
        });
    }

    handleStatus(event){
        event.preventDefault();
        const id = event.target.id;
        const status = event.target.value;
        self = this;
        let url = '/api/agents';
        let request = putRequest(url,{
            id : id,
            status : status
        });
        request.then((response) => {
            let data = response.data;
            Sandesh(data.message,data.type);
            if(data.result){
                let new_state = self.state.data.map(function(data){
                    if(data.user_id == id){
                        data.is_access_allowed = (status == 0) ? 1  : 0;
                    } 
                    return data ;
                });
                self.setState({
                    data : new_state
                });
            }
        });
    }

    render() {
        const { data, pages, loading } = this.state;
        return (
            <div>
                <div className="dashboard">
                    <div className="card-panel">
                        <div className="row">
                            <div className=" col s6 m6 l3">
                                <Link to="/agents-details">
                                    <button className="btn cyan waves-effect waves-light  ">
                                        <i className="mdi-content-create left"></i>
                                        Create Agents
                                    </button>
                                </Link>
                            </div>
                            <div className="col s6 m6 l4 center-align">
                                <h4 className="header2">
                                    Agents
                                </h4>
                            </div>
                        </div>
                        
                        <ReactTable
                            columns={[
                                {
                                    Header: "Agent's Name",
                                    accessor: "emp_name"
                                } ,
                                {
                                    Header: "Mobile No.",
                                    accessor: "mobile_number"
                                },
                                {
                                    Header: "Email",
                                    accessor: "oemail_id"
                                },
                                {
                                    Header: "Status",
                                    accessor: "is_access_allowed",
                                    Cell:(row)=>(
                                            <div className="switch">
                                                <label>
                                                    Off
                                                    <input
                                                        type="checkbox"
                                                        checked={(row.original.is_access_allowed == 1) ? 'checked':'' }
                                                        onChange={this.handleStatus}
                                                        name="leave_status"
                                                        row = {JSON.stringify(row)}
                                                        id={row.original.user_id} 
                                                        value={(row.original.is_access_allowed == 1) ? '1':'0' }
                                                    />

                                                    <span className="lever"></span> On
                                                </label>
                                            </div>
                                        )

                                },
                                {
                                    Header : "Created At" ,
                                    accessor : "created_at"
                                },
                                {
                                    Header: "Action",
                                    accessor: "user_id",
                                    Cell:(row)=>(
                                            <div className="switch">
                                                <Link to={'/agents-details/'+ row.original.user_id} className="input-field ">
                                                    <button className="btn light waves-effect waves-light action_button "><i className="mdi-action-launch"></i></button>
                                                </Link>
                                            </div>
                                        )

                                }
                            ]}
                            manual // Forces table not to paginate or sort automatically, so we can handle it server-side
                            data={data}
                            pages={pages} // Display the total number of pages
                            loading={loading} // Display the loading overlay when we need it
                            onFetchData={this.fetchData} // Request new data when things change
                            filterable
                            defaultPageSize={ITEM_PER_PAGE}
                            className="-highlight" //-striped
                        />
                        <br />
                    </div>
                </div>

            </div>
        );
    }
}


