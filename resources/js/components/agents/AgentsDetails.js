/**
 * Created by Sandeep Maurya on 20th April 2019.
 */

import React from 'react';
import axios from 'axios';
import { toast } from 'react-toastify';
import {Link} from 'react-router-dom';
import {authHeader,logout,ITEM_PER_PAGE,CARD} from  "./../../helpers/responseAuthozisation";
import  Loader from './../admin-settings/usable_components/Loader';
import Sandesh from './../admin-settings/usable_components/Sandesh'; 
import DatePicker from "react-datepicker"; 
import "react-datepicker/dist/react-datepicker.css";
import TimeField from 'react-simple-timefield';

import ReactFC from "react-fusioncharts";
import FusionCharts from "fusioncharts";
import Pie2D from "fusioncharts/fusioncharts.charts";
import Column2D from "fusioncharts/fusioncharts.charts";
import FusionTheme from "fusioncharts/themes/fusioncharts.theme.fusion";
ReactFC.fcRoot(FusionCharts, Pie2D, FusionTheme);
 
import {getRequest,postRequest} from  "./../../helpers/ApiRequest";
export default class AgentsDetails extends React.Component{
    constructor(props){
        super(props);
        var d_date = '00:00'  ;
        this.state = {
            em_id : '',
            emp_name : '', 
            mobile_number : '', 
            oemail_id : '', 
            password : '',
            updated_at : '', 
            is_deleted : '',
            create_at : '',
            created_by : '',
            message : '',
            flag : true ,
            hasError:false,
            action: 'Create' ,
            agent_data : [],
            is_loading:true ,
            chartConfigs1 : {
                type: "pie2d",
                width: "1000",
                height: "400",
                dataFormat: "json",
                dataSource:  {
                    "chart": {
                        "caption": "Activity",
                        "subCaption": "",
                        "use3DLighting": "0",
                        "showPercentValues": "1",
                        "decimals": "1",
                        "useDataPlotColorForLabels": "1",
                        "theme": "fusion"
                    },
                    "data": [ 
                    ]
                }
              }
        }


        this.handleClient = this.handleClient.bind(this);
        this.handleFieldChange   = this.handleFieldChange.bind(this);
        this.handleRadioChange = this.handleRadioChange.bind(this);
        this.handleChange = this.handleChange.bind(this);

    }


    componentDidMount(){

        let self = this;
        const { id } = this.props.match.params;
        
        if(id != undefined){
            
            this.loadDetails(id);
            this.loadClientsCount(id);
        }else{
            this.setState({
                is_loading : false 
            });
        }
        
    }
    loadClientsCount( id ){
        let self = this;
          var url = "/api/agents-history";
          let request = getRequest(url,{id:id});
          request.then((response)=>{
              const data = response.data.data; 
              self.setState({
                  'agent_data' : data.bar,
                  chartConfigs1 : {
                    type: "pie2d",
                    width: "1000",
                    height: "400",
                    dataFormat: "json",
                    dataSource:  {
                        "chart": {
                            "caption": "Activity",
                            "subCaption": "",
                            "use3DLighting": "0",
                            "showPercentValues": "1",
                            "decimals": "1",
                            "useDataPlotColorForLabels": "1",
                            "theme": "fusion"
                        },
                        "data": data.pie
                    }
                  },
                  is_loading:false
              });
          });
      }

    static getDerivedStateFromError(error) {
        // Update state so the next render will show the fallback UI.
        return { hasError: true };
    }

    loadDetails(id){
        let self = this;
        let url = '/api/agents-data';
        let request = getRequest(url,{id : id});
        request.then((response)=>{
            const data = response.data; 
            
            if(data.result){ 
                let info = data.data;
                self.setState({
                    em_id : info.em_id,
                    emp_name : info.emp_name, 
                    user_id : info.user_id,
                    mobile_number : info.mobile_number, 
                    oemail_id : info.oemail_id, 
                    password : info.password,
                    updated_at : '', 
                    is_deleted : '',
                    create_at : '',
                    created_by : '',
                    message : '',
                    flag : true ,
                    hasError:false,
                    action: 'Update' ,
                    is_loading:false 
                });
            }
            
        });
    }

    /**
     * form validation
     * @returns {boolean|*}
     */
    validateRule(){
        
        if(this.state.emp_name =='' || this.state.emp_name == 'undefined'){
            Sandesh('Please enter Name ','warn');
            return  false;
        }
        if(this.state.mobile_number =='' || this.state.mobile_number == 'undefined'){
            Sandesh('Please enter Mobile Number','warn');
            return  false;
        }
        
        return this.state.flag;
    }



    handleClient(event){
        event.preventDefault();
        let {history} = this.props;
        let info = this.state;
        let data = {
            em_id : info.em_id,
            emp_name : info.emp_name, 
            mobile_number : info.mobile_number, 
            oemail_id : info.oemail_id, 
            password : info.password,
        }
        this.setState({
            is_validating : true
        });
        if(this.validateRule()){
            let url ='/api/submit-agents';
            let request = postRequest(url,info);
            request.then(response=>{
                let data = response.data;
                Sandesh(data.message,data.type);
                if(data.result){
                    Sandesh(data.message,data.type);
                    setTimeout(()=>{history.push('/agents')},3000);
                }
            })
        }
        this.setState({
            is_validating : false
        });

    }
    handleFieldChange(event){
        
        this.setState({
            [event.target.name] : event.target.value
        });
    }
    
    handleRadioChange(event){
        this.setState({
            [event.target.name] :  event.target.value
        }); 
    }
     
    handleChange(name,date) {  
         
        this.setState({
          [name]: date
        });
    }

    ClientsCount(){
        const agent_data = [];

        this.state.agent_data.map((balance,index)=>{
            let color = CARD[parseInt(Math.random()*10)];
            let c_l = "card-content white-text " +balance.code ;
            let b_l = "card-action "+ balance.code+" darken-2 white-text";
            agent_data.push(
                <div key={index} className="col s12 m4 l3">
                    <div className="card">
                        <div className={c_l}>
                            {/* <p className="card-stats-title center-align">  {balance.title.toUpperCase()}  </p> */}
                            <h4 className="card-stats-number"><Link  to={balance.link}>{balance.count} </Link></h4>
                            <p className="card-stats-compare">
                            </p>
                        </div>
                        <div className={b_l}>
                            <div  className="center-align row"> 
                              <div className="col s12">{balance.title.toUpperCase()}  </div>
                            </div>
                        </div>
                    </div>
                </div>
            )
        })
        return agent_data;
    }
 
    render(){
        let agent_data = this.ClientsCount();
        if(this.state.is_loading){
            return <Loader />
        }
        if(this.state.hasError){
           return <h4 className="header2">Something went wrong</h4>
        }   else{
            const is_break_shift = (this.state.break == 1) ? true : false;
            return(
                <div className="dashboard">
                    <div className="card-panel">
                        <div className="row">
                            <form className="col s12" onSubmit={this.handleClient}>
                                <div id="leave_htmlForm">
                                    <div className="row">                                                
                                        
                                        <div className="col s12 l12 center-align">
                                            <h4 className="header2">{this.state.action} Agent </h4>
                                        </div>
                                    </div>
                                    <div className="row"> 
                                            
                                        <div className="input-field col s12 m3">
                                            <input
                                                id="name"
                                                type="text"
                                                name="emp_name"
                                                value={this.state.emp_name}
                                                onChange={this.handleFieldChange}
                                            /> 
                                            <label htmlFor="name" className="active">Name Of Agent
                                            </label> 
                                                
                                        </div>
                                        <div className="input-field col s12 m3 ">
                                            <input
                                                id="mobile"
                                                type="text"
                                                name="mobile_number"
                                                value={this.state.mobile_number}
                                                onChange={this.handleFieldChange}
                                            /> 
                                            <label htmlFor="mobile" className="active">Mobile No.
                                            </label>
                                        </div> 
                                            
                                    </div> 
                                    <div className="row">  
                                        <div className="input-field col s12 m3">
                                            <input
                                                id="email"
                                                type="text"
                                                name="oemail_id"
                                                value={this.state.oemail_id}
                                                onChange={this.handleFieldChange}
                                            /> 
                                            <label htmlFor="email" className="active">Email
                                            </label>
                                        </div>
                                        <div className="input-field col s12 m3">
                                            <input
                                                id="password"
                                                type="text"
                                                name="password"
                                                value={this.state.password}
                                                onChange={this.handleFieldChange}
                                            /> 
                                            <label htmlFor="password" className="active">Password 
                                            </label>
                                        </div> 
                                            
                                    </div> 
                                         
                                </div>
                                <div className="row">                                            
                                    <div className="input-field col s12">
                                        <Link to="/agents" >
                                            <button className="btn default   waves-light"   shift_name="action">
                                                <i className="mdi-navigation-arrow-back left "></i>
                                                cancel
                                            </button>
                                        </Link>
                                        <button className="btn cyan waves-effect waves-light  " type="submit" shift_name="action">{(this.state.action)}
                                            <i className="mdi-content-send right"></i>
                                        </button>
                                    </div>
                                </div>
                            </form>
                            <ReactFC {...this.state.chartConfigs1} />
                            <div id="card-stats">
                                <div className="row">
                                    {
                                        agent_data
                                    }
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            );
        }
    }
}