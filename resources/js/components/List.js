    <ul id="task-card" className="collection with-header">
                                          <li className="collection-header cyan">
                                              <h4 className="task-card-title">My Task</h4>
                                              <p className="task-card-date">March 26, 2015</p>
                                          </li>
                                          <li className="collection-item dismissable">
                                              <input type="checkbox" id="task1" />
                                              <label for="task1">Create Mobile App UI. <a href="#" className="secondary-content"><span className="ultra-small">Today</span></a>
                                              </label>
                                              <span className="task-cat teal">Mobile App</span>
                                          </li>
                                          <li className="collection-item dismissable">
                                              <input type="checkbox" id="task2" />
                                              <label for="task2">Check the new API standerds. <a href="#" className="secondary-content"><span className="ultra-small">Monday</span></a>
                                              </label>
                                              <span className="task-cat purple">Web API</span>
                                          </li>
                                          <li className="collection-item dismissable">
                                              <input type="checkbox" id="task3" checked="checked" />
                                              <label for="task3">Check the new Mockup of ABC. <a href="#" className="secondary-content"><span className="ultra-small">Wednesday</span></a>
                                              </label>
                                              <span className="task-cat pink">Mockup</span>
                                          </li>
                                          <li className="collection-item dismissable">
                                              <input type="checkbox" id="task4" checked="checked" disabled="disabled" />
                                              <label for="task4">I did it !</label>
                                              <span className="task-cat cyan">Mobile App</span>
                                          </li>
                                        </ul>