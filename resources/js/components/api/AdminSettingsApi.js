/**
 * Created by Sandeep Maurya on 11/25/2018.
 */
import axios from 'axios';

export default class AdminSettingsApi{
    fetchMenuListData() {
        axios.get('/api/fetchMenuList')
            .then((response) => {
                // redirect to the homepage
                return response.data;
            })
            .catch(error => {
                console.log('could not fetch menu list ');
        })
    }
}