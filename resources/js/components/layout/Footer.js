/**
 * Created by Sandeep Maurya on 11/18/2018.
 */
import React from 'react';

export default class Footer extends React.Component{
    render(){
        return(
            <footer className="page-footer">
                
                <div className="footer-copyright">
                    <div className="container">
                        Copyright © 2020 <a className="grey-text text-lighten-4" href="" target="_blank"> ,</a> All rights reserved.
                        <span className="right"> Design and Developed by <a className="grey-text text-lighten-4" href=" ">Sandeep Kumar Maurya</a></span>
                    </div>
                </div>
            </footer>
        );
    }
}