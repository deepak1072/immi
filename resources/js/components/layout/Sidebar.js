/**
 * Created by Sandeep Maurya on 11/18/2018.
 */

import React from 'react';
import { Link } from 'react-router-dom';
import {authHeader,logout} from  "./../../helpers/responseAuthozisation";
import axios from 'axios';
import RenderParentMenu from './RenderParentMenu';
import Sandesh from './../admin-settings/usable_components/Sandesh';
export default class Sidebar extends React.Component{
	constructor(props){
		super(props);
		let user = this.props;
	   	this.state = {
	   		selected_role : user.active_role,
			selected_role_name : user.active_role_name,
			role_masters_id : user.role_masters_id,
			roles:[] ,
			user_menu_access : []
		};
		this.handleAssignedMenus(user.role_masters_id);

	}
	componentDidMount(){

		this.handleAssignedRoles();
		
		
	}

	handleAssignedMenus(role_masters_id){
	 
		let self = this ; 
		axios.post('/api/assigned-menus',{
			role_masters_id : role_masters_id 
		},{headers  : authHeader() })
			.then(function(response) { 
				if(response.status == 200){
					const user_menu_access = response.data;

					self.setState({
						user_menu_access : user_menu_access
					});

				}   else{
					Sandesh('Something went wrong in assigned role , please try after some time','error');
					logout();
				}

			})
			.catch(function(error){ 
				switch(error.response.status){
					case 500 :
						// logout();
						Sandesh(error.response.data.message,'error');
						break ;
					case 401 : 
						Sandesh(error.response.data.error,'error');
						logout();
						break;
					default : 
						Sandesh(' could not fetch menus list  ','error');
						break;
				}
			})
	}
	handleAssignedRoles(){
		let user = this.props;
		let self = this ;
		axios.post('/api/assigned-roles',{
			user_code : user.user_code,
			company_code : user.company_code
		},{headers  : authHeader() })
			.then(function(response) {
				if(response.status == 200){
					const assigned_role = response.data;

					self.setState({
						roles : assigned_role
					});

				}   else{
					Sandesh('Something went wrong in assigned role , please try after some time ','error');
					logout();
				}

			})
			.catch(function(error){

				switch(error.response.status){
					case 500 :
						// logout(); 
						Sandesh(error.response.data.message,'error');
						break ;
					case 401 :
						Sandesh(error.response.data.error,'error');
						logout();
						break;
					default : 
						Sandesh(' could not fetch role list ','error');
						break;
				}
			})

	}

	handleRoleChanges(event){
		let role_name = "";
		let role_masters_id = "";
		this.state.roles.map((role,index)=>{
			if(role.role_masters_id == event.target.value ){
				role_name =  role.role_name;
				role_masters_id = role.role_masters_id;
				this.setState({
					selected_role_name : role.role_name ,
					selected_role : event.target.value ,
					role_masters_id : role.role_masters_id
				});
			}
		});

		this.props.handleRoleChanges(event,role_name,role_masters_id);
		this.handleAssignedMenus(role_masters_id);
		
	}

	// logout from system
	handleLogout(event){
		let user = this.props;
		let self = this ;
		axios.post('/api/logout',{
			user_code : user.user_code,
			company_code : user.company_code
		},{headers  : authHeader() })
			.then(function(response) {
				if(response.status == 200){
					 
					Sandesh(response.data.message,'success');
					logout();

				}   else{ 
					Sandesh('Something went wrong in assigned role , please try after some time  ','error');
					logout();
				}

			})
			.catch(function(error){

				switch(error.response.status){
					case 500 :
						// logout(); 
						Sandesh( error.response.data.message,'error');
						break ;
					case 401 : 
						Sandesh( error.response.data.error,'error');
						logout();
						break;
					default : 
						Sandesh( ' could not fetch leave list ','error');
						break;
				}
			})

	}

    render(){
        return(
           
	      	<aside id="left-sidebar-nav">
		        <ul id="slide-out" className="side-nav fixed leftside-navigation">
		            <li className="user-details cyan darken-2">
		            <div className="row">
		                <div className="col col s4 m4 l4">
		                    {/* <img src="images/avatar.jpg" alt="" className="circle responsive-img valign profile-image" /> */}
		                </div>
		                <div className="col col s8 m8 l8">
		                    <ul id="profile-dropdown" className="dropdown-content">
 		                         
								{
									this.state.roles.map((role,index)=>{
										let class_name = (this.props.active_role == role.super_role_id) ? 'role_selected' : '' ;
										return(
											<li
												onClick={this.handleRoleChanges.bind(this)}
												className={class_name}
												key={index}
												value={role.super_role_id}
											>
												<i style={{padding:8}}  ></i>
												{role.role_name}
											</li>
										)
									})
								}
		                        <li className="divider"></li>
								{
									(this.state.selected_role == 2) ? (
										<li><Link to="/change-password" ><i className="mdi-communication-vpn-key"></i> <small> Password</small></Link></li>
									) :('')
								}
		                        
		                        
		                        <li className="divider"></li>
		                        <li><Link to="#" onClick={this.handleLogout.bind(this)}><i className="mdi-action-settings-power"></i> Logout</Link>
		                        </li>
		                    </ul>
		                    <Link className="btn-flat dropdown-button waves-effect waves-light white-text profile-btn" to="/" data-activates="profile-dropdown">{this.props.user_name}<i className="mdi-navigation-arrow-drop-down right"></i></Link>
		                    <p className="user-roal"> {this.state.selected_role_name}</p>
		                </div>
		            </div>
		            </li>
		            <li className="no-padding" >
				        <ul className="collapsible collapsible-accordion">
				            <RenderParentMenu
				            	user_menu_access = {this.state.user_menu_access}
				            />
				            <li className="bold" >
								<Link to= "#"className=" ">
									<i className=" "></i> 
								</Link>
							</li>
			            </ul>
			        </li>
		            <li className="li-hover"><div className="divider"></div></li>
		            <li className="li-hover"><p className="ultra-small margin more-text"> No more Contents</p></li>
		             
		        </ul>
		        <Link to="/" data-activates="slide-out" className="sidebar-collapse btn-floating btn-medium waves-effect waves-light hide-on-large-only cyan"><i className="mdi-navigation-menu"></i></Link>
	        </aside>
      
        );
    }
}