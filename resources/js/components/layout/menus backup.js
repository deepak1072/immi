<li className="bold"><Link to="/dashboard" className="waves-effect waves-cyan"><i className="mdi-action-dashboard"></i> Dashboard</Link>
		            </li>
		            
		          
		            <li className="no-padding">
		                <ul className="collapsible collapsible-accordion">
		                    <li className="bold">
		                    	<Link to="/sandeep" className="collapsible-header waves-effect waves-cyan">
		                    		<i className="mdi-action-settings"></i> Settings
		                    	</Link>
		                        <div className="collapsible-body">
		                            <ul>


		                                <li><Link to="/menu-list">Menu</Link>
		                                </li>
		                                <li><Link to="/company-list">Company</Link>
		                                </li>
		                                
		                                <li><Link to="/role-setup">Roles</Link>
		                                </li>
		                                <li><Link to="/assigned-roles-list">Role Assignment</Link>
		                                </li>
		                                <li><Link to="/leave-setup">Leave</Link>
		                                </li>
		                                <li><Link to="/attendance-setup">Attendance</Link>
		                                </li>
		                                <li><Link to="/profile-setup">Profile</Link>
		                                </li>
		                                <li><Link to="/report-setup">Reports</Link>
		                                </li>
		                                <li><Link to="/division-setup">Division</Link>
		                                </li>
		                                <li><Link to="/process-setup">Process</Link>
		                                </li>
		                                <li><Link to="/grade-setup">Grade</Link>
		                                </li>
		                                <li><Link to="/employee-type-setup">Employee Type</Link>
		                                </li>
		                                <li><Link to="/regions-setup">Regions</Link>
		                                </li>
		                                <li><Link to="/designation-setup">Designation</Link>
		                                </li>
		                                <li><Link to="/cost-center-setup">Cost Centers</Link>
		                                </li>
		                                <li><Link to="/qualification-setup">Qualifications</Link>
		                                </li>
		                                <li><Link to="/function-setup">Function</Link>
		                                </li>
		                            </ul>
		                        </div>
		                    </li>
		                   
		                   
		               
		                    <li className="bold"><Link to="#" className="collapsible-header  waves-effect waves-cyan"><i className="mdi-action-account-box"></i> Profile</Link>
		                        <div className="collapsible-body">
		                            <ul>                                        
		                                <li><Link to="page-contact.html">Contact Page</Link>
		                                </li>
		                                <li><Link to="page-todo.html">ToDos</Link>
		                                </li>
		                                <li><Link to="page-blog-1.html">Blog Type 1</Link>
		                                </li>
		                                <li><Link to="page-blog-2.html">Blog Type 2</Link>
		                                </li>
		                                <li><Link to="page-404.html">404</Link>
		                                </li>
		                                <li><Link to="page-500.html">500</Link>
		                                </li>
		                                <li><Link to="page-blank.html">Blank</Link>
		                                </li>
		                            </ul>
		                        </div>
		                    </li>

		                     <li className="bold"><Link to="#" className="collapsible-header  waves-effect waves-cyan"><i className="mdi-notification-event-available"></i> Attendance</Link>
		                        <div className="collapsible-body">
		                            <ul>                                        
		                                <li><Link to="page-contact.html">Contact Page</Link>
		                                </li>
		                                <li><Link to="page-todo.html">ToDos</Link>
		                                </li>
		                                <li><Link to="page-blog-1.html">Blog Type 1</Link>
		                                </li>
		                                <li><Link to="page-blog-2.html">Blog Type 2</Link>
		                                </li>
		                                <li><Link to="page-404.html">404</Link>
		                                </li>
		                                <li><Link to="page-500.html">500</Link>
		                                </li>
		                                <li><Link to="page-blank.html">Blank</Link>
		                                </li>
		                            </ul>
		                        </div>
		                    </li>
		                     
		                     <li className="bold"><Link to="#" className="collapsible-header  waves-effect waves-cyan"><i className="mdi-action-schedule
"></i> Leave</Link>
		                        <div className="collapsible-body">
		                            <ul>                                        
		                                <li><Link to="/create-leave-request">Create Leave Request</Link>
		                                </li>
		                                <li><Link to="/leave-balance">Leave Balance</Link>
		                                </li>
		                                <li><Link to="leave-transactions">Leave Transactions</Link>
		                                </li>
		                                <li><Link to="create-comp-off-request">Create Comp Off Request</Link>
		                                </li>
		                                <li><Link to="comp-off-transactions">Comp Off Transactions</Link>
		                                </li>
		                                 
		                            </ul>
		                        </div>
		                    </li> 

		                     <li className="bold"><Link to="#" className="collapsible-header  waves-effect waves-cyan"><i className="mdi-social-pages"></i> Reports</Link>
		                        <div className="collapsible-body">
		                            <ul>                                        
		                                <li><Link to="page-contact.html">Contact Page</Link>
		                                </li>
		                                <li><Link to="page-todo.html">ToDos</Link>
		                                </li>
		                                <li><Link to="page-blog-1.html">Blog Type 1</Link>
		                                </li>
		                                <li><Link to="page-blog-2.html">Blog Type 2</Link>
		                                </li>
		                                <li><Link to="page-404.html">404</Link>
		                                </li>
		                                <li><Link to="page-500.html">500</Link>
		                                </li>
		                                <li><Link to="page-blank.html">Blank</Link>
		                                </li>
		                            </ul>
		                        </div>
		                    </li>
		                    
		                   
		                </ul>
		            </li>





		            <li className="no-padding" >
				        <ul className="collapsible collapsible-accordion">
		            
				            <RenderParentMenu
				            	user_menu_access = {this.state.user_menu_access}
				            />
			            </ul>
			        </li>