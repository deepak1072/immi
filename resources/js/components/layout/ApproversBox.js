import React ,{Fragment} from 'react';
import ProfilePic from './../admin-settings/usable_components/ProfilePic';
import ShortName from './../admin-settings/usable_components/ShortName';
const RenderApprovers= (props)=>{ 
    if(props.approvers.list.lenght < 1){
        return '';
    } 
    return(
        props.approvers.list.map((value,index)=>{ 
            return(
                <Fragment key={index+'_'+value.id+'_'+value.short_name}>
                    <li className="collection-item avatar">
                        {
                            (value.pr_image) ? (<ProfilePic pic={value.pr_image} />) : (<ShortName name={value.short_name}/>)
                        }
                        
                        <span className="title box-title">{value.name}</span>
                        <p className="box-sub-title"> {value.designation} </p> 
                    </li>
                </Fragment>
            ) 
        })
    )
}
 
const ApproversBox =(props)=>{  
    let is_approver = (props.approvers.retrieved > 0) ? true : false;
	return(
		<Fragment>
			<div className="form-group"> 
                <div className="row">
                     
                    <div className="col s12">
                        <ul className="collection">
                            <li className="collection-item box-label"><span className="title">APPROVER(S) </span></li>
                            {
                                (is_approver) ? (
                                    <RenderApprovers
                                        approvers = {props.approvers}
                                     />
                                ) : ('')
                            } 
                        </ul>

                    </div>
                </div>
            </div>
		</Fragment>
	) 
}
export default ApproversBox ;