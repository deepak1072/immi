/**
 * Created by Sandeep Maurya on 2/9/2019.
 */

import React from 'react';
import { Link } from 'react-router-dom';
 
 


export default class RenderChildMenu extends React.Component{
	constructor(props){
		super(props);
	}

	onClickChield(event){
		event.stopPropagation();
	}


	render(){  
		return (
			<li key= {this.props.keyItem} onClick = {this.onClickChield.bind(this)}>
				<Link to={this.props.menu_url}>{this.props.menu_name}</Link>
			</li>
		)
	}
}