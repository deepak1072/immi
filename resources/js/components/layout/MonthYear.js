/**
 * Created by Sandeep Maurya  .
 */

import React,{Fragment} from 'react';
 
export default class MonthYear extends React.Component{
    constructor(props){
        super(props);
        
    }
    prevNext(val,event){
        event.preventDefault();
        this.props.handlePrevNext(val)
    }

    render(){
        return(
            <Fragment>
                <div className="row">
                    <div className="col s3 m1 l1 prev_next" onClick = {this.prevNext.bind(this,-1)}>
                        <i className=" mdi-navigation-chevron-left"></i>
                    </div>
                    <div className="col s6 m3 l3">
                        {this.props.month}&nbsp;&nbsp;
                        {this.props.year}
                    </div>
                    <div className="col s3 m1 l1 prev_next" onClick = {this.prevNext.bind(this,1)} >
                        <i className=" mdi-navigation-chevron-right"></i>
                    </div>
                </div>
            </Fragment>
        );
    }
}