/**
 * Created by Sandeep Maurya on 11/18/2018.
 */

import React from 'react';
import { Link } from 'react-router-dom';
import ImportExportLatest from './../reports/ImportExportLatest';
import {getRequest} from  "./../../helpers/ApiRequest";
export default class Header extends React.Component{
    constructor(props){
        super(props);

        this.state = {
            hasError : false,
            message : '',  
            logo:'',
            flag : true,
            data : []
        };
        this.loadLatestReports = this.loadLatestReports.bind(this);
    }
    componentDidMount(){
      let url = '/api/company-login-info';
      let self = this;
      axios.get(url,{
          params:{}
      }).then(function(response){
          if(response.status == 200){
              let info = response.data;
              self.setState({
                  logo:info.logo_top,
                  id : info.id,
                  code : info.code,
                  theme :info.theme,
                  url : info.url
              });
          }
      }).catch(function(error){
      });  
      this.loadLatest();
    }
    loadLatest(){
        let self = this;
        let url = '/api/import-exports-latest';
        let request = getRequest(url);
        request.then((response)=>{
            const data = response.data.data;
            self.setState({
                'data' : data
            });
        });
    }
    loadLatestReports(){
      this.loadLatest();
    }
    render(){
        return(
            <header id="header" className="page-topbar">
                <div className="navbar-fixed">
                    <nav className="navbar-color">
                        <div className="nav-wrapper">
                            <ul className="left">                      
                              <li>
                                <h1 className="logo-wrapper">
                                  <Link to="/" className="logo-link">
                                    <img className="logo-dashboard" src = {this.state.logo} />
                                  </Link>
                                </h1>
                              </li>
                            </ul>
                            {/* <div className="header-search-wrapper hide-on-med-and-down">
                                <i className="mdi-action-search"></i>
                                <input type="text" name="Search" className="header-search-input z-depth-2" placeholder="Search your team "/>
                            </div> */}
                            <ul className="right hide-on-med-and-down">

                                <li><Link to="#" className="waves-effect waves-block waves-light toggle-fullscreen"><i className="mdi-action-settings-overscan"></i></Link>
                                </li>
                                { 
                                  (this.props.active_role == 2)?(
                                    <li onClick={this.loadLatestReports}  ><Link to="#" className="waves-effect waves-block waves-light notification-button" data-activates="import-export-dropdown"><i className="mdi-communication-import-export"></i>
                                
                                </Link>
                                </li>
                                  ):('')
                                }

                                

                                {/* <li><Link to="javascript:void(0);" className="waves-effect waves-block waves-light notification-button" data-activates="notifications-dropdown"><i className="mdi-social-notifications"><small className="notification-badge">5</small></i>
                                
                                </Link>
                                </li> */}
                                <li><Link to="javascript:void(0);" className="waves-effect waves-block waves-light notification-button"  ><i className=" "> </i>

                                </Link>
                                </li>

                            </ul>
                            {/* <ul id="notifications-dropdown" className="dropdown-content">
                              <li>
                                <h5>NOTIFICATIONS <span className="new badge">5</span></h5>
                              </li>
                              <li className="divider"></li>
                              <li>
                                <Link to="#!"><i className="mdi-action-add-shopping-cart"></i> A new order has been placed!</Link>
                                <time className="media-meta" >2 hours ago</time>
                              </li>
                               
                            </ul> */}
                            <ul  id="import-export-dropdown" className="dropdown-content">
                              <li>
                                <h5>IMPORT / EXPORT </h5>
                                <Link to="/import-export"> More</Link>
                              </li>
                              <li className="divider"></li>
                                <ImportExportLatest data = {this.state.data} />
                            </ul>
                        </div>
                    </nav>
                </div>
                
          </header>

        );
    }
}