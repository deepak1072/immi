/**
 * Created by Sandeep Maurya on 11/18/2018.
 */

import React,{Fragment} from 'react';
import { Link } from 'react-router-dom';
import {authHeader,logout} from  "./../../helpers/responseAuthozisation";
import axios from 'axios';
import { toast } from 'react-toastify';
import RenderChildMenu from './RenderChildMenu';
 
const display_block = {
	display : 'block'
}
const display_none = {} 
export default class RenderParentMenu extends React.Component{

	constructor(props){
		super(props);
		this.state = {
			menu_id : '',
			class_name : '',
			display_san : display_none
		};
	}

	handleMenuExpansions(menu_id,parent_menu_id,event){
		 
		if(parent_menu_id == 0){
			let status = this.state.class_name == 'active' ? '' : 'active';
			let current_active_menu_id = this.state.menu_id;
			status = current_active_menu_id != menu_id ? 'active' : status ; 
			let display = this.state.class_name == 'active' ? display_none : display_block;
			this.setState({
				menu_id : menu_id,
				class_name : status 
			});
		}
		
	}
	
	
	handleMenus(){
		const data= this.props.user_menu_access;
		let menusItem = []; 
		 
		data.map((menus,index)=>{
				let childMenus = [];
				let active_menu = 	menus.menu_id  == this.state.menu_id  ? true : false; 
				let active_block = this.state.class_name == 'active' && active_menu ? true : false;
				menus.hasOwnProperty('children') ? 
			 	(  
			 		menusItem.push( 
	                    <li className= {active_menu ? this.state.class_name + ' bold' : ' bold'}  key={menus.menu_id} onClick= {this.handleMenuExpansions.bind(this,menus.menu_id,menus.parent_menu_id)}>
	                    	<Link to="#" className= {active_menu ? this.state.class_name + ' collapsible-header waves-effect waves-cyan' : ' collapsible-header waves-effect waves-cyan'}     >
	                    		<i className={menus.menu_icons}></i> {menus.menu_name}
	                    	</Link>
	                        <div   className="collapsible-body" style = { (active_block ) ? display_block : display_none} >
	                            <ul>
							 		{menus.children.map((menu,i)=>{
							 			childMenus.push(<RenderChildMenu
							 				key = {menu.menu_id}
							 				keyItem = {menu.menu_id}
							 				menu_url = {menu.menu_url}
							 				menu_name = {menu.menu_name}
							 				 
							 			/>) 
							 		})}
							 		{childMenus} 
						 		</ul>
						 	</div>
						</li>		 
		 			)
			 	) :  
			 	(
			 		menusItem.push(
		 				<li className="bold" key={menus.menu_id}>
							<Link to= {menus.menu_url} className="waves-effect waves-cyan">
								<i className= {menus.menu_icons}></i>
								 	{menus.menu_name}
							</Link>
						</li>
		 			)	
				)
			}) 

		return menusItem;
	}

	render(){ 
		const menusItem = this.handleMenus();

		return(
			<div> {menusItem}</div> 
		)	
	}


}