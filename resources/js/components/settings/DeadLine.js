/**
 * Created by Sandeep Maurya on 20th April 2019.
 */

import React from 'react';
import axios from 'axios';
import { toast } from 'react-toastify';
import {Link} from 'react-router-dom';
import {authHeader,logout,ITEM_PER_PAGE} from  "./../../helpers/responseAuthozisation";
import  Loader from './../admin-settings/usable_components/Loader';
import Sandesh from './../admin-settings/usable_components/Sandesh'; 
import DatePicker from "react-datepicker"; 
import "react-datepicker/dist/react-datepicker.css";
import TimeField from 'react-simple-timefield';
 
import {getRequest,postRequest} from  "./../../helpers/ApiRequest";
export default class DeadLine extends React.Component{
    constructor(props){
        super(props);
        var d_date = '00:00'  ;
        this.state = {
            id : '',
            dl_doc : '0', 
            dl_crm : '', 
            dl_online : '', 
            dl_oncall : '',
            dl_cic : 0, 
            dl_mail : 0, 
            dl_approval : 0, 
            dl_visit : 0,  
            updated_at : '', 
            is_deleted : '',
            create_at : '',
            created_by : '',
            message : '',
            flag : true ,
            hasError:false,
            action: 'Update' ,
            is_loading:true 
        }
        this.handleClient = this.handleClient.bind(this);
        this.handleFieldChange   = this.handleFieldChange.bind(this);
        this.handleChange = this.handleChange.bind(this);
    }


    componentDidMount(){
        this.loadDetails();
    }

    static getDerivedStateFromError(error) {
        // Update state so the next render will show the fallback UI.
        return { hasError: true };
    }

    loadDetails(){
        let self = this;
        let url = '/api/deadline-info';
        let request = getRequest(url);
        request.then((response)=>{
            const data = response.data; 
            
            if(data.result){ 
                let info = data.data;
                self.setState({
                    dl_doc : info.dl_doc, 
                    dl_crm : info.dl_crm, 
                    dl_online : info.dl_online, 
                    dl_oncall : info.dl_oncall,
                    dl_cic : info.dl_cic, 
                    dl_mail : info.dl_mail, 
                    dl_approval : info.dl_approval, 
                    dl_visit : info.dl_visit,  
                   
                    updated_at : '', 
                    is_deleted : '',
                    create_at : '',
                    created_by : '',
                    message : '',
                    flag : true ,
                    hasError:false,
                    action: 'Update' ,
                    is_loading:false 
                });
            }
            
        });
    }

    /**
     * form validation
     * @returns {boolean|*}
     */
    validateRule(){
        
        if(this.state.name =='' || this.state.name == 'undefined'){
            Sandesh('Please enter Client Name ','warn');
            return  false;
        }
        if(this.state.mobile =='' || this.state.mobile == 'undefined'){
            Sandesh('Please enter Mobile Number','warn');
            return  false;
        }
        if(this.state.location =='' || this.state.location == 'undefined'){
            Sandesh('Please enter Location','warn');
            return  false;
        }
        
        return this.state.flag;
    }



    handleClient(event){
        event.preventDefault();
        let {history} = this.props;
        let info = this.state;
        
        this.setState({
            is_validating : true
        });
        if(this.validateRule()){
            let url ='/api/deadline';
            let request = postRequest(url,info);
            request.then(response=>{
                let data = response.data;
                Sandesh(data.message,data.type);
                if(data.result){
                    Sandesh(data.message,data.type);
                    setTimeout(()=>{history.push('/welcome')},3000);
                }
            })
        }
        this.setState({
            is_validating : false
        });

    }
    handleFieldChange(event){
        
        this.setState({
            [event.target.name] : event.target.value
        });
    }
    
    handleRadioChange(event){
        this.setState({
            [event.target.name] :  event.target.value
        }); 
    }
     
    handleChange(name,date) {  
         
        this.setState({
          [name]: date
        });
    }
 
    render(){
        if(this.state.is_loading){
            return <Loader />
        }
        if(this.state.hasError){
           return <h4 className="header2">Something went wrong</h4>
        }   else{
            const is_break_shift = (this.state.break == 1) ? true : false;
            return(
                <div className="dashboard">
                    <div className="card-panel">
                        <div className="row">
                            <form className="col s12" onSubmit={this.handleClient}>
                                <div id="leave_htmlForm">
                                    <div className="row">                                                
                                        
                                        <div className="col s12 l12 center-align">
                                            <h4 className="header2">{this.state.action} DeadLine </h4>
                                            
                                        </div>
                                    </div>
                                    <div class="card light-blue lighten-5">
                                        <div class="card-content light-blue-text">
                                            <p>All Deadlines are in Hour(s)</p>
                                        </div>
                                    </div>
                                    <div className="row"> 
                                            
                                        <div className="input-field col s12 m3">
                                            <input
                                                id="dl_doc"
                                                type="text"
                                                name="dl_doc"
                                                value={this.state.dl_doc}
                                                onChange={this.handleFieldChange}
                                            /> 
                                            <label htmlFor="dl_doc" className="active">DOC Deadline
                                            </label> 
                                                
                                        </div>
                                        <div className="input-field col s12 m3">
                                            <input
                                                id="dl_crm"
                                                type="text"
                                                name="dl_crm"
                                                value={this.state.dl_crm}
                                                onChange={this.handleFieldChange}
                                            /> 
                                            <label htmlFor="dl_crm" className="active">CRM Deadline
                                            </label>
                                        </div>  
                                        <div className="input-field col s12 m3">
                                            <input
                                                id="dl_online"
                                                type="text"
                                                name="dl_online"
                                                value={this.state.dl_online}
                                                onChange={this.handleFieldChange}
                                            /> 
                                            <label htmlFor="dl_online" className="active">Online Counselling Deadline
                                            </label>
                                        </div>
                                        <div className="input-field col s12 m3">
                                            <input
                                                id="dl_oncall"
                                                type="text"
                                                name="dl_oncall"
                                                value={this.state.dl_oncall}
                                                onChange={this.handleFieldChange}
                                            /> 
                                            <label htmlFor="dl_oncall" className="active">Oncall Counselling Deadline
                                            </label>
                                        </div>
                                    </div>
                                    <div className="row"> 
                                            
                                        <div className="input-field col s12 m3">
                                            <input
                                                id="dl_cic"
                                                type="text"
                                                name="dl_cic"
                                                value={this.state.dl_cic}
                                                onChange={this.handleFieldChange}
                                            /> 
                                            <label htmlFor="dl_cic" className="active">CIC Deadline
                                            </label> 
                                                
                                        </div>
                                        <div className="input-field col s12 m3">
                                            <input
                                                id="dl_mail"
                                                type="text"
                                                name="dl_mail"
                                                value={this.state.dl_mail}
                                                onChange={this.handleFieldChange}
                                            /> 
                                            <label htmlFor="dl_mail" className="active">Mail Verification Deadline
                                            </label>
                                        </div>  
                                        <div className="input-field col s12 m3">
                                            <input
                                                id="dl_approval"
                                                type="text"
                                                name="dl_approval"
                                                value={this.state.dl_approval}
                                                onChange={this.handleFieldChange}
                                            /> 
                                            <label htmlFor="dl_approval" className="active">Approval Deadline
                                            </label>
                                        </div>
                                        <div className="input-field col s12 m3">
                                            <input
                                                id="dl_visit"
                                                type="text"
                                                name="dl_visit"
                                                value={this.state.dl_visit}
                                                onChange={this.handleFieldChange}
                                            /> 
                                            <label htmlFor="dl_visit" className="active">Visit Deadline
                                            </label>
                                        </div>
                                    </div> 
                                         
                                </div>
                                <div className="row">                                            
                                    <div className="input-field col s12">
                                        <Link to="/welcome">
                                            <button className="btn default   waves-light"   shift_name="action">
                                                <i className="mdi-navigation-arrow-back left "></i>
                                                cancel
                                            </button>
                                        </Link>
                                        <button className="btn cyan waves-effect waves-light  " type="submit" shift_name="action">{(this.state.action)}
                                            <i className="mdi-content-send right"></i>
                                        </button>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            );
        }
    }
}