     <ul id="profile-page-about-feed" className="collection z-depth-1">
                                          <li className="collection-item avatar">
                                            <img src="images/avatar.jpg" alt="" className="circle" />
                                            <span className="title">Project Title</span>
                                            <p>Task assigned to new changes.
                                              <br /> <span className="ultra-small">Second Line</span>
                                            </p>
                                            <a href="#!" className="secondary-content"><i className="mdi-action-grade"></i></a>
                                          </li>
                                          <li className="collection-item avatar">
                                            <i className="mdi-file-folder circle"></i>
                                            <span className="title">New Project</span>
                                            <p>First Line of Project Work 
                                              <br /> <span className="ultra-small">Second Line</span>
                                            </p>
                                            <a href="#!" className="secondary-content"><i className="mdi-social-domain"></i></a>
                                          </li>
                                          <li className="collection-item avatar">
                                            <i className="mdi-action-assessment circle green"></i>
                                            <span className="title">New Payment</span>
                                            <p>Last UK Project Payment
                                              <br /> <span className="ultra-small">$ 3,684.00</span>
                                            </p>
                                            <a href="#!" className="secondary-content"><i className="mdi-editor-attach-money"></i></a>
                                          </li>
                                          <li className="collection-item avatar">
                                            <i className="mdi-av-play-arrow circle red"></i>
                                            <span className="title">Latest News</span>
                                            <p>company management news
                                              <br /> <span className="ultra-small">Second Line</span>
                                            </p>
                                            <a href="#!" className="secondary-content"><i className="mdi-action-track-changes"></i></a>
                                          </li>
                                        </ul> 