<?php

return [

    /*
    |--------------------------------------------------------------------------
    |  Attendance Language Lines
    |--------------------------------------------------------------------------
    | 
    */
    // success 
    'success_mark_create' => ' Dear :name (:code) , your attendance mark request for :date have been submitted successfully',
    'success_od_create' => ' Dear :name (:code) , your OD request for :date have been submitted successfully',
    'success_mark_update' => ' Dear :name (:code) , your attendance mark request for :date have been updated successfully',
    'success_od_update' => ' Dear :name (:code) , your OD request for :date have been updated successfully',
    'success_mark_approval' => 'Mark Attendance request of :name (:code) for :date have been approved successfully ',
    'success_request_rejected' => 'Mark request have been rejected successfully ',
    'success_od_cancel' => 'Dear :name (:code) , your OD request for :date have been cancelled successfully',
    'success_od_cancellation' => 'Dear :name (:code) , your OD request for :date have been submitted for cancellation successfully',
    // error 
    'failed' => 'These credentials do not match our records.', 
    'no_rule' => 'Attendance rule not found .',
    'error_mark_create' => ' Dear :name (:code), your attendance mark request for :date can not be submitted , please try after sometime  ',
    'error_od_create' => ' Dear :name (:code), your OD request for :date can not be submitted , please try after sometime  ',
    'error_no_workflow' => 'We can not find applicable workflow for you ',
    'error_approver_missing' => 'Some of your approver(s) are not updated in system, please ask HR to update same ' ,
    'error_commencement_days' => ' Request prior to :date can not be accepted ',
    'error_invalid_user' => ' No record(s) found for user id :id  ',
    'error_mark_update' => ' Dear :name (:code), your attendance mark request for :date can not be updated , please try after sometime  ',
    'error_duplicate' => ' Dear :name (:code), your attendance mark request for :date have already been applied ',
    'error_no_ar_details' =>'No mark attendance details found',
    'error_mark_transaction_invalid' => 'Sorry, we can not find out the valid transactions',
    'error_update_trx_error' => 'We can not process your request, please try later',
    'error_request_date' => 'Mark attendance request is not allowed for selected date, please select valid date for request',
    'error_od_date' => 'OD request is not allowed for selected date, please select valid date for request',
    'error_od_duplicate' => ' Dear :name (:code), your OD request for :date have already been applied ',
    'error_no_od_details' =>'No OD details found',
    'error_od_transaction_invalid' => 'Sorry, we can not find out the valid transactions',
    'success_od_approval' => 'OD request of :name (:code) for :date have been approved successfully ',
    'success_od_request_rejected' => 'OD request have been rejected successfully ',
    'error_od_cancel_not_allowed' => 'Cancellation of this request is not allowed',
    'error_od_cancel_period_exceeds' => 'OD request are allowed to cancel within 30 days',
    'error_od_approval_in_process' => 'We could not cancel this request as this is in approval process on further level',
    'error_ar_approval_in_process' => 'We could not cancel this request as this is in approval process on further level',
    'error_ar_cancel_not_allowed' => 'Cancellation of this request is not allowed',
    'error_ar_cancel_period_exceeds' => 'Mark request are allowed to cancel within 30 days',

];
