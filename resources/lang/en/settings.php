<?php 
return [

    /*
    |--------------------------------------------------------------------------
    | Settings Language Lines
    |--------------------------------------------------------------------------
    | 
    */

    'failed' => 'These credentials do not match our records.',
    'workflow_duplicate' =>" Workflow with same name or code have been already created, please try with different one ", 
    'workflow_success'=>'Workflow created successfully',
    'workflow_error'=>'Workflow could not be created, please try after sometime',
    'workflow_update' =>"Workflow updated successfully",
    'workflow_update_error' =>"Workflow could not be updated, please try after sometime ",
    'approver_not_selected' => "Please select approver",
    'incomplete' => 'Please select/fill all options ',
    'error_creation' => 'Roster can not be created, please try after sometime',
    'success_creation' => 'Roster created successfully',
    'roster_duplicate' => 'Roster with same name, already have been created !',
    'error_date_selections' => 'Start date can not be greater than end date',
    'success_roster_map' => 'Roster mapping have been queued and soon it will be mapped ',
    'error_no_records' => 'No record(s) found'
];
