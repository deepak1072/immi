<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Notifications Language Lines
    |--------------------------------------------------------------------------
    |
    |  error message notifications while interacting the system 
    */

    'user_code_error' => 'Please provide employee code',
    'next' => 'Next &raquo;',

];
