<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Leave Language Lines
    |--------------------------------------------------------------------------
    | 
    */

    'failed'            =>  'These credentials do not match our records.',
    'credit_success'    =>  'Balance credited successfully' ,
    'no_policy'         =>  'Leave policy is not found ',
    'no_leave_year'     =>  'Leave year is not found ',
    'no_user'           =>  'No user found ',
    'credit_not_allowed'=>  'Previous year credit is not allowed ',
    'leave_id_error'    => '  leave type is not valid ',
    'leave_not_found'   => 'Leave rule not found ',
    'mandate_field_error' =>'Please provide mandatory field',
    'success_request' => 'Leave request applied successfully',
    // errors 
    'error_from_date_gte' => ' To Date can not be less than From Date',
    'error_from_type' => 'From Type is invalid ',
    'error_to_type' => 'To Type is invalid ',
    'error_leave_type' => 'Leave Type selected is invalid ',
    'error_planned_unplanned' => 'Planned unplanned is required',
    'error_unplanned_reason' => 'Unplanned reason is required ',
    'error_reason' =>'Reason is required ',
    'error_attachments' => 'Attachment is required ',
    'error_attachment_invalid' => 'Invalid attachments',
    'error_invalid_extensions' =>'File format is not allowed to upload',
    'error_declaration' => 'Declaration is required ',
    'error_commencement_days' => ' Request prior to :date can not be accepted ',
    'error_no_workflow' => 'We can not find applicable workflow for you ',
    'error_leave_days' => 'Leave days can not be 0 or less ' ,
    'error_leave_balance' => 'Applied leave does not have sufficient balance ',
    'error_leave_transaction_invalid' => 'Leave request you are trying to approve is invalid',
    'error_prev_yr_leave_not_allowed' => 'Previous year leave are not allowed to approve ' ,
    'error_update_trx_error' => ' Leave can not be processed, please try after sometime ',
    'error_no_leave_details' => ' Leave details can not be found, please try after sometime ',
    'error_approver_missing' => 'Some of your leave approver(s) are not updated in system, please ask HR to update same ' ,
    'error_leave_create' => ' Dear :name (:code), your leave request from :from to :to for :day day(s) can not be submitted , please try after sometime  ',
    'error_invalid_user' => ' No record(s) found for user id :id  ',
    'error_leave_update' => ' Dear :name (:code), your leave request from :from to :to for :day day(s) can not be updated , please try after sometime  ',
    'error_leave_approval_in_process' => 'We can not cancel your leave request  as it is in approval process on further level',
    'error_leave_cancel_period_exceeds' => 'Leave request can be cancelled within 30 days ',
    'error_leave_cancel_not_allowed' => 'Leave request are not allowed to cancel',
    'no_compoff_rule' => 'No compoff rule found',
    'error_request_date' => 'Comp-Off request is not allowed for selected date, please select valid date for request',
    'error_coff_duplicate' => 'A Comp-Off request  have already been applied for selected date',
    'error_coff_approver_missing' => 'Some of your Comp-Off approver(s) are not updated in system, please ask HR to update same',
    'error_coff_create' => 'Dear :name (:code) , your Comp-Off request for :date can not be submitted, please try after sometime',
    // success 
    'success_leave_create' => ' Dear :name (:code) , your leave request from :from to :to for :day day(s) have been submitted successfully', 
    'success_coff_create' => 'Dear :name (:code) , your Comp-Off request for :date have been submitted successfully',
    'success_leave_update' => ' Dear :name (:code) , your leave request from :from to :to for :day day(s) have been updated successfully',
    'success_leave_approval' => ':leave request of :name (:code) from :from to :to for :day day(s) have been approved successfully ',
    'success_request_rejected' => 'Leave request have been rejected successfully ',
    'success_leave_cancel' => ' Dear :name (:code) , your leave request from :from to :to have been cancelled successfully',
    
    
];
