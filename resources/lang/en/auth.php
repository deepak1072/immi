<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Authentication Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used during authentication for various
    | messages that we need to display to the user. You are free to modify
    | these language lines according to your application's requirements.
    |
    */

    'failed' => 'These credentials do not match our records.',
    'throttle' => 'Too many login attempts. Please try again in :seconds seconds.',
    'mandate_fields'=>'Please provide mandatory fields',
    'invalid_credentials' => 'Password you have entered, is incorrect ' ,
    'account_locked' => 'Account have been locked ',
    'access_not_allowed' => 'You are not allowed to access this portal',
    'token_not_created' =>' Token could not be created ',
    'welcome'=>'Please wait a moment, redirecting you to portal',
    'logout_success'=>'Successfully logged out, see you soon ',
    'logout_failed'=>'System could not logged you out, please wait for a moment',
    'incorrect_user_id' => 'User id you are trying to access with, is not registered ',
    'read_password_policy' => 'Please read password policy for a valid password ',
    'no_password_update' => "Password can't be updated, please try after sometime ",
    'password_success' => 'Password updated successfully',
    'same_password' => 'New password can not be same as old password'
];
